<?php
/*
# @package BSC CMS
# Copy right code 
# MySQL settings - You can get this info from your web host
# database connection execute database query.

# Developer by jugal kishore kiroriwal
*/

class dbc
{  
	function __construct()
	{
		global $ARR_DBS;
		if (!isset($GLOBALS['dbcon']))
		{
			$GLOBALS['dbcon'] =	mysql_connect($ARR_DBS["dbs"]['host'], $ARR_DBS["dbs"]['user'], $ARR_DBS["dbs"]['password']);
			mysql_select_db($ARR_DBS["dbs"]['name']) or die("Could not connect to database. Please check configuration and ensure MySQL is running.");
		}
	}
	
	public function parse_input($val)
	{
	   
	   $val=mysql_real_escape_string($val);
	   return $val;	
	
	}
	
	public function parse_output($val)
	{
	   
	   $val=stripslashes($val);
	   return $val;	
	
	}
	
	public function checkpoint($from_start = false) {
		global $PREV_CHECKPOINT;
		if($PREV_CHECKPOINT=='') {
			$PREV_CHECKPOINT = SCRIPT_START_TIME;
		}
		$cur_microtime = $this->getmicrotime();
	
		if($from_start) {
			return $cur_microtime - SCRIPT_START_TIME;
		} else {
			$time_taken = $cur_microtime - $PREV_CHECKPOINT;
			$PREV_CHECKPOINT = $cur_microtime;
			return $time_taken;
		}
	}
	
	public function db_query_($sql, $dbcon2 = null)
	{
		 $dbcon2	= $GLOBALS['dbcon'];
		 $time_before_sql = $this->checkpoint();
		 $result	= mysql_query($sql,	$dbcon2) or	die($this->db_error($sql));
		 return $result;
	}
	
	public function db_error($sql) {
		echo "<div style='font-family: tahoma; font-size: 11px; color: #333333'><br>".mysql_error()."<br>";
		$this->print_error();
		if(LOCAL_MODE) {
			echo "<br>sql: $sql";
		}
		echo "</div>";
	}
	
	public function getmicrotime()
	{
		list($usec,	$sec) =	explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	
	public function getSingleresult($sql, $dbcon2 = null) {
		if($dbcon2=='') {
			if(!isset($GLOBALS['dbcon'])) {
				$this->connect_db();
			}
			$dbcon2	= $GLOBALS['dbcon'];
		}
		$result	=$this->db_query($sql, $dbcon2);
		if ($line =	$this->db_fetch_array($result)) {
			$response =	$line[0];
		}
		return $response;
	}
	
	
	public function db_query($sql, $dbcon2 = null)
	{
		$sql = str_replace("#_", tb_Prefix, $sql);
		if($dbcon2=='') {
			if(!isset($GLOBALS['dbcon'])) {
				$this->connect_db();
			}
			$dbcon2	= $GLOBALS['dbcon'];
		}
		$time_before_sql = $this->checkpoint();
		$result	= mysql_query($sql,	$dbcon2) or	die($this->db_error($sql));
		return $result;
	}
	
	public function sqlquery($rs='exe',$tablename,$arr,$update='',$id='',$update2='',$id2='')
	{
	
		$sql = $this->db_query("DESC ".tb_Prefix."$tablename");
		$row = mysql_fetch_array($sql);
		if($update == '')
			$makesql = "insert into ";
		else
			$makesql = "update " ;
		$makesql .= tb_Prefix."$tablename set ";
	
		$i = 1;
		while($row = mysql_fetch_array($sql)) {
			if(array_key_exists($row['Field'], $arr)) {
	
	
				if($i != 1)
					$makesql .= ", ";
	
				//$makesql .= $row['Field']."='".$this->ms_addslashes((is_array($arr[$row['Field']]))?implode(":",$arr[$row['Field']]):$arr[$row['Field']])."'";
				
				$makesql .= $row['Field']."='".$this->parse_input((is_array($arr[$row['Field']]))?implode(":",$arr[$row['Field']]):$arr[$row['Field']])."'";
				
				
				
				$i++;
			}
	
		}
		if($update)
			$makesql .= " where ".$update."='".$id."'".(($update2 && $id2)?" and ".$update2."='".$id2."'":"");
		if($rs == 'show') {
			echo $makesql;
			exit;
		}
		else {
			$this->db_query($makesql);
		}
		return ($update)?$id:mysql_insert_id();
	}
	
	public function db_scalar($sql, $dbcon2 = null) {
		if($dbcon2=='') {
			if(!isset($GLOBALS['dbcon'])) {
				$this->connect_db();
			}
			$dbcon2	= $GLOBALS['dbcon'];
		}
		$result	= $this->db_query($sql, $dbcon2);
		if ($line =	$this->db_fetch_array($result)) {
			$response =	$line[0];
		}
		return $response;
	}
	
	
	public function db_fetch_array($rs) {
		$array	= mysql_fetch_array($rs);
		return $array;
	}
	
	public function sessset($val, $msg="")
	{
		$_SESSION['sessmsg'] = $val;
		$_SESSION['alert'] = $msg;
	}
	
	public function print_error() {
		$debug_backtrace = debug_backtrace();
		for ($i = 1; $i < count($debug_backtrace); $i++) {
			$error = $debug_backtrace[$i];
			echo "<br><div><span>File:</span> ".str_replace(SITE_FS_PATH, '',$error['file'])."<br><span>Line:</span> ".$error['line']."<br><span>Function:</span> ".$error['function']."<br></div>";
		}
	}
	
	function product_url($proid)
	{
		   $product_query = $this->db_query("select * from #_products where status ='1' and pp_id='".$proid."' ");
		   $product_rows = $this->db_fetch_array($product_query);
		   
		   $brand_url=$this->getSingleResult("select url from #_brands where status ='1' and brand_id='".$product_rows['brand_id']."' ");
           $series_url=$this->getSingleResult("select url from #_series where status ='1' and sid='".$product_rows['series_id']."' ");
		   $model_url=$this->getSingleResult("select url from #_models where status ='1' and model_id='".$product_rows['model_id']."' ");
		   
		   $die_type_url=$this->getSingleResult("select DT.url FROM  #_die_types as DT INNER JOIN   #_dies as D ON DT.dt_id =D.die_type where D.die_id='".$product_rows['die_id']."'");
		   
		   $designer_url=$this->getSingleResult("select url from #_print_images where status ='1' and img_id='".$product_rows['design_id']."' ");
	       $product_url =  SITE_PATH.'product/'.$brand_url.(($series_url)?'/'.$series_url:'').'/'.$model_url.'/'.$die_type_url.'/'.$designer_url.'/'.$product_rows['product_code'].".html";
		  
		   return $product_url;
	}
	
	public function pageinfo($page){
		$pageInfo = array();
		$pageInfo[title] = $this->get_static_content('meta_title',$page);
		$pageInfo[keyword] = $this->get_static_content('meta_keyword',$page);
		$pageInfo[description] = $this->get_static_content('meta_description',$page);
		$pageInfo[heading] = $this->get_static_content('heading',$page);
		$pageInfo[body] = $this->get_static_content('body',$page);
		$pageInfo[pagename] = $this->get_static_content('pagename',$page);
		$pageInfo[url] = $this->get_static_content('url',$page);
	
		//$pageInfo[sort_body] = $this->get_static_content('sort_body',$page);
		//$pageInfo[pimage] = $this->get_static_content('pimage',$page);
		return $pageInfo;
	
	}
	public function get_static_content($key,$pname)
	{
		return $rs = $this->db_scalar("select ".$key." from #_pages where url='$pname'");
	}
	
	public function get_product_quantity($die_id)
	{
		
		  $dies_query = $this->db_query("select * from #_dies where   die_id ='".$die_id."'  ");
		  $dies_rows = $this->db_fetch_array($dies_query);
		  if($dies_rows['die_type'] ==1)
		  {								 
			    $quantity=$this->getSingleResult("select quantity from #_die_details where   die_id ='".$die_id."'  ");
		  }else {
			   $quantity=$dies_rows['quantity'];  	
		 }
		 return $quantity;
		
	}
	
	public function baseurl($string)
	{
		
		$string=strtolower($string);
		$string=preg_replace('/\s+/',' ',$string);
		$string=trim($string);
		$string = str_replace(' ', '-', $string);
		$string =preg_replace('/[^A-Za-z0-9\-]/', '', $string);
		$string = preg_replace('/-+/', '-', $string);
		
		/*$vals = str_replace(" ", "",trim(strtolower($vals)));
		$vals = str_replace("/", "",$vals);
		$vals = str_replace("(", "",$vals);
		$vals = str_replace(")", "",$vals);
		$vals = str_replace("&", "",$vals);
		$vals = str_replace("#", "",$vals);
		$vals = str_replace("---", "",$vals);
		$vals = str_replace("--", "",$vals);
		$vals = str_replace("-", "",$vals);
		$vals = str_replace("&shy;", "",$vals);
		$vals = str_replace("&minus;", "",$vals);
		$vals = str_replace("'", "",$vals);
		$vals = str_replace('"', "",$vals);
		$vals = str_replace(" –", "",$vals);
		$vals = str_replace("+", "",$vals);
		$vals = str_replace(",", "",$vals);
		*/
		return $string;
	}
	
	 public function  add_country($data)
	 {
	   
	     $query=$this->db_query("select * from #_countries where country_name ='".$this->parse_input($data['country_name'])."' "); 
	     if(mysql_num_rows($query)==0)
	     {
	   
			   $data['created_on']=date('Y-m-d H:i:s');
			   $data['create_by']=$_SESSION["AMD"][0];
			   $data['status']=1;
			   $data['shortorder']=$this->getSingleresult("select max(shortorder) as shortorder from #_countries ")+1;
			   $con_id= $this->sqlquery("rs",'countries',$data);
		  
		 }else {
			   
			  $con_id =  $this->getSingleresult("select pid from #_countries  where country_name ='".$this->parse_input($data['country_name'])."'"); 
		 }
		 return $con_id;
	 }
	 
	  public function  add_state($data)
	 {
	   
	     $query=$this->db_query("select * from #_states where state_name ='".$this->parse_input($data['state_name'])."' and con_id='".$con_id."' "); 
	     if(mysql_num_rows($query)==0)
	     {
	   
			   $data['created_on']=date('Y-m-d H:i:s');
			   $data['create_by']=$_SESSION["AMD"][0];
			   $data['status']=1;
			   $data['shortorder']=$this->getSingleresult("select max(shortorder) as shortorder from #_states ")+1;
			   $state_id= $this->sqlquery("rs",'states',$data);
		  
		 }else {
			   
			  $state_id =  $this->getSingleresult("select pid from #_states  where state_name ='".$this->parse_input($data['state_name'])."' and con_id='".$con_id."' "); 
		 }
		 return $state_id;
	 }
	 
	 
	  public function  add_cities($data)
	 {
	   
	     $query=$this->db_query("select * from #_cities where city_name ='".$this->parse_input($data['state_name'])."' and con_id='".$con_id."'  and state_id='".$state_id."' "); 
	     if(mysql_num_rows($query)==0)
	     {
	   
			   $data['created_on']=date('Y-m-d H:i:s');
			   $data['create_by']=$_SESSION["AMD"][0];
			   $data['status']=1;
			   $data['shortorder']=$this->getSingleresult("select max(shortorder) as shortorder from #_cities ")+1;
			   $state_id= $this->sqlquery("rs",'cities',$data);
		  
		 }else {
			   
			  $state_id =  $this->getSingleresult("select pid from #_cities  where city_name ='".$this->parse_input($data['city_name'])."' and con_id='".$con_id."'  and state_id='".$state_id."' "); 
		 }
		 return $state_id;
	 }
	 
	 
	 
	 public function get_laon_institute($employment_type,$property_type,$gender,$borrow_cost,$borrow_time,$core_prod_id=3)
	 {
				 
				  $loan_institute_arr =array();
				  $i=0;
				  $wh ='';
				  
				  if($core_prod_id!='')
				  {
					  $wh .=" and core_prod_id='".$core_prod_id."' ";
				  }
				  
				  if($employment_type!='')
				  {
					  $wh .=" and (finacial_profile='".$employment_type."'  or finacial_profile='ALL') ";
				  }
				 
				  if($property_type!='')
				  {
					  $wh .=" and (property_type='".$property_type."' or property_type='ALL') ";
				  }
				  
				  if($gender)
				  {
				     $wh .=" and (gender ='".$gender."' or gender='Both')";
				  }
				  
				  if($borrow_cost!='')
				  {
					    $wh .=" and min_loan_amt <='".$borrow_cost."'  and (max_loan_amt >='".$borrow_cost."' or max_loan_amt =0)  ";
				  }
				  
				  
				  if($borrow_time!='')
				  {
					 // $wh .=" and (min_tenure <='".$borrow_time."' or min_tenure=0) and (max_tenure >='".$borrow_time."' or max_tenure=0)  ";
				  }
				  
				  
				  //echo  "select * from #_home_loan_products where status ='1' ".$wh."<br>";;
				  
				  $home_loan_prd_query =$this->db_query("select * from #_home_loan_products where status ='1' ".$wh);
				  
				 // echo mysql_num_rows($home_loan_prd_query);
				  while ($home_loan_prd_row = $this->db_fetch_array($home_loan_prd_query))
				  {
					 $institute_name = $this->getSingleresult("select institute_name from #_financial_institutes where pid='".$home_loan_prd_row['bank_id']."' ");       
					 if($institute_name!='')
					 {
						 $loan_institute_arr[$i]['institute_name']=$institute_name;
						 $loan_institute_arr[$i]['bank_id']= $home_loan_prd_row['bank_id'];
						 $loan_institute_arr[$i]['roi']= $home_loan_prd_row['roi'];
						 $loan_institute_arr[$i]['product_type_id']= $home_loan_prd_row['product_type_id'];
						 $loan_institute_arr[$i]['home_loan_products_id']= $home_loan_prd_row['pid'];
						 $loan_institute_arr[$i]['home_loan_products_gender']= $home_loan_prd_row['gender'];
						 $i++;
					 }
					  
				  }
				  return $loan_institute_arr;
				 
				 
	 }
	 
	 
	 
	 
	 public function senp_netincome_calc($bank_id,$core_prod_id,$employment_type,$emp_profit_after_tax,$emp_depriciation,$emp_interestpaid,$emp_rentalincome,$emp_renumeration,$emp_interest_capital	)
	{
	
	     $netincome_arr =array();
		 $i=0;
		 
		// echo "select * from #_net_income_calc where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and employment_type='".$employment_type."' ";
		 
		 $net_income_query = $this->db_query("select * from #_net_income_calc where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and employment_type='".$employment_type."' ");
		 while ($net_income_row = $this->db_fetch_array($net_income_query))
	     {
			  
			 if($net_income_row['income_particulars']==9)
			 {
				$monthly_profit_after_tax =($emp_profit_after_tax*$net_income_row['parcentage'])/100;
				$netincome_arr['profit_after_tax']['value']=$emp_profit_after_tax;
				$netincome_arr['profit_after_tax']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['profit_after_tax']['monthly_profit_after_tax']=$monthly_profit_after_tax;
				
			 }
			 
			 
			 if($emp_depriciation>0 && $net_income_row['income_particulars']==12)
			 {
				$monthly_depriciation =($emp_depriciation*$net_income_row['parcentage'])/100;
				$netincome_arr['depriciation']['value']=$emp_depriciation;
				$netincome_arr['depriciation']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['depriciation']['monthly_depriciation']=$monthly_depriciation;
				
			 }
			 
			 if($emp_interestpaid>0 && $net_income_row['income_particulars']==10)
			 {
				$monthly_interestpaid =($emp_interestpaid*$net_income_row['parcentage'])/100;
				$netincome_arr['interestpaid']['value']=$emp_interestpaid;
				$netincome_arr['interestpaid']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['interestpaid']['monthly_interestpaid']=$monthly_interestpaid;
				
			 }
			 
			 if($emp_rentalincome>0 && $net_income_row['income_particulars']==11)
			 {
				$monthly_rentalincome =($emp_rentalincome*$net_income_row['parcentage'])/100;
				$netincome_arr['rentalincome']['value']=$emp_rentalincome;
				$netincome_arr['rentalincome']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['rentalincome']['monthly_rentalincome']=$monthly_rentalincome;
				
			 }
			 
			 if($emp_odcc_limit>0 && $net_income_row['income_particulars']==13)
			 {
				$monthly_odcc_limit =($emp_odcc_limit*$net_income_row['parcentage'])/100;
				$netincome_arr['odcc_limit']['value']=$emp_odcc_limit;
				$netincome_arr['odcc_limit']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['odcc_limit']['monthly_odcc_limit']=$monthly_odcc_limit;
				
			 }
			 
			 if($emp_renumeration>0 && $net_income_row['income_particulars']==17)
			 {
				$monthly_renumeration =($emp_renumeration*$net_income_row['parcentage'])/100;
				$netincome_arr['renumeration']['value']=$emp_renumeration;
				$netincome_arr['renumeration']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['renumeration']['monthly_renumeration']=$monthly_renumeration;
				
			 }
			 
			 if($emp_interest_capital>0 && $net_income_row['income_particulars']==18)
			 {
				$monthly_interest_capital =($emp_interest_capital*$net_income_row['parcentage'])/100;
				$netincome_arr['interest_capital']['value']=$emp_interest_capital;
				$netincome_arr['interest_capital']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['interest_capital']['monthly_interest_capital']=$monthly_interest_capital;
				
			 }
			 
			
		 }
		 
		 return $netincome_arr;	
		
	}
	 
	 
	 public function senp_netincome_individual($bank_id,$core_prod_id,$employment_type,$fixedsalary,$bonus,$rentalincome,$reimbersment)
	{
	  
	   
	    $netincome_arr =array();
		$i=0;	
		
		
		$net_income_query = $this->db_query("select * from #_net_income_calc where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and employment_type='".$employment_type."' ");
		
		//echo mysql_num_rows($net_income_query);
		
		while ($net_income_row = $this->db_fetch_array($net_income_query))
	    {
			 if($fixedsalary>0 && $net_income_row['income_particulars']==14)
			 {
				$monthly_gincome =($fixedsalary*$net_income_row['parcentage'])/100;
				$netincome_arr['fixedsalary']['value']=$fixedsalary;
				$netincome_arr['fixedsalary']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['fixedsalary']['monthly_gincome']=$monthly_gincome;
				
			 }
			 
			 if($bonus>0 && $net_income_row['income_particulars']==15)
			 {
				$annual_bonus=(($bonus/12)*$net_income_row['parcentage'])/100;
				$netincome_arr['bonus']['value']=$bonus;
				$netincome_arr['bonus']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['bonus']['annual_bonus']=$annual_bonus;
				
			 }
			 
			 if($rentalincome>0 && $net_income_row['income_particulars']==11)
			 {
				$rental_income=($rentalincome*$net_income_row['parcentage'])/100;
				$netincome_arr['rentalincome']['value']=$rentalincome;
				$netincome_arr['rentalincome']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['rentalincome']['rental_income']=$rental_income;
				
			 }
			 
			 if($reimbersment>0 && $net_income_row['income_particulars']==16)
			 {
				$reimbursement_cal=(($reimbersment)*$net_income_row['parcentage'])/100;
				$netincome_arr['reimbersment']['value']=$reimbersment;
				$netincome_arr['reimbersment']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['reimbersment']['reimbersment']=$reimbursement_cal;
				
			 }
			 
		}
		
		//print_r($netincome_arr);
		
		return $netincome_arr;
		
	}
	 
	 
	public function sep_netincome_calc($bank_id,$core_prod_id,$employment_type,$emp_profit_after_tax,$emp_depriciation,$emp_interestpaid,$emp_rentalincome)
	{
	     $netincome_arr =array();
		 $i=0;	
		
		
		 $net_income_query = $this->db_query("select * from #_net_income_calc where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and employment_type='".$employment_type."' ");
		 while ($net_income_row = $this->db_fetch_array($net_income_query))
	     {
			  
			 if($net_income_row['income_particulars']==5)
			 {
				$monthly_profit_after_tax =($emp_profit_after_tax*$net_income_row['parcentage'])/100;
				$netincome_arr['profit_after_tax']['value']=$emp_profit_after_tax;
				$netincome_arr['profit_after_tax']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['profit_after_tax']['monthly_profit_after_tax']=$monthly_profit_after_tax;
				
			 }
			 
			 
			 if($emp_depriciation>0 && $net_income_row['income_particulars']==6)
			 {
				$monthly_depriciation =($emp_depriciation*$net_income_row['parcentage'])/100;
				$netincome_arr['depriciation']['value']=$emp_depriciation;
				$netincome_arr['depriciation']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['depriciation']['monthly_depriciation']=$monthly_depriciation;
				
			 }
			 
			 if($emp_interestpaid>0 && $net_income_row['income_particulars']==7)
			 {
				$monthly_interestpaid =($emp_interestpaid*$net_income_row['parcentage'])/100;
				$netincome_arr['interestpaid']['value']=$emp_interestpaid;
				$netincome_arr['interestpaid']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['interestpaid']['monthly_interestpaid']=$monthly_interestpaid;
				
			 }
			 
			 if($emp_rentalincome>0 && $net_income_row['income_particulars']==8)
			 {
				$monthly_rentalincome =($emp_rentalincome*$net_income_row['parcentage'])/100;
				$netincome_arr['rentalincome']['value']=$emp_rentalincome;
				$netincome_arr['rentalincome']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['rentalincome']['monthly_rentalincome']=$monthly_rentalincome;
				
			 }
			 
			
		 }
		 
		 return $netincome_arr;
		 
	}
	
	public function nri_netincome_calc($bank_id,$core_prod_id,$employment_type,$fixedsalary,$bonus,$rentalincome,$reimbersment)
	{
	  
	   
	    $netincome_arr =array();
		$i=0;	
		
		//echo "select * from #_net_income_calc where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and employment_type='".$employment_type."' "; exit;
		
		//echo "select * from #_net_income_calc where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and employment_type='".$employment_type."' <br>	";
		
		$net_income_query = $this->db_query("select * from #_net_income_calc where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and employment_type='".$employment_type."' ");
		
		 //echo mysql_num_rows($net_income_query);
		
		while ($net_income_row = $this->db_fetch_array($net_income_query))
	    {
			
			/*echo '<pre>';
			print_r($net_income_row);
			echo '</pre>'; exit;*/
			 if($fixedsalary>0 && $net_income_row['income_particulars']==19)
			 {
				 
				$monthly_gincome =($fixedsalary*$net_income_row['parcentage'])/100;
				$netincome_arr['fixedsalary']['value']=$fixedsalary;
				$netincome_arr['fixedsalary']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['fixedsalary']['monthly_gincome']=$monthly_gincome;
				
			 }
			 
			 if($bonus>0 && $net_income_row['income_particulars']==20)
			 {
				$annual_bonus=(($bonus/12)*$net_income_row['parcentage'])/100;
				$netincome_arr['bonus']['value']=$bonus;
				$netincome_arr['bonus']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['bonus']['annual_bonus']=$annual_bonus;
				
			 }
			 
			 if($rentalincome>0 && $net_income_row['income_particulars']==22)
			 {
				$rental_income=($rentalincome*$net_income_row['parcentage'])/100;
				$netincome_arr['rentalincome']['value']=$rentalincome;
				$netincome_arr['rentalincome']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['rentalincome']['rental_income']=$rental_income;
				
			 }
			 
			 if($reimbersment>0 && $net_income_row['income_particulars']==21)
			 {
				$reimbursement_cal=(($reimbersment)*$net_income_row['parcentage'])/100;
				$netincome_arr['reimbersment']['value']=$reimbersment;
				$netincome_arr['reimbersment']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['reimbersment']['reimbersment']=$reimbursement_cal;
				
			 }
			 
		}
		return $netincome_arr;
		
	}
	
	 
	public function netincome_calc($bank_id,$core_prod_id,$employment_type,$fixedsalary,$bonus,$rentalincome,$reimbersment)
	{
	  
	   
	    $netincome_arr =array();
		$i=0;	
		
		//echo "select * from #_net_income_calc where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and employment_type='".$employment_type."' <br>"; 
		
		$net_income_query = $this->db_query("select * from #_net_income_calc where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and employment_type='".$employment_type."' ");
		
		while ($net_income_row = $this->db_fetch_array($net_income_query))
	    {
			 if($fixedsalary>0 && $net_income_row['income_particulars']==2)
			 {
				$monthly_gincome =($fixedsalary*$net_income_row['parcentage'])/100;
				$netincome_arr['fixedsalary']['value']=$fixedsalary;
				$netincome_arr['fixedsalary']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['fixedsalary']['monthly_gincome']=$monthly_gincome;
				
			 }
			 
			 if($bonus>0 && $net_income_row['income_particulars']==1)
			 {
				$annual_bonus=(($bonus/12)*$net_income_row['parcentage'])/100;
				$netincome_arr['bonus']['value']=$bonus;
				$netincome_arr['bonus']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['bonus']['annual_bonus']=$annual_bonus;
				
			 }
			 
			 if($rentalincome>0 && $net_income_row['income_particulars']==3)
			 {
				$rental_income=($rentalincome*$net_income_row['parcentage'])/100;
				$netincome_arr['rentalincome']['value']=$rentalincome;
				$netincome_arr['rentalincome']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['rentalincome']['rental_income']=$rental_income;
				
			 }
			 
			 if($reimbersment>0 && $net_income_row['income_particulars']==4)
			 {
				$reimbursement_cal=(($reimbersment)*$net_income_row['parcentage'])/100;
				$netincome_arr['reimbersment']['value']=$reimbersment;
				$netincome_arr['reimbersment']['parcentage']=$net_income_row['parcentage'];
				$netincome_arr['reimbersment']['reimbersment']=$reimbursement_cal;
				
			 }
			 
		}
		
		
		return $netincome_arr;
		
	}
	
	public function getTenure($bank_id,$emp_age,$employment_type,$core_prod_id,$property_type)
	{
		
		 $tenure_age_wise =0;
		 $tenure_property_wise =0;
		 $tenure=array();
		 
		 $whh ='';
		 $wh='';
		 if($core_prod_id!='')
		 {
			 $wh.=" and core_prod_id='".$core_prod_id."'";
		 }	 
		 if($employment_type!='')
		 {
			 $whh .=" and employment_type='".$employment_type."' ";
		 }
		 
		 $max_age_limit	=$this->getSingleresult("select max_age_limit from #_tenure_age where status='1' and  bank_id ='".$bank_id."'  and min_age_limit <= '".$emp_age."'  and max_age_limit >= '".$emp_age."'".$wh.$whh); 
		 
		 if($max_age_limit ==0)
		 {
			 
			 $whh='';
		     if($employment_type!='')
		     {
			   $whh .=" and (employment_type='".$employment_type."' or employment_type='ALL')";
		     }
			   
			  $max_age_limit	=$this->getSingleresult("select max_age_limit from #_tenure_age where status='1' and  bank_id ='".$bank_id."'  and min_age_limit <= '".$emp_age."'  and max_age_limit >= '".$emp_age."'".$wh.$whh);  
		 }
		 
			  
		 
		 $tenure_age_wise	= $max_age_limit - $emp_age;
		 
		
		 $whh=''; 
		 if($property_type!='')
		 {
			 $whh .=" and prop_type='".$property_type."'";
		 }
		 
		 if($employment_type!='')
		 {
			 $whh .=" and employment_type='".$employment_type."' ";
		 }
		
			
		$tenure_property_wise	=$this->getSingleresult("select max_tenure_period from #_tenure_property_types where status='1' and bank_id ='".$bank_id."' ".$wh.$whh);
		
		
		if($tenure_property_wise==0)
		{
		     $whh='';
			 
			  if($property_type!='')
			  {
				 $whh .=" and (prop_type='".$property_type."' or prop_type='ALL') ";
			  }
			 
			 if($employment_type!='')
			 {
				 $whh .=" and (employment_type='".$employment_type."' or employment_type='ALL')";
			 }
						
			 $tenure_property_wise	=$this->getSingleresult("select max_tenure_period from #_tenure_property_types where status='1' and bank_id ='".$bank_id."' ".$wh.$whh);	
		}
		  
		$tenure_age_wise =($tenure_age_wise > 0)?$tenure_age_wise:0;
		
	    $tenure_property_wise =($tenure_property_wise > 0)?$tenure_property_wise:0;
		
		if($tenure_age_wise!=0  && ($tenure_property_wise <  $tenure_age_wise && $tenure_property_wise > 0) )
		{
			$tenure_age =$tenure_property_wise;
		
		}else {
		
			$tenure_age =$tenure_age_wise;	
		}
		
		$tenure['tenure_age_wise']=$tenure_age_wise;
		$tenure['tenure_property_wise']=$tenure_property_wise;
		$tenure['tenure_age']=$tenure_age;
		
		/*echo '<pre>';
		print_r($tenure); 
		echo '</pre>';*/
		
		return $tenure;
		
		
	}
	
	public function getEligibility($bank_id,$final_net_income,$emi_per_month,$cost_of_property, 
	$employment_type,$core_prod_id,$property_type)
	{
		     $eligility =array();
	         $wh ='';
			 $whh ='';
			 $eligibility_profile_wise =0;
			 $property_eligility =0;
			 $loan_eligility =0;
			 
			 
			 if($core_prod_id!='')
			 {
				 $wh .=" and core_prod_id='".$core_prod_id."'";
			 }
			 
			 if($cost_of_property!='')
			 {
				 $wh .=" and min_prop_value <= '".$cost_of_property."'  and ( max_prop_value >= '".$cost_of_property."' or max_prop_value='0' )";
			 }
			 
			 if($employment_type!='')
			 {
				// $whh .=" and (employment_type='".$employment_type."' or employment_type ='ALL') ";
				 $whh .=" and employment_type='".$employment_type."'  ";
			 }		
			 
			 if($property_type!='')
			 {
				// $whh .=" and (prop_type_id='".$property_type."' or prop_type_id ='ALL')";
				 $whh .=" and prop_type_id='".$property_type."' ";
			 }
			 
			
			 
			 
		   // echo "select loan_percentage from #_ltv where bank_id='".$bank_id."' and status='1'".$wh.$whh." order by loan_percentage desc <br>"; exit;
							 
			 $ltv_percentage=$this->getSingleresult("select loan_percentage from #_ltv where bank_id='".$bank_id."' and status='1'".$wh.$whh." order by loan_percentage desc");
			 
			 if($ltv_percentage==0)
			 {
				  $whh ='';
				  if($employment_type!='')
			      {
				    $whh .=" and (employment_type='".$employment_type."' or employment_type ='ALL') ";
				  }		
			 
			     if($property_type!='')
			     {
				    $whh .=" and (prop_type_id='".$property_type."' or prop_type_id ='ALL')";
				 }
				
				 $ltv_percentage=$this->getSingleresult("select loan_percentage from #_ltv where bank_id='".$bank_id."' and status='1'".$wh.$whh." order by loan_percentage desc");
				 
			 }
			 
			 $property_eligility = (($cost_of_property*$ltv_percentage)/100);
			 
			 
			 $eligibility_profile_wise =($final_net_income/$emi_per_month)*100000;
			 
			 if($eligibility_profile_wise == 0)
			 {
				   $loan_eligility =$eligibility_profile_wise; 	
				   		 
			 }else if($eligibility_profile_wise > $property_eligility && $property_eligility > 0){
				  
				   $loan_eligility =$property_eligility; 
			 
			 }else {
				 
				  $loan_eligility =$eligibility_profile_wise;  
			 }
			 
			$eligility['eligibility_profile_wise']=$eligibility_profile_wise;
			$eligility['property_eligility']=$property_eligility;
			$eligility['loan_eligility']=$loan_eligility;
			
			//print_r($eligility); exit;
			
			return $eligility;
	}
	
	public function add_core_product($data)
	{
	     $query=$this->db_query("select * from #_core_products where cor_prod_name ='".$this->parse_input($data['cor_prod_name'])."' "); 
	     if(mysql_num_rows($query)==0)
	     {
	   
			   $data['created_on']=date('Y-m-d H:i:s');
			   $data['create_by']=$_SESSION["AMD"][0];
			   $data['status']=1;
			   $data['shortorder']=$this->getSingleresult("select max(shortorder) as shortorder from #_core_products ")+1;
			   $state_id= $this->sqlquery("rs",'core_products',$data);
		  
		 }else {
			   
			  $state_id =  $this->getSingleresult("select pid from #_core_products  where cor_prod_name ='".$this->parse_input($data['cor_prod_name'])."'  "); 
		 }
		 return $state_id;	
	}
	
	public function add_banks($data)
	{
		
		 $query=$this->db_query("select * from #_financial_institutes where institute_name ='".$this->parse_input($data['institute_name'])."' "); 
	     if(mysql_num_rows($query)==0)
	     {
	   
			   $data['created_on']=date('Y-m-d H:i:s');
			   $data['create_by']=$_SESSION["AMD"][0];
			   $data['status']=1;
			   $data['shortorder']=$this->getSingleresult("select max(shortorder) as shortorder from #_financial_institutes ")+1;
			   $state_id= $this->sqlquery("rs",'financial_institutes',$data);
		  
		 }else {
			   
			  $state_id =  $this->getSingleresult("select pid from #_financial_institutes  where institute_name ='".$this->parse_input($data['institute_name'])."'  "); 
		 }
		 return $state_id;	
	}
	
	
	public function add_property($data)
	{
		
		 if(strtolower($data['prop_type_name'])=='all')
		 {
		     $state_id ='ALL';
		 }else {
		
		
			 $query=$this->db_query("select * from #_property_types where core_prod_id ='".$this->parse_input($data['core_prod_id'])."'  and   (prop_type_name ='".$this->parse_input($data['prop_type_name'])."' or prop_code ='".$this->parse_input($data['prop_code'])."')"); 
			 if(mysql_num_rows($query)==0)
			 {
		   
				   $data['created_on']=date('Y-m-d H:i:s');
				   $data['create_by']=$_SESSION["AMD"][0];
				   $data['status']=1;
				   $data['shortorder']=$this->getSingleresult("select max(shortorder) as shortorder from #_property_types ")+1;
				   $state_id= $this->sqlquery("rs",'property_types',$data);
				   $state_id =$data['prop_code'];
			  
			 }else {
				   
				  $state_id =  $this->getSingleresult("select prop_code from #_property_types  where core_prod_id ='".$this->parse_input($data['core_prod_id'])."'  and   (prop_type_name ='".$this->parse_input($data['prop_type_name'])."' or prop_code ='".$this->parse_input($data['prop_code'])."') "); 
			 }
			
		 }
		 return $state_id;	
	}
	
	
	public function add_employment_type($data)
	{
		
		 
		 if(strtolower($data['emp_type_name'])=='all')
		 {
		     $state_id ='ALL';
		 }else {
			 $query=$this->db_query("select * from #_employment_types where emp_code ='".$this->parse_input($data['emp_code'])."' or emp_type_name ='".$this->parse_input($data['emp_type_name'])."' "); 
			 if(mysql_num_rows($query)==0)
			 {
		   
				   $data['created_on']=date('Y-m-d H:i:s');
				   $data['create_by']=$_SESSION["AMD"][0];
				   $data['status']=1;
				   $data['shortorder']=$this->getSingleresult("select max(shortorder) as shortorder from #_employment_types ")+1;
				   $state_id= $this->sqlquery("rs",'employment_types',$data);
			  
			 }else {
				   
				  $state_id =  $this->getSingleresult("select pid from #_employment_types  where emp_code ='".$this->parse_input($data['emp_code'])."'   or emp_type_name ='".$this->parse_input($data['emp_type_name'])."'"); 
			 }
		 }
		 return $state_id;	
	}
	
	
	public function add_professsion_types($data)
	{
		
		 if(strtolower($data['proff_name'])=='all')
		 {
		     $state_id ='ALL';
		 }else {
		
		
		 $query=$this->db_query("select * from #_professsion_types where employement_type ='".$this->parse_input($data['employement_type'])."' or proff_name ='".$this->parse_input($data['proff_name'])."' "); 
	     if(mysql_num_rows($query)==0)
	     {
	   
			   $data['created_on']=date('Y-m-d H:i:s');
			   $data['create_by']=$_SESSION["AMD"][0];
			   $data['status']=1;
			   $data['shortorder']=$this->getSingleresult("select max(shortorder) as shortorder from #_professsion_types ")+1;
			   $state_id= $this->sqlquery("rs",'professsion_types',$data);
		  
		 }else {
			   
			 
		      $state_id =  $this->getSingleresult("select pid from #_professsion_types  where employement_type ='".$this->parse_input($data['employement_type'])."' and proff_name ='".$this->parse_input($data['proff_name'])."'"); 
		 }
		 
		 }
		 
		 return $state_id;	
	}
	
	
	public function add_nature_of_business($data)
	{
		
		
		 if(strtolower($data['nat_buss_name'])=='all')
		 {
		     $state_id ='ALL';
		 }else {
		
		 $query=$this->db_query("select * from #_nature_of_busineses where nat_buss_name ='".$this->parse_input($data['nat_buss_name'])."'  "); 
	     if(mysql_num_rows($query)==0)
	     {
	   
			   $data['created_on']=date('Y-m-d H:i:s');
			   $data['create_by']=$_SESSION["AMD"][0];
			   $data['status']=1;
			   $data['shortorder']=$this->getSingleresult("select max(shortorder) as shortorder from #_nature_of_busineses ")+1;
			   $state_id= $this->sqlquery("rs",'nature_of_busineses',$data);
		  
		 }else {
			   
			  $state_id =  $this->getSingleresult("select pid from #_nature_of_busineses  where nat_buss_name ='".$this->parse_input($data['nat_buss_name'])."'"); 
		 }
		 
		 }
		 return $state_id;	
	}
	
	

  
	
	public function get_finacial_profile($data)
	{
	    if($data =='ALL')
		{
		   $finacial_profile =$data;	
		}else {
		   $finacial_profile =  $this->getSingleresult("select emp_code from #_employment_types where pid='".$data."' ");	
		}
		return $this->parse_output($finacial_profile);
	}
	
	
	
	 public function get_laon_institute_property($employment_type,$property_type,$gender,$borrow_cost,$borrow_time)
	 {
				 
				  $loan_institute_arr =array();
				  $i=0;
				  $wh ='';
				  if($employment_type!='')
				  {
					  $wh .=" and (finacial_profile='".$employment_type."'  or finacial_profile='ALL')";
				  }
				 
				  if($property_type!='')
				  {
					  $wh .=" and (property_type='".$property_type."' or property_type='ALL') ";
				  }
				  
				  if($gender)
				  {
				     $wh .=" and (gender ='".$gender."' or gender='Both')";
				  }
				  
				  if($borrow_cost!='')
				  {
					    $wh .=" and min_loan_amt <='".$borrow_cost."'  and (max_loan_amt >='".$borrow_cost."' or max_loan_amt =0)  ";
				  }
				  
				  
				  if($borrow_time!='')
				  {
					  $wh .=" and (min_tenure <='".$borrow_time."' or min_tenure=0) and (max_tenure >='".$borrow_time."' or max_tenure=0)  ";
				  }
				  
				  
				 // echo  "select * from #_loan_against_property_products where status ='1' ".$wh."<br>";;
				  
				  $home_loan_prd_query =$this->db_query("select * from #_loan_against_property_products where status ='1' ".$wh);
				  
				 // echo mysql_num_rows($home_loan_prd_query);
				  while ($home_loan_prd_row = $this->db_fetch_array($home_loan_prd_query))
				  {
					 $institute_name = $this->getSingleresult("select institute_name from #_financial_institutes where pid='".$home_loan_prd_row['bank_id']."' ");
					 $loan_institute_arr[$i]['institute_name']=$institute_name;
					 $loan_institute_arr[$i]['bank_id']= $home_loan_prd_row['bank_id'];
					 $loan_institute_arr[$i]['roi']= $home_loan_prd_row['roi'];
					 $loan_institute_arr[$i]['product_type_id']= $home_loan_prd_row['product_type_id'];
					 $loan_institute_arr[$i]['home_loan_products_id']= $home_loan_prd_row['pid'];
					 $loan_institute_arr[$i]['home_loan_products_gender']= $home_loan_prd_row['gender'];
					 $i++;
					  
				  }
				  return $loan_institute_arr;
				 
				 
	 }
	 
	 
	 public function getFior($core_prod_id,$employment_type_code,$employment_type,$bank_id,$net_income)
	 {
		  
		   if($employment_type_code =='SENP' || $employment_type_code=='SEP')
		   {
				$fwh =" and appraised_on ='NetIncome' ";
		   }else if($employment_type_code =='salaried')  {
				$fwh =" and appraised_on ='Salary' ";
		   }
		   
		  
		   $foir_query = $this->db_query("select * from #_foir where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and lower_limit <= '".$net_income."'   and (upper_limit >= '".$net_income."' or upper_limit=0)   and employment_type='".$employment_type."' ".$fwh);
		   
		 
		   
		 //  echo mysql_num_rows($foir_query)."==select * from #_foir where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and lower_limit <= '".$net_income."'   and (upper_limit >= '".$net_income."' or upper_limit=0)   and employment_type='".$employment_type."' ".$fwh."<br>"; 
		  	
		   
		   if(mysql_num_rows($foir_query)==0)
		   {
			 
				   if($employment_type_code =='SENP' || $employment_type_code=='SEP')
				   {
						$fwh =" and (appraised_on ='NetIncome' or appraised_on ='ALL' or appraised_on ='') ";
				   }else if($employment_type_code =='salaried')  {
						$fwh =" and (appraised_on ='Salary' or appraised_on ='ALL' or  appraised_on ='') ";
				   }
				   
				 			
				   $foir_query = $this->db_query("select * from #_foir where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and lower_limit <= '".$net_income."'   and (upper_limit >= '".$net_income."' or upper_limit=0)   and (employment_type='".$employment_type."' or employment_type='ALL') ".$fwh);
			   
		   }
		   
		   $foir_rows =array();
		   $foir_rows = $this->db_fetch_array($foir_query);
		   
		   return $foir_rows;
		  // print_r($foir_rows); exit;
		   
	 }
	 
	 
	  public function getGprGtrFior($core_prod_id,$employment_type_code,$employment_type,$bank_id,$net_income)
	 {
		  
		   if($employment_type_code =='SENP')
		   {
				 $fwh =" and appraised_on ='GTR' ";
		   
		   }else if($employment_type_code =='SEP')  {
				 $fwh =" and appraised_on ='GPR' ";
		   }
		   
		  
		   $foir_query = $this->db_query("select * from #_foir where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and lower_limit <= '".$net_income."'   and (upper_limit >= '".$net_income."' or upper_limit=0)   and employment_type='".$employment_type."' ".$fwh);
		   
		 
		     
		  // echo mysql_num_rows($foir_query)."==select * from #_foir where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and lower_limit <= '".$net_income."'   and (upper_limit >= '".$net_income."' or upper_limit=0)   and employment_type='".$employment_type."' ".$fwh."<br>"; 
		   
		   
		   if(mysql_num_rows($foir_query)==0)
		   {
			 
				   if($employment_type_code =='SENP')
				   {
						$fwh =" and (appraised_on ='GTR' or appraised_on ='ALL' or appraised_on ='') ";
				   }else if($employment_type_code =='SEP')  {
						$fwh =" and (appraised_on ='GPR' or appraised_on ='ALL' or  appraised_on ='') ";
				   }
				   
				 			
				   $foir_query = $this->db_query("select * from #_foir where bank_id ='".$bank_id."' and core_prod_id='".$core_prod_id."'  and lower_limit <= '".$net_income."'   and (upper_limit >= '".$net_income."' or upper_limit=0)   and (employment_type='".$employment_type."' or employment_type='ALL') ".$fwh);
			   
		   }
		   
		   $foir_rows =array();
		   $foir_rows = $this->db_fetch_array($foir_query);
		   
		   return $foir_rows;
		  // print_r($foir_rows); exit;
		   
	 }
	 

   public function  RandomString($core_prod_id)
   {
     
	 $prod_code = $this->getSingleresult("select prod_code from #_core_products where pid='".$core_prod_id."' ");
	 
	 $ref_number = $this->getSingleresult("select MAX(ref_number2) from #_searchdata where core_prod_id='".$core_prod_id."' ");
	 
	 $ref_number =($ref_number ==0)?5001:$ref_number+1;
	 
	 $randstring ='C2E'.$prod_code.$ref_number;	
	 
	 $data[0]=$randstring;
	 $data[1]=$ref_number;
	 
     return $data;
  }
	
}
?>