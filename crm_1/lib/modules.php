<?php 
	/*
	# @package BSC CMS
	
	# Copy right code 
	# The base configurations of the BSC CMS.
	# This file has the following configurations: MySQL settings, Table Prefix,
	# by visiting {config.inc.php} Codex page. You can get the MySQL settings from your web host.
	
	# MySQL settings - You can get this info from your web host
	# The name of the database for BSC CMS
	
	# Developer by kigal kishore kiroriwal
	# Company Name : Blue sapphire Creations
	# Website : www.bluesapphirecreations.com
	*/
	
	
//echo $BSC->sform((($mode or $_GET[stags])?'onsubmit="return formvalid(this)"':''));	
echo $BSC->sform((($mode or $_GET[stags])?'':''));	
 
switch ($comp)
{
	
	case $comp:
				if($comp)
				{
					if(is_dir(FS_ADMIN._MODS."/".$comp) === true)
					{
						     include(FS_ADMIN._MODS."/".$comp."/".(($mode)?$mode:'manage').".php");
					} else{
						     include(FS_ADMIN._MODS."/404.php");	
					}
				
				} else {
					
					include(FS_ADMIN._MODS."/dashboard.php");		
				}
				break;					
	default:
	         include(FS_ADMIN._MODS."/dashboard.php");
			 break;
} 
?>
<input type="hidden" name="action" id="action"/>
<?=$BSC->eform();?>


<?php if($comp=='patient_registration') {?>
<!-- Modal popup-->
<div id="Subscription" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
 
  <form method="post" action="" name="SubscriptionForm" id="SubscriptionForm">
  <div class="modal-body">
  <ul class="popupul">
  <li><input placeholder="Subscriber ID number" class="required" required name="subscriber_id_number" id="subscriber_id_number" type="text"></li>
  <li><input placeholder="Subscribed date and time" class=" required" required name="subscribed_date" id="subscribed_date" type="text"></li>
  <li><input placeholder="health plan category (silver / gold / diamond membership plan )" class="required" required name="health_plan_category" id="health_plan_category" type="text"></li>
  <li><input placeholder="health plan expired date" class=" required" required name="health_plan_expired_date" id="health_plan_expired_date" type="text"></li>
  <li><input placeholder="name of subscriber" class=" required" required name="name_of_subscriber" id="name_of_subscriber" type="text"></li>
  <li><input placeholder="date of birth , age , sex" class=" required" required name="date_of_birth_age_sex" id="date_of_birth_age_sex" type="text"></li>
  <li><input placeholder="national registration card number (NRC number)" class="required" required name="national_registration_card_number" id="national_registration_card_number" type="text"></li>
  <li><input placeholder="father’s name" class=" required" required name="fathers_name" id="fathers_name" type="text"></li>
  <li><input placeholder="permanent address" class=" required" required name="permanent_address" id="permanent_address" type="text"></li>
  <li><input placeholder="current address" class=" required" required name="current_address" id="current_address" type="text"></li>
  <li><input placeholder="contact ph number" class=" required" required name="contact_ph_number" id="contact_ph_number" type="text"></li>
  <li><input placeholder="email address" class=" required" required name="email_address" id="email_address" type="text"></li>
  <li><input placeholder="occupation" class=" required" required name="occupation" id="occupation" type="text"></li>
  <li><input placeholder="marital status" class=" required" required name="marital_status" id="marital_status" type="text"></li>
  <li><input placeholder="medical history (health record)" class=" required" required name="medical_history" id="medical_history" type="text"></li>
  <li><input placeholder="choose payment method for subscription" class=" required" required name="payment_method_for_subscription" id="payment_method_for_subscription" type="text"></li>
  </ul>
   <div id="SubscriptionMsg"  style="color:#0C0; text-align:center"></div>
   <button type="button" class="greenbutton inputsearch" onclick="SubscriptionCall()">Submit</button>
  </div>
  </form>
  <!-- <div class="modal-footer">
     <a class="btn nextbutton  next pull-right greenbutton" data-dismiss="modal" href="screen2.html">Close </a>
  </div> -->
</div>
</div>
</div>
<!--close modal popup-->



<?php } ?>


<div id="doctorHistory" class="modal fade" role="dialog">


<div class="modal-dialog modal-dialog-timeline-php">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
 <?php //echo "<pre>"; print_r($_SERVER); echo "</pre>";?>
  <form method="post" action="<?=$_SERVER['REQUEST_URI']?>" name="doctorHistoryfrom" id="doctorHistoryfrom" enctype= "multipart/form-data">
  
  <input type="hidden" name="call_detal_id" id="call_detal_id_history" />
  <div>		
		  	<div class="row">
              <div class="col-md-12">
              		
              		<div class="section-body">
                  
                  <div class="question_ans">
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">Chief Complaint</span>  </li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" name="chief_complaint" value="<?=$chief_complaint?>" class="que_input_box" placeholder=""></span></li>
                  </ul>
                  </div>
                  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">History of present illness </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="history_of_present_illness" ><?=$history_of_present_illness?></textarea> </span></li>
                    </ul>
                  </div>
                  <div class="question_ans">
                  <span class="que_heading">Past history  </span>
                  <div class="nastingul">
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">past medical  history</span></li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder=""  name="past_medical_history" value="<?=$past_medical_history?>"></span></li>
                  </ul>
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">Past surgical history</span></li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="" name="past_surgical_history" value="<?=$past_surgical_history?>" ></span></li>
                  </ul>
                  </div>
                  </div>
                  
                  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Social history  </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="" name="social_history" value="<?=$social_history?>" > </span></li>
                    </ul>
                  </div>
                  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Family history </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder=""  name="family_history" value="<?=$family_history?>" > 
                      </span></li>
                    </ul>
                  </div>
                  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Occupational history</span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder=" " name="occupational_history" value="<?=$occupational_history?>" > 
                      </span></li>
                    </ul>
                  </div>
                    <div class="question_ans">
                      <ul>
                        <li><span class="que_heading">Medication History  </span>  </li>
                        <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">history of drug allergy </span>  </li>
                        <li class="editli"><!-- <img src="img/ans.jpg"> --><span class="que_inner"> <span class="checkbox_callinfo"><input  type="radio" name="history_of_drug_allergy" value="Yes" <?=($history_of_drug_allergy=='Yes')?'checked="checked"':''?> ><label class="check_box">Yes</label></span><span class="checkbox_callinfo"><input type="radio"  name="history_of_drug_allergy" value="No" <?=($history_of_drug_allergy=='No')?'checked="checked"':''?> ><label class="check_box">No</label></span></span></span></li>
                      <br>
                      <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">if yes <input type="text" class="que_input_box" placeholder="" name="drug_allergy_comment"  value="<?=$drug_allergy_comment?>"> </span>  </li>
                        <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">Regular taking medication <input type="text" class="que_input_box" placeholder="" name="drug_allergy_regular_taking_medication"  value="<?=$drug_allergy_regular_taking_medication?>"> </span>  </li>
                       
                      </ul>
                    </div>
                    <div class="question_ans">
						<ul> <li><span class="que_heading">Physical examination findings</span>  </li></ul>
                      <ul class="libox">
						
                        <li><span class="text">BP- </span> <span class="input"><input type="text" class="que_input_box"  name="bp" value="<?=$bp?>" ></span> </li>
                        <li><span class="text">PR-</span> <span class="input"><input type="text" class="que_input_box"  name="pr" value="<?=$pr?>" ></span> </li>
                       <li><span class="text">SaO2-</span> <span class="input"><input type="text" class="que_input_box"  name="sao" value="<?=$sao?>" ></span> </li>
                       <li><span class="text">Weight-</span> <span class="input"><input type="text" class="que_input_box"  name="weight" value="<?=$weight?>"  ></span> </li>
                        <li><span class="text">RBS-</span> <span class="input"><input type="text" class="que_input_box"  name="rbs" value="<?=$rbs?>" ></span> </li>
                       
                      </ul>
                    </div>
					<div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Heart </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="heart" ><?=$heart?></textarea> </span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Lung </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="lung" ><?=$lung?></textarea> </span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Abdomen </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="abdomen" ><?=$abdomen?></textarea> </span></li>
                    </ul>
                  </div>
				   <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Extremities </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="extremities" ><?=$extremities?></textarea> </span></li>
                    </ul>
                  </div>
                    <div class="question_ans">
                      <ul>
                        <li><img src="img/que.jpg"><span class="que_inner">provisional diagnosis   </span>  </li>
                        <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder=""  name="provisional_diagnosis"  value="<?=$provisional_diagnosis?>"> </span></li>
                      </ul>
                    </div>
                     <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Investigations and treatments given  </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="investigations_and_treatments" ><?=$investigations_and_treatments?></textarea> </span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Next follow up appointment on </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box datepicker" placeholder=""  name="next_follow_up_appointment"  value="<?=$next_follow_up_appointment?>"></span></li>
                    </ul>
                  </div>
				   <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Imaging and investigation results </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="file" name="imaging_investigation1" class="" data-errormessage-value-missing="File is required!"></span></li>
					  <li><img src="img/ans.jpg"><span class="que_inner"><input type="file" name="imaging_investigation2" class="" data-errormessage-value-missing="File is required!"></span></li>
					  <li><img src="img/ans.jpg"><span class="que_inner"><input type="file" name="imaging_investigation3" class="" data-errormessage-value-missing="File is required!"></span></li>
					  <li><img src="img/ans.jpg"><span class="que_inner"><input type="file" name="imaging_investigation4" class="" data-errormessage-value-missing="File is required!"></span></li>
					  <li><img src="img/ans.jpg"><span class="que_inner"><input type="file" name="imaging_investigation5" class="" data-errormessage-value-missing="File is required!"></span></li>
					  <li><img src="img/ans.jpg"><span class="que_inner"><label style="color:#E10000; font-size:10px">NOTE: File type allowed("jpeg,jpg,png,bmp,gif,txt,pdf,doc and docx only") and Maximum file size 2 MB</label></span></li>
                    </ul>
                  </div>
				  
                     <div id="doctorhistoryfromerrormsg" style="color:#53A600; text-align:center"></div>
       

			<!-- close add-update form-->
              		</div>
                <div class="col-sm-12"> 
				           <a class="btn nextbutton  next pull-right greenbutton" href="javascript:doctorHistory()">Save <span class="menu-icon"><i class="fa fa-fw fa-chevron-circle-right"></i></span></a> 
			</div>   
			</div>
              </div>
            </div>
	
  </form>
 
</div>
</div>
</div>

