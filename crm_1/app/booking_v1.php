<?php
include ("../lib/open.inc.php");
include 'class/all_class_files.php';
$flag = mysql_real_escape_string ( $_POST ['flag'] );
$json = array ();
include_once 'notification/GCM.php';
$gcm = new GCM ();
date_default_timezone_set("Asia/Rangoon");

// initiate doctor object
$doctor_function = new doctor ();

switch ($flag) {
	
	case 'booking_app' :
		$appointmentdata = array ();
		$appointmentBookMapping = array ();
		$title='';
		$sender_id=0;
		$sender_type='';
		$book_type_name='';
		
		$calldata ['book_type'] = mysql_real_escape_string ( $_POST ['book_type'] );
		$calldata ['service_type'] = mysql_real_escape_string ( $_POST ['service_type'] );
		$calldata ['book_id'] = mysql_real_escape_string ( $_POST ['book_id'] );
		$calldata ['gsm_tocken'] = mysql_real_escape_string ( $_POST ['gsm_tocken'] );
		$lab_test = mysql_real_escape_string ( $_POST ['lab_test'] );
		$lab_test = explode ( ",", $lab_test );
		$calldata ['lab_test'] = json_encode ( $lab_test );
		$calldata ['book_date'] = date ( 'Y-m-d', strtotime ( mysql_real_escape_string ( $_POST ['book_date'] ) ) );
		$calldata ['book_time'] = mysql_real_escape_string ( $_POST ['book_time'] );
		$appointmentdata ['app_date'] = date ( 'Y-m-d', strtotime ( mysql_real_escape_string ( $_POST ['book_date'] ) ) );
		
		$appointmentdata ['app_time'] = mysql_real_escape_string ( $_POST ['book_time'] );
		$appointmentdata ['comments'] = mysql_real_escape_string ( $_POST ['booking_comments'] );
		$calldata ['booking_comments'] = mysql_real_escape_string ( $_POST ['booking_comments'] );
		$calldata ['home_visit_service'] = mysql_real_escape_string ( $_POST ['home_visit_service'] );
		
		if ($calldata ['book_type'] == 'Lab') {
			$title='Appointment with Lab';
			$sender_id=$calldata ['book_id'];
			$sender_type='L';
			$appointmentdata ['appointment_with'] = 'L';
			$calldata ['book_id'] = $PDO->getSingleResult ( "select pid from #_labs where pid='" . $calldata ['book_id'] . "'" );
			$book_type_name=$PDO->getSingleResult ( "select name from #_labs where pid='" . $calldata ['book_id'] . "'" );
		} else if ($calldata ['book_type'] == 'Hospital') {
			$calldata ['book_id'] = $PDO->getSingleResult ( "select pid from #_hospitals where pid='" . $calldata ['book_id'] . "'" );
			$book_type_name=$PDO->getSingleResult ( "select name from #_hospitals where pid='" . $calldata ['book_id'] . "'" );
		} else if ($calldata ['book_type'] == 'Clinics') {
			$calldata ['book_id'] = $PDO->getSingleResult ( "select pid from #_clinics where pid='" . $calldata ['book_id'] . "'" );
			$book_type_name=$PDO->getSingleResult ( "select name from #_clinics where pid='" . $calldata ['book_id'] . "'" );
		} else if ($calldata ['book_type'] == 'Healthcare') {
			$appointmentdata ['appointment_with'] = 'H';
			$title='Appointment with health care';
			$sender_id=$calldata ['book_id'];
			$sender_type='O';
			$calldata ['book_id'] = $PDO->getSingleResult ( "select pid from #_healthcare_organization where pid='" . $calldata ['book_id'] . "'" );
			$book_type_name=$PDO->getSingleResult ( "select name from #_healthcare_organization where pid='" . $calldata ['book_id'] . "'" );
		}
		
		$calldata ['patient_id'] = mysql_real_escape_string ( $_POST ['patient_id'] );
		$patient_id = mysql_real_escape_string ( $_POST ['patient_id'] );
		// $patient_id =$PDO->getSingleResult("select pid from #_patients where patient_id='".$calldata['patient_id']."'");
		
		$calldata ['patient_id'] = $patient_id;
		
		$call_details ['patient_id'] = $patient_id;
		$call_details ['service_type'] = $_POST ['service_type'];
		$call_details ['status'] = 1;
		$call_details ['call_by'] = 'app';
		$call_details ['created_on'] = @date ( 'Y-m-d H:i:s' );
		$call_details ['shortorder'] = $PDO->getSingleresult ( "select max(shortorder) as shortorder from #_call_details where 1=1 " ) + 1;
		$calldata ['call_id'] = $PDO->sqlquery ( "rs", 'call_details', $call_details );
		
		$appointmentdata ['call_details_id'] = $calldata ['call_id'];
		$query = "select * from #_booking where book_id ='" . $calldata ['book_id'] . "' and call_id='" . $calldata ['call_id'] . "' and  book_type='" . $calldata ['book_type'] . "' and book_date ='" . $calldata ['book_date'] . "' and book_time ='" . $calldata ['book_time'] . "' ";
		
		$booking_query = $PDO->db_query ( $query );
		if (mysql_num_rows ( $booking_query ) == 0) {
			$calldata ['status'] = 1;
			$calldata ['created_on'] = @date ( 'Y-m-d H:i:s' );
			$calldata ['create_by'] = $_SESSION ["AMD"] [0];
			$calldata ['shortorder'] = $PDO->getSingleresult ( "select max(shortorder) as shortorder from #_booking where 1=1 " ) + 1;
			$call_id = $PDO->sqlquery ( "rs", 'booking', $calldata );
			
			$appointmentdata ['patient_id'] = $patient_id;
			$appointmentdata ['doctor_id'] = $calldata ['book_id'];
			$appointmentdata ['created_on'] = @date ( 'Y-m-d H:i:s' );
			$appointmentdata ['modified_on'] = @date ( 'Y-m-d H:i:s' );
			$appointmentdata ['status'] = 1;
			$appointment_id = $PDO->sqlquery ( "rs", 'appointment', $appointmentdata );
			
			$appointmentBookMapping ['doctor_id'] = $calldata ['book_id'];
			$appointmentBookMapping ['patient_id'] = $patient_id;
			$appointmentBookMapping ['call_details_id'] = $calldata ['call_id'];
			$appointmentBookMapping ['booking_id'] = $call_id;
			$appointmentBookMapping ['appoinment_id'] = $appointment_id;
			$appointmentBookMapping ['created_on'] = @date ( 'Y-m-d H:i:s' );
			$appointmentBookMapping ['modified_on'] = @date ( 'Y-m-d H:i:s' );
			
			$appointment_mapping_id = $PDO->sqlquery ( "rs", 'book_app_mapping', $appointmentBookMapping );
			
			$patient_gsm = $PDO->getSingleResult ( "select gsm_tocken from #_patients where pid=" . $patient_id );
			if ($patient_gsm) {
					
				$message = array (
						"message" => "Appointment with ".$book_type_name."(".$calldata ['book_type'].") fixed with reference id(" . $appointment_id . ")",
						"flag" => 'booking_app'
				);
				$is_ios_device = $PDO->getSingleResult ( "select is_ios_device from #_patients where pid=" . $patient_id );
				$result = $gcm->send_notification ( $patient_gsm, $message, $title, $sender_id, $sender_type, $patient_id, 'P',$is_ios_device );
				
			}
			
			$json ['status'] = 'true';
		} else {
			$json ['status'] = 'false';
		}
		
		break;
	
	case 'appointment_fix' :
		
		// $patient_id =$PDO->getSingleResult("select pid from #_patients where pid='".$_POST['patient_id']."'");
		$doctor_location_id=$_POST ['doctor_location_id'];
		$doctor_id=$_POST ['doctor_id'];
		$patient_query = $PDO->db_query ( "select * from #_patients where pid='" . $_POST ['patient_id'] . "'" );
		$patient_data = $PDO->db_fetch_array ( $patient_query );
		$mapping_data = array ();
		$call_details = array ();
		$calldata = array ();
		$response_data = array ();
		$time_availablity=NULL;
		$patient_id = $patient_data ['pid'];
		$time_availablity=NULL;
		
		$json ['response_data'] = $response_data;
		$json ['status'] = 'false';
		$json ['msg'] = 'Try Again!!! Failed to get appointment';
		$time_availablity=$doctor_function->TimeIsBetweenTwoTimes(date( 'H:i',strtotime($_POST ['form_time'])),date( 'H:i',strtotime($_POST ['to_time'])),date('H:i'));
		if ((strtotime ( date( 'Y-m-d',strtotime($_POST ['app_date']))) == strtotime ( date ( 'Y-m-d' ))) || (strtotime ( date( 'Y-m-d',strtotime($_POST ['app_date']))) > strtotime ( date ( 'Y-m-d' ))) )
		{
		$query="SELECT * FROM #_doctors_locations WHERE '".date('Y-m-d',strtotime($_POST ['app_date']))."' BETWEEN available_from and available_to and pid='".$_POST ['doctor_location_id']."' and doctor_id='".$_POST ['doctor_id']."' and is_verified=1 and status=1 and isDeleted=0";
		//print_r($query);exit;
		$check_booking_date = $PDO->db_query ( $query );
		if (mysql_num_rows ( $check_booking_date )==0)
		{
		$call_details ['patient_id'] = $patient_id;
		$call_details ['doctor_location_id'] = $doctor_location_id;
		$call_details ['status'] = 1;
		$call_details ['call_by'] = 'app';
		$call_details ['created_on'] = @date ( 'Y-m-d H:i:s' );
		$call_details ['shortorder'] = $PDO->getSingleresult ( "select max(shortorder) as shortorder from #_call_details where 1=1 " ) + 1;
		$calldata ['call_details_id'] = $PDO->sqlquery ( "rs", 'call_details', $call_details );
		$calldata ['doctor_id'] = $doctor_id;
		$calldata ['doctor_location_id'] = $doctor_location_id;
		$calldata ['patient_id'] = $patient_id;
		$calldata ['app_date'] = date ( 'Y-m-d', strtotime ( $_POST ['app_date'] ) );
		// $calldata['app_time'] =$_POST['form_time'].' TO '.$_POST['to_time'];
		$calldata ['status'] = 1;
		$calldata ['created_on'] = @date ( 'Y-m-d H:i:s' );
		$query = "select * from #_appointment where doctor_id ='" . $calldata ['doctor_id'] . "'  and  app_date ='" . $calldata ['app_date'] . "'  and  doctor_location_id ='" . $_POST ['doctor_location_id'] . "' and app_time>='" . date ( 'H:i', strtotime ( $_POST ['form_time'] ) ) . "' and app_time<='" . date ( 'H:i', strtotime ( $_POST ['to_time'] ) ) . "' order by pid desc limit 0,1";
		// print_r($query);exit;
		$booking_query = $PDO->db_query ( $query );
		/*
		 * if (mysql_num_rows ( $booking_query )> 0) {
		 *
		 * while ( $row_data = $PDO->db_fetch_array ( $booking_query ) ) {
		 * $calldata ['app_time'] = $calldata ['app_time'] = date ( 'H:i', strtotime ( '+'.APPOINTMENT_SLOT.' minutes', strtotime ( $row_data ['app_time'] ) ) );
		 * $calldata ['token_no'] = $row_data ['token_no']+1;
		 *
		 * }
		 * }
		 * else if (mysql_num_rows ( $booking_query ) == 0) {
		 * $calldata ['app_time'] = date ( 'H:i',strtotime ( $_POST['form_time'] ) );
		 * $calldata ['token_no'] = 1;
		 * }
		 */
		if (mysql_num_rows ( $booking_query ) > 0) {
			while ( $row_data = $PDO->db_fetch_array ( $booking_query ) ) {
				
				if (strtotime ( $_POST ['app_date'] ) == strtotime ( date ( 'Y-m-d' ) ) && strtotime ( $row_data ['app_time'] ) <= strtotime ( date ( 'H:i' ) )) {
					$row_data ['app_time'] = date ( 'H:i', strtotime ( '+1 Hour', strtotime ( $row_data ['app_time'] ) ) );
					$calldata ['app_time'] = date ( 'H:i', strtotime ( '+' . APPOINTMENT_SLOT . ' minutes', strtotime ( $row_data ['app_time'] ) ) );
					$calldata ['token_no'] = $row_data ['token_no'] + 1;
				} else {
					$calldata ['app_time'] = date ( 'H:i', strtotime ( '+' . APPOINTMENT_SLOT . ' minutes', strtotime ( $row_data ['app_time'] ) ) );
					$calldata ['token_no'] = $row_data ['token_no'] + 1;
				}
			}
		} else if (mysql_num_rows ( $booking_query ) == 0) {
			if (strtotime ( $_POST ['app_date'] ) == strtotime ( date ( 'Y-m-d' ) ) && strtotime ( $_POST ['form_time'] ) <= strtotime ( date ( 'H:i' ) )) {
				$calldata ['app_time'] = date ( 'H:i', strtotime ( '+1 hour', strtotime ( date ( 'H:i' ) ) ) );
				$calldata ['token_no'] = 1;
			} else {
				$calldata ['app_time'] = date ( 'H:i', strtotime ( $_POST ['form_time'] ) );
				$calldata ['token_no'] = 1;
			}
		}
		
		$calldata ['shortorder'] = $PDO->getSingleresult ( "select max(shortorder) as shortorder from #_appointment where 1=1 " ) + 1;
		
		$call_id = $PDO->sqlquery ( "rs", 'appointment', $calldata );
		
		$mapping_data ['doctor_id'] = $_POST ['doctor_id'];
		$mapping_data ['patient_id'] = $_POST ['patient_id'];
		$mapping_data ['call_details_id'] = $calldata ['call_details_id'];
		$mapping_data ['appoinment_id'] = $call_id;
		$mapping_data ['created_on'] = @date ( 'Y-m-d H:i:s' );
		$mapping_data ['modified_on'] = @date ( 'Y-m-d H:i:s' );
		
		$mapping_id = $PDO->sqlquery ( "rs", 'book_app_mapping', $mapping_data );
		
		if ($mapping_id) {
			$doctor_id = $_POST ['doctor_id'];
			$response_data ['doctor_id'] = $doctor_id;
			$app_date = date ( 'D, d F Y', strtotime ( $calldata ['app_date'] ) );
			$app_time = date ( 'h:i A', strtotime ( $calldata ['app_time'] ) );
			$response_data ['app_date_time'] = $app_date . " - " . $app_time;
			$doctor_location_id = $_POST ['doctor_location_id'];
			$response_data ['doctor_location_id'] = $doctor_location_id;
			
			$response_data ['doctorId'] = ($doctor_id === NULL) ? '' : $doctor_id;
			$response_data ['doctorName'] = ($doctor_id === NULL) ? '' : $PDO->getSingleResult ( "select name from #_doctors where pid='" . $doctor_id . "'" );
			$response_data ['doctorEmail'] = ($doctor_id === NULL || $PDO->getSingleResult ( "select email from #_doctors where pid='" . $doctor_id . "'" ) == '') ? '' : $PDO->getSingleResult ( "select email from #_doctors where pid='" . $doctor_id . "'" );
			$response_data ['doctorEducation'] = ($doctor_id === NULL || $PDO->getSingleResult ( "select education from #_doctors where pid='" . $doctor_id . "'" ) == '') ? '' : $PDO->getSingleResult ( "select education from #_doctors where pid='" . $doctor_id . "'" );
			$response_data ['doctorProfileImage'] = ($doctor_id === NULL || $PDO->getSingleResult ( "select profile_image from #_doctors where pid='" . $doctor_id . "'" ) == '') ? '' : SITE_PATH . "uploaded_files/doctors/" . $PDO->getSingleResult ( "select profile_image from #_doctors where pid='" . $doctor_id . "'" );
			$response_data ['doctorExperience'] = ($doctor_id === NULL || $PDO->getSingleResult ( "select years_of_experience from #_doctors where pid='" . $doctor_id . "'" ) == '') ? '' : $PDO->getSingleResult ( "select years_of_experience from #_doctors where pid='" . $doctor_id . "'" );
			 
			 $speciality=$PDO->getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_id . "'" );

			if($speciality==0){
                $response_data ['doctorSpeciality']='General Practitioner';

            }else{
                $response_data ['doctorSpeciality'] = ($doctor_id === NULL || $PDO->getSingleResult ( "select name from #_specialities where pid='" . $PDO->getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_id . "'" ) . "'" ) == '') ? '' : $PDO->getSingleResult ( "select name from #_specialities where pid='" . $PDO->getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_id . "'" ) . "'" );
            }
			/*$response_data ['doctorSpeciality'] = ($doctor_id === NULL || $PDO->getSingleResult ( "select name from #_specialities where pid='" . $PDO->getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_id . "'" ) . "'" ) == '') ? '' : $PDO->getSingleResult ( "select name from #_specialities where pid='" . $PDO->getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_id . "'" ) . "'" );
			*/
			$location_type = $PDO->getSingleResult ( "select location_type from #_doctors_locations where pid='" . $doctor_location_id . "'" );
			$clinic_id = $PDO->getSingleResult ( "select clinic_id from #_doctors_locations where pid='" . $doctor_location_id . "'" );
			$hospital_id = $PDO->getSingleResult ( "select hospital_id from #_doctors_locations where pid='" . $doctor_location_id . "'" );
			$response_data ['locationName'] = ($location_type === NULL) ? '' : (($location_type == 1) ? $PDO->getSingleResult ( "select name from #_clinics where pid='" . $clinic_id . "'" ) . "(Clinic)" : $PDO->getSingleResult ( "select name from #_hospitals where pid='" . $hospital_id . "'" ) . "(Hospital)");
			$response_data ['locationAddress'] = ($doctor_location_id === NULL || $PDO->getSingleResult ( "select location from #_doctors_locations where pid='" . $doctor_location_id . "'" ) == '') ? '' : $PDO->getSingleResult ( "select location from #_doctors_locations where pid='" . $doctor_location_id . "'" );
		}
		$user_id=$PDO->getSingleResult ( "select user_id from #_doctors where pid='" . $doctor_id . "'" );
		$user_type = 'doctor';
		$gcmuser = $PDO->db_query ( "select * from #_gcmuser where user_id ='" . $user_id . "'  and user_type='" . $user_type . "' and flag='1' ORDER BY pid DESC LIMIT 0,1 " );
		// echo "select * from #_gcmuser where user_id ='".$user_id."' and user_type='".$user_type."' and flag='1' ";
		if (mysql_num_rows ( $gcmuser ) > 0) {
			
			$patient_name = $PDO->getSingleResult ( "select name from #_patients where pid='" . $patient_id . "'" );
			while ( $gcmuser_row = $PDO->db_fetch_array ( $gcmuser ) ) {
				
				$message = array (
						"message" => $patient_name . " booked an appointment with you.",
						"flag" => 'doctor' 
				);
				$is_ios_device = $PDO->getSingleResult ( "select is_ios_device from #_patients where pid=" . $patient_id );
				$result = $gcm->send_notification ( $gcmuser_row ['gsm_tocken'], $message, "Appointment", $patient_id, 'P', $user_id, 'D',$is_ios_device ); // print_r($result);
			}
		}
		
		$mobile = $PDO->getSingleresult ( "select phone from #_doctors where pid='" . $_POST ['doctor_id'] . "' " );
		$message = urlencode ( (($patient_data ['sex'] == 'Female') ? 'Ms' : 'Mr') . ' ' . $patient_data ['name'] . ' has fixed an appointment with you at ' . $calldata ['app_time'] . ' form  ' . $calldata ['app_date'] );
		
		file_get_contents ( SITE_SUB_PATH . "app/sms/send_sms.php?mobile=" . $mobile . "&message=" . $message );
		
		$json ['response_data'] = $response_data;
		$json ['status'] = 'true';
		$json ['msg'] = 'Appointment fixed with doctor';
		}
		else 
		{
			$json ['msg'] = 'Selected date doctor is unavailable!! Please try with another date';
		}
		}
		else 
		{
			$json ['msg'] = 'Selected date doctor is unavailable!! Please try with another date';
		}
		break;
	case 'emergency_location' :
		
		$patient_id = mysql_real_escape_string ( $_POST ['patient_id'] );
		// $patient_id =$PDO->getSingleResult("select pid from #_patients where patient_id='".$patient_id."'");
		$calldata ['patient_id'] = $patient_id;
		$calldata ['longitude'] = mysql_real_escape_string ( $_POST ['longitude'] );
		$calldata ['attitude'] = mysql_real_escape_string ( $_POST ['lattitude'] );
		$calldata ['location'] = file_get_contents ( "https://maps.googleapis.com/maps/api/geocode/json?latlng=" . $calldata ['attitude'] . "," . $calldata ['longitude'] . "&key=AIzaSyAFcge70nYYfzdjJ7IloOyA9zJIZrxJQb0" );
		$calldata ['created_on'] = date ( 'Y-m-d H:i:s' );
		$pid = $PDO->sqlquery ( "rs", 'emergency', $calldata );
		$json ['pid'] = $pid;
		$json ['status'] = 'true';
		
		break;
	
	case 'appointment_finish' :
		$appointment_id = mysql_real_escape_string ( $_POST ['appointment_id'] );
		$comments = mysql_real_escape_string ( $_POST ['comments'] );
		$PDO->db_query ( "update #_appointment  SET  comments ='" . $comments . "', status ='2' where pid ='" . $appointment_id . "' " );
		$json ['status'] = 'true';
		
		break;
	
	case 'booking_all' :
		
		$pid = mysql_real_escape_string ( $_POST ['pid'] );
		$user_type = mysql_real_escape_string ( $_POST ['user_type'] );
		
		if ($user_type == 'clinics') {
			$wh = " and book_type='Clinics'";
		} else if ($user_type == 'healthcare_organisation') {
			
			$wh = "  and book_type='Healthcare'";
		} else if ($user_type == 'labs') {
			$wh = " and book_type='Lab'";
		} else if ($user_type == 'hospital') {
			$wh = " and book_type='Hospital'";
		}
		
		$booking_query = $PDO->db_query ( "select * from #_booking  where book_id ='" . $pid . "'  " . $wh . " order by pid desc " );
		
		$json ['detail'] = array ();
		if (mysql_num_rows ( $booking_query ) > 0) {
			$i = 0;
			$data = array ();
			while ( $booking_data = mysql_fetch_assoc ( $booking_query ) ) {
				
				$patient_query = $PDO->db_query ( "select * from #_patients where pid='" . $booking_data ['patient_id'] . "'" );
				$patient_row = $PDO->db_fetch_array ( $patient_query );
				
				$division_query = $PDO->db_query ( "select * from #_division where pid='" . $patient_row ['division'] . "'" );
				$division_row = $PDO->db_fetch_array ( $division_query );
				
				$township_query = $PDO->db_query ( "select * from #_township where division_id='" . $division_row ['pid'] . "'" );
				$township_row = $PDO->db_fetch_array ( $township_query );
				
				$booking_data ['patient_name'] = $patient_row ['name'];
				$booking_data ['patient_id'] = $patient_row ['patient_id'];
				$booking_data ['patient_age'] = $patient_row ['age'];
				$booking_data ['patient_sex'] = $patient_row ['sex'];
				$booking_data ['patient_phone'] = $patient_row ['phone'];
				$booking_data ['patient_address'] = $patient_row ['address'];
				$booking_data ['patient_postal_code'] = $patient_row ['postal_code'];
				$booking_data ['patient_township'] = $township_row ['name'];
				$booking_data ['patient_division'] = $division_row ['name'];
				
				$data [] = $booking_data;
			}
			
			$json ['detail'] = $data;
			$json ['status'] = 'true';
		} else {
			$json ['status'] = 'false';
		}
		
		break;
	
	case 'booking_finish' :
		$pid = mysql_real_escape_string ( $_POST ['pid'] );
		$comments = mysql_real_escape_string ( $_POST ['comments'] );
		$PDO->db_query ( "update #_booking  SET  booking_complete_comments ='" . $comments . "', booking_complete_flag ='1' where pid ='" . $pid . "' " );
		$json ['status'] = 'true';
		
		break;
	
	default :
		break;
}

/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode ( $json );
?>