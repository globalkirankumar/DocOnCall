<?php
include("../lib/open.inc.php");
include 'class/all_class_files.php';
$flag = mysql_real_escape_string($_POST['flag']);
$json = array();
$pagesize = 100;
$doctor_function = new doctor();
switch ($flag) {

    case 'doctorsearch':
        $division =mysql_real_escape_string($_POST['division']);
        $township =mysql_real_escape_string($_POST['township']);
        $hospital_id =mysql_real_escape_string($_POST['hospital_id']);
        $clinic_id =mysql_real_escape_string($_POST['clinic_id']);
        $isHomeservice =mysql_real_escape_string($_POST['isHomeservice']);
        $isChatNeeded =mysql_real_escape_string($_POST['isChatNeeded']);
        $is_voip =mysql_real_escape_string($_POST['is_voip']);
        $speciality =mysql_real_escape_string($_POST['speciality']);
        $name =mysql_real_escape_string($_POST['name']);
        $phone =mysql_real_escape_string($_POST['phone']);
        $location =mysql_real_escape_string($_POST['location']);
        $lang_flag =mysql_real_escape_string($_POST['lang_flag']);
        $page_num=mysql_real_escape_string($_POST['page_num']);
        $start =($page_num==0)?0:($page_num-1)*$pagesize;
        $wh='';
         if ($hospital_id != '') {
            $wh = " and dl.hospital_id ='" . $hospital_id . "'";
        }
        if ($clinic_id != '') {
            $wh = " and dl.clinic_id ='" . $clinic_id . "'";
        }
        if ($isHomeservice != '') {
            $wh = " and D.isHomeservice ='" . $isHomeservice . "'";
        }
        if ($isChatNeeded != '') {
            $wh = " and D.isChatNeeded ='" . $isChatNeeded . "'";
        }
		if ($is_voip != '') {
            $wh = " and D.is_voip ='" . $is_voip . "'";
        }
        if ($division != '') {
            $wh = " and D.division ='" . $division . "'";
        }

        if ($township != '') {
            $wh .= " and D.township ='" . $township . "'";
        }

        if ($speciality != '') {
            $wh .= " and D.speciality ='" . $speciality . "'";
        }
        if ($name != '' && $lang_flag == "en") {
            $wh .= " and D.name like '%" . $name . "%'";
        }
        if ($name != '' && $lang_flag == "my") {
            $wh .= " and D.name_my like '%" . $name . "%'";
        }
        if ($phone != '') {
            $wh .= " and D.phone like '%" . $phone . "%'";
        }
        if ($location != '' && $lang_flag == "en") {
            $wh .= " and dl.location like '%" . $location . "%'";
        }
        if ($location != '' && $lang_flag == "my") {
            $wh .= " and dl.location_my like '%" . $location_my . "%'";
        }

        $json['doctor'] = array();

        date_default_timezone_set("Asia/Yangon");
        $today_date = date("Y-m-d");

        // $wh.="  And D.busy_free=1 and D.is_verified=1";
        $wh .= "   and D.is_verified=1 and dl.is_verified=1 and dl.status=1 and dl.isDeleted=0 and dl.available_status=1";

        //	echo "select D.*, S.position as sponsored_position  from #_doctors as D  LEFT JOIN #_sponsored as S ON S.drname = D.pid  where  D.status=1 ".$wh." group by D.pid order by  S.position desc, D.name asc limit $start,$pagesize";
        $query = "select D.pid,D.speciality,D.name,D.name_my,D.email,D.phone,D.sex,D.date_of_birth,D.doctor_type,D.rs_number,D.fees,D.education,D.area_code,D.address,D.live_status,D.township,D.division,D.isHomeservice,D.isChatNeeded,D.profile_image, dl.hospital_id,dl.clinic_id,dl.location,dl.location_my,dl.dates ,dl.available_status from #_doctors as D LEFT JOIN #_doctors_locations as dl ON dl.doctor_id = D.pid  where  D.status=1 " . $wh . " group by D.pid order by D.pid, D.name asc limit 0,100";//limit ". $start.",".$pagesize

		
     /*   $query = "SELECT * FROM `crm_doctors` as D
      where  D.status=1 " . $wh . " group by D.pid order by D.name asc limit $start,$pagesize";//limit ". $start.",".$pagesize
        exit;*/
        /* $query="SELECT D.*,dl.location_type as location_type,dl.hospital_id as hospitalId,dl.clinic_id as clinicId FROM `crm_doctors` as D LEFT JOIN #_doctor_locations as dl ON dl.doctor_id = D.pid
       where  D.status=1 ".$wh." group by D.pid order by D.name asc limit 0,100";//limit ". $start.",".$pagesize
 */
        // print_r($query);exit();
        $doctor_query = $PDO->db_query($query);

        //print_r(mysql_num_rows($doctor_query));exit;

        // echo mysql_num_rows($doctor_query);

       
        if(mysql_num_rows($doctor_query) >0 )
        {
            $doctor =array();
            $d=0;
            while ($doctor_rows = $PDO->db_fetch_array($doctor_query))
            {
                // get the doctor location details from doctor_locations table
                $doctors_location_list_values = $doctor_function->get_doctors_location_list ( $doctor_rows['pid'], $lang_flag );
                if ($doctors_location_list_values != 'false') {
                    $doctor[$d]['doctors_location_list'] = $doctors_location_list_values;
                } else {
                    $doctor[$d]['doctors_location_list']=array();
                }
                if ($lang_flag == "en") {
                    $doctor[$d]['hospital'] = $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                    $doctor[$d]['hospital_phone'] = $PDO->getSingleResult("select phone from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                    $doctor[$d]['clinic'] = $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                    $doctor[$d]['clinic_phone'] = $PDO->getSingleResult("select phone from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                    if($doctor_rows['speciality']==0){
                        $doctor[$d]['speciality'] = 'General Practitioner';
                    }else{
                        $doctor[$d]['speciality'] = $PDO->getSingleResult("select name from #_specialities where pid='" . $doctor_rows['speciality'] . "'");
                    }
                }
                if ($lang_flag == "my") {
                    $doctor[$d]['hospital'] = $PDO->getSingleResult("select name_be from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                    $doctor[$d]['hospital_phone'] = $PDO->getSingleResult("select name_be from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                    $doctor[$d]['clinic'] = $PDO->getSingleResult("select name_be from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                    $doctor[$d]['clinic_phone'] = $PDO->getSingleResult("select name_be from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                    if($doctor_rows['speciality']==0){
                        $doctor[$d]['speciality'] = 'General Practitioner';
                    }else{
                        $doctor[$d]['speciality'] = $PDO->getSingleResult("select name_my from #_specialities where pid='" . $doctor_rows['speciality'] . "'");
                    }
                }
				
                $doctor[$d]['user_id'] =$doctor_rows['pid'];									if($lang_flag=="en"){
                $doctor[$d]['name'] =$doctor_rows['name'];
                $doctor[$d]['location'] =$doctor_rows['location'];
            }
                if($lang_flag=="my")
                {
                    $doctor[$d]['name'] =$doctor_rows['name_my'];
                    $doctor[$d]['location'] =$doctor_rows['location_my'];
                }
                $doctor[$d]['email'] =$doctor_rows['email'];
                $doctor[$d]['phone'] =$doctor_rows['phone'];
                $doctor[$d]['sex'] =$doctor_rows['sex'];
                $doctor[$d]['date_of_birth'] =$doctor_rows['date_of_birth'];
                $doctor[$d]['doctor_type'] =$doctor_rows['doctor_type'];
                $doctor[$d]['rs_number'] =$doctor_rows['rs_number'];
                $doctor[$d]['fees'] =$doctor_rows['fees'];
                $doctor[$d]['education'] =$doctor_rows['education'];
                $doctor[$d]['area_code'] =$doctor_rows['area_code'];
                $doctor[$d]['address'] =$doctor_rows['address'];
                $doctor[$d]['live_status'] =$doctor_rows['live_status'];
                $doctor[$d]['dates'] =$doctor_rows['dates'];
                if($lang_flag=="en")
                {
                    $doctor[$d]['township']=$PDO->getSingleResult("select name from #_township where pid='".$doctor_rows['township']."'");
                    $doctor[$d]['division']=$PDO->getSingleResult("select name from #_division where pid='".$doctor_rows['division']."'");
                }
                if($lang_flag=="my")
                {
                    $doctor[$d]['township']=$PDO->getSingleResult("select name_my from #_township where pid='".$doctor_rows['township']."'");
                    $doctor[$d]['division']=$PDO->getSingleResult("select name_my from #_division where pid='".$doctor_rows['division']."'");

                }
                /*      $doctor[$d]['sponsored_position']  =($doctor_rows['sponsored_position']>0)?$doctor_rows['sponsored_position']:0;

                      $doctor[$d]['sponsored_position']  =($doctor_rows['sponsored_position']>0)?$doctor_rows['sponsored_position']:0;
                      $doctor[$d]['doctorLocationId']  =$doctor_rows['doctorLocationId'];
                      $doctor[$d]['doctorLocationType']  =$doctor_rows['doctorLocationType'];
                      $doctor[$d]['doctorLocation']  =$doctor_rows['doctorLocation'];
                      $doctor[$d]['doctorLatLong']  =$doctor_rows['doctorLatLong'];
                      //$doctor[$d]['doctorAvailableStatus']  =$doctor_rows['doctorAvailableStatus'];
                      $doctor[$d]['availableFrom']  =$doctor_rows['availableFrom'];
                      $doctor[$d]['availableTo']  =$doctor_rows['availableTo'];*/
                $doctor[$d]['isHomeservice']  =$doctor_rows['isHomeservice'];
                /*   $doctor[$d]['doctorAvailableStatus']  ="0";
                   $doctor[$d]['currentAvailableStatus']="0"; */
                $doctor[$d]['isChatNeeded']=$doctor_rows['isChatNeeded'];
                /*   $current_available_status=$doctor_function->check_available_unavailable($doctor_rows['pid'],$doctor_rows['doctorLocationId']);
                   if ($current_available_status['status']=='true')
                   {
                       $doctor[$d]['doctorAvailableStatus']  ="1";
                       $doctor[$d]['currentAvailableStatus']="1";
                   }
                */
                if (($doctor_rows ['profile_image'] == NULL)||($doctor_rows ['profile_image'] == '')) {
                    $doctor[$d] ['profile_image'] = '';
                } else {
                    $doctor[$d] ['profile_image'] = SITE_PATH_ADM. "uploaded_files/doctors/profile/" . $doctor_rows ['profile_image'];
                }

                $d++;
            }
            $json['doctor']  =$doctor;

            $json['status']  ='true';

        }else {  $json['status']  ='false';  }

        break;

    case 'hospitalsearch':
        $division = mysql_real_escape_string($_POST['division']);
        $township = mysql_real_escape_string($_POST['township']);
        $name = mysql_real_escape_string($_POST['name']);
        $phone = mysql_real_escape_string($_POST['phone']);
        $address = mysql_real_escape_string($_POST['address']);
        $lang_flag = mysql_real_escape_string($_POST['lang_flag']);
        $page_num = mysql_real_escape_string($_POST['page_num']);
        $start = ($page_num == 0) ? 0 : ($page_num - 1) * $pagesize;
        $wh = '';
        if ($division != '') {
            $wh = " and H.division ='" . $division . "'";
        }

        if ($township != '') {
            $wh .= " and H.township ='" . $township . "'";
        }
        if ($name != '' && $lang_flag == "en") {
            $wh .= " and H.name like '%" . $name . "%'";
        }
        if ($name != '' && $lang_flag == "my") {
            $wh .= " and H.name_be like '%" . $name . "%'";
        }
        if ($phone != '') {
            $wh .= " and H.phone like '%" . $phone . "%'";
        }
        if ($address != '' && $lang_flag == "en") {
            $wh .= " and H.address like '%" . $address . "%'";
        }
        if ($address != '' && $lang_flag == "my") {
            $wh .= " and H.address_be like '%" . $address . "%'";
        }


        $json['hospital'] = array();


        $hospital_query = $PDO->db_query("select H.*, S.position as sponsored_position from #_hospitals as H LEFT JOIN #_sponsore_hospital as S ON S.honame = H.pid  where  H.status=1 " . $wh . " group by H.pid order by  S.position desc, H.shortorder desc limit $start,$pagesize");


        // echo mysql_num_rows($doctor_query);

        if (mysql_num_rows($hospital_query) > 0) {
            $hospital = array();
            $d = 0;
            while ($hospital_rows = $PDO->db_fetch_array($hospital_query)) {

                if ($lang_flag == "en") {
                    $hospital[$d]['hospital_name'] = $hospital_rows['name'];
                    $hospital[$d]['address'] = $hospital_rows['address'];
                    $hospital[$d]['nearest_land_mark'] = $hospital_rows['nearest_land_mark'];
                    $hospital[$d]['nearest_bus_stop'] = $hospital_rows['nearest_bus_stop'];
                    $hospital[$d]['ambulance_service'] = $hospital_rows['ambulance_service'];
                    $hospital[$d]['medical_check_up_package'] = $hospital_rows['medical_check_up_package'];
                    $hospital[$d]['home_visit_coverage_area'] = $hospital_rows['home_visit_coverage_area'];
                }
                if ($lang_flag == "my") {
                    $hospital[$d]['hospital_name'] = $hospital_rows['name_be'];
                    $hospital[$d]['address'] = $hospital_rows['address_be'];
                    $hospital[$d]['nearest_land_mark'] = $hospital_rows['nearest_land_mark_be'];
                    $hospital[$d]['nearest_bus_stop'] = $hospital_rows['nearest_bus_stop_be'];
                    $hospital[$d]['ambulance_service'] = $hospital_rows['ambulance_service_be'];
                    $hospital[$d]['medical_check_up_package'] = $hospital_rows['medical_check_up_package_be'];
                    $hospital[$d]['home_visit_coverage_area'] = $hospital_rows['home_visit_coverage_area_be'];

                }

                $hospital[$d]['user_id'] = $hospital_rows['user_id'];
                $hospital[$d]['email'] = $hospital_rows['email'];
                $hospital[$d]['phone'] = $hospital_rows['phone'];
                $hospital[$d]['hot_line_number'] = $hospital_rows['hot_line_number'];
                $hospital[$d]['home_visit_service'] = $hospital_rows['home_visit_service'];
                $hospital[$d]['home_visit_available_start_time'] = $hospital_rows['home_visit_available_start_time'];
                $hospital[$d]['home_visit_available_end_time'] = $hospital_rows['home_visit_available_end_time'];
                $hospital[$d]['home_visit_service_charges'] = $hospital_rows['home_visit_service_charges'];
                if ($lang_flag == "en") {
                    $hospital[$d]['township'] = $PDO->getSingleResult("select name from #_township where pid='" . $hospital_rows['township'] . "'");
                    $hospital[$d]['division'] = $PDO->getSingleResult("select name from #_division where pid='" . $hospital_rows['division'] . "'");
                }
                if ($lang_flag == "my") {
                    $hospital[$d]['township'] = $PDO->getSingleResult("select name_my from #_township where pid='" . $hospital_rows['township'] . "'");
                    $hospital[$d]['division'] = $PDO->getSingleResult("select name_my from #_division where pid='" . $hospital_rows['division'] . "'");
                }

                $hospital[$d]['sponsored_position'] = ($hospital_rows['sponsored_position'] > 0) ? $hospital_rows['sponsored_position'] : 0;
                $d++;
            }
            $json['hospital'] = $hospital;

            $json['status'] = 'true';

        } else {
            $json['status'] = 'false';
        }

        break;

    case 'clinicsearch':
        $division = mysql_real_escape_string($_POST['division']);
        $township = mysql_real_escape_string($_POST['township']);
        $name = mysql_real_escape_string($_POST['name']);
        $phone = mysql_real_escape_string($_POST['phone']);
        $address = mysql_real_escape_string($_POST['address']);
        $lang_flag = mysql_real_escape_string($_POST['lang_flag']);
        $page_num = mysql_real_escape_string($_POST['page_num']);
        $start = ($page_num == 0) ? 0 : ($page_num - 1) * $pagesize;
        $wh = '';
        if ($division != '') {
            $wh = " and C.division ='" . $division . "'";
        }

        if ($township != '') {
            $wh .= " and C.township ='" . $township . "'";
        }
        if ($name != '' && $lang_flag == "en") {
            $wh .= " and C.name like '%" . $name . "%'";
        }
        if ($name != '' && $lang_flag == "my") {
            $wh .= " and C.name_be like '%" . $name . "%'";
        }
        if ($phone != '') {
            $wh .= " and C.phone like '%" . $phone . "%'";
        }
        if ($address != '' && $lang_flag == "en") {
            $wh .= " and C.address like '%" . $address . "%'";
        }
        if ($address != '' && $lang_flag == "my") {
            $wh .= " and C.address_be like '%" . $address . "%'";
        }


        $json['clinics'] = array();


        $clinics_query = $PDO->db_query("select C.*, S.position as sponsored_position from #_clinics as C LEFT JOIN #_sponsore_clinics as S ON S.clname = C.pid  where  C.status=1 " . $wh . " group by C.pid order by  S.position desc, C.shortorder desc limit $start,$pagesize");


        // echo mysql_num_rows($doctor_query);

        if (mysql_num_rows($clinics_query) > 0) {
            $clinics = array();
            $d = 0;
            while ($clinics_rows = $PDO->db_fetch_array($clinics_query)) {

                if ($lang_flag == "en") {
                    $clinics[$d]['hospital_name'] = $clinics_rows['name'];
                    $clinics[$d]['address'] = $clinics_rows['address'];
                    $clinics[$d]['nearest_land_mark'] = $clinics_rows['nearest_land_mark'];
                    $clinics[$d]['nearest_bus_stop'] = $clinics_rows['nearest_bus_stop'];
                    $clinics[$d]['home_visit_coverage_area'] = $clinics_rows['home_visit_coverage_area'];
                }
                if ($lang_flag == "my") {
                    $clinics[$d]['hospital_name'] = $clinics_rows['name_be'];
                    $clinics[$d]['address'] = $clinics_rows['address_be'];
                    $clinics[$d]['nearest_land_mark'] = $clinics_rows['nearest_land_mark_be'];
                    $clinics[$d]['nearest_bus_stop'] = $clinics_rows['nearest_bus_stop_be'];
                    $clinics[$d]['home_visit_coverage_area'] = $clinics_rows['home_visit_coverage_area_be'];

                }

                $clinics[$d]['user_id'] = $clinics_rows['user_id'];
                $clinics[$d]['email'] = $clinics_rows['email'];
                $clinics[$d]['phone'] = $clinics_rows['phone'];
                $clinics[$d]['postal_code'] = $clinics_rows['postal_code'];
                $clinics[$d]['contact_person_name'] = $clinics_rows['contact_person_name'];
                $clinics[$d]['contact_person_email'] = $clinics_rows['contact_person_email'];
                $clinics[$d]['contact_person_phone'] = $clinics_rows['contact_person_phone'];
                $clinics[$d]['clinic_open_time'] = $clinics_rows['clinic_open_time'];
                $clinics[$d]['clinic_close_time'] = $clinics_rows['clinic_close_time'];
                $clinics[$d]['clinic_condition'] = $clinics_rows['clinic_condition'];
                $clinics[$d]['home_visit_service'] = $clinics_rows['home_visit_service'];
                $clinics[$d]['home_visit_available_start_time'] = $clinics_rows['home_visit_available_start_time'];
                $clinics[$d]['home_visit_available_end_time'] = $clinics_rows['home_visit_available_end_time'];
                $clinics[$d]['home_visit_charges'] = $clinics_rows['home_visit_charges'];
                if ($lang_flag == "en") {
                    $clinics[$d]['township'] = $PDO->getSingleResult("select name from #_township where pid='" . $clinics_rows['township'] . "'");
                    $clinics[$d]['division'] = $PDO->getSingleResult("select name from #_division where pid='" . $clinics_rows['division'] . "'");
                }
                if ($lang_flag == "my") {
                    $clinics[$d]['township'] = $PDO->getSingleResult("select name_my from #_township where pid='" . $clinics_rows['township'] . "'");
                    $clinics[$d]['division'] = $PDO->getSingleResult("select name_my from #_division where pid='" . $clinics_rows['division'] . "'");
                }

                $clinics[$d]['sponsored_position'] = ($clinics_rows['sponsored_position'] > 0) ? $clinics_rows['sponsored_position'] : 0;
                $d++;
            }
            $json['clinics'] = $clinics;

            $json['status'] = 'true';

        } else {
            $json['status'] = 'false';
        }

        break;

    case 'labsearch':
        $division = mysql_real_escape_string($_POST['division']);
        $township = mysql_real_escape_string($_POST['township']);
        $name = mysql_real_escape_string($_POST['name']);
        $phone = mysql_real_escape_string($_POST['phone']);
        $address = mysql_real_escape_string($_POST['address']);
        $lang_flag = mysql_real_escape_string($_POST['lang_flag']);
        $page_num = mysql_real_escape_string($_POST['page_num']);
        $start = ($page_num == 0) ? 0 : ($page_num - 1) * $pagesize;
        if ($division != '') {
            $wh = " and division ='" . $division . "'";
        }

        if ($township != '') {
            $wh .= " and township ='" . $township . "'";
        }
        if ($name != '' && $lang_flag == "en") {
            $wh .= " and name like '%" . $name . "%'";
        }
        if ($name != '' && $lang_flag == "my") {
            $wh .= " and name_be like '%" . $name . "%'";
        }
        if ($phone != '') {
            $wh .= " and phone like '%" . $phone . "%'";
        }
        if ($address != '' && $lang_flag == "en") {
            $wh .= " and address like '%" . $address . "%'";
        }
        if ($address != '' && $lang_flag == "my") {
            $wh .= " and address_be like '%" . $address . "%'";
        }
        $lab = array();
        $d = 0;

        /// Lab
        $lab_query = $PDO->db_query("select * from #_labs where  status=1 " . $wh . " and services !='null' order by shortorder desc limit $start,$pagesize");
        if (mysql_num_rows($lab_query) > 0) {

            while ($lab_rows = $PDO->db_fetch_array($lab_query)) {
                $lab[$d]['pid'] = $lab_rows['pid'];
                $lab[$d]['book_type'] = 'Lab';
                $lab[$d]['user_id'] = $lab_rows['user_id'];
                if ($lang_flag == "en") {
                    $lab[$d]['hospital_name'] = $lab_rows['name'];
                    $lab[$d]['address'] = $lab_rows['address'];
                    $lab[$d]['nearest_land_mark'] = $lab_rows['nearest_land_mark'];
                    $lab[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop'];
                    $lab[$d]['home_visit_coverage_area'] = $lab_rows['home_visit_coverage_area'];
                }
                if ($lang_flag == "my") {
                    $lab[$d]['hospital_name'] = $lab_rows['name_be'];
                    $lab[$d]['address'] = $lab_rows['address_be'];
                    $lab[$d]['nearest_land_mark'] = $lab_rows['nearest_land_mark_be'];
                    $lab[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop_be'];
                    $lab[$d]['home_visit_coverage_area'] = $lab_rows['home_visit_coverage_area_be'];

                }
                $lab[$d]['email'] = $lab_rows['email'];
                $lab[$d]['phone'] = $lab_rows['phone'];
                $lab[$d]['image'] = $lab_rows['image'];
                $lab[$d]['open_time'] = $lab_rows['open_time'];
                $lab[$d]['close_time'] = $lab_rows['close_time'];
                $lab[$d]['refer_fee'] = $lab_rows['refer_fee'];
                $lab[$d]['postal_code'] = $lab_rows['postal_code'];
                if ($lang_flag == "en") {
                    $lab[$d]['township'] = $PDO->getSingleResult("select name from #_township where pid='" . $lab_rows['township'] . "'");
                    $lab[$d]['division'] = $PDO->getSingleResult("select name from #_division where pid='" . $lab_rows['division'] . "'");
                }
                if ($lang_flag == "my") {
                    $lab[$d]['township'] = $PDO->getSingleResult("select name_my from #_township where pid='" . $lab_rows['township'] . "'");
                    $lab[$d]['division'] = $PDO->getSingleResult("select name_my from #_division where pid='" . $lab_rows['division'] . "'");
                }
                $lab[$d]['contact_person_name'] = $lab_rows['contact_person_name'];
                $lab[$d]['contact_person_email'] = $lab_rows['contact_person_email'];
                $lab[$d]['contact_person_phone'] = $lab_rows['contact_person_phone'];
                $lab[$d]['home_visit_service'] = $lab_rows['home_visit_service'];
                $lab[$d]['home_visit_service_charges'] = $lab_rows['home_visit_service_charges'];
                $lab[$d]['home_visit_available_start_time'] = $lab_rows['home_visit_available_start_time'];
                $lab[$d]['home_visit_available_end_time'] = $lab_rows['home_visit_available_end_time'];
                $lab[$d]['medical_checkup_plan'] = $lab_rows['medical_checkup_plan'];
                $lab[$d]['services'] = $lab_rows['services'];
                $lab[$d]['left_services'] = $lab_rows['left_services'];
                $lab[$d]['imagin_services'] = $lab_rows['imagin_services'];
                $lab[$d]['sponsored_position'] = 0;
                $d++;

            }

        }

        // Hospital

        $wh = '';
        if ($division != '') {
            $wh = " and H.division ='" . $division . "'";
        }

        if ($township != '') {
            $wh .= " and H.township ='" . $township . "'";
        }
        if ($name != '' && $lang_flag == "en") {
            $wh .= " and H.name like '%" . $name . "%'";
        }
        if ($name != '' && $lang_flag == "my") {
            $wh .= " and H.name_be like '%" . $name . "%'";
        }
        if ($phone != '') {
            $wh .= " and H.phone like '%" . $phone . "%'";
        }
        if ($address != '' && $lang_flag == "en") {
            $wh .= " and H.address like '%" . $address . "%'";
        }
        if ($address != '' && $lang_flag == "my") {
            $wh .= " and H.address_be like '%" . $address . "%'";
        }

        $lab_query = $PDO->db_query("select H.*, S.position as sponsored_position  from #_hospitals as H  LEFT JOIN #_sponsore_hospital as S ON S.honame =H.pid  where  H.status=1 " . $wh . " and H.services !='null' order by S.position desc,H.shortorder desc limit $start,$pagesize");
        if (mysql_num_rows($lab_query) > 0) {

            while ($lab_rows = $PDO->db_fetch_array($lab_query)) {
                $lab[$d]['pid'] = $lab_rows['pid'];
                $lab[$d]['book_type'] = 'Hospital';
                $lab[$d]['user_id'] = $lab_rows['user_id'];
                if ($lang_flag == "en") {
                    $lab[$d]['hospital_name'] = $lab_rows['name'];
                    $lab[$d]['address'] = $lab_rows['address'];
                    $lab[$d]['nearest_land_mark'] = $lab_rows['nearest_land_mark'];
                    $lab[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop'];
                    $lab[$d]['home_visit_coverage_area'] = $lab_rows['home_visit_coverage_area'];
                }
                if ($lang_flag == "my") {
                    $lab[$d]['hospital_name'] = $lab_rows['name_be'];
                    $lab[$d]['address'] = $lab_rows['address_be'];
                    $lab[$d]['nearest_land_mark'] = $lab_rows['nearest_land_mark_be'];
                    $lab[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop_be'];
                    $lab[$d]['home_visit_coverage_area'] = $lab_rows['home_visit_coverage_area_be'];

                }
                $lab[$d]['email'] = $lab_rows['email'];
                $lab[$d]['phone'] = $lab_rows['phone'];
                $lab[$d]['image'] = $lab_rows['image'];
                $lab[$d]['open_time'] = $lab_rows['open_time'];
                $lab[$d]['close_time'] = $lab_rows['close_time'];
                $lab[$d]['refer_fee'] = $lab_rows['refer_fee'];
                $lab[$d]['postal_code'] = $lab_rows['postal_code'];
                if ($lang_flag == "en") {
                    $lab[$d]['township'] = $PDO->getSingleResult("select name from #_township where pid='" . $lab_rows['township'] . "'");
                    $lab[$d]['division'] = $PDO->getSingleResult("select name from #_division where pid='" . $lab_rows['division'] . "'");
                }
                if ($lang_flag == "my") {
                    $lab[$d]['township'] = $PDO->getSingleResult("select name_my from #_township where pid='" . $lab_rows['township'] . "'");
                    $lab[$d]['division'] = $PDO->getSingleResult("select name_my from #_division where pid='" . $lab_rows['division'] . "'");
                }
                $lab[$d]['contact_person_name'] = $lab_rows['contact_person_name'];
                $lab[$d]['contact_person_email'] = $lab_rows['contact_person_email'];
                $lab[$d]['contact_person_phone'] = $lab_rows['contact_person_phone'];
                $lab[$d]['home_visit_service'] = $lab_rows['home_visit_service'];
                $lab[$d]['home_visit_service_charges'] = $lab_rows['home_visit_service_charges'];
                $lab[$d]['home_visit_available_start_time'] = $lab_rows['home_visit_available_start_time'];
                $lab[$d]['home_visit_available_end_time'] = $lab_rows['home_visit_available_end_time'];
                $lab[$d]['medical_checkup_plan'] = $lab_rows['medical_checkup_plan'];
                $lab[$d]['services'] = $lab_rows['services'];
                $lab[$d]['left_services'] = $lab_rows['left_services'];
                $lab[$d]['imagin_services'] = $lab_rows['imagin_services'];
                $lab[$d]['sponsored_position'] = ($lab_rows['sponsored_position'] > 0) ? $lab_rows['sponsored_position'] : 0;
                $d++;

            }

        }

        // Clinics

        // $doctor_query = $PDO->db_query("select D.*, S.position as sponsored_position  from #_doctors as D left join #_sponsored as S ON S.drname =D.pid  where  D.status=1 ".$wh." group by D.pid order by  S.position asc, D.shortorder desc ");

        $wh = '';
        if ($division != '') {
            $wh = " and C.division ='" . $division . "'";
        }

        if ($township != '') {
            $wh .= " and C.township ='" . $township . "'";
        }
        if ($name != '' && $lang_flag == "en") {
            $wh .= " and C.name like '%" . $name . "%'";
        }
        if ($name != '' && $lang_flag == "my") {
            $wh .= " and C.name_be like '%" . $name . "%'";
        }
        if ($phone != '') {
            $wh .= " and C.phone like '%" . $phone . "%'";
        }
        if ($address != '' && $lang_flag == "en") {
            $wh .= " and C.address like '%" . $address . "%'";
        }
        if ($address != '' && $lang_flag == "my") {
            $wh .= " and C.address_be like '%" . $address . "%'";
        }

        $lab_query = $PDO->db_query("select C.*, S.position as sponsored_position from #_clinics  as C  LEFT JOIN #_sponsore_clinics as S ON S.clname =C.pid  where  C.status=1 " . $wh . " and C.services !='null' order by S.position desc,C.shortorder desc limit $start,$pagesize");
        if (mysql_num_rows($lab_query) > 0) {

            while ($lab_rows = $PDO->db_fetch_array($lab_query)) {
                $lab[$d]['pid'] = $lab_rows['pid'];
                $lab[$d]['book_type'] = 'Clinics';
                $lab[$d]['user_id'] = $lab_rows['user_id'];
                if ($lang_flag == "en") {
                    $lab[$d]['hospital_name'] = $lab_rows['name'];
                    $lab[$d]['address'] = $lab_rows['address'];
                    $lab[$d]['nearest_land_mark'] = $lab_rows['nearest_land_mark'];
                    $lab[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop'];
                    $lab[$d]['home_visit_coverage_area'] = $lab_rows['home_visit_coverage_area'];
                }
                if ($lang_flag == "my") {
                    $lab[$d]['hospital_name'] = $lab_rows['name_be'];
                    $lab[$d]['address'] = $lab_rows['address_be'];
                    $lab[$d]['nearest_land_mark'] = $lab_rows['nearest_land_mark_be'];
                    $lab[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop_be'];
                    $lab[$d]['home_visit_coverage_area'] = $lab_rows['home_visit_coverage_area_be'];

                }
                $lab[$d]['email'] = $lab_rows['email'];
                $lab[$d]['phone'] = $lab_rows['phone'];
                $lab[$d]['image'] = $lab_rows['image'];
                $lab[$d]['open_time'] = $lab_rows['open_time'];
                $lab[$d]['close_time'] = $lab_rows['close_time'];
                $lab[$d]['refer_fee'] = $lab_rows['refer_fee'];
                $lab[$d]['postal_code'] = $lab_rows['postal_code'];
                if ($lang_flag == "en") {
                    $lab[$d]['township'] = $PDO->getSingleResult("select name from #_township where pid='" . $lab_rows['township'] . "'");
                    $lab[$d]['division'] = $PDO->getSingleResult("select name from #_division where pid='" . $lab_rows['division'] . "'");
                }
                if ($lang_flag == "my") {
                    $lab[$d]['township'] = $PDO->getSingleResult("select name_my from #_township where pid='" . $lab_rows['township'] . "'");
                    $lab[$d]['division'] = $PDO->getSingleResult("select name_my from #_division where pid='" . $lab_rows['division'] . "'");
                }

                $lab[$d]['contact_person_name'] = $lab_rows['contact_person_name'];
                $lab[$d]['contact_person_email'] = $lab_rows['contact_person_email'];
                $lab[$d]['contact_person_phone'] = $lab_rows['contact_person_phone'];
                $lab[$d]['home_visit_service'] = $lab_rows['home_visit_service'];
                $lab[$d]['home_visit_service_charges'] = $lab_rows['home_visit_service_charges'];
                $lab[$d]['home_visit_available_start_time'] = $lab_rows['home_visit_available_start_time'];
                $lab[$d]['home_visit_available_end_time'] = $lab_rows['home_visit_available_end_time'];
                $lab[$d]['medical_checkup_plan'] = $lab_rows['medical_checkup_plan'];
                $lab[$d]['services'] = $lab_rows['services'];
                $lab[$d]['left_services'] = $lab_rows['left_services'];
                $lab[$d]['imagin_services'] = $lab_rows['imagin_services'];
                $lab[$d]['sponsored_position'] = ($lab_rows['sponsored_position'] > 0) ? $lab_rows['sponsored_position'] : 0;
                $d++;

            }

        }

        // crm_healthcare_organization


        $wh = '';
        if ($division != '') {
            $wh = " and H.division ='" . $division . "'";
        }

        if ($township != '') {
            $wh .= " and H.township ='" . $township . "'";
        }
        if ($name != '' && $lang_flag == "en") {
            $wh .= " and H.name like '%" . $name . "%'";
        }
        if ($name != '' && $lang_flag == "my") {
            $wh .= " and H.name_be like '%" . $name . "%'";
        }
        if ($phone != '') {
            $wh .= " and H.phone like '%" . $phone . "%'";
        }
        if ($address != '' && $lang_flag == "en") {
            $wh .= " and H.address like '%" . $address . "%'";
        }
        if ($address != '' && $lang_flag == "my") {
            $wh .= " and H.address_be like '%" . $address . "%'";
        }

        $lab_query = $PDO->db_query("select H.*, S.position as sponsored_position  from #_healthcare_organization as H  LEFT JOIN #_sponsore_healthcare as S ON S.hcname =H.pid  where  H.status=1 " . $wh . " and H.services !='null' order by S.position desc, H.shortorder desc limit $start,$pagesize");
        if (mysql_num_rows($lab_query) > 0) {

            while ($lab_rows = $PDO->db_fetch_array($lab_query)) {
                $lab[$d]['pid'] = $lab_rows['pid'];
                $lab[$d]['book_type'] = 'Healthcare';
                $lab[$d]['user_id'] = $lab_rows['user_id'];
                if ($lang_flag == "en") {
                    $lab[$d]['hospital_name'] = $lab_rows['name'];
                    $lab[$d]['address'] = $lab_rows['address'];
                    $lab[$d]['nearest_land_mark'] = $lab_rows['nearest_land_mark'];
                    $lab[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop'];
                    $lab[$d]['home_visit_coverage_area'] = $lab_rows['home_visit_coverage_area'];
                }
                if ($lang_flag == "my") {
                    $lab[$d]['hospital_name'] = $lab_rows['name_be'];
                    $lab[$d]['address'] = $lab_rows['address_be'];
                    $lab[$d]['nearest_land_mark'] = $lab_rows['nearest_land_mark_be'];
                    $lab[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop_be'];
                    $lab[$d]['home_visit_coverage_area'] = $lab_rows['home_visit_coverage_area_be'];

                }
                $lab[$d]['email'] = $lab_rows['email'];
                $lab[$d]['phone'] = $lab_rows['phone'];
                $lab[$d]['image'] = $lab_rows['image'];
                $lab[$d]['open_time'] = $lab_rows['open_time'];
                $lab[$d]['close_time'] = $lab_rows['close_time'];
                $lab[$d]['refer_fee'] = $lab_rows['refer_fee'];
                $lab[$d]['postal_code'] = $lab_rows['postal_code'];

                if ($lang_flag == "en") {
                    $lab[$d]['township'] = $PDO->getSingleResult("select name from #_township where pid='" . $lab_rows['township'] . "'");
                    $lab[$d]['division'] = $PDO->getSingleResult("select name from #_division where pid='" . $lab_rows['division'] . "'");
                }
                if ($lang_flag == "my") {
                    $lab[$d]['township'] = $PDO->getSingleResult("select name_my from #_township where pid='" . $lab_rows['township'] . "'");
                    $lab[$d]['division'] = $PDO->getSingleResult("select name_my from #_division where pid='" . $lab_rows['division'] . "'");
                }

                $lab[$d]['contact_person_name'] = $lab_rows['contact_person_name'];
                $lab[$d]['contact_person_email'] = $lab_rows['contact_person_email'];
                $lab[$d]['contact_person_phone'] = $lab_rows['contact_person_phone'];
                $lab[$d]['home_visit_service'] = $lab_rows['home_visit_service'];
                $lab[$d]['home_visit_service_charges'] = $lab_rows['home_visit_service_charges'];
                $lab[$d]['home_visit_available_start_time'] = $lab_rows['home_visit_available_start_time'];
                $lab[$d]['home_visit_available_end_time'] = $lab_rows['home_visit_available_end_time'];
                $lab[$d]['medical_checkup_plan'] = $lab_rows['medical_checkup_plan'];
                $lab[$d]['services'] = $lab_rows['services'];
                $lab[$d]['left_services'] = $lab_rows['left_services'];
                $lab[$d]['imagin_services'] = $lab_rows['imagin_services'];
                $lab[$d]['sponsored_position'] = ($lab_rows['sponsored_position'] > 0) ? $lab_rows['sponsored_position'] : 0;
                $d++;

            }

        }


        if (!empty($lab)) {
            $json['lab'] = $lab;
            $json['status'] = 'true';

        } else {
            $json['lab'] = array();
            $json['status'] = 'false';

        }

        break;


    case 'healthcaresearch':

        $division = mysql_real_escape_string($_POST['division']);
        $township = mysql_real_escape_string($_POST['township']);
        $name = mysql_real_escape_string($_POST['name']);
        $phone = mysql_real_escape_string($_POST['phone']);
        $address = mysql_real_escape_string($_POST['address']);
        $lang_flag = mysql_real_escape_string($_POST['lang_flag']);
        $page_num = mysql_real_escape_string($_POST['page_num']);
        $start = ($page_num == 0) ? 0 : ($page_num - 1) * $pagesize;
        if ($division != '') {
            $wh = " and HO.division ='" . $division . "'";
        }

        if ($township != '') {
            $wh .= " and HO.township ='" . $township . "'";
        }
        if ($name != '' && $lang_flag == "en") {
            $wh .= " and HO.name like '%" . $name . "%'";
        }
        if ($name != '' && $lang_flag == "my") {
            $wh .= " and HO.name_be like '%" . $name . "%'";
        }
        if ($phone != '') {
            $wh .= " and HO.phone like '%" . $phone . "%'";
        }
        if ($address != '' && $lang_flag == "en") {
            $wh .= " and HO.address like '%" . $address . "%'";
        }
        if ($address != '' && $lang_flag == "my") {
            $wh .= " and HO.address_be like '%" . $address . "%'";
        }


        $lab_query = $PDO->db_query("select HO.*, SH.position as sponsored_position  from #_healthcare_organization As HO  left JOIN #_sponsore_healthcare AS SH ON SH.hcname =  HO.pid where  1=1 $wh  group by HO.pid order by SH.position desc limit $start,$pagesize");


        if (mysql_num_rows($lab_query) > 0) {
            $healthcare = array();
            $d = 0;
            while ($lab_rows = $PDO->db_fetch_array($lab_query)) {
                $healthcare[$d]['pid'] = $lab_rows['pid'];
                $healthcare[$d]['user_id'] = $lab_rows['user_id'];
                if ($lang_flag == "en") {
                    $healthcare[$d]['hospital_name'] = $lab_rows['name'];
                    $healthcare[$d]['address'] = $lab_rows['address'];
                    $healthcare[$d]['nearest_land_mark'] = $lab_rows['nearest_land_mark'];
                    $healthcare[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop'];
                    $healthcare[$d]['home_visit_coverage_area'] = $lab_rows['home_visit_coverage_area'];
                }
                if ($lang_flag == "my") {
                    $healthcare[$d]['hospital_name'] = $lab_rows['name_be'];
                    $healthcare[$d]['address'] = $lab_rows['address_be'];
                    $healthcare[$d]['nearest_land_mark'] = $lab_rows['nearest_land_mark_be'];
                    $healthcare[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop_be'];
                    $healthcare[$d]['home_visit_coverage_area'] = $lab_rows['home_visit_coverage_area_be'];

                }
                $healthcare[$d]['email'] = $lab_rows['email'];
                $healthcare[$d]['phone'] = $lab_rows['phone'];
                $healthcare[$d]['image'] = $lab_rows['image'];
                $healthcare[$d]['postal_code'] = $lab_rows['postal_code'];

                if ($lang_flag == "en") {
                    $healthcare[$d]['township'] = $PDO->getSingleResult("select name from #_township where pid='" . $lab_rows['township'] . "'");
                    $healthcare[$d]['division'] = $PDO->getSingleResult("select name from #_division where pid='" . $lab_rows['division'] . "'");
                }
                if ($lang_flag == "my") {
                    $healthcare[$d]['township'] = $PDO->getSingleResult("select name_my from #_township where pid='" . $lab_rows['township'] . "'");
                    $healthcare[$d]['division'] = $PDO->getSingleResult("select name_my from #_division where pid='" . $lab_rows['division'] . "'");
                }

                $healthcare[$d]['contact_person_name'] = $lab_rows['contact_person_name'];
                $healthcare[$d]['contact_person_email'] = $lab_rows['contact_person_email'];
                $healthcare[$d]['contact_person_phone'] = $lab_rows['contact_person_phone'];
                $healthcare[$d]['available_services'] = $lab_rows['available_services'];
                $healthcare[$d]['service_charges'] = $lab_rows['service_charges'];
                $healthcare[$d]['home_visit_available_start_time'] = $lab_rows['home_visit_available_start_time'];
                $healthcare[$d]['home_visit_available_end_time'] = $lab_rows['home_visit_available_end_time'];
                $healthcare[$d]['membership_plan'] = $lab_rows['membership_plan'];
                $healthcare[$d]['home_visit_service'] = $lab_rows['home_visit_service'];
                $healthcare[$d]['services'] = $lab_rows['services'];
                $healthcare[$d]['left_services'] = $lab_rows['left_services'];
                $healthcare[$d]['imagin_services'] = $lab_rows['imagin_services'];
                $healthcare[$d]['healthcare_services'] = $lab_rows['healthcare_services'];
                $healthcare[$d]['sponsored_position'] = ($lab_rows['sponsored_position'] > 0) ? $lab_rows['sponsored_position'] : 0;


                $d++;


            }
            $json['healthcare'] = $healthcare;
            $json['status'] = 'true';
        } else {
            $json['healthcare'] = array();
            $json['status'] = 'false';
        }
        break;


    case 'division':
        $lang_flag = mysql_real_escape_string($_POST['lang_flag']);

        $lab_query = $PDO->db_query("select * from #_division where  status=1 " . $wh . " order by name");
        if (mysql_num_rows($lab_query) > 0) {
            $division = array();
            $d = 0;
            while ($lab_rows = $PDO->db_fetch_array($lab_query)) {

                $division[$d]['pid'] = $lab_rows['pid'];
                if ($lang_flag == "en") {
                    $division[$d]['name'] = $lab_rows['name'];
                }
                if ($lang_flag == "my") {
                    $division[$d]['name'] = $lab_rows['name_my'];
                }
                $d++;


            }
            $json['division'] = $division;
            $json['status'] = 'true';
        } else {
            $json['division'] = array();
            $json['status'] = 'false';
        }
        break;

    case 'township':
        $division = mysql_real_escape_string($_POST['division']);
        $lang_flag = mysql_real_escape_string($_POST['lang_flag']);
        $lab_query = $PDO->db_query("select * from #_township where  status=1 and division_id ='" . $division . "' order by name ");
        if (mysql_num_rows($lab_query) > 0) {
            $township = array();
            $d = 0;
            while ($lab_rows = $PDO->db_fetch_array($lab_query)) {

                $township[$d]['pid'] = $lab_rows['pid'];
                if ($lang_flag == "en") {
                    $township[$d]['name'] = $lab_rows['name'];
                }
                if ($lang_flag == "my") {
                    $township[$d]['name'] = $lab_rows['name_my'];
                }
                $township[$d]['postal_code'] = $lab_rows['postal_code'];
                $d++;


            }
            $json['township'] = $township;
            $json['status'] = 'true';
        } else {
            $json['township'] = array();
            $json['status'] = 'false';
        }
        break;

    case 'LabTest':
        $lang_flag = mysql_real_escape_string($_POST['lang_flag']);
	$service_type=mysql_real_escape_string($_POST['service_type']);
	$book_type=mysql_real_escape_string($_POST['book_type']);
        if ($book_type == 'Hospital') {
            $lab_query = $PDO->db_query("select * from #_hospitals where pid ='" . $_POST['book_id'] . "' ");
            $lab_data = $PDO->db_fetch_array($lab_query);
            //print_r($lab_data);
        } else if ($book_type == 'Lab') {

            $lab_query = $PDO->db_query("select * from #_labs where pid ='" . $_POST['book_id'] . "' ");
            $lab_data = $PDO->db_fetch_array($lab_query);

        } else if ($book_type == 'Clinics') {

            $lab_query = $PDO->db_query("select * from #_clinics where pid ='" . $_POST['book_id'] . "' ");
            $lab_data = $PDO->db_fetch_array($lab_query);
            // print_r($lab_data);

        } else if ($book_type == 'Healthcare') {

            $lab_query = $PDO->db_query("select * from #_healthcare_organization where pid ='" . $_POST['book_id'] . "' ");
            $lab_data = $PDO->db_fetch_array($lab_query);
            // print_r($lab_data);
        }


        if ($service_type == 'Lab') {
            $left_services = (array)json_decode($lab_data['left_services'], true);
            $j = 0;
            $labtest = array();
            foreach ($left_services as $key1 => $values1) {

                $i = 0;
                $ser_date = array();
                foreach ($values1 as $key2 => $values2) {

                    if ($left_services[$key1][$key2] != "") {


                        $catquery = $PDO->db_query("select S.*,SC.name as sc_name,SC.pid as sc_pid from #_service_subcat as S JOIN #_services_cat as SC ON S.service_cat=SC.pid where S.status=1 and S.service_cat='" . $key1 . "' and S.pid='" . $key2 . "' and SC.service_type=1 group by SC.pid");
                        $catrow = $PDO->db_fetch_array($catquery);

                        if ($i == 0) {
                            $labtest[$j]['service_cat'] = $catrow['sc_name'];

                        }
                        $ser_date[$i]['pid'] = $catrow['pid'];
                        if ($lang_flag = 'en') {
                            $ser_date[$i]['name'] = $catrow['name'];
                        }
                        if ($lang_flag = 'my') {
                            $ser_date[$i]['name'] = $catrow['name_my'];
                        }
                        $i++;
                    }

                    if (!empty($ser_date)) {
                        $labtest[$j]['test'] = $ser_date;
                    }
                }

                if (!empty($ser_date)) {
                    $j++;

                }

            }
        }


        $imgtest = array();
        if ($service_type == 'Imaging') {
            $imagin_services = (array)json_decode($lab_data['imagin_services'], true);
            $j = 0;

            //print_r($imagin_services);

            foreach ($imagin_services as $key1 => $values1) {

                $i = 0;

                foreach ($values1 as $key2 => $values2) {

                    if ($imagin_services[$key1][$key2] != "") {


                        $catquery = $PDO->db_query("select S.*,SC.name as sc_name,SC.pid as sc_pid from #_service_subcat as S JOIN #_services_cat as SC ON S.service_cat=SC.pid where S.status=1 and S.service_cat='" . $key1 . "' and S.pid='" . $key2 . "' and SC.service_type=2 group by SC.pid");
                        $catrow = $PDO->db_fetch_array($catquery);

                        if ($i == 0) {
                            $labtest[$j]['service_cat'] = $catrow['sc_name'];

                        }
                        $ser_date[$i]['pid'] = $catrow['pid'];
                        if ($lang_flag = 'en') {
                            $ser_date[$i]['name'] = $catrow['name'];
                        }
                        if ($lang_flag = 'my') {
                            $ser_date[$i]['name'] = $catrow['name_my'];
                        }

                        $i++;
                    }

                }

                $imgtest[$j]['imgtest'] = $ser_date;
                if (!empty($ser_date)) {
                    $j++;
                }

            }
        }

        $json['labtest'] = $labtest;
        $json['imgtest'] = $imgtest;
        $json['status'] = 'true';
        break;


    case 'login':
        $email = mysql_real_escape_string($_POST['email']);
        $password = mysql_real_escape_string($_POST['password']);
        $password = md5($password);
        $password = base64_encode($password);
        $json['detail'] = array();
        $query = $PDO->db_query("select * from #_users_login  where email ='" . $email . "'  and password='" . $password . "' ");
        if (mysql_num_rows($query) == 1) {

            $row = $PDO->db_fetch_array($query);
            $id = $row['pid'];

            $table == '';
            if ($row['user_type'] == "clinics") {
                $table = "clinics";
            } else if ($row['user_type'] == "labs") {
                $table = "labs";
            } else if ($row['user_type'] == "hospital") {
                $table = "hospitals";
            } else if ($row['user_type'] == "healthcare_organization") {
                $table = "healthcare_organization";
            }

            if ($table != '') {
                $json['user_type'] = $row['user_type'];

                $data = $PDO->db_fetch_array($PDO->db_query("select * from #_" . $table . " where user_id='{$id}'"));


                $json['detail'] = $data;
                $json['detail']['left_services'] = json_decode($data['left_services']);
                $json['detail']['imagin_services'] = json_decode($data['imagin_services']);

                $json['status'] = 'true';


            } else {
                $json['status'] = 'false';

            }


        } else {

            $json['status'] = 'false';
        }
        break;

    default:
        break;
}


/* Output header */
//header('Content-type: application/json');
//echo json_encode($_POST);
echo json_encode($json);
?>