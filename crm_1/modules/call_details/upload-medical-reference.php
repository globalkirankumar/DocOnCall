<!-- Modal popup-->
<div id="medicalreference " class="modal medicalreference fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="append-records">
			<label>Medical Reference File  Name:</label>	
			<input type="text" name="title" style="margin-bottom:10px;" class="validate[required]" data-errormessage-value-missing="Title is required!">
			<label>Upload File:</label>
			<input type="file" name="reports" class="validate[required]" data-errormessage-value-missing="File is required!">
            <label style="color:#D20000; font-size:11px">NOTE:File type allowed("jpeg,jpg,png,bmp,gif,txt,pdf,doc and docx only") and Maximum file size 2 MB</label>
			<input type="hidden" name="call_details_id" value="<?=$uid?>">
            <input type="hidden" name="medicalreference" value="yes">
			<input type="hidden" name="create_by" value="<?=$_SESSION['AMD'][0]?>">
			<input type="hidden" name="user_type" value="<?=$_SESSION['AMD'][2]?>">
            <div style="clear:both"></div>
			<div class="right" style="float: right;margin-top: 10px;">
				<input type="submit" class="btn vd_bg-green white send_file" name="send_file" value="Upload">
			</div>	
	   </div>

    </div>
  </div>
</div>
<!--close modal popup-->