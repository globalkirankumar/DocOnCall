<?php 
include(FS_ADMIN._MODS."/sponsore_clinics/class.inc.php");


$OP = new Options();

if($BSC->is_post_back())
{
   if($uid)
   {
	   $_POST['updateid']=$uid;
       $flag = $OP->update($_POST);
  
   }else {
	 
	   $flag = $OP->add($_POST);
	   
	     
   }
   
   if($flag==1)
   {
   
     $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($division_id)?'&division_id='.$division_id:'')).$dlr, true);
   }
}
else if($uid)
{
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row" id="form-basic">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Add/Health <?=$ADMIN->compname($comp)?> </h3>
                </div>
              		
              		<div class="panel-body">
				
			<!--add-update form-->
                 <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">
                 
                	<div class="form-group">
                        <label class="control-label  col-sm-4">Division <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
						<select name="division" data-name="township" data-folder="doctor_agents" class="validate[required] cat" data-errormessage-value-missing="Division is required!">
							<option value="">Select Division</option>
							<?php $record=$PDO->db_query("select * from #_division");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						<?php } ?>
							
						</select>	
                         
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="control-label  col-sm-4">Township <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <select name="township"  id="township" class="add_records validate[required]" data-errormessage-value-missing="Township is required!"  onchange="return getTownship(this.value)" >
							<option value="">Select Township</option>
							<?php if($township!='' and $township!=0){?>
							<option value="<?php echo $township;?>" selected><?php echo $PDO->getSingleresult("select name from #_township where pid='".$township."'");?></option>
							<?php }?>
						</select>	
                        </div>
                    </div>
                    
                    
                    <div class="form-group" id="clinic">
                        <label class="control-label col-sm-4"> Clinic <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
						<select class="validate[required]" name="clname">
							<option value="">Select Clinics</option>
                            <?php if($clname!='' and $clname!=0){?>
			<option value="<?php echo $clname;?>" selected><?php echo $PDO->getSingleresult("select name from #_clinics where pid='".$clname."'");?></option>
							<?php }?>
						</select>	
                        </div>
                    </div>
                                 
                   <div class="form-group">
                      <label class="control-label  col-sm-4">Position <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-3">
                  <input class="validate[required]" data-errormessage-value-missing="Position is required!" name="position" value="<?=$position?>" type="text">
                        </div>
                    </div>
                    
                    <div class="form-group">
                         <label class="col-sm-4 control-label"> Date Form <span class="star">*</span></label>
                            <div class="col-sm-3 controls">
                              <div class="vd_input-wrapper light-theme no-icon">
                             <input type="text" name="date_from" id="date_from" value="<?=$date_from?>" class="datepicker">
                            </div>
                          </div>
                    </div>
                    
                    <div class="form-group">
                         <label class="col-sm-4 control-label"> Date To <span class="star">*</span></label>
                            <div class="col-sm-3 controls">
                              <div class="vd_input-wrapper light-theme no-icon">
                             <input type="text" name="date_to" id="date_to" value="<?=$date_to?>" class="datepicker">
                            </div>
                          </div>
                    </div>
                    
					
					<div class="form-group">
                      <label class="control-label col-sm-4 col-xs-12">Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls col-sm-6 col-xs-12">
                          <select name="status" class="validate [required]" data-errormessage-value-missing="Status is required!">
                              <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?>  >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                    </div>
                  </div>
                
                  
                  <div class="form-group form-actions">
                    <div class="col-sm-6"></div>
                    <div class="col-md-6">
					 <input type="hidden" name="pid" value="<?=$pid?>" />
                       <button id="submit-register" name="submit-register" class="btn vd_btn vd_bg-green vd_white uibutton loading" type="submit"><i class="icon-ok" ></i> Save</button>
                          <button onclick="location.reload();" class="btn vd_btn" type="button">Cancel</button>
                    </div>
                  </div>
             <!--   </form>-->
			<!-- close add-update form-->
              		</div>
              </div>
            </div>
		</div>
<script src="<?=SITE_PATH?>validation/js/jquery-1.8.2.min.js"></script>
<script src="<?=SITE_PATH?>validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=SITE_PATH?>validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#formID").validationEngine();
		});
		$('.hide_show').change(function(){
			var id = $(this).attr("data-id");
			if($(this).val()=="Available")
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
		$('.services').click(function(){
			var id = $(this).attr("data-id");
			if(this.checked)
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
	
</script>

<script>
function getTownship(val) {
	$.ajax({
	type: "POST",
	url: "<?=SITE_PATH_ADM?>modules/sponsore_clinics/getClinic.php",
	data:'pid='+val,
	success: function(data){
		//salert(data);		
	$("#clinic").html(data);
	}
	});
}
</script>