<?php

$patient_query=$PDO->db_query("select * from #_patients where pid ='".$_SESSION['patient_id']."'  "); 
$patient_data = $PDO->db_fetch_array($patient_query);  

$caller_query=$PDO->db_query("select * from #_call_details where pid ='".$_SESSION['call_id']."'  "); 
$caller_data = $PDO->db_fetch_array($caller_query);  
?>
<div class="row" id="form-basic">
              <div class="col-md-12">
               <div class="panel widget">
                  <div class="panel-body-list">
                   
                      <div id="wizard-3" class="form-wizard condensed">
                        <div class="tab-content no-bd pd-25">
                          <div class="tab-pane active" id="tab31">
                            
                            <!---user information-->
							<div class="paneledit widget">
							  <div class="panel-heading vd_bg-green">
								<h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-info-circle"></i> </span>Patient / Caller Information</h3>
								<div class="vd_panel-menu">
									<div data-original-title="Config" data-toggle="tooltip" data-placement="bottom" class="nav-medium-button menu entypo-icon smaller-font">
									</div>
								</div>
							  </div>
							  <div class="panel-body2">
								<div class="row mgbt-xs-0">
								  <div class="col-md-4 col-sm-4 col-xs-12">
									<ul class="information">
									<li><span>Patient ID:</span><?=$patient_data['patient_id']?></li>
									<li><span>Age:</span><?=$patient_data['age']?></li>
									</ul>
								  </div>
								  <div class="col-md-4 col-sm-4 col-xs-12">
									<ul class="information">
									<li><span>Name:</span><?=$patient_data['name']?></li>
									<li><span>Sex:</span><?=$patient_data['sex']?></li>
									</ul>
								  </div>
								  <div class="col-md-4 col-sm-4 col-xs-12">
									<ul class="information">
									<li><span>Phone No:</span><?=$patient_data['phone']?></li>
									<li><span>Township:</span><?=$PDO->getSingleresult("select name from #_township where pid='".$patient_data['township']."' ")?></li>
									</ul>
								  </div>
								</div>
							  </div>
							</div>  
							<!---Close user information-->
							
					
					       <div class="panel-group" id="accordion">
                        
						   <div class="panel panel-default" id="panel1">
							<div class="custom-heading">Advise</div>
							<div id="collapseOne" class="panel-collapse collapse in">
								<div class="panel-body" id="#collapse-1">
									 <textarea class="comment-box" placeholder="Advise" name="health_consultation"><?=$health_consultation?></textarea>
								</div>
							</div>
						</div>
                        
                      
						
	               </div>
                    
                
                    
                    
                    
                    
                     
                            
                          
                         
                      </div>
                  
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>
            <!-- row -->
          
            </div>
            
            
          </div>

<div>
		  	<div class="row">
              
                <div class="col-sm-12" > 
                           <a class="btn nextbutton  next greenbutton" href="javascript:health_saveappointment()" >Save  and Book Appointment<span class="menu-icon"><i class="fa fa-fw fa-chevron-circle-right"></i></span></a> 
             
				           <a class="btn nextbutton  next pull-right greenbutton" href="javascript:health_consultation()" onclick="$('#formID' ).submit();">Save  and Finish<span class="menu-icon"><i class="fa fa-fw fa-chevron-circle-right"></i></span></a> 
			</div>
            </div>
		</div>
       
<script>
	   
function health_saveappointment()
{

var str = $("#formID").serialize(); 
$.ajax({type: "POST",
	 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=service_provided&service_provided=Specialist",
	 data: str,
	 cache: false,
	 success: function(html){ 
	 
		$('#PatientDiv').html(html);	
	 }
});
}
 </script>