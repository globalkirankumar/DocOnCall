<?php

/*
# @package BSC CMS
# Copy right code 
# The base configurations of the BSC CMS.
# manage admin user add update and delete
# Developer by jugal kishore kiroriwal

*/

class Users extends dbc
{

    public function update($data)
    {
        @extract($data);


        $tblName = 'doctors_locations';
        // pid is a user id  And Update id is  pid of module table
        $query = parent::db_query("select * from #_" . $tblName . " where pid='" . $updateid . "' ");

        $querydata = mysql_fetch_array($query);
        $is_verified_from_db = parent::getSingleresult("select is_verified from #_doctors_locations where pid='" . $updateid . "'");

        if ($querydata['pid'] == $pid) {
            $data['modified_on'] = @date('Y-m-d H:i:s');
            parent::sqlquery("rs", $tblName, $data, 'pid', $updateid);
            parent::sessset('Record has been updated', 's');
            $flag = 1;

            //send Mail to user
            // $email = parent::getSingleresult("select email from #_doctors where pid='{$updateid}'");
            $email = $data['email'];
            $name = $data['name'];
            $doctor_name = $name;
            $reply_email = 'noreply@doctoroncall.com.in';
            $reply_name = 'noreplydoc';
            $from_email = 'admin@doctoroncall.com.in';
            $from_name = 'Doctor On Call';
            $to_email = $email;
            $to_name = $doctor_name;

            if ($is_verified_from_db == 0) {
                if ($data['is_verified'] == 1) {
                    $subject = 'Location Activated';
                    $message = "Hi $doctor_name <br>Thanks For Joining in Doctor On Call.<br>Your location is activated now.";
                    $hospital_id_from_db = parent::getSingleresult("select hospital_id from #_doctors_locations where pid='" . $updateid . "'");
                    $clinic_id_from_db = parent::getSingleresult("select clinic_id from #_doctors_locations where pid='" . $updateid . "'");
                    $latlong_from_db = parent::getSingleresult("select latlong from #_doctors_locations where pid='" . $updateid . "'");
                    $available_from_db = parent::getSingleresult("select available_from from #_doctors_locations where pid='" . $updateid . "'");
                    $available_to_db = parent::getSingleresult("select available_to from #_doctors_locations where pid='" . $updateid . "'");
                    $available_status_from_db = parent::getSingleresult("select available_status from #_doctors_locations where pid='" . $updateid . "'");

                    $today_date = date('Y-m-d');
                    $date = date_create($today_date);
                    date_add($date, date_interval_create_from_date_string('365 days'));
                    $plus_one_year_date = date_format($date, 'Y-m-d');


                    /*if (($available_from_db == '0000-00-00') || ($available_to_db == '0000-00-00') || ($available_to_db == NULL) || ($available_from_db == NULL) || ($available_to_db == '') || ($available_from_db == '')) {
                        parent::db_query("update  #_doctors_locations set available_from='" . $today_date . "' where pid='" . $updateid . "' ");
                        parent::db_query("update  #_doctors_locations set available_to='" . $plus_one_year_date . "' where pid='" . $updateid . "' ");
                        parent::db_query("update  #_doctors_locations set available_status=1 where pid='" . $updateid . "' ");
                    }
                    */
                    if (($available_from_db == '0000-00-00') || ($available_to_db == '0000-00-00') || ($available_to_db == NULL) || ($available_from_db == NULL) || ($available_to_db == '') || ($available_from_db == '')) {
                        parent::db_query("update  #_doctors_locations set available_from='0000-00-00' where pid='" . $updateid . "' ");
                        parent::db_query("update  #_doctors_locations set available_to='0000-00-00' where pid='" . $updateid . "' ");
                        parent::db_query("update  #_doctors_locations set available_status=1 where pid='" . $updateid . "' ");
                    }
                   /*
				   if ($hospital_id_from_db != 0) {
                        parent::db_query("update  #_hospitals set latlong='" . $latlong_from_db . "' where pid in ($hospital_id_from_db)");
                    }
                    if ($clinic_id_from_db != 0) {
                        parent::db_query("update  #_clinics set latlong='" . $latlong_from_db . "' where pid in ($clinic_id_from_db)");
                    }
					*/

                    $message_content = $message;

                    //include(FS_ADMIN . "lib/open.inc.php");
                    include(FS_ADMIN . "app/class/all_class_files.php");
                    $m = new MyMail ();
                    //$doctor_function = new doctor ();
                    /*$hospital_name_list = $doctor_function->hospital_name_list('en');
                    print_r($hospital_name_list);*/
                    $email_details = array(
                        'from_email' => 'doctor@doctoroncall.com.mm',
                        'from_name' => 'Doctor CRM',
                        'to_email' => $to_email,
                        'to_name' => $from_name,
                        'subject' => $subject,
                        'message_content' => $message_content
                    );
                    $send_user_Email = $m->sendMail($email_details);


                } else {
                    $subject = 'Locations Deactivated';
                    $message = "Your location is deactivated now.Please contact admin";

                    $message_content = $message;

                    //include(FS_ADMIN . "lib/open.inc.php");
                    include(FS_ADMIN . "app/class/all_class_files.php");
                    $m = new MyMail ();
                    //$doctor_function = new doctor ();
                    /*$hospital_name_list = $doctor_function->hospital_name_list('en');
                    print_r($hospital_name_list);*/
                    $email_details = array(
                        'from_email' => 'doctor@doctoroncall.com.mm',
                        'from_name' => 'Doctor CRM',
                        'to_email' => $to_email,
                        'to_name' => $from_name,
                        'subject' => $subject,
                        'message_content' => $message_content
                    );
                    $send_user_Email = $m->sendMail($email_details);

                }


            }


        } else {
            parent::sessset('location is not available', 'e');
            $flag = 0;
        }

        return $flag;

    }


    public function status($updateid, $status)
    {
        $tblName = 'doctors_locations';
        $updateidarrr = $updateid;
        if (is_array($updateid)) {
            $updateid = implode(',', $updateid);

        }

        parent::db_query("update  #_" . $tblName . " set status='" . $status . "' where pid in ($updateid)");
        //send Mail to user
        foreach ($updateidarrr as $key => $value) {
            // $arr[3] will be updated with each value from $arr...
            $doctor_id = parent::getSingleresult("select doctor_id from #_doctors_locations where pid='{$value}'");
            $email = parent::getSingleresult("select email from #_doctors where pid='{$doctor_id}'");
            //   $email=$data['email'];
            $name = parent::getSingleresult("select name from #_doctors where pid='{$doctor_id}'");

            $doctor_name = $name;
            $reply_email = 'noreply@doc.com';
            $reply_name = 'noreplydoc';
            $from_email = 'admin@doctoroncall.com';
            $from_name = 'Doctor On Call';
            $to_email = $email;
            $to_name = $doctor_name;
            if ($status == 1) {
                $subject = 'Account Activated';
                $message = "Thanks For Joining in Doctor On Call.<br>Your Registeration is completed.Your account is activated now.";

            } else {
                $subject = 'Account Deactivated';
                $message = "Your account is deactivated now.Please contact admin";

            }
            $body = $message;

            $message_content = $message;

            //include(FS_ADMIN . "lib/open.inc.php");
            include(FS_ADMIN . "app/class/all_class_files.php");
            $m = new MyMail ();
            //$doctor_function = new doctor ();
            /*$hospital_name_list = $doctor_function->hospital_name_list('en');
            print_r($hospital_name_list);*/
            $email_details = array(
                'from_email' => 'doctor@doctoroncall.com.mm',
                'from_name' => 'Doctor CRM',
                'to_email' => $to_email,
                'to_name' => $from_name,
                'subject' => $subject,
                'message_content' => $message_content
            );
            $send_user_Email = $m->sendMail($email_details);
        }


    }


    public function display($start, $pagesize, $fld, $otype, $search_data, $did)
    {
        $tblName = 'doctors_locations';
        $start = intval($start);
        $columns = "select * ";

        if (trim($search_data) != '') {
            $wh = " and (username like '%" . parent::parse_input($search_data) . "%' or email like '%" . parent::parse_input($search_data) . "%' or user_type like '%" . parent::parse_input($search_data) . "%') ";
        }

        $sql = " from #_" . $tblName . " where status=1 AND isDeleted=0 AND doctor_id='$did' " . $zone . $mtype . $extra . $extra1 . $extra2 . $wh;
        $order_by == '' ? $order_by = (($ord) ? 'orders' : (($fld) ? $fld : 'pid')) : true;
        $order_by2 == '' ? $order_by2 = (($otype) ? $otype : 'DESC') : true;
        $sql_count = "select count(*) " . $sql;
        $sql .= "order by $order_by $order_by2 ";
        $sql .= "limit $start, $pagesize ";
        $sql = $columns . $sql;

        $result = parent::db_query($sql);
        $reccnt = parent::db_scalar($sql_count);
        return array($result, $reccnt);
    }


    public function password($password)
    {
        $password = md5($password);
        $password = base64_encode($password);
        return $password;
    }

    public function today_weekday_num_find()
    {

        $today_date = date("Y-m-d");
        $current_time = date("H:i:s");
        $today_weekday = date("l");
        $weekday = 0;
        if ($today_weekday == 'Monday') {
            $weekday = 1;
        }
        if ($today_weekday == 'Tuesday') {
            $weekday = 2;
        }
        if ($today_weekday == 'Wednesday') {
            $weekday = 3;
        }
        if ($today_weekday == 'Thursday') {
            $weekday = 4;
        }
        if ($today_weekday == 'Friday') {
            $weekday = 5;
        }
        if ($today_weekday == 'Saturday') {
            $weekday = 6;
        }
        if ($today_weekday == 'Sunday') {
            $weekday = 7;
        }
        return $weekday;


    }


    public function check_available_unavailable($doctor_id, $location_id)
    {

        if (!empty($location_id) && !empty($doctor_id)) {
            // date_default_timezone_set("Asia/Karachi");
            date_default_timezone_set("Asia/Yangon");
            $today_date = date("Y-m-d");
            $current_time = date("H:i:s");
            $today_weekday = date("l");
            $weekday = self::today_weekday_num_find();

            //check whether the doctor is free or busy
            $query_busy_free = parent::db_query("select * from #_doctors_locations  where `pid`='" . $location_id . "' and doctor_id= '" . $doctor_id . "' and busy_free=1 ");
            if (mysql_num_rows($query_busy_free) > 0) {

                //check whether the location is available
                $query = parent::db_query("select * from #_doctors_locations  where `pid`='" . $location_id . "' and doctor_id= '" . $doctor_id . "' ");
                if (mysql_num_rows($query) > 0) {
                    $available_from_from_db = parent::getSingleResult("select available_from from #_doctors_locations where pid='" . $location_id . "'");
                    $available_to_from_db = parent::getSingleResult("select available_to from #_doctors_locations where pid='" . $location_id . "'");
                    if (($available_from_from_db != NULL) || ($available_to_from_db != NULL)) {
                        //check given time is available in time slot
                        $sql = "SELECT * FROM `#_doctors_locations` WHERE pid='" . $location_id . "' AND doctor_id='" . $doctor_id . "' AND(available_from <= '" . $today_date . "' AND available_to >= '" . $today_date . "') and available_status=0 and status=1 ";
                        $query_2 = parent::db_query($sql);
                        if (mysql_num_rows($query_2) == 0) {

                            //check current date is inbetween the availbele date
                            $sql_2 = "SELECT * FROM `#_doctors_locations_time_slots` WHERE location_id='" . $location_id . "' AND weekdays='" . $weekday . "' AND(start_time <= '" . $current_time . "' AND end_time >= '" . $current_time . "') and available_status=1 and status=1 ";

                            $query_3 = parent::db_query($sql_2);
                            if (mysql_num_rows($query_3) > 0) {

                                $data['msg'] = 'Doctor is available';
                                $data['status'] = 'true';

                            } else {
                                $data['msg'] = 'Doctor is not available in this location now';
                                $data['status'] = 'false';
                            }
                        } else {
                            $data['msg'] = 'Doctor is not available in this location on today';
                            $data['status'] = 'false';
                        }
                    } else {

                        //check current date is inbetween the availbele date
                        $sql_2 = "SELECT * FROM `#_doctors_locations_time_slots` WHERE location_id='" . $location_id . "' AND weekdays='" . $weekday . "' AND(start_time <= '" . $current_time . "' AND end_time >= '" . $current_time . "') and  status=1 ";

                        $query_3 = parent::db_query($sql_2);
                        if (mysql_num_rows($query_3) > 0) {

                            $data['msg'] = 'Doctor is available';
                            $data['status'] = 'true';

                        } else {
                            $data['msg'] = 'Doctor is not available in this location now';
                            $data['status'] = 'false';
                        }

                    }


                } else {
                    $data['msg'] = 'Doctor id and location id not matched';
                    $data['status'] = 'false';

                }


            } else {
                $data['msg'] = 'Doctor is busy now';
                $data['status'] = 'false';
            }

        } else {
            $data['msg'] = 'Doctor id and location id are empty';
            $data['status'] = 'false';
        }

        return $data;


    }


}


?>