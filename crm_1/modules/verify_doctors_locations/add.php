<?php
include(FS_ADMIN . _MODS . "/verify_doctors_locations/user.inc.php");


$US = new Users();

if ($BSC->is_post_back()) {

    if ($uid) {
        $_POST['updateid'] = $uid;
        $flag = $US->update($_POST);

    }
    if ($flag == 1) {


        $BSC->redir($ADMIN->iurl('verify_doctors_locations&did='.$_POST['did'].''. (($start) ? '&start=' . $start : '') . (($subpage_id) ? '&subpage_id=' . $subpage_id : '') . (($alumniid) ? '&alumniid=' . $alumniid : '') . (($galleryid) ? '&galleryid=' . $galleryid : '')) . $dlr, true);
    }
}


if ($uid) {
    $tblName = 'doctors_locations';
    $query = $PDO->db_query("select * from #_" . $tblName . " where pid ='" . $uid . "' ");
    $row = $PDO->db_fetch_array($query);
    @extract($row);

}

?>
<style>
    .controls-button {display:none;}
</style>
<div class="vd_content-section clearfix">
    <div class="row" id="form-basic">
        <?= $ADMIN->alert() ?>
        <div class="panel-heading vd_bg-grey">
            <h3 class="panel-title"><span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                Verify/Doctors Locations <?php $ADMIN->compname($comp) ?> </h3>
        </div>

        <div class="panel-body">


            <!--add-update form-->
            <!--    <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">-->
            <?php
            $email = $PDO->getSingleresult("select email from #_doctors where pid='{$doctor_id}'");
            $name = $PDO->getSingleresult("select name from #_doctors where pid='{$doctor_id}'");
            if ($hospital_id != 0) {
                $hospital_name = $PDO->getSingleresult("select name from #_hospitals where pid='{$hospital_id}'");
            } else {
                $hospital_name = $PDO->getSingleresult("select name from #_clinics where pid='{$clinic_id}'");
            }
            $location_name = explode('~', $location);
            $location_name_full = $location_name[0] . '<br>' . $location_name[1];
            ?>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label  col-sm-2">Name <span class="vd_red">*</span></label>
                    <div id="first-name-input-wrapper" class="controls col-sm-8">
                        <input class="validate[required]" data-errormessage-value-missing="Name is required!"
                               name="name" id="name" value="<?= $name ?>" type="text" readonly>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label  col-sm-2">Email <span class="vd_red">*</span></label>
                    <div id="first-name-input-wrapper" class="controls col-sm-8">
                        <input class="validate[required]" data-errormessage-value-missing="Email is required!"
                               name="email" id="email" value="<?= $email ?>" type="text" readonly>
                    </div>
                </div>
            </div>

           <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label  col-sm-2">Hospital/Clinic <span class="vd_red">*</span></label>
                    <div id="first-name-input-wrapper" class="controls col-sm-8">
                        <input class="validate[required]" data-errormessage-value-missing="Hospital/Clinic is required!"
                               name="hospital_name" id="hospital_name" value="<?= $hospital_name ?>" type="text" readonly>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label  col-sm-2">Location 1 <span class="vd_red">*</span></label>
                    <div id="first-name-input-wrapper" class="controls col-sm-8">
                        <input class="validate[required]" data-errormessage-value-missing="Email is required!"
                               name="location_name" id="location_name" value="<?= $location_name[0] ?>" type="text" readonly>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label  col-sm-2">Location 2 <span class="vd_red">*</span></label>
                    <div id="first-name-input-wrapper" class="controls col-sm-8">
                        <input class="validate[required]" data-errormessage-value-missing="Email is required!"
                               name="location1" id="email" value="<?= $location_name[1] ?>" type="text" readonly>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label col-sm-2 col-xs-12">Verification Status <span class="vd_red">*</span></label>
                    <div id="website-input-wrapper" class="controls col-sm-4 col-xs-12">

                        <select name="is_verified" class="validate[required]" <?= ($is_verified == 1) ? 'disabled' : '' ?>
                                data-errormessage-value-missing="Status is required!" >
                            <option value="">-------Select Verification Status------</option>
                            <option value="1" <?= ($is_verified == 1) ? 'selected="selected"' : '' ?> >Done</option>
                            <option value="0" <?= (isset($is_verified) && $is_verified == 0) ? 'selected="selected"' : '' ?>>
                                Not Done
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label col-sm-2 col-xs-12">Status <span class="vd_red">*</span></label>
                    <div id="website-input-wrapper" class="controls col-sm-4 col-xs-12">
                        <select name="status" class="validate[required]"
                                data-errormessage-value-missing="Status is required!">
                            <option value="">-------Select Status------</option>
                            <option value="1" <?= ($status == 1) ? 'selected="selected"' : '' ?> >Active</option>
                            <option value="0" <?= (isset($status) && $status == 0) ? 'selected="selected"' : '' ?>>
                                Inactive
                            </option>
                        </select>
                    </div>
                </div> 
            </div>

            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-md-6 mgbt-xs-10 mgtp-20">


                    <div class="mgtp-10">
                        <input type="hidden" name="pid" value="<?= $pid ?>"/>
                        <input type="hidden" name="did" value="<?= $doctor_id ?>"/>
                        <button class="btn vd_bg-green vd_white greenbutton uibutton loading" type="submit"
                                id="submit-register" name="submit-register">Submit
                        </button>
                    </div>
                </div>
                <div class="col-md-12 mgbt-xs-5"></div>
            </div>
            <!--   </form>-->
            <!-- close add-update form-->
        </div>
    </div>
</div>
</div>
