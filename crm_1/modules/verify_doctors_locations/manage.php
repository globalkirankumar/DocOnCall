<?php
//include(FS_ADMIN ."app/class/doctor.php");
include(FS_ADMIN . _MODS . "/verify_doctors_locations/user.inc.php");
$US = new Users();
$did = $_GET['did'];

if ($action) {
    if ($uid > 0 || !empty($arr_ids)) {

        switch ($action) {

            case "Active":
                $US->status($arr_ids, 1);
                $ADMIN->sessset(count($arr_ids) . ' Item(s) Active', 's');

                break;

            case "Inactive":
                $US->status($arr_ids, 0);
                $ADMIN->sessset(count($arr_ids) . ' Item(s) Inactive', 's');
                break;


            default:
        }

        $BSC->redir($ADMIN->iurl('verify_doctors_locations') . '&did=' . $did, true);
    }
}

$start = intval($start);
$pagesize = intval($pagesize) == 0 ? (($_SESSION["totpaging"]) ? $_SESSION["totpaging"] : DEF_PAGE_SIZE) : $pagesize;

list($result, $reccnt) = $US->display($start, $pagesize, $fld, $otype, $search_data, $did);

?>
<style>
    .controls-button li:first-child {
        display: none;
    }

    .controls-button li:last-child {
        display: none;
    }
</style>
<!--right section panel-->
<div class="vd_content-section clearfix">
    <div class="row">

        <div class="col-md-12">
            <?= $ADMIN->alert() ?>
            <div class="panel-heading vd_bg-green white">
                <h3 class="panel-title">Doctors/Locations </h3>
            </div>
            <div class="section-body">


                <!--edit table-->
                <div class="table-responsive " id="ordrz">
                    <table class="table data-tbl custom-style table-striped">
                        <?php if (!empty($reccnt)) { ?>
                            <thead>
                            <tr class="tbl-head">
                                <th><?= $ADMIN->check_all() ?></th>
                                <th>No.</th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>

                                <th>Hospital/Clinics</th>
                                <th>Location</th>
                                <th>Available Satus</th>
                                <th>Free/Busy</th>
                                <th>Verification Status</th>
                                <th>
                                    <a href="<?= $ADMIN->iurl($comp) ?>&did=<?= $did ?>&fld=status<?= (($otype == 'asc') ? "&otype=desc" : '&otype=asc') ?>" <?= (($fld == 'status') ? 'class="selectedTab"' : '') ?>><span <?= (($otype == 'asc') ? 'class="des"' : 'class="asc"') ?>>Status</span></a>
                                </th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        <?php } ?>
                        <!--</table>
                             <ul class="sortable-liststyle" style="list-style:none;">
                  -->
                        <tbody>
                        <?php if ($reccnt) {

                            $nums = (($start) ? $start + 1 : 1);
                            $k = 0;
                            while ($line = $PDO->db_fetch_array($result)) {
                                @extract($line);
                                $k++;
                                $css = ($k % 2 != 0) ? 'success' : '';
                                $email = $PDO->getSingleresult("select email from #_doctors where pid='{$doctor_id}'");
                                $name = $PDO->getSingleresult("select name from #_doctors where pid='{$doctor_id}'");
                                if ($hospital_id != 0) {
                                    $hospital_name = $PDO->getSingleresult("select name from #_hospitals where pid='{$hospital_id}'");
                                } else {
                                    $hospital_name = $PDO->getSingleresult("select name from #_clinics where pid='{$clinic_id}'");
                                }
                                $location_name = explode('~', $location);
                                $location_name_full = $location_name[0] . '<br>' . $location_name[1];
								
								$available_unavailable_details = $US->check_available_unavailable($doctor_id, $pid);
								if ($available_unavailable_details['status'] != 'false') {
									$current_available_status = 1;							

								} else {
									$current_available_status = 0;
								}
								
								
								
                                ?>
                                <!--<li style="list-style:none;" id="recordsArray_<?= $pid ?>">
			<table class="table data-tbl custom-style">
			 <thead>-->
                                <tr data-item-id=1 class="item <?= $css ?>">
                                    <th><?= $ADMIN->check_input($pid) ?></th>
                                    <th><?= $nums ?></th>
                                    <th><?= ucwords($name) ?></th>
                                    <th><?= $email ?></th>
                                    <th><?= $hospital_name ?></th>
                                    <th><?= $location_name_full ?></th>
                                    <th><?= ($current_available_status) ? 'Available' : 'Not Available' ?></th>
                                    <th><?= ($busy_free) ? 'Free' : 'Busy' ?></th>
                                    <th><?= ($is_verified == 1) ? 'Done' : 'Not Done' ?></th>
                                    <th><?= $ADMIN->displaystatus($status) ?></th>
                                    <th><?= $ADMIN->verify_action($comp, $pid) ?></th>
                                </tr>
                                <!-- </thead>
                                </table>
                                </li>-->
                                <?php $nums++;
                            } ?>


                        <?php } else {
                            echo '<div align="center" class="norecord">No Record Found</div>';
                        } ?>

                        <!-- </ul>-->
                        </tbody>
                    </table>

                    <?php include("cuts/paging.inc.php"); ?>
                    <div class="pull-right pagination" style="display:none;">
                        <ul class="pagination">
                            <li class="page-pre"><a href="#">‹</a></li>
                            <li class="page-number active"><a href="#">1</a></li>
                            <li class="page-number"><a href="#">2</a></li>
                            <li class="page-number"><a href="#">3</a></li>
                            <li class="page-number"><a href="#">4</a></li>
                            <li class="page-number"><a href="#">5</a></li>
                            <li class="page-last-separator disabled"><a href="#">...</a></li>
                            <li class="page-last"><a href="#">80</a></li>
                            <li class="page-next"><a href="#">›</a></li>
                        </ul>
                    </div>
                </div>
                <!-- close edit table-->
            </div>

            <!--next button-->

            <!--Close next button-->
        </div>
    </div>
</div>
<!--Close right section panel-->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
    jQuery(document).ready(function () {
        jQuery(function () {
            jQuery("#ordrz ul").sortable({
                opacity: 0.6, cursor: 'move', update: function () {
                    var order = jQuery(this).sortable("serialize") + '&tbl=<?=tblName?>&field=pid';
                    $.post("<?=SITE_PATH_ADM?>modules/orders.php", order, function (theResponse) {
                    });
                }
            });
        });
    });
</script>