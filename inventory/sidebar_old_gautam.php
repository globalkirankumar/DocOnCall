				<div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left  ">
				<div class="navbar-menu clearfix">
				<h3 class="menu-title hide-nav-medium hide-nav-small">INVENTORY</h3>
				<div class="vd_menu">
				<ul>
				
				<li>
				<a href="welcome.php" data-action="">
				<span class="menu-icon"><i class="fa fa-home"></i></span>
				<span class="menu-text">HOME</span>
				</a>
				</li>

				<?php if($_SESSION['user_type']=='admin'){?>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Category</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				<li>
				<a href="category-addf.php">
				<span class="menu-text">Add Category</span>
				</a>
				</li>
				<li>
				<a href="category-list.php">
				<span class="menu-text">Category List</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
                
                <li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Brand</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="brand-addf.php">
				<span class="menu-text">Add Brand</span>
				</a>
				</li>
				<li>
				<a href="brand-list.php">
				<span class="menu-text">Brand List</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Generic Name</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="genericname-addf.php">
				<span class="menu-text">Add Generic Name</span>
				</a>
				</li>
				<li>
				<a href="genericname-list.php">
				<span class="menu-text">Generic Name List</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Form Of Medicine</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="formofmadic-addf.php">
				<span class="menu-text">Add Form Of Medicine </span>
				</a>
				</li>
				<li>
				<a href="formofmadic-list.php">
				<span class="menu-text">Form Of Medicine List</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Strength Of Medic</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				<li>
				<a href="strengthofmdic-addf.php">
				<span class="menu-text">Add Strength of Medicine </span>
				</a>
				</li>
				<li>
				<a href="strengthofmdic-list.php">
				<span class="menu-text">Strength of Medicine List</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Usage category</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				<li>
				<a href="usage-addf.php">
				<span class="menu-text">Add Usage Category </span>
				</a>
				</li>
				<li>
				<a href="usage-list.php">
				<span class="menu-text">Usage Category List</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Subscription Plan</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				<li>
				<a href="subscription-addf.php">
				<span class="menu-text">Add New Subscription </span>
				</a>
				</li>
				<li>
				<a href="subscription-list.php">
				<span class="menu-text">Subscription Plan View</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Services</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				<li>
				<a href="service-addf.php">
				<span class="menu-text">Add New Service</span>
				</a>
				</li>
				<li>
				<a href="service-list.php">
				<span class="menu-text">Services List View</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				
				
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Store</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="store-addf.php">
				<span class="menu-text">Add Store</span>
				</a>
				</li>
				<li>
				<a href="store-list.php">
				<span class="menu-text">Store List</span>
				</a>
				</li>
				</ul>
				</div>
				</li>

				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Staff</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="staff-addf.php">
				<span class="menu-text">Add Staff</span>
				</a>
				</li>
				<li>
				<a href="staff-list.php">
				<span class="menu-text">Staff List</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Buyer Inventory</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="inventory-addf.php">
				<span class="menu-text">Add Inventory</span>
				</a>
				</li>
				<li>
				<a href="inventory-list.php">
				<span class="menu-text">Inventory List</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Assign Inventory</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="select-store.php">
				<span class="menu-text">Assign to store</span>
				</a>
				</li>
				
				</ul>
				</div>
				</li>
                
                <li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Reports</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="current_inv_stores.php">
				<span class="menu-text">Current Inventory of Stores</span>
				</a>
				</li>
				
				</ul>
				</div>
				</li>
                
                <li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Low Stock Reports</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="low-stock.php">
				<span class="menu-text">My Low Stock Reports</span>
				</a>
				</li>
				
				<li>
				<a href="stores-low-stock.php">
				<span class="menu-text">Store Low Stock Reports</span>
				</a>
				</li>
				
				
				</ul>
				</div>
				</li>
                
				<?php }else{ ?>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Inventory Received</span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="store-inventory-new-list.php">
				<span class="menu-text">Store of Inventory</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text"> Sale</span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="sale-addf.php">
				<span class="menu-text">Medicine Sale</span>
				</a>
				</li>
                <li>
				<a href="subscription-sale.php">
				<span class="menu-text">Subscription Sale</span>
				</a>
				</li>
                <li>
				<a href="service-sale.php">
				<span class="menu-text">Services Sale</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Current Inventory</span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="current-sale-inventroy-store.php">
				<span class="menu-text">Current Inventory </span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
                <li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Subscription Plan</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				
				<li>
				<a href="subscription-list.php">
				<span class="menu-text">Subscription Plan View</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Services</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				
				<li>
				<a href="service-list.php">
				<span class="menu-text">Services List View</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Sale Report</span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="view-store-sale.php">
				<span class="menu-text">Medicine Report </span>
				</a>
				</li>
				<li>
				<a href="subscription-report.php">
				<span class="menu-text">Subscription Report </span>
				</a>
				</li>
				<li>
				<a href="service-report.php">
				<span class="menu-text">Service Report </span>
				</a>
				</li>
				</ul>
				</div>
				</li>
                
                <li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Low Stock Reports</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="store-low-stock.php">
				<span class="menu-text">Low Stock Reports</span>
				</a>
				</li>
				
				</ul>
				</div>
				</li>

				<?php } ?>

				</ul>
				</div>
				</div>
				<div class="navbar-spacing clearfix">
				</div>
				</div>