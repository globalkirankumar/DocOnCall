<?php 

	include("include/config.php");
	include("include/functions.php");

	if($_SESSION['sess_username']==''){
		header("Location:".SITE_URL);	
	}
?>

<!DOCTYPE html>

<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->

<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->

<!--[if gt IE 9]><!-->

<html>

<!--<![endif]-->

<head>
<meta charset="utf-8" />
<title>Inventory</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>

<!-- Header Ends --> 

<!---------sidebar---->

<div class="content">
  <div class="container">
    <?php include("sidebar.php");?>
    <!-- Middle Content Start -->
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-header">
                <ul class="breadcrumb">
                  <li><a href="welcome.php">Home</a> </li>
                  <li><a href="services-sold-for-store.php">Services Sold</a> </li>
                </ul>
              </div>
             
              <div class="vd_panel-inner-part-content">
                <div class="search_formbox listing_formbox">
                <div class="clearfix"></div>
                  <div class="panel panel-default table-responsive">
                  <?php 
                    
                    $where='';
                    
					if($_REQUEST['id']!=''){

					$where.=" and store_id = '".$_REQUEST['id']."%' ";		

					}
					
                    $start=0;
                    
                    if(isset($_GET['start'])) $start=$_GET['start'];
                    
                    $pagesize=15;
                    
                    if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];
                    
                    $order_by='id';
                    
                    if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];
                    
                    $order_by2='desc';
                    
                    if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];
                    
                    $sql=$obj->Query("select * from inv_sale_service where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize");
                    
                    $sql2=$obj->query("select * from inv_sale_service where 1=1 $where order by $order_by $order_by2",$debug=-1);
                    
                    $reccnt=$obj->numRows($sql2);
                    
                    if($reccnt==0){?>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="center" valign="middle"><font face="Arial, Helvetica, sans-serif"   color="#FF0000" size="+1">No Record</font></td>
                      </tr>
                    </table>
                    <?php } else { ?>
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr class="item">
                          <th>S.N.</th>
                          <th>Store Name</th>
                          <th>Patient Name</th>
                          <th>Service Name</th>
                          <th>Date</th>
                          <th>Amount Taken</th>
                        </tr>
                      </thead>
                      <tbody>
                     <?php $i=0; 
						 while($line=$obj->fetchNextObject($sql)){ $i++;?>
                        <tbody>
                        <tr>
                          <td><?php echo $i+$start;?></td>
                           <td><?php $storeArr = $obj->query("select name from inv_stores where id='".$line->store_id."'");
						  		    $srtoRes = $obj->fetchNextObject($storeArr);
									echo $srtoRes->name;?>
                                    </td>
                          <td><?php echo getPatient($line->patcient_id);?>
                            <?php $pt = $obj->query("SELECT * FROM crm_patients WHERE pid='".$line->patcient_id."'");?>
                            <?php $ptResult = $obj->fetchNextObject($pt);?>
                            (<?php echo $ptResult->patient_id?>)</td>
                          <td><?php echo getField('name','inv_services',$line->service_id);?></td>
                          <td><?php echo date('d-m-Y',strtotime($line->created_on));?></td>
                          <td><?php echo $line->amount_taken;?></td>
                        </tr>
                    	</tbody>
                        <?php } ?>
                    </table>
                  </div>
                  <ul class="pagination">
                    <?php include("include/paging.inc.php"); ?>
                  </ul>
                  </div>
               <?php }?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Footer Start -->

<?php include("footer.php");?>

<!-- Footer END -->

<body>
</html>
