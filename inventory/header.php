 <div class="vd_top-menu-wrapper">
        <div class="container ">
          <div class="vd_top-nav vd_nav-width ">
          <div class="vd_panel-header">
          	<div class="logo">
            	<a href="welcome.php"><img alt="logo" src="img/logo.png"></a>
            </div>
            <!-- logo -->
		<div class="vd_panel-menu  hidden-sm hidden-xs" data-intro="<strong>Minimize Left Navigation</strong><br/>Toggle navigation size to medium or small size. You can set both button or one button only. See full option at documentation." data-step=1>
		<span class="nav-small-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Small Nav Toggle" data-action="nav-left-small">
		<i class="fa fa-bars" aria-hidden="true"></i>
		</span>
		</div>
            <div class="vd_panel-menu left-pos visible-sm visible-xs">
            <span class="menu" data-action="toggle-navbar-left">
            <i class="fa fa-ellipsis-v"></i>
            </span>
            </div>
            <div class="vd_panel-menu visible-sm visible-xs">
            <span class="menu visible-xs" data-action="submenu">
            <i class="fa fa-bars"></i>
            </span>

            </div>
          </div>

          </div>
          <div class="vd_container">
          	<div class="row">
            	<div class="col-sm-5 col-xs-12">
                </div>
                <div class="col-sm-7 col-xs-12">
              		<div class="vd_mega-menu-wrapper">
                    	<div class="vd_mega-menu pull-right">
            				    <ul class="mega-ul">
                          <li id="top-menu-profile" class="profile mega-li">
                          <a href="#" class="mega-link"  data-action="click-trigger">
                          <span  class="mega-image">
                          <i class="fa fa-user"></i>
                          </span>
                          <span class="mega-name">
                          <?php if($_SESSION['user_type']=='admin'){?>admin <?php } else { ?>
                          <?php echo $_SESSION['store_name']; } ?><i class="fa fa-caret-down fa-fw"></i>
                          </span>
                          </a>
                          <div class="vd_mega-menu-content  width-xs-2  left-xs left-sm" data-action="click-target">
                          <div class="child-menu">
                          <div class="content-list content-menu">
                          <ul class="list-wrapper pd-lr-10">
                          <li> <a href="#"> <div class="menu-icon"><i class="fa fa-user"></i></div> <div class="menu-text">Edit Profile</div> </a> </li>
                          <li> <a href="change-password.php"> <div class="menu-icon"><i class="fa fa-key"></i></div> <div class="menu-text">Change Password</div> </a> </li>
						   <li> <a href="logout.php"> <div class="menu-icon"><i class="fa fa-sign-out"></i></div> <div class="menu-text">Logout</div> </a> </li>
                          </ul>
                          </div>
                          </div>
                          </div>
                          </li>
                         
	                       </ul>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>