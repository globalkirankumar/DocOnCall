<?php 
	include("include/config.php");
	include("include/functions.php");
	
	if($_SESSION['sess_username']==''){
			header("Location:".SITE_URL);	
		}
	if(isset($_SESSION['sess_username']))
		{
			$table_middle_name = stripslashes(getField('name','inv_stores',$_SESSION['store_id']));
			$table_name = 'inv_'.$table_middle_name.'_inventory';
	}
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>Inventory</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
</head>
<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>
<!-- Header Ends --> 

<!---------sidebar---->
<div class="content">
  <div class="container">
    <?php include("sidebar.php");?>
    
    <!-- Middle Content Start -->
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-header">
                <ul class="breadcrumb">
                  <li><a href="welcome.php">Home</a> </li>
                  <li><a href="assigned-inventory-history-details.php">Assigned Inventory History</a> </li>
                </ul>
              </div>
              <?php if($_SESSION['sess_msg']){ ?>
              <div class="vd_panel-inner-part-content"><span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
              <?php }?>
              <div class="vd_panel-inner-part-content">
         <?php
			$new_product_query=$obj->Query("select * from inv_bucket_inventory where store_status=1 and store_id='".$_SESSION['store_id']."' GROUP by ses_id order by id desc",$debug=-1);?>
                <div class="search_formbox listing_formbox">
                <!--    <input type="button" name="add" value="Add Brand" class="btn btn-fright" onclick="location.href='brand-addf.php'"> -->
                <div class="clearfix"></div>
                <form name="frm" method="post" action="brand-del.php" enctype="multipart/form-data">
                  <div class="panel panel-default table-responsive">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr class="item">
                          <th>No.</th>
                          <th>Date</th>
                          <th>Internal ID</th>
                          <th>View</th>
                          <!-- <th><input name="check_all" type="checkbox"  id="check_all" onclick="checkall(this.form)" value="check_all" /></th>--> 
                        </tr>
                      </thead>
                      <?php 
						$i=0;
						while($line_result=$obj->fetchNextObject($new)){$i++;?>
                      <tbody>
                        <tr>
                          <td><?php echo $i+$start;?></td>
                          <td><?php echo date('d-m-Y (H:i:s)',strtotime($line_result->created_on));?></td>
                          <td><?php echo $line_result->ses_id;?></td>
                          <td><a href="store-inventory-new-list1.php?view=<?php echo $line_result->ses_id;?>">View</a></td>
                        </tr>
                      </tbody>
                      <?php } ?>
                    </table>
                  </div>
                  <div class="custom-edit">
                    <input type="hidden" name="what" value="what" />
                    <!-- <input type="submit" name="Submit" value="Activate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />
                      <input type="submit" name="Submit" value="Deactivate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />
                      <input type="submit" name="Submit" value="Delete" class="edit-btn" onclick="return del_prompt(this.form,this.value)" /> --> 
                  </div>
                  <ul class="pagination">
                    <?php include("include/paging.inc.php"); ?>
                  </ul>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Footer Start -->
<?php include("footer.php");?>
<!-- Footer END -->
<body>
</html>
