<?php 

	include("include/config.php");

	include("include/functions.php");
	
	  if($_SESSION['sess_username']==''){

		header("Location:".SITE_URL);	

	}

?>

<!DOCTYPE html>

<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->

<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->

<!--[if gt IE 9]><!-->

<html>
<!--<![endif]-->

<head>
<meta charset="utf-8" />
<title>DOCTOR ON CALL</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms "  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>

<!-- Header Ends --> 

<!---------sidebar---->

<div class="content">
  <div class="container">
    <?php include("sidebar.php");?>
    
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
          
          <?php if($_SESSION['user_type']=='admin'){?>
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-header">
                <ul class="breadcrumb">
                  <li><a href="welcome.php">Home</a> </li>
                </ul>
              </div>
              <div class="vd_panel-inner-part-content">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Total Sale <small> 
					(<?php $sql=$obj->query("select * from inv_sales WHERE is_sale_done='1'");
						   while($line=$obj->fetchNextObject($sql)){
							$qun=$line->qty;
						    $price=$line->sale_price;
						    $total=$qun*$price;
						    $grandtotal += $total;
		  				}  
						$sql_subscription_due=$obj->query("SELECT SUM(amount_taken) as m FROM inv_sale_subscription WHERE status=1",$debug=-1);
	               		$row1=$obj->fetchNextObject($sql_subscription_due);
					    $taken = $row1->m;
						
						$sqlArr = $obj->query("SELECT SUM(amount_taken) as b FROM inv_sale_service WHERE status=1",$debug=-1);
						$serviceResult=$obj->fetchNextObject($sqlArr);
						$service = $serviceResult->b;
						 echo $grandtotal+$taken+$service;?> )
                      </small></h2>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Total Customer <small> (<?php 
					   $candArr=$obj->query("select COUNT(DISTINCT patient_id) as cs from inv_sales where is_sale_done=1",$debug=-1);
					   $rs=$obj->fetchNextObject($candArr);
					   echo $rs->cs;
					   ?>)</small></h2>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Total Staff <small>(<?php 
					   $userArr=$obj->query("select COUNT(*) as u from inv_users where usertype='staff'",$debug=-1);
					   $rsUser=$obj->fetchNextObject($userArr);
					   echo $rsUser->u;
					   ?>)</small></h2>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Total Store <small> (<?php 
					   $storeArr=$obj->query("select COUNT(*) as st from inv_stores where status=1",$debug=-1);
					   $rsStore=$obj->fetchNextObject($storeArr);
					   echo $rsStore->st;
					   ?>)</small></h2>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Total Subscription Plan <small>(<?php 
					   $subArr=$obj->query("select COUNT(*) as sub from inv_subscription_plan where status=1",$debug=-1);
					   $rsSub=$obj->fetchNextObject($subArr);
					   echo $rsSub->sub;
					   ?>)</small></h2>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Total Service Plan <small>(<?php 
					   $healthArr=$obj->query("select COUNT(*) as he from inv_services where status=1",$debug=-1);
					   $rshealth=$obj->fetchNextObject($healthArr);
					   echo $rshealth->he;
					   ?>)</small></h2>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <?php } else { ?>
            
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-header">
                <ul class="breadcrumb">
                  <li><a href="welcome.php">Home</a> </li>
                </ul>
              </div>
              <div class="vd_panel-inner-part-content">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Total Sale <small> 
						(<?php $totalArr=$obj->query("select * from inv_sales WHERE store_id='$_SESSION[store_id]' and is_sale_done='1'");
						   while($lineResult=$obj->fetchNextObject($totalArr)){
								 $qunRs=$lineResult->qty;
						   		 $priceRs=$lineResult->sale_price;
						    	 $total1=$qunRs*$priceRs;
						    	 $grandtotal1 += $total1;
		  				    	}  
						$subscription=$obj->query("SELECT SUM(amount_taken) as a FROM inv_sale_subscription WHERE store_id='$_SESSION[store_id]' and status=1",$debug=-1);
	               		$SubsResult=$obj->fetchNextObject($subscription);
					    $Amount_sub = $SubsResult->a;
						
						$serviceArr = $obj->query("SELECT SUM(amount_taken) as dk FROM inv_sale_service WHERE store_id='$_SESSION[store_id]' and status=1",$debug=-1);
						$serResult=$obj->fetchNextObject($serviceArr);
						$serviceTotal = $serResult->dk;
						  
						 echo $grandtotal1+$Amount_sub+$serviceTotal;?> )
                      </small></h2>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Total Customer Purchage medicine<small> (<?php 
					   $storeCustomer=$obj->query("select COUNT(patient_id) as SC from inv_sales where is_sale_done=1",$debug=-1);
					   $storeCustresult=$obj->fetchNextObject($storeCustomer);
					   echo $storeCustresult->SC;
					   ?>)</small></h2>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Low Stock Inventory <small>(<?php 
					   $store = stripslashes(getField('name','inv_stores',$_SESSION['store_id']));
					  $lowStock=$obj->query("select COUNT(*) as stock from inv_".$store."_inventory where qty < 0",$debug=-1);
					  if($obj->numRows($lowStock)!=0){
					     $lowStockResult=$obj->fetchNextObject($lowStock);
					      echo $lowStockResult->stock;}
					   ?>)</small></h2>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Current Store Medicine <small> (<?php 
					  $medicineQty=$obj->query("select * from inv_".$store."_inventory",$debug=-1);
					   if($obj->numRows($medicineQty)!=0){
					     $medicineResult=$obj->fetchNextObject($medicineQty);
						  echo $medicineResult->qty;}
					   ?>)</small></h2>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Total Subscription Plan Sold <small>(<?php 
					   $subscriPlan=$obj->query("select COUNT(amount_taken) as Subplan from inv_sale_subscription where store_id='$_SESSION[store_id]' and status=1",$debug=-1);
					   $subplanResult=$obj->fetchNextObject($subscriPlan);
					   echo $subplanResult->Subplan;
					   ?>)</small></h2>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="box">
                    <h2>Total Service Plan Sold<small>(<?php 
					   $servicePlan=$obj->query("select COUNT(amount_taken) as SP from inv_sale_service where store_id='$_SESSION[store_id]' and status=1",$debug=-1);
					   $planResult=$obj->fetchNextObject($servicePlan);
					   echo $planResult->SP;
					   ?>)</small></h2>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Footer Start -->

<?php include("footer.php");?>

<!-- Footer END -->

<body>
</html>
