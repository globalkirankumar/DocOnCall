<?php 
	include("include/config.php");
	include("include/functions.php");
	
	if($_SESSION['sess_username']==''){
		header("Location:".SITE_URL);	
	}
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>DOCTOR ON CALL</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script>
	function checkall(objForm)

    {

	len = objForm.elements.length;

	var i=0;

	for( i=0 ; i<len ; i++){

		if (objForm.elements[i].type=='checkbox') 

		objForm.elements[i].checked=objForm.check_all.checked;

	}

   }

	function del_prompt(frmobj,comb)

		{
			if(comb=='Delete'){


				if(confirm ("Are you sure you want to delete record(s)"))

				{

					frmobj.action = "notification-del.php";

					frmobj.what.value="Delete";

					frmobj.submit();

				}

				else{ 

				return false;

				}

			}

		else if(comb=='Deactivate'){
			frmobj.action = "notification-del.php";
			frmobj.what.value="Deactivate";

			frmobj.submit();

		}

		else if(comb=='Activate'){

			frmobj.action = "notification-del.php";

			frmobj.what.value="Activate";

			frmobj.submit();

		}
	}
</script>

<script type="text/javascript" language="javascript">
function validate(obj)
{
if(obj.category.value==''){
alert("Please enter Category");
obj.category.focus();
return false;
}
}
</script>
</head>
<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
  <header class="header-1" id="header">
    <?php include("header.php");?>
  </header>
  <!-- Header Ends --> 
  
  <!---------sidebar---->
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      
      <!-- Middle Content Start -->
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
                    <li><a href="notification_list.php">Notifications List</a> </li>
                  </ul>
                </div>
               
                <?php if($_SESSION['sess_msg']){ ?>
                <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
                <?php }?>
                <div class="vd_panel-inner-part-content">
                  <div class="search_formbox listing_formbox">
                  <input type="button" name="add" value="Add Notification" class="btn btn-fright" onclick="location.href='notification-addf.php'">
                  <div class="clearfix"></div>
                  <form name="frm" method="post" action="notification-del.php" enctype="multipart/form-data">
                    <div class="panel panel-default table-responsive">
	<?php 

$where='';

if($_REQUEST['name']!=''){

$where.=" and name like '%".mysql_real_escape_string($_REQUEST['name'])."%' ";		

}

$start=0;


if(isset($_GET['start'])) $start=$_GET['start'];


$pagesize=25;


if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];


$order_by='pid';


if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];


$order_by2='desc';


if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];


$sql=$obj->Query("select * from crm_custom_notification where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize");


$sql2=$obj->query("select * from crm_custom_notification where 1=1 $where order by $order_by $order_by2",$debug=-1);


$reccnt=$obj->numRows($sql2);

if($reccnt==0){?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="center" valign="middle"><font face="Arial, Helvetica, sans-serif"   color="#FF0000" size="+1">No Record</font></td>
                        </tr>
                      </table>
                      <?php } else { ?>
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr style="background:#6C9465; color:#fff;">
                            <th>No.</th>
                            <th>Notification Text</th>
							<th>Patient Name</th>
                            <th>Status</th>
                            
                            <th><input name="check_all" type="checkbox"  id="check_all" onclick="checkall(this.form)" value="check_all" /></th>
                          </tr>
                        </thead>
                   <?php 
			    		$i=0;
						while($line=$obj->fetchNextObject($sql)){
							$i++;
							if($i%2==0){
									$bgcolor = "success";
								}else{
									$bgcolor = "warning";
							}
							?>
                        <tbody>
                          <tr class="<?php echo $bgcolor;?>">
                            <td><?php echo $i+$start;?></td>
                            <td><?php echo stripslashes($line->name);?></td>
                            <td><?php $sq=mysql_query("select * from crm_patients where pid='".$line->patient_id."'");
							          $rw=mysql_fetch_array($sq);
									  echo $rw['name'];
							?></td>
                            <?php if($line->status=='1'){?>
                            <td>Active</td>
                            <?php }else{ ?>
                            <td>Inactive</td>
                            <?php } ?>
                            
                            <th><input type="checkbox" name="ids[]" value="<?php echo $line->pid;?>" /></th>
                          </tr>
                        </tbody>
                        <?php } ?>
                      </table>
                    </div>
                    <div class="custom-edit">
                      <input type="hidden" name="what" value="what" />
                      <input type="submit" name="Submit" value="Activate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />
                      <input type="submit" name="Submit" value="Deactivate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />
                      <input type="submit" name="Submit" value="Delete" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />
                    </div>
                    <ul class="pagination">
                      <?php include("include/paging.inc.php"); ?>
                    </ul>
                    </div>
                    <?php }?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer Start -->
  <?php include("footer.php");?>
<!-- Footer END -->

<body>
</html>
