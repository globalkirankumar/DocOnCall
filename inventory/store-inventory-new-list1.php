<?php 
	include("include/config.php");
	include("include/functions.php");
	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	
	}
	
if(isset($_SESSION['sess_username']))
{
	
$table_middle_name = stripslashes(getField('name','inv_stores',$_SESSION['store_id']));

$table_name = 'inv_'.$table_middle_name.'_inventory';
}

?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>Inventory</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>

<script type="text/javascript" language="javascript">
function validate(obj)
{
if(obj.brand.value==''){
alert("Please enter brand name");
obj.brand.focus();
return false;
}
}
</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
  <header class="header-1" id="header">
    <?php include("header.php");?>
  </header>
  <!-- Header Ends --> 
  
  <!---------sidebar---->
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      
      <!-- Middle Content Start -->
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
                    <li><a href="store-inventory-new-list.php">Inventory History</a> </li>
                  </ul>
                </div>
               
                <?php if($_SESSION['sess_msg']){ ?>
                <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
                <?php }?>
                <div class="vd_panel-inner-part-content">
				
				<?php 
				$new_product_query=$obj->Query("select * from inv_bucket_inventory where ses_id = '".$_GET['view']."' and store_status=1");
				
//$new_product_query=$obj->Query("select * from $table_name where 1=1 group by created_on order by id desc limit 0, 3");
    while($line_result=$obj->fetchNextObject($new_product_query))
    {
    ?>

  
                      
  <?php } ?>
				
                  <div class="search_formbox listing_formbox">
              <!--    <input type="button" name="add" value="Add Brand" class="btn btn-fright" onclick="location.href='brand-addf.php'"> -->
                  <div class="clearfix"></div>
                  <form name="frm" method="post" action="brand-del.php" enctype="multipart/form-data">
                    <div class="panel panel-default table-responsive">
	<?php 


$where='';

if($_REQUEST['name']!=''){

$where.=" and name like '%".$_REQUEST['name']."%' ";		

}
if(isset($_GET[date]))
{
$where.=" and created_on = '".$_GET['date']."'";  
}

/*if($_REQUEST['name']!='')
{
    getField('name','inv_brand',$line->category_id)
}*/


$start=0;


if(isset($_GET['start'])) $start=$_GET['start'];


$pagesize=500;


if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];


$order_by='id';


if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];


$order_by2='desc';


if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];


$sql=$obj->Query("select * from inv_bucket_inventory where store_status= 1 and  ses_id = '".$_GET['view']."'  order by $order_by $order_by2 limit $start, $pagesize");

$sql2=$obj->query("select * from inv_bucket_inventory where 1=1 group by ses_id order by $order_by $order_by2",$debug=-1);

if($sql!='')
{
$reccnt=$obj->numRows($sql2);
}



if($reccnt==0){?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
	<?php 
	/*	$new_product_query=$obj->Query("select * from inv_bucket_inventory where 1=1 group by ses_id");
		while($line_result=$obj->fetchNextObject($new_product_query))
    {
    ?>
                          <td><h6><?php echo $line_result->created_on ?><h6>&nbsp;&nbsp;<h6><?php echo $line_result->ses_id?></h6></td>
                        
	<?php }*/ ?>		
                         
                        </tr>
                      </table>
                      <?php } else { ?>
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr class="item">
                            <th>No.</th>
                            <th>Store</th>
							<th>Category ID</th>
							<th>Brand</th>
							<th>Generic Name</th>
							<th>Medicine form</th>
							<th>Expire Date</th>
							<th>Quantity</th>
							<th>Strength</th>
							<th>Usage</th>
							<th>Price</th>
                           
                           <!-- <th><input name="check_all" type="checkbox"  id="check_all" onclick="checkall(this.form)" value="check_all" /></th>-->
                          </tr>
                        </thead>
                     <?php 
						$i=1;
						$total = 0;
						while($line_result=$obj->fetchNextObject($sql)){?>
                        <tbody>
                          <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo stripslashes(getField('name','inv_stores',$line_result->store_id)); ?></td>
							<td><?php echo stripslashes(getField('name','inv_category',$line_result->category_id)); ?></td>
							<td><?php echo stripslashes(getField('name','inv_brand',$line_result->brand_id));?></td>
							<td><?php echo stripslashes(getField('generic_name','inv_generic_name',$line_result->generic_id)); ?></td>
                            <td><?php echo stripslashes(getField('form_of_medicine_name','inv_form_of_medicine',$line_result->formof_id)); ?></td>
							<td><?php echo date('d/m/Y',strtotime($line_result->expire_date));?></td>
							<td><?php echo $line_result->qty; ?></td>
							<td><?php echo stripslashes(getField('strength_of_medicine_name','inv_strength_of_medicine',$line_result->strength_id));?></td>
							<td><?php echo stripslashes(getField('usage_category_name','inv_usage_category',$line_result->usage_id));?></td>
							<td><?php echo $line_result->price;?></td>
						  </tr>
                           <?php $total += $line_result->qty*$line_result->price;?>
                        </tbody>
                        <?php } ?>
                        <tr>
							<td colspan="13" > <span style="padding-left:570px;"><b>Grand Total : <?php echo $total; ?> </b></span></td>
						</tr>
                      </table>
                    </div>
                    <div class="custom-edit">
                      <input type="hidden" name="what" value="what" />
                     <!-- <input type="submit" name="Submit" value="Activate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />
                      <input type="submit" name="Submit" value="Deactivate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />
                      <input type="submit" name="Submit" value="Delete" class="edit-btn" onclick="return del_prompt(this.form,this.value)" /> -->
                    </div>
                   
                    </div>
                    <?php }?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Footer Start -->
  <?php include("footer.php");?>
<!-- Footer END -->
<body>
</html>
