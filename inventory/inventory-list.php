<?php 
	include("include/config.php");
	include("include/functions.php");
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>DOCTOR ON CALL</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>

<script type="text/javascript" language="javascript">
function validate(obj)
{
if(obj.category.value==''){
alert("Please Select Category");
obj.category.focus();
return false;
}
}
</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms "  data-smooth-scrolling="1">
<div class="vd_body">
  <header class="header-1" id="header">
    <?php include("header.php");?>
  </header>
  <!-- Header Ends --> 
  
  <!---------sidebar---->
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      
      <!-- Middle Content Start -->
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
                    <li><a href="inventory-list.php">Inventory List</a> </li>
                  </ul>
                </div>
                <div class="vd_panel-inner-part-content">
                  <form name="searchForm" method="post" action="inventory-list.php" onSubmit="return validate(this)">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                      <div class="search_formbox">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group form-group2">
                          <div class="controls">
                            <select name="category_id" id="category" required>
                            <option value="">Select Category</option>
                            <?php $catArr = $obj->query("select * from inv_category where status=1");
						  		while($catResult = $obj->fetchNextObject($catArr)){?>
                            <option value="<?php echo $catResult->id?>" <?php if($_REQUEST['category_id']==$catResult->id){?>selected<?php }?>><?php echo $catResult->name;?></option>
                            <?php } ?>
                          </select>
                       	</div>
                         </div>
                        </div>
                        
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="submit" name="search" class="btn greenbutton fright" value="Search">
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12"> <a href="inventory-list.php">View All</a> </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </form>
                  <div class="clearfix"></div>
                </div>
                <?php if($_SESSION['sess_msg']){ ?>
                <div class="vd_panel-inner-part-content"> 
                <span class="successful-message" style="color:#FF0000"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
                <?php }?>
                <div class="vd_panel-inner-part-content">
                  <div class="search_formbox listing_formbox">
                  <input type="button" name="add" value="Add Inventory" class="btn btn-fright" onclick="location.href='inventory-addf.php'">
                  <div class="clearfix"></div>
                  <form name="frm" method="post" action="inventory-del.php" enctype="multipart/form-data">
                    <div class="panel panel-default table-responsive">
 <?php 

$where='';

if($_REQUEST['category_id']!=''){

$where.=" and category_id like '%".$_REQUEST['category_id']."%' ";		

}

$start=0;


if(isset($_GET['start'])) $start=$_GET['start'];


$pagesize=25;


if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];


$order_by='id';


if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];


$order_by2='desc';


if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];


$sql = $obj->query("select * from inv_buyer_inventory where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize",$debug=-1);


$sql2 = $obj->query("select * from inv_buyer_inventory where 1=1 $where order by $order_by $order_by2");


$reccnt=$obj->numRows($sql2);

if($reccnt==0){?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="center" valign="middle"><font face="Arial, Helvetica, sans-serif"   color="#FF0000" size="+1">No Record</font></td>
                        </tr>
                      </table>
                      <?php } else { ?>
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr style="background:#6C9465; color:#fff;">
                            <th>No.</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Generic</th>
                            <th>Form Medicine</th>
                            <th>Strength</th>
                            <th>Usage</th>
                            <th>Expiry Date</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                   <?php 
			 			$i=0;
						while($line=$obj->fetchNextObject($sql)){
							$i++;
							if($i%2==0){
									$bgcolor = "success";
								}else{
									$bgcolor = "warning";
							}?>
                        <tbody>
                          <tr class="<?php echo $bgcolor;?>">
                            <td><?php echo $i+$start;?></td>
                            <td><?php echo getField('name','inv_category',$line->category_id);?></td>
                            <td><?php echo getField('name','inv_brand',$line->brand_id);?></td>
                            <td><?php echo getField('generic_name','inv_generic_name',$line->generic_id);?></td>
                            <td><?php echo getField('form_of_medicine_name','inv_form_of_medicine',$line->formof_id);?></td>
                            <td><?php echo getField('strength_of_medicine_name','inv_strength_of_medicine',$line->strength_id);?></td>
                            <td><?php echo getField('usage_category_name','inv_usage_category',$line->usage_id);?></td>
                            <td><?php echo date('d/m/Y', strtotime($line->expire_date));?></td>
                            <td><?php echo $line->qty;?></td>
                            <td><?php echo $line->price;?></td>
                            <td><?php echo date('d/m/Y', strtotime($line->created_on));?></td>
                            <?php if($line->status=='1'){?>
                            <td>Active</td>
                            <?php }else{ ?>
                            <td>Deactive</td>
                            <?php } ?>
                            <td><a class="editfile" href="inventory-addf.php?id=<?php echo $line->id;?>"><i class="fa fa-pencil"></i></a></td>
                          </tr>
                        </tbody>
                        <?php } ?>
                      </table>
                    </div>
                    <ul class="pagination">
                      <?php include("include/paging.inc.php"); ?>
                    </ul>
                    <?php }?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <!-- Footer Start -->
  <?php include("footer.php");?>
<!-- Footer END -->
<body>
</html>