<?php 
	include("include/config.php");
	include("include/functions.php");
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->

<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->

<!--[if gt IE 9]><!-->

<html>

<!--<![endif]-->

<head>
<meta charset="utf-8" />
<title>DOCTOR ON CALL</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>
<div class="content">
  <div class="container">
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-content">
                <div class="search_formbox listing_formbox">
                  <div class="panel panel-default table-responsive">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr class="item">
                          <th>No.</th>
                          <th>Category</th>
                          <th>Brand</th>
                          <th>Generic</th>
                          <th>Form Medicine</th>
                          <th>Strength</th>
                          <th>Usage</th>
                          <th>Quantity</th>
                          <th>Price</th>
                        </tr>
                      </thead>
                  <?php 
					$i=0;
					$total = 0;
		$sql=$obj->Query("select * from inv_sales where patient_id='".$_REQUEST['pid']."' and ses_id='".$_REQUEST['ses_id']."' and is_sale_done=1",$debug=-1);
					while($line = $obj->fetchNextObject($sql)){ 
						$i++;?>
                      <tbody>
                        <tr>
                          <td><?php echo $i;?></td>
                          <td><?php echo getField('name','inv_category',$line->category_id);?></td>
                          <td><?php echo getField('name','inv_brand',$line->brand_id);?></td>
                          <td><?php echo getField('generic_name','inv_generic_name',$line->generic_id);?></td>
                          <td><?php echo getField('form_of_medicine_name','inv_form_of_medicine',$line->formof_id);?></td>
                          <td><?php echo getField('strength_of_medicine_name','inv_strength_of_medicine',$line->strength_id);?></td>
                          <td><?php echo getField('usage_category_name','inv_usage_category',$line->usage_id);?></td>
                          <td><?php echo $line->qty;?></td>
                          <td><?php echo $line->sale_price;?></td>
                        </tr>
                        <?php $total += $line->qty*$line->sale_price;?>
                      </tbody>
                      <?php } ?>
                      <tr>
                        <td colspan="13" ><span style="padding-left:570px;" ><b>Grand Total : <?php echo $total; ?> </b></span></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Footer Start -->
<?php include("footer.php");?>
<!-- Footer END -->
<body>
</html>