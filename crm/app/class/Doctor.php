<?php
class Doctor extends dbc {
	// To get the sevice list done Kiran Kumar
	public function getServiceTypeList($service_type) {
		$service_list = array ();
		
		$query = parent::db_query ( "select S.pid,S.name,S.name_my from #_services_cat as S Where S.service_type='" . $service_type . "' order by S.name" );
		while ( $row = parent::db_fetch_array ( $query ) ) {
			$query1 = parent::db_query ( "select * from #_service_subcat where service_cat ='" . $row ['pid'] . "'" );
			$sub_service_type_data = array ();
			while ( $row1 = parent::db_fetch_array ( $query1 ) ) {
				$sub_service_type_data [$row1 ['pid']] = '';
			}
			$service_list [$row ['pid']] = $sub_service_type_data;
		}
		
		return (json_encode ( $service_list ));
	}
	
	// infi get the doctor type
	public function doctor_type() {
		$data = array ();
		$query = parent::db_query ( "select * from #_doctor_type  where status=1 order by name asc" );
		if (mysql_num_rows ( $query ) > 0) {
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				$data [$i] ['pid'] = $row ['pid'];
				$data [$i] ['name'] = $row ['name'];
				$i ++;
			}
		} else {
			return 'false';
		}
		
		return $data;
	}
	
	// get all the division names
	public function division($lang_flag) {
		$data = array ();
		$query = parent::db_query ( "select * from #_division  where status=1 order by name asc" );
		if (mysql_num_rows ( $query ) > 0) {
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				$data [$i] ['pid'] = $row ['pid'];
				if ($lang_flag == "en") {
					$data [$i] ['name'] = $row ['name'];
				} else {
					$data [$i] ['name'] = $row ['name_my'];
				}
				$i ++;
			}
		} else {
			return 'false';
		}
		
		return $data;
	}
	
	// get the township for purticular division id
	public function township($division_id, $lang_flag) {
		$data = array ();
		$query = parent::db_query ( "select * from #_township where division_id='" . $division_id . "' AND status=1 " );
		if (mysql_num_rows ( $query ) > 0) {
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				$data [$i] ['pid'] = $row ['pid'];
				if ($lang_flag == "en") {
					$data [$i] ['name'] = $row ['name'];
				} else {
					$data [$i] ['name'] = $row ['name_my'];
				}
				$data [$i] ['postal_code'] = $row ['postal_code'];
				$i ++;
			}
		} else {
			return 'false';
		}
		
		return $data;
	}
	public function getdoctor_location_from_doctor_table($doctor_id, $lang_flag) {
		$doctor_query = parent::db_query ( "select * from #_doctors where doctor_id='" . $doctor_id . "' " );
		$doctor_data = parent::db_fetch_array ( $doctor_query );
		if ($lang_flag == "en") {
			$data ['location'] = $doctor_data ['location'];
		} else {
			$data ['location_my'] = $doctor_data ['location_my'];
		}
		return $data;
	}
	public function insert_location()
    {
        $data = array();
        $query = parent::db_query("SELECT * FROM `#_doctors` WHERE `pid` < 9160 ");
        if (mysql_num_rows($query) > 0) {
            $i = 0;
            while ($row = parent::db_fetch_array($query)) {

                $data [$i] ['doctor_id'] = $row ['pid'];
                $data [$i] ['hospital_id'] = $row ['hospital_id'];
                $data [$i] ['clinic_id'] = $row ['clinic_id'];
                if ($row ['hospital_id'] == 0) {
                    $data [$i] ['location_type'] = 1;
                    $latlong_of_hos = parent::getSingleResult("select latlong from #_clinics where pid='" . $data [$i] ['clinic_id'] . "'");
                } else {
                    $data [$i] ['location_type'] = 2;
                    $latlong_of_hos = parent::getSingleResult("select latlong from #_hospitals where pid='" . $data [$i] ['hospital_id'] . "'");
                }
                $data [$i] ['location'] = $row ['location'];
                $data [$i] ['location_my'] = $row ['location_my'];
                $township = parent::getSingleResult("select name from #_township where pid='" . $row ['township'] . "'");
                $division = parent::getSingleResult("select name from #_division where pid='" . $row ['division'] . "'");
                $address_from_db = $row ['address'];
                if (!empty ($division)) {
                    $address = $division;
                }

                $data [$i] ['latlong'] = $latlong_of_hos;
                $data [$i] ['isHomeservice'] = 0;
                $data [$i] ['area_code'] = $row ['area_code'];
                $data [$i] ['dates'] = $row ['dates'];
                $data [$i] ['isChatNeeded'] = 0;
                $data [$i] ['available_status'] = 1;
                $data [$i] ['busy_free'] = $row ['busy_free'];
                $data [$i] ['status'] = $row ['status'];
                $data [$i] ['is_verified'] = 0;
                $data [$i] ['isDeleted'] = 0;
                $data [$i] ['created_on'] = $row ['created_on'];
                $data [$i] ['modified_on'] = date('Y-m-d H:i:s');
                $data [$i] ['create_by'] = $row ['create_by'];
                $inser = 0;
                if ($row ['pid'] != 2834) {
                    if ($latlong_of_hos == ',') {
                        $inser = 1;
                    }
                    if ($latlong_of_hos == '') {
                        $inser = 1;
                    }
                    if ($inser == 0) {
                        $location_id_last_inserted = parent::sqlquery("rs", 'doctors_locations', $data [$i]);
                        $location_insert_step3 = self::add_location_insert_step3(json_decode($row ['dates']), $row ['pid'], $location_id_last_inserted);
                    }

                }


                $i++;
            }

            return 'true';
        } else {
            return 'false';

        }

    }
	
	    public function get_doctors_location_list($doctor_id, $lang_flag)
    {
        $data = array();
        $query = parent::db_query("select * from #_doctors_locations where doctor_id='" . $doctor_id . "' and status=1 and isDeleted=0 ");
        if (mysql_num_rows($query) > 0) {
            $i = 0;
            while ($row = parent::db_fetch_array($query)) {
                $hospital_id = $row ['hospital_id'];
                $hospital_status = 0;
                $hospital_is_verified = 0;
                if ($hospital_id != 0) {
                    $hospital_status = parent::getSingleResult("select status from #_hospitals where pid='" . $hospital_id . "'");
                    $hospital_is_verified = parent::getSingleResult("select is_verified from #_hospitals where pid='" . $hospital_id . "'");
                }
                $clinic_id = $row ['clinic_id'];
                if ($clinic_id != 0) {
                    $hospital_status = parent::getSingleResult("select status from #_clinics where pid='" . $clinic_id . "'");
                    $hospital_is_verified = parent::getSingleResult("select is_verified from #_clinics where pid='" . $clinic_id . "'");
                }
                if (($hospital_status != 0) && ($hospital_is_verified != 0)) {
                    $data [$i] ['pid'] = $row ['pid'];
                    $data [$i] ['doctor_id'] = $row ['doctor_id'];
                    $data [$i] ['location_type'] = $row ['location_type'];
                    if ($lang_flag == "en") {
                        $data [$i] ['location'] = $row ['location'];
                        $location = explode("~", $row ['location']);
                        $data [$i] ['location_one'] = $location [0];
                        $data [$i] ['location_two'] = $location [1];
                    } else {
                        $data [$i] ['location_my'] = $row ['location_my'];
                        $location = explode("~", $row ['location_my']);
                        $data [$i] ['location_one'] = $location [0];
                        $data [$i] ['location_two'] = $location [1];
                    }
                    $data [$i] ['hospital_id'] = $row ['hospital_id'];
                    if ($data [$i] ['hospital_id'] != 0) {
                        $data [$i] ['name'] = parent::getSingleResult("select name from #_hospitals where pid='" . $data [$i] ['hospital_id'] . "'");
                        $data [$i] ['address'] = parent::getSingleResult("select address from #_hospitals where pid='" . $data [$i] ['hospital_id'] . "'");
                    }
                    $data [$i] ['clinic_id'] = $row ['clinic_id'];
                    if ($data [$i] ['clinic_id'] != 0) {
                        $data [$i] ['name'] = parent::getSingleResult("select name from #_clinics where pid='" . $data [$i] ['clinic_id'] . "'");
                        $data [$i] ['address'] = parent::getSingleResult("select address from #_clinics where pid='" . $data [$i] ['clinic_id'] . "'");
                    }
                    $data [$i] ['area_code'] = $row ['area_code'];
                    $data [$i] ['dates'] = json_decode($row ['dates']);
                    $available_unavailable_details = self::check_available_unavailable($data [$i] ['doctor_id'], $data [$i] ['pid']);
                    if ($available_unavailable_details ['status'] != 'false') {

                        $data [$i] ['available_status'] = "1";
                        $data [$i] ['available_from'] = ($row ['available_from'] === NULL || $row ['available_from'] == 0) ? '' : $row ['available_from'];
                        $data [$i] ['available_to'] = ($row ['available_to'] === NULL || $row ['available_to'] == 0) ? '' : $row ['available_to'];
                    } else {
                        $data [$i] ['available_status'] = "0";
                        $data [$i] ['available_from'] = ($row ['available_from'] === NULL || $row ['available_from'] == 0) ? '' : $row ['available_from'];
                        $data [$i] ['available_to'] = ($row ['available_to'] === NULL || $row ['available_to'] == 0) ? '' : $row ['available_to'];
                    }
                    // $data[$i]['available_status'] = $row['available_status'];

                    $data [$i] ['status'] = $row ['status'];
                    $data [$i] ['is_verified'] = $row ['is_verified'];
                    if ($row ['latlong'] == NULL) {
                        $data [$i] ['latlong'] = '';
                    } else {
                        $data [$i] ['latlong'] = $row ['latlong'];
                    }
                    $data [$i] ['isChatNeeded'] = parent::getSingleResult("select isChatNeeded from #_doctors where pid='" . $data [$i] ['doctor_id'] . "'");
                    $data [$i] ['isHomeservice'] = parent::getSingleResult("select isHomeservice from #_doctors where pid='" . $data [$i] ['doctor_id'] . "'");
                    $data [$i] ['busy_free'] = parent::getSingleResult("select busy_free from #_doctors where pid='" . $data [$i] ['doctor_id'] . "'");

                    $i++;
                }

            }
        } else {
            return 'false';
        }

        return $data;
    }

	    public function get_doctors_location_verified_list($doctor_id, $lang_flag)
    {
        $data = array();
        $query = parent::db_query("select * from #_doctors_locations where doctor_id='" . $doctor_id . "' and status=1 and is_verified=1 and isDeleted=0 ");
        if (mysql_num_rows($query) > 0) {
            $i = 0;
            while ($row = parent::db_fetch_array($query)) {
                $hospital_id = $row ['hospital_id'];
                $hospital_status = 0;
                $hospital_is_verified = 0;
                if ($hospital_id != 0) {
                    $hospital_status = parent::getSingleResult("select status from #_hospitals where pid='" . $hospital_id . "'");
                    $hospital_is_verified = parent::getSingleResult("select is_verified from #_hospitals where pid='" . $hospital_id . "'");
                }
                $clinic_id = $row ['clinic_id'];
                if ($clinic_id != 0) {
                    $hospital_status = parent::getSingleResult("select status from #_clinics where pid='" . $clinic_id . "'");
                    $hospital_is_verified = parent::getSingleResult("select is_verified from #_clinics where pid='" . $clinic_id . "'");
                }
                if (($hospital_status != 0) && ($hospital_is_verified != 0)) {
                    $data [$i] ['pid'] = $row ['pid'];
                    $data [$i] ['doctor_id'] = $row ['doctor_id'];
                    $data [$i] ['location_type'] = $row ['location_type'];
                    if ($lang_flag == "en") {
                        $data [$i] ['location'] = $row ['location'];
                        $location = explode("~", $row ['location']);
                        $data [$i] ['location_one'] = $location [0];
                        $data [$i] ['location_two'] = $location [1];
                    } else {
                        $data [$i] ['location_my'] = $row ['location_my'];
                        $location = explode("~", $row ['location_my']);
                        $data [$i] ['location_one'] = $location [0];
                        $data [$i] ['location_two'] = $location [1];
                    }
                    $data [$i] ['hospital_id'] = $row ['hospital_id'];
                    if ($data [$i] ['hospital_id'] != 0) {
                        $data [$i] ['name'] = parent::getSingleResult("select name from #_hospitals where pid='" . $data [$i] ['hospital_id'] . "'");
                        $data [$i] ['address'] = parent::getSingleResult("select address from #_hospitals where pid='" . $data [$i] ['hospital_id'] . "'");
                    }
                    $data [$i] ['clinic_id'] = $row ['clinic_id'];
                    if ($data [$i] ['clinic_id'] != 0) {
                        $data [$i] ['name'] = parent::getSingleResult("select name from #_clinics where pid='" . $data [$i] ['clinic_id'] . "'");
                        $data [$i] ['address'] = parent::getSingleResult("select address from #_clinics where pid='" . $data [$i] ['clinic_id'] . "'");
                    }
                    $data [$i] ['area_code'] = $row ['area_code'];
                    $data [$i] ['dates'] = json_decode($row ['dates']);
                    $available_unavailable_details = self::check_available_unavailable($data [$i] ['doctor_id'], $data [$i] ['pid']);
                    if ($available_unavailable_details ['status'] != 'false') {

                        $data [$i] ['available_status'] = "1";
                        $data [$i] ['available_from'] = ($row ['available_from'] === NULL || $row ['available_from'] == 0) ? '' : $row ['available_from'];
                        $data [$i] ['available_to'] = ($row ['available_to'] === NULL || $row ['available_to'] == 0) ? '' : $row ['available_to'];
                    } else {
                        $data [$i] ['available_status'] = "0";
                        $data [$i] ['available_from'] = ($row ['available_from'] === NULL || $row ['available_from'] == 0) ? '' : $row ['available_from'];
                        $data [$i] ['available_to'] = ($row ['available_to'] === NULL || $row ['available_to'] == 0) ? '' : $row ['available_to'];
                    }
                    // $data[$i]['available_status'] = $row['available_status'];

                    $data [$i] ['status'] = $row ['status'];
                    $data [$i] ['is_verified'] = $row ['is_verified'];
                    if ($row ['latlong'] == NULL) {
                        $data [$i] ['latlong'] = '';
                    } else {
                        $data [$i] ['latlong'] = $row ['latlong'];
                    }
                    $data [$i] ['isChatNeeded'] = parent::getSingleResult("select isChatNeeded from #_doctors where pid='" . $data [$i] ['doctor_id'] . "'");
                    $data [$i] ['isHomeservice'] = parent::getSingleResult("select isHomeservice from #_doctors where pid='" . $data [$i] ['doctor_id'] . "'");
                    $data [$i] ['busy_free'] = parent::getSingleResult("select busy_free from #_doctors where pid='" . $data [$i] ['doctor_id'] . "'");

                    $i++;
                }

            }
        } else {
            return 'false';
        }

        return $data;
    }

	//new search php
    public function get_doctors_location_search_list($doctor_id, $lang_flag)
    {
        $data = array();
        $query = parent::db_query("select * from #_doctors_locations where doctor_id='" . $doctor_id . "' and status=1 and isDeleted=0 and available_status=1 and busy_free=1 and is_verified=1 ");
        if (mysql_num_rows($query) > 0) {
            $i = 0;
            while ($row = parent::db_fetch_array($query)) {
                $data [$i] ['pid'] = $row ['pid'];
                $data [$i] ['doctor_id'] = $row ['doctor_id'];
                $data [$i] ['location_type'] = $row ['location_type'];
                if ($lang_flag == "en") {
                    $data [$i] ['location'] = $row ['location'];
                    $location = explode("~", $row ['location']);
                    $data [$i]['location_one'] = $location [0];
                    $data [$i]['location_two'] = $location [1];
                } else {
                    $data [$i] ['location_my'] = $row ['location_my'];
                    $location = explode("~", $row ['location_my']);
                    $data [$i]['location_one'] = $location [0];
                    $data [$i]['location_two'] = $location [1];
                }
                $data [$i] ['hospital_id'] = $row ['hospital_id'];
                if ($data [$i] ['hospital_id'] != 0) {
                    $data [$i] ['name'] = parent::getSingleResult("select name from #_hospitals where pid='" . $data [$i] ['hospital_id'] . "'");
                    $data [$i] ['address'] = parent::getSingleResult("select address from #_hospitals where pid='" . $data [$i] ['hospital_id'] . "'");
                }
                $data [$i] ['clinic_id'] = $row ['clinic_id'];
                if ($data [$i] ['clinic_id'] != 0) {
                    $data [$i] ['name'] = parent::getSingleResult("select name from #_clinics where pid='" . $data [$i] ['clinic_id'] . "'");
                    $data [$i] ['address'] = parent::getSingleResult("select address from #_clinics where pid='" . $data [$i] ['clinic_id'] . "'");
                }
                $data [$i] ['area_code'] = $row ['area_code'];
                $data [$i] ['dates'] = json_decode($row ['dates']);
                $available_unavailable_details = self::check_available_unavailable($data [$i] ['doctor_id'], $data [$i] ['pid']);
                if ($available_unavailable_details ['status'] != 'false') {

                    $data [$i] ['available_status'] = "1";
                    $data [$i] ['available_from'] = $row ['available_from'];
                    $data [$i] ['available_to'] = $row ['available_to'];
                } else {
                    $data [$i] ['available_status'] = "0";
                    $data [$i] ['available_from'] = $row ['available_from'];
                    $data [$i] ['available_to'] = $row ['available_to'];
                }
                // $data[$i]['available_status'] = $row['available_status'];

                $data [$i] ['status'] = $row ['status'];
                $data [$i] ['is_verified'] = $row ['is_verified'];
                if ($row ['latlong'] == NULL) {
                    $data [$i] ['latlong'] = '';
                } else {
                    $data [$i] ['latlong'] = $row ['latlong'];
                }
                $data [$i] ['isChatNeeded'] = parent::getSingleResult("select isChatNeeded from #_doctors where pid='" . $data [$i] ['doctor_id'] . "'");
                $data [$i] ['isHomeservice'] = parent::getSingleResult("select isHomeservice from #_doctors where pid='" . $data [$i] ['doctor_id'] . "'");
                $data [$i] ['busy_free'] = parent::getSingleResult("select busy_free from #_doctors where pid='" . $data [$i] ['doctor_id'] . "'");

                $i++;
            }
        } else {
            return 'false';
        }

        return $data;
    }

	
	// check email is exsist
	public function email_check($table_name, $email) {
		$query = parent::db_query ( "select * from #_" . $table_name . " where email ='" . $email . "'  " );
		if (mysql_num_rows ( $query ) == 0) {
			return false;
		} else {
			return true;
		}
	}
	public function signup_email_check($table_name, $email) {
		$query = parent::db_query ( "select * from #_" . $table_name . " where email ='" . $email . "' " );
		if (mysql_num_rows ( $query ) == 0) {
			return false;
		} else {
			return true;
		}
	}
	public function phone_check($table_name, $phone) {
		$query = parent::db_query ( "select * from #_" . $table_name . " where phone ='" . $phone . "' " );
		if (mysql_num_rows ( $query ) == 0) {
			return true;
		} else {
			return false;
		}
	}
	public function get_doctor_details_by_email($table_name, $email) {
		$doctor_query = parent::db_query ( "select * from #_" . $table_name . " where email='" . $email . "' " );
		$doctor_data = parent::db_fetch_array ( $doctor_query );
		$data ['doctor_id'] = $doctor_data ['pid'];
		$data ['hospital_id'] = $doctor_data ['hospital_id'];
		$data ['hospital'] = parent::getSingleResult ( "select name from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'" );
		$data ['clinic_id'] = $doctor_data ['clinic_id'];
		$data ['clinic'] = parent::getSingleResult ( "select name from #_clinics where pid='" . $doctor_data ['clinic_id'] . "'" );
		$data ['user_id'] = $doctor_data ['user_id'];
		$data ['speciality'] = $doctor_data ['speciality'];
		if ($doctor_data ['speciality'] == 0) {
            $data ['speciality_name'] = 'General Practitioner';

        } else {
            $data ['speciality_name'] = parent::getSingleResult("select name from #_specialities where pid='" . $doctor_data ['speciality'] . "'");
        }
		$data ['name'] = $doctor_data ['name'];
		$data ['email'] = $doctor_data ['email'];
		$data ['type'] = $doctor_data ['type'];
		$data ['phone'] = $doctor_data ['phone'];
		if (substr ( $doctor_data ['phone'], 0, 3 ) === '+95') {
			
			$data ['phone'] = $doctor_data ['phone'];
			// $data['phone'] = substr($data['phone'], 3);
		} else {
			$data ['phone'] = '+95' . $data ['phone'];
		}
		$data ['sex'] = $doctor_data ['sex'];
		$data ['date_of_birth'] = $doctor_data ['date_of_birth'];
		$data ['doctor_type'] = $doctor_data ['doctor_type'];
		$data ['rs_number'] = $doctor_data ['rs_number'];
		$data ['fees'] = $doctor_data ['fees'];
		$data ['education'] = $doctor_data ['education'];
		$data ['location'] = $doctor_data ['location'];
		$data ['area_code'] = $doctor_data ['area_code'];
		$data ['dates'] = json_decode ( $doctor_data ['dates'] );
		$data ['address'] = $doctor_data ['address'];
		$data ['township_id'] = $doctor_data ['township'];
		$data ['division_id'] = $doctor_data ['division'];
		$data ['is_verified'] = $doctor_data ['is_verified'];
		$data ['isHomeservice'] = $doctor_data ['isHomeservice'];
		$data ['isChatNeeded'] = $doctor_data ['isChatNeeded'];
		$data ['years_of_experience'] = $doctor_data ['years_of_experience'];
		$data ['busy_free'] = $doctor_data ['busy_free'];
		$data ['township'] = parent::getSingleResult ( "select name from #_township where pid='" . $doctor_data ['township'] . "'" );
		$data ['division'] = parent::getSingleResult ( "select name from #_division where pid='" . $doctor_data ['division'] . "'" );
		if ($doctor_data ['latlong'] == NULL) {
			$data ['latlong'] = '';
		} else {
			$data ['latlong'] = $doctor_data ['latlong'];
		}
		
		$currentPath = $_SERVER ['PHP_SELF'];
		// output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
		$pathInfo = pathinfo ( $currentPath );
		// output: localhost
		$hostName = $_SERVER ['HTTP_HOST'];
		// output: http://
		$protocol = strtolower ( substr ( $_SERVER ["SERVER_PROTOCOL"], 0, 5 ) ) == 'https://' ? 'https://' : 'http://';
		if ($doctor_data ['id_card'] == NULL) {
			$data ['id_card'] = '';
		} else {
			$data ['id_card'] = SITE_PATH_ADM . "uploaded_files/doctors/idcard/" . $doctor_data ['id_card'];
		}
		if ($doctor_data ['profile_image'] == NULL) {
			$data ['profile_image'] = '';
		} else {
			$data ['profile_image'] = SITE_PATH_ADM . "uploaded_files/doctors/profile/" . $doctor_data ['profile_image'];
		}
		if ($doctor_data ['registration_image'] == NULL) {
			$data ['registration_image'] = '';
		} else {
			$data ['registration_image'] = SITE_PATH_ADM . "uploaded_files/doctors/registration/" . $doctor_data ['registration_image'];
		}
		
		return $data;
	}
	public function signup_step1($data) {
		$user_id = parent::sqlquery ( "rs", 'users_login', $data );
		return $user_id;
	}
	public function signup_step2($doctor_table_data) {
		$doctor_table_data ['shortorder'] = parent::getSingleresult ( "select max(shortorder) as shortorder from #_doctors where 1=1 " ) + 1;
		
		$doctor_id = parent::sqlquery ( "rs", 'doctors', $doctor_table_data );
		
		return $doctor_id;
	}
	public function doctor_details($doctor_user_id) {
		$doctor_query = parent::db_query ( "select * from #_doctors where pid='" . $doctor_user_id . "' " );
		$doctor_data = parent::db_fetch_array ( $doctor_query );
		$data ['doctor_id'] = $doctor_data ['pid'];
		$data ['hospital_id'] = $doctor_data ['hospital_id'];
		$data ['hospital'] = parent::getSingleResult ( "select name from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'" );
		$data ['clinic_id'] = $doctor_data ['clinic_id'];
		$data ['clinic'] = parent::getSingleResult ( "select name from #_clinics where pid='" . $doctor_data ['clinic_id'] . "'" );
		$data ['user_id'] = $doctor_data ['user_id'];
		$data ['speciality'] = $doctor_data ['speciality'];
		if ($doctor_data ['speciality'] == 0) {
            $data ['speciality_name'] = 'General Practitioner';

        } else {
            $data ['speciality_name'] = parent::getSingleResult("select name from #_specialities where pid='" . $doctor_data ['speciality'] . "'");
        }		
		$data ['name'] = $doctor_data ['name'];
		$data ['email'] = $doctor_data ['email'];
		$data ['phone'] = $doctor_data ['phone'];
		
		if (substr ( $doctor_data ['phone'], 0, 3 ) === '+95') {
			
			$data ['phone'] = $doctor_data ['phone'];
			// $data['phone'] = substr($data['phone'], 3);
		} else {
			$data ['phone'] = '+95' . $data ['phone'];
		}
		$data ['sex'] = $doctor_data ['sex'];
		$data ['date_of_birth'] = $doctor_data ['date_of_birth'];
		$data ['doctor_type'] = $doctor_data ['doctor_type'];
		$data ['rs_number'] = $doctor_data ['rs_number'];
		$data ['fees'] = $doctor_data ['fees'];
		$data ['education'] = $doctor_data ['education'];
		$data ['location'] = $doctor_data ['location'];
		$data ['area_code'] = $doctor_data ['area_code'];
		$data ['dates'] = json_decode ( $doctor_data ['dates'] );
		$data ['address'] = $doctor_data ['address'];
		$data ['township_id'] = $doctor_data ['township'];
		$data ['division_id'] = $doctor_data ['division'];
		$data ['is_verified'] = $doctor_data ['is_verified'];
		$data ['isHomeservice'] = $doctor_data ['isHomeservice'];
		$data ['is_voip'] = $doctor_data ['is_voip'];
		$data ['isChatNeeded'] = $doctor_data ['isChatNeeded'];
		$data ['years_of_experience'] = $doctor_data ['years_of_experience'];
		$data ['busy_free'] = $doctor_data ['busy_free'];
		$data ['township'] = parent::getSingleResult ( "select name from #_township where pid='" . $doctor_data ['township'] . "'" );
		$data ['division'] = parent::getSingleResult ( "select name from #_division where pid='" . $doctor_data ['division'] . "'" );
		
		$data ['unread_appointment_count'] = parent::getSingleResult ( "select count(pid) from #_appointment where doctor_id='" . $doctor_user_id . "' and status=1 AND STR_TO_DATE(app_date, '%Y-%m-%d')>='" . date ( 'Y-m-d' ) . "'" );
		$data ['unread_chat_count'] = parent::getSingleResult ( "select count(pid) from #_chat_details where doctor_id='" . $doctor_user_id . "' and chat_from='P' and un_read=1" );
		$data ['completed_feedback_count'] = parent::getSingleResult ( "select count(app.pid) from #_appointment as app LEFT JOIN #_call_details as cd on cd.pid=app.call_details_id  where cd.doctor_rated > 0 and app.status=3  " );
		$data ['unread_wallet_count'] = parent::getSingleResult ( "SELECT count(cnd.pid) FROM crm_notification_details cnd WHERE cnd.title Like '%Wallet%' And cnd.receiver_id='".$doctor_user_id."' and cnd.receiver_type='D' ANd cnd.un_read=1" );
		
		if ($doctor_data ['latlong'] == NULL) {
			$data ['latlong'] = '';
		} else {
			$data ['latlong'] = $doctor_data ['latlong'];
		}
		
		if ($doctor_data ['id_card'] == NULL) {
			$data ['id_card'] = '';
		} else {
			$data ['id_card'] = SITE_PATH_ADM . "uploaded_files/doctors/idcard/" . $doctor_data ['id_card'];
		}
		if ($doctor_data ['profile_image'] == NULL) {
			$data ['profile_image'] = '';
		} else {
			$data ['profile_image'] = SITE_PATH_ADM . "uploaded_files/doctors/profile/" . $doctor_data ['profile_image'];
		}
		if ($doctor_data ['registration_image'] == NULL) {
			$data ['registration_image'] = '';
		} else {
			$data ['registration_image'] = SITE_PATH_ADM . "uploaded_files/doctors/registration/" . $doctor_data ['registration_image'];
		}
		
		return $data;
	}
	public function doctor_short_details($doctor_user_id) {
		$doctor_query = parent::db_query ( "select * from #_doctors where pid='" . $doctor_user_id . "' " );
		$doctor_data = parent::db_fetch_array ( $doctor_query );
		$data ['doctor_id'] = $doctor_data ['pid'];
		$data ['email'] = $doctor_data ['email'];
		$data ['hospital'] = parent::getSingleResult ( "select name from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'" );
		$data ['clinic'] = parent::getSingleResult ( "select name from #_clinics where pid='" . $doctor_data ['clinic_id'] . "'" );
		$data ['speciality'] = $doctor_data ['speciality'];
		if ($doctor_data ['speciality'] == 0) {
            $data ['speciality_name'] = 'General Practitioner';

        } else {
            $data ['speciality_name'] = parent::getSingleResult("select name from #_specialities where pid='" . $doctor_data ['speciality'] . "'");
        }
		$data ['name'] = $doctor_data ['name'];
		$data ['education'] = $doctor_data ['education'];
		$data ['location'] = $doctor_data ['location'];
		$data ['address'] = $doctor_data ['address'];
		$data ['is_voip'] = $doctor_data ['is_voip'];
		$data ['isChatNeeded'] = $doctor_data ['isChatNeeded'];
		$data ['years_of_experience'] = $doctor_data ['years_of_experience'];
		$data ['busy_free'] = $doctor_data ['busy_free'];
		$data ['township'] = parent::getSingleResult ( "select name from #_township where pid='" . $doctor_data ['township'] . "'" );
		$data ['division'] = parent::getSingleResult ( "select name from #_division where pid='" . $doctor_data ['division'] . "'" );
		if ($doctor_data ['latlong'] == NULL) {
			$data ['latlong'] = '';
		} else {
			$data ['latlong'] = $doctor_data ['latlong'];
		}
		$data ['ratings'] = 3;
		$currentPath = $_SERVER ['PHP_SELF'];
		// output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
		$pathInfo = pathinfo ( $currentPath );
		// output: localhost
		$hostName = $_SERVER ['HTTP_HOST'];
		// output: http://
		$protocol = strtolower ( substr ( $_SERVER ["SERVER_PROTOCOL"], 0, 5 ) ) == 'https://' ? 'https://' : 'http://';
		
		if ($doctor_data ['profile_image'] == NULL) {
			$data ['profile_image'] = '';
		} else {
			$data ['profile_image'] = SITE_PATH_ADM . "uploaded_files/doctors/profile/" . $doctor_data ['profile_image'];
		}
		
		return $data;
	}
	public function hospital_name_list($lang_flag) {
		$data = array ();
		$query = parent::db_query ( "select * from #_hospitals where status=1 and is_verified=1 " );
		if (mysql_num_rows ( $query ) > 0) {
			$total_count = mysql_num_rows ( $query );
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				$data [$i] ['pid'] = $row ['pid'];
				if ($lang_flag == "en") {
					$data [$i] ['name'] = $row ['name'];
					$data [$i] ['address'] = $row ['address'];
				} else {
					$data [$i] ['name_be'] = $row ['name_be'];
					$data [$i] ['address_be'] = $row ['address_be'];
				}
				if ($row ['latlong'] == NULL) {
					$data [$i] ['latlong'] = '';
				} else {
					$data [$i] ['latlong'] = $row ['latlong'];
				}
				$data [$i] ['type'] = 'hospital';
				$i ++;
				$last_pid = $row ['pid'];
				
				/*
				 * if ($total_count == $i) {
				 * $data [$i] ['pid'] = $last_pid + 1;
				 * $data [$i] ['name'] = 'Others';
				 * $data [$i] ['address'] = '';
				 * $data [$i] ['latlong'] = '';
				 * $data [$i] ['type'] = 'clinic';
				 * }
				 */
			}
		} else {
			return 'false';
		}
		
		return $data;
	}
	public function clinics_name_list($lang_flag) {
		$data = array ();
		$query = parent::db_query ( "select * from #_clinics where status=1 and is_verified=1" );
		if (mysql_num_rows ( $query ) > 0) {
			$total_count = mysql_num_rows ( $query );
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				$data [$i] ['pid'] = $row ['pid'];
				if ($lang_flag == "en") {
					$data [$i] ['name'] = $row ['name'];
					$data [$i] ['address'] = $row ['address'];
				} else {
					$data [$i] ['name_be'] = $row ['name_be'];
					$data [$i] ['address_be'] = $row ['address_be'];
				}
				if ($row ['latlong'] == NULL) {
					$data [$i] ['latlong'] = '';
				} else {
					$data [$i] ['latlong'] = $row ['latlong'];
				}
				$data [$i] ['type'] = 'clinic';
				$i ++;
				$last_pid = $row ['pid'];
				
				/*
				 * if ($total_count == $i) {
				 * $data [$i] ['pid'] = $last_pid + 1;
				 * $data [$i] ['name'] = 'Others';
				 * $data [$i] ['address'] = '';
				 * $data [$i] ['latlong'] = '';
				 * $data [$i] ['type'] = 'clinic';
				 * }
				 */
			}
		} else {
			return 'false';
		}
		
		return $data;
	}
	public function send_res($json) {
		echo json_encode ( $json );
	}
	public function check_hospital_id_exist($doctor_id, $location_type, $hospital_id) {
		// $location_type = 1 means home location //$location_type = 2 means work location
		$query = parent::db_query ( "select * from #_doctors_locations where doctor_id='" . $doctor_id . "' and location_type='" . $location_type . "' and hospital_id='" . $hospital_id . "' and isDeleted=0 " );
		if (mysql_num_rows ( $query ) > 0) {
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				$data [$i] ['pid'] = $row ['pid'];
				$data [$i] ['doctor_id'] = $row ['doctor_id'];
				$data [$i] ['location_type'] = $row ['location_type'];
				$data [$i] ['hospital_id'] = $row ['hospital_id'];
				$data [$i] ['clinic_id'] = $row ['clinic_id'];
				$data [$i] ['location'] = $row ['location'];
				$data [$i] ['location_my'] = $row ['location_my'];
				$data [$i] ['area_code'] = $row ['area_code'];
				$data [$i] ['dates'] = $row ['dates'];
				$data [$i] ['status'] = $row ['status'];
				$i ++;
			}
			$data ['query_status'] = 'true';
			return $data;
		} else {
			$data ['query_status'] = 'false';
			return $data;
		}
	}
	public function edit_check_hospital_id_exist($doctor_id, $location_type, $hospital_id, $location_id) {
		// $location_type = 1 means home location //$location_type = 2 means work location
		$query = parent::db_query ( "select * from #_doctors_locations where doctor_id='" . $doctor_id . "' and location_type='" . $location_type . "' and hospital_id='" . $hospital_id . "' and pid!='" . $location_id . "' and isDeleted=0 " );
		if (mysql_num_rows ( $query ) > 0) {
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				$data [$i] ['pid'] = $row ['pid'];
				$data [$i] ['doctor_id'] = $row ['doctor_id'];
				$data [$i] ['location_type'] = $row ['location_type'];
				$data [$i] ['hospital_id'] = $row ['hospital_id'];
				$data [$i] ['clinic_id'] = $row ['clinic_id'];
				$data [$i] ['location'] = $row ['location'];
				$data [$i] ['location_my'] = $row ['location_my'];
				$data [$i] ['area_code'] = $row ['area_code'];
				$data [$i] ['dates'] = $row ['dates'];
				$data [$i] ['status'] = $row ['status'];
				$i ++;
			}
			$data ['query_status'] = 'true';
			return $data;
		} else {
			$data ['query_status'] = 'false';
			return $data;
		}
	}
	public function check_clinic_id_exist($doctor_id, $location_type, $clinic_id) {
		$data = array ();
		// $location_type = 1 means home location //$location_type = 2 means work location
		$query = parent::db_query ( "select * from #_doctors_locations where doctor_id='" . $doctor_id . "' and location_type='" . $location_type . "' and clinic_id='" . $clinic_id . "' and isDeleted=0 " );
		if (mysql_num_rows ( $query ) > 0) {
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				$data [$i] ['pid'] = $row ['pid'];
				$data [$i] ['doctor_id'] = $row ['doctor_id'];
				$data [$i] ['location_type'] = $row ['location_type'];
				$data [$i] ['hospital_id'] = $row ['hospital_id'];
				$data [$i] ['clinic_id'] = $row ['clinic_id'];
				$data [$i] ['location'] = $row ['location'];
				$data [$i] ['location_my'] = $row ['location_my'];
				$data [$i] ['area_code'] = $row ['area_code'];
				$data [$i] ['dates'] = $row ['dates'];
				$data [$i] ['status'] = $row ['status'];
				$i ++;
			}
			$data ['query_status'] = 'true';
			return $data;
		} else {
			$data ['query_status'] = 'false';
			return $data;
		}
	}
	public function edit_check_clinic_id_exist($doctor_id, $location_type, $clinic_id, $location_id) {
		$data = array ();
		// $location_type = 1 means home location //$location_type = 2 means work location
		$query = parent::db_query ( "select * from #_doctors_locations where doctor_id='" . $doctor_id . "' and location_type='" . $location_type . "' and clinic_id='" . $clinic_id . "' and pid!='" . $location_id . "' and isDeleted=0 " );
		if (mysql_num_rows ( $query ) > 0) {
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				$data [$i] ['pid'] = $row ['pid'];
				$data [$i] ['doctor_id'] = $row ['doctor_id'];
				$data [$i] ['location_type'] = $row ['location_type'];
				$data [$i] ['hospital_id'] = $row ['hospital_id'];
				$data [$i] ['clinic_id'] = $row ['clinic_id'];
				$data [$i] ['location'] = $row ['location'];
				$data [$i] ['location_my'] = $row ['location_my'];
				$data [$i] ['area_code'] = $row ['area_code'];
				$data [$i] ['dates'] = $row ['dates'];
				$data [$i] ['status'] = $row ['status'];
				$i ++;
			}
			$data ['query_status'] = 'true';
			return $data;
		} else {
			$data ['query_status'] = 'false';
			return $data;
		}
	}
	public function timeslot_loop($timeslots, $doctor_id) {
		$timeslots_exist = false;
		$time_slot_details = array ();
		// print_r($timeslots);
		
		foreach ( $timeslots as $day => $category ) {
			// echo $day . '<br />';
			$weekday = 0;
			if ($day == 'monday') {
				$weekday = 1;
			}
			if ($day == 'tuesday') {
				$weekday = 2;
			}
			if ($day == 'wednesday') {
				$weekday = 3;
			}
			if ($day == 'thursday') {
				$weekday = 4;
			}
			if ($day == 'friday') {
				$weekday = 5;
			}
			if ($day == 'saturday') {
				$weekday = 6;
			}
			if ($day == 'sunday') {
				$weekday = 7;
			}
			
			$fromarr = $category->from;
			$toarr = $category->to;
			// print_r($fromarr);
			// print_r($toarr);
			$count = count ( $toarr );
			for($i = 0; $i < $count; $i ++) {
				// echo $from_and_to = $fromarr[$i] . '-' . $toarr[$i];
				$start_time = $fromarr [$i];
				$end_time = $toarr [$i];
				if (! empty ( $start_time ) || ! empty ( $end_time )) {
					$start_time = date ( "G:i:s", strtotime ( $start_time ) );
					$end_time = date ( "G:i:s", strtotime ( $end_time ) );
					
					$time_slot_exist = self::check_timeslot ( $doctor_id, $weekday, $start_time, $end_time );
					if ($time_slot_exist == 'true') {
						$time_slot_details ['time_slot_is_exist'] = $time_slot_exist;
						$exist_weekday [] = $weekday;
					}
				}
			}
		}
		$time_slot_details ['time_slot_is_exist_weedkday'] = $exist_weekday;
		return $time_slot_details;
	}
	public function check_timeslot($doctor_id, $weekdays, $start_time, $end_time) {
		// $location_type = 1 means home location //$location_type = 2 means work location
		// echo '<br>';
		// echo $sql = "SELECT * FROM `#_doctors_locations_time_slots` WHERE doctor_id='" . $doctor_id . "' AND location_id='" . $location_id . "' AND weekdays='" . $weekdays . "' AND start_time BETWEEN '" . $start_time . "' AND '" . $end_time . "' OR end_time BETWEEN '" . $start_time . "' AND '" . $end_time . "' AND status=1 ";
		
		// check whether the given time slot is inbetween the existing time slot
		$sql = "SELECT * FROM `#_doctors_locations_time_slots` WHERE doctor_id='" . $doctor_id . "' AND weekdays='" . $weekdays . "' AND((start_time <= '" . $start_time . "' AND end_time > '" . $start_time . "')  OR   (start_time < '" . $end_time . "' AND end_time >= '" . $end_time . "')) AND status=1 and isDeleted=0 ";
		// echo '<br>';
		$query = parent::db_query ( $sql );
		if (mysql_num_rows ( $query ) > 0) {
			return 'true';
		} else {
			return 'false';
		}
	}
	
	// edit time slot loop check
	public function edit_timeslot_loop($timeslots, $doctor_id, $location_id) {
		$timeslots_exist = false;
		$time_slot_details = array ();
		// print_r($timeslots);
		
		foreach ( $timeslots as $day => $category ) {
			// echo $day . '<br />';
			$weekday = 0;
			if ($day == 'monday') {
				$weekday = 1;
			}
			if ($day == 'tuesday') {
				$weekday = 2;
			}
			if ($day == 'wednesday') {
				$weekday = 3;
			}
			if ($day == 'thursday') {
				$weekday = 4;
			}
			if ($day == 'friday') {
				$weekday = 5;
			}
			if ($day == 'saturday') {
				$weekday = 6;
			}
			if ($day == 'sunday') {
				$weekday = 7;
			}
			
			$fromarr = $category->from;
			$toarr = $category->to;
			// print_r($fromarr);
			// print_r($toarr);
			
			$count = count ( $toarr );
			for($i = 0; $i < $count; $i ++) {
				// echo $from_and_to = $fromarr[$i] . '-' . $toarr[$i];
				$start_time = $fromarr [$i];
				$end_time = $toarr [$i];
				if (! empty ( $start_time ) || ! empty ( $end_time )) {
					$start_time = date ( "G:i:s", strtotime ( $start_time ) );
					$end_time = date ( "G:i:s", strtotime ( $end_time ) );
					
					$time_slot_exist = self::edit_check_timeslot ( $doctor_id, $location_id, $weekday, $start_time, $end_time );
					if ($time_slot_exist == 'true') {
						// echo $time_slot_exist;
						$time_slot_details ['time_slot_is_exist'] = $time_slot_exist;
						$exist_weekday [] = $weekday;
					} else {
						// echo $time_slot_exist;
					}
				}
			}
		}
		$time_slot_details ['time_slot_is_exist_weedkday'] = $exist_weekday;
		return $time_slot_details;
	}
	public function edit_check_timeslot($doctor_id, $location_id, $weekdays, $start_time, $end_time) {
		// $location_type = 1 means home location //$location_type = 2 means work location
		// echo '<br>';
		// echo $sql = "SELECT * FROM `#_doctors_locations_time_slots` WHERE doctor_id='" . $doctor_id . "' AND location_id='" . $location_id . "' AND weekdays='" . $weekdays . "' AND start_time BETWEEN '" . $start_time . "' AND '" . $end_time . "' OR end_time BETWEEN '" . $start_time . "' AND '" . $end_time . "' AND status=1 ";
		
		// check whether the given time slot is inbetween the existing time slot
		$sql = "SELECT * FROM `#_doctors_locations_time_slots` WHERE doctor_id='" . $doctor_id . "' AND weekdays='" . $weekdays . "' AND((start_time <= '" . $start_time . "' AND end_time > '" . $start_time . "')  OR   (start_time < '" . $end_time . "' AND end_time >= '" . $end_time . "')) AND status=1 AND location_id!='" . $location_id . "' and isDeleted=0 ";
		// echo '<br>';
		$query = parent::db_query ( $sql );
		if (mysql_num_rows ( $query ) > 0) {
			return 'true';
		} else {
			return 'false';
		}
	}
	public function check_location_count($doctor_id) {
		// $location_type = 1 means home location //$location_type = 2 means work location
		$query = parent::db_query ( "select * from #_doctors_locations where doctor_id='" . $doctor_id . "' and status=1 and isDeleted=0  " );
		if (mysql_num_rows ( $query ) > 0) {
			$location_count = mysql_num_rows ( $query );
			return $location_count;
		} else {
			return 'false';
		}
	}
	public function check_location_type_count($doctor_id, $location_type) {
		// $location_type = 1 means home location //$location_type = 2 means work location
		$query = parent::db_query ( "select * from #_doctors_locations where doctor_id='" . $doctor_id . "' and location_type='" . $location_type . "'  and isDeleted=0  " );
		if (mysql_num_rows ( $query ) > 0) {
			$location_count = mysql_num_rows ( $query );
			return $location_count;
		} else {
			return 'false';
		}
	}
	public function check_detail_already_exist($doctor_id, $location_type, $lang_flag, $location_1, $location_2) {
		if ($lang_flag == 'en') {
			$location = $location_1 . '</br>' . $location_2;
			$query = parent::db_query ( "select * from #_doctors_locations where doctor_id='" . $doctor_id . "' and location_type='" . $location_type . "' and  location='" . $location . "' and status=1 and isDeleted=0 " );
		} else {
			$location_my = $location_1 . '</br>' . $location_2;
			$query = parent::db_query ( "select * from #_doctors_locations where doctor_id='" . $doctor_id . "' and location_type='" . $location_type . "' and  location_my='" . $location_my . "'  and status=1 and isDeleted=0  " );
		}
		
		if (mysql_num_rows ( $query ) > 0) {
			return 'true';
		} else {
			return 'false';
		}
	}
	public function add_location_insert_step1($doctor_table_insert_details) {
		// update
		if ($doctor_table_insert_details ['clinic_id'] != 0) {
			$dates = $doctor_table_insert_details ['dates'];
			$dates = json_encode ( $dates );
			$sql = "UPDATE #_doctors SET 
            clinic_id='" . $doctor_table_insert_details ['clinic_id'] . "'  ,  hospital_id=0, location='" . $doctor_table_insert_details ['location'] . "'
          , dates='" . $dates . "'     
           WHERE pid='" . $doctor_table_insert_details ['doctor_id'] . "'  ";
			$query = parent::db_query ( $sql );
			
			return 'true';
		} else {
			$dates = $doctor_table_insert_details ['dates'];
			$dates = json_encode ( $dates );
			
			$sql = "UPDATE #_doctors SET 
            hospital_id='" . $doctor_table_insert_details ['hospital_id'] . "' ,  clinic_id=0
          ,  location='" . $doctor_table_insert_details ['location'] . "'
          , dates='" . $dates . "' 
           WHERE pid='" . $doctor_table_insert_details ['doctor_id'] . "' ";
			$query = parent::db_query ( $sql );
			return 'true';
		}
	}
	public function add_location_insert_step2($doctor_table_insert_details) {
		// insert the details
		$result = parent::sqlquery ( "rs", 'doctors_locations', $doctor_table_insert_details );
		return $result;
		// return $doctor_table_insert_details2;
	}
	public function add_location_insert_step3($timeslots, $doctor_id, $location_id) {
		$time_slot_details = array ();
		
		foreach ( $timeslots as $day => $category ) {
			// echo $day . '<br />';
			$weekday = 0;
			if ($day == 'monday') {
				$weekday = 1;
			}
			if ($day == 'tuesday') {
				$weekday = 2;
			}
			if ($day == 'wednesday') {
				$weekday = 3;
			}
			if ($day == 'thursday') {
				$weekday = 4;
			}
			if ($day == 'friday') {
				$weekday = 5;
			}
			if ($day == 'saturday') {
				$weekday = 6;
			}
			if ($day == 'sunday') {
				$weekday = 7;
			}
			
			$fromarr = $category->from;
			$toarr = $category->to;
			// print_r($fromarr);
			// print_r($toarr);
			$count = count ( $toarr );
			
			for($i = 0; $i < $count; $i ++) {
				$from_and_to = $fromarr [$i] . '-' . $toarr [$i];
				$start_time = $fromarr [$i];
				$end_time = $toarr [$i];
				// echo $start_time.''.$end_time;
				if (! empty ( $start_time ) || ! empty ( $end_time )) {
					$start_time = date ( "G:i:s", strtotime ( $start_time ) );
					$end_time = date ( "G:i:s", strtotime ( $end_time ) );
					$available_status = 1;
					$status = 1;
					$time_slot_exist = self::insert_timeslot ( $doctor_id, $location_id, $weekday, $start_time, $end_time, $available_status, $status );
				}
			}
		}
		return true;
	}
	public function insert_timeslot($doctor_id, $location_id, $weekday, $start_time, $end_time, $available_status, $status) {
		$timeslot_insert_details = array (
				'doctor_id' => $doctor_id,
				'location_id' => $location_id,
				'weekdays' => $weekday,
				'start_time' => $start_time,
				'end_time' => $end_time,
				'available_status' => $available_status,
				'status' => $status 
		);
		// print_r($timeslot_insert_details);
		// insert the details
		$result = parent::sqlquery ( "rs", 'doctors_locations_time_slots', $timeslot_insert_details );
		return $result;
	}
	public function edit_location_update_step2($doctor_table_insert_details) {
		// update
		if ($doctor_table_insert_details ['clinic_id'] != 0) {
			
			$dates = $doctor_table_insert_details ['dates'];
			// $dates = json_encode($dates);
			$sql = "UPDATE #_doctors_locations SET 
            clinic_id='" . $doctor_table_insert_details ['clinic_id'] . "'  ,location_type='" . $doctor_table_insert_details ['location_type'] . "',  hospital_id=0, location='" . $doctor_table_insert_details ['location'] . "', latlong='" . $doctor_table_insert_details ['latlong'] . "' , isHomeservice='" . $doctor_table_insert_details ['isHomeservice'] . "' , isChatNeeded='" . $doctor_table_insert_details ['isChatNeeded'] . "'
          , dates='" . $dates . "' , status='" . $doctor_table_insert_details ['status'] . "', is_verified='" . $doctor_table_insert_details ['is_verified'] . "'
           WHERE  pid='" . $doctor_table_insert_details ['location_id'] . "' and status=1 and isDeleted=0  ";
			$query = parent::db_query ( $sql );
			return 'true';
		} else {
			$dates = $doctor_table_insert_details ['dates'];
			// $dates = json_encode($dates);
			
			$sql = "UPDATE #_doctors_locations SET 
            hospital_id='" . $doctor_table_insert_details ['hospital_id'] . "' ,  clinic_id=0 ,location_type='" . $doctor_table_insert_details ['location_type'] . "'  ,  location='" . $doctor_table_insert_details ['location'] . "', latlong='" . $doctor_table_insert_details ['latlong'] . "', isHomeservice='" . $doctor_table_insert_details ['isHomeservice'] . "' , isChatNeeded='" . $doctor_table_insert_details ['isChatNeeded'] . "'
          , dates='" . $dates . "'  , status='" . $doctor_table_insert_details ['status'] . "', is_verified='" . $doctor_table_insert_details ['is_verified'] . "'
           WHERE  pid='" . $doctor_table_insert_details ['location_id'] . "' and status=1 and isDeleted=0  ";
			$query = parent::db_query ( $sql );
			return 'true';
		}
	}
	public function delete_location_time_slot_details($doctor_id, $location_id) {
		// $sql = "DELETE FROM `#_doctors_locations_time_slots` WHERE `doctor_id`='" . $doctor_id . "' AND `location_id`='" . $location_id . "' and status=1 and isDeleted=0 ";
		// $query = parent::db_query($sql);
		$loc_details = array (
				'isDeleted' => 1 
		);
		$update_id = parent::sqlquery ( "rs", 'doctors_locations_time_slots', $loc_details, $update = 'location_id', $id = $location_id );
		return 'true';
	}
	
	// newly done the changes
	public function delete_doc_location($doctor_id, $location_id) {
		$gcm = new GCM ();
		$sql_tes = "select * FROM `#_doctors_locations` WHERE `doctor_id`='" . $doctor_id . "' and `pid`='" . $location_id . "' and status=1 and isDeleted=0 ";
		$query = parent::db_query ( $sql_tes );
		if (mysql_num_rows ( $query ) > 0) {
			
			// $sql = "DELETE FROM `#_doctors_locations` WHERE `pid`='" . $location_id . "' and status=1 and isDeleted=0 ";
			// $query = parent::db_query($sql);
			$loc_details = array (
					'isDeleted' => 1 
			);
			$update_id = parent::sqlquery ( "rs", 'doctors_locations', $loc_details, $update = 'pid', $id = $location_id );
			
			self::delete_location_time_slot_details ( $doctor_id, $location_id );
			
			$latest_location = parent::getSingleresult ( "select max(pid) as pid from #_doctors_locations where `doctor_id`='" . $doctor_id . "' and status=1 and isDeleted=0 " );
			if (! empty ( $latest_location )) {
				$latest_doctor_location_details = self::get_selected_doctor_location_details ( $latest_location );
				$location_type = $latest_doctor_location_details ['location_type'];
				// $doctor_table_insert_details['lang_flag']=$obj->{'lang_flag'};
				$doctor_table_insert_details ['doctor_id'] = $latest_doctor_location_details ['doctor_id'];
				$doctor_table_insert_details ['clinic_id'] = $latest_doctor_location_details ['clinic_id'];
				$doctor_table_insert_details ['hospital_id'] = $latest_doctor_location_details ['hospital_id'];
				$doctor_table_insert_details ['location'] = $latest_doctor_location_details ['location'];
				$doctor_table_insert_details ['isChatNeeded'] = $latest_doctor_location_details ['isChatNeeded'];
				// $doctor_table_insert_details['status']=$obj->{'status'};
				$doctor_table_insert_details ['dates'] = $latest_doctor_location_details ['dates'];
			} else {
				// $doctor_table_insert_details['lang_flag']=$obj->{'lang_flag'};
				$doctor_table_insert_details ['doctor_id'] = $doctor_id;
				$doctor_table_insert_details ['clinic_id'] = 0;
				$doctor_table_insert_details ['hospital_id'] = 0;
				$doctor_table_insert_details ['location'] = '';
				$doctor_table_insert_details ['isChatNeeded'] = 0;
				$doctor_table_insert_details ['dates'] = NULL;
			}
			
			// print_r($doctor_table_insert_details);
			// update details into doctor table
			self::add_location_insert_step1 ( $doctor_table_insert_details );
			
			// Cancel all the appointments comes within the unavailable date range.
			$appointment_query = parent::db_query ( "SELECT * FROM `#_appointment` WHERE doctor_id='" . $doctor_id . "' AND doctor_location_id='" . $location_id . "' AND status=1  " );
			if (mysql_num_rows ( $appointment_query ) > 0) {
				$i = 0;
				while ( $appointment_data = parent::db_fetch_array ( $appointment_query ) ) {
					$appointment_id = $appointment_data ['pid'];
					$patient_id = $appointment_data ['patient_id'];
					$call_details_id = $appointment_data ['call_details_id'];
					$appointment_update_details = array (
							'status' => 4 
					);
					
					// Update Appointment status into cancel
					$udpated_id = parent::sqlquery ( "rs", 'appointment', $appointment_update_details, $update = 'pid', $id = $appointment_id );
					
					// send push notification to the patient
					$patient_gsm_tocken = parent::getSingleResult ( "select gsm_tocken from #_patients where pid='" . $patient_id . "'" );
					
					if ($patient_gsm_tocken) {
						$doctor_name = parent::getSingleResult ( "select name from #_doctors where pid=" . $doctor_id );
						$registatoin_ids = $patient_gsm_tocken;
						$message = array (
								"message" => "Appointment reference id(" . $appointment_id . ")is canceled by Dr." . $doctor_name,
								"flag" => 'appointment_cancelled' 
						);
						$is_ios_device =  parent::getSingleResult ( "select is_ios_device from #_patients where pid=" . $patient_id );
						$result = $gcm->send_notification ( $registatoin_ids, $message, "Appointment Canceled", $doctor_id, 'D', $patient_id, 'P',$is_ios_device );
					}
					// Unavailable date appointment cancel
					$doctor_user_id = parent::getSingleResult ( "select user_id from #_doctors where pid='" . $doctor_id . "'" );
					$doctor_gsm_tocken = parent::getSingleResult ( "select gsm_tocken from #_gcmuser where user_id='" . $doctor_user_id . "'" );
					if ($doctor_gsm_tocken) {
						$doctor_name = parent::getSingleResult ( "select name from #_doctors where pid='" . $doctor_id . "'" );
						$message = array (
								"message" => "Appointment reference id(" . $appointment_id . ")is canceled in the unavailable dates.",
								"flag" => 'appointment_cancelled' 
						);
						$is_ios_device = parent::getSingleResult ( "select is_ios_device from #_doctors where pid=" . $doctor_id );
						$result = $gcm->send_notification ( $doctor_gsm_tocken, $message, "Appointment Canceled", $patient_id, 'P', $doctor_id, 'D',$is_ios_device );
					}
					$i ++;
				}
			}
			
			return 'true';
		} else {
			return 'false';
		}
	}
	public function get_selected_doctor_location_details($location_id) {
		$doctor_query = parent::db_query ( "select * from #_doctors_locations where `pid`='" . $location_id . "'  " );
		if (mysql_num_rows ( $doctor_query ) > 0) {
			$doctor_location_data = parent::db_fetch_array ( $doctor_query );
			
			$data ['doctor_id'] = $doctor_location_data ['doctor_id'];
			$data ['location_type'] = $doctor_location_data ['location_type'];
			$data ['hospital_id'] = $doctor_location_data ['hospital_id'];
			$data ['clinic_id'] = $doctor_location_data ['clinic_id'];
			$data ['location'] = $doctor_location_data ['location'];
			$location = explode ( "~", $doctor_location_data ['location'] );
			$data ['location_1'] = $location [0];
			$data ['location_2'] = $location [1];
			$data ['location_my'] = $doctor_location_data ['location_my'];
			$data ['area_code'] = $doctor_location_data ['area_code'];
			$data ['dates'] = json_decode ( $doctor_location_data ['dates'] );
			$data ['isChatNeeded'] = parent::getSingleResult ( "select isChatNeeded from #_doctors where pid='" . $data [$i] ['doctor_id'] . "'" );
			$data ['status'] = $doctor_location_data ['status'];
			$data ['created_on'] = $doctor_location_data ['created_on'];
			$data ['modified_on'] = $doctor_location_data ['modified_on'];
			$data ['create_by'] = $doctor_location_data ['create_by'];
			$data ['is_verified'] = $doctor_location_data ['is_verified'];
			if ($doctor_location_data ['latlong'] == NULL) {
				$data ['latlong'] = '';
			} else {
				$data ['latlong'] = $doctor_location_data ['latlong'];
			}
			$doctor_location_data ['isHomeservice'] = parent::getSingleResult ( "select isHomeservice from #_doctors where pid='" . $data [$i] ['doctor_id'] . "'" );
			if ($doctor_location_data ['isHomeservice'] == 0) {
				$data ['isHomeservice'] = "0";
			} else {
				$data ['isHomeservice'] = "" . $doctor_location_data ['isHomeservice'] . "";
			}
			
			return $data;
		} else {
			return 'false';
		}
	}
	public function given_weekday_num_find($today_weekday) {
		$weekday = 0;
		if ($today_weekday == 'Monday') {
			$weekday = 1;
		}
		if ($today_weekday == 'Tuesday') {
			$weekday = 2;
		}
		if ($today_weekday == 'Wednesday') {
			$weekday = 3;
		}
		if ($today_weekday == 'Thursday') {
			$weekday = 4;
		}
		if ($today_weekday == 'Friday') {
			$weekday = 5;
		}
		if ($today_weekday == 'Saturday') {
			$weekday = 6;
		}
		if ($today_weekday == 'Sunday') {
			$weekday = 7;
		}
		return $weekday;
	}
	public function today_weekday_num_find() {
		$today_date = date ( "Y-m-d" );
		$current_time = date ( "H:i:s" );
		$today_weekday = date ( "l" );
		$weekday = 0;
		if ($today_weekday == 'Monday') {
			$weekday = 1;
		}
		if ($today_weekday == 'Tuesday') {
			$weekday = 2;
		}
		if ($today_weekday == 'Wednesday') {
			$weekday = 3;
		}
		if ($today_weekday == 'Thursday') {
			$weekday = 4;
		}
		if ($today_weekday == 'Friday') {
			$weekday = 5;
		}
		if ($today_weekday == 'Saturday') {
			$weekday = 6;
		}
		if ($today_weekday == 'Sunday') {
			$weekday = 7;
		}
		return $weekday;
	}
	public function set_isHomeservice($doctor_id, $isHomeservice) {
		$query = parent::db_query ( "select * from #_doctors  where `pid`='" . $doctor_id . "' " );
		if (mysql_num_rows ( $query ) > 0) {
			$sql = "UPDATE #_doctors SET 
            isHomeservice='" . $isHomeservice . "' 
           WHERE  pid='" . $doctor_id . "'  ";
			$query = parent::db_query ( $sql );
			return 'true';
		} else {
			return 'false';
		}
	}
	public function set_voip($doctor_id, $set_voip) {
		$query = parent::db_query ( "select * from #_doctors  where `pid`='" . $doctor_id . "' " );
		if (mysql_num_rows ( $query ) > 0) {
			$sql = "UPDATE #_doctors SET 
            is_voip='" . $set_voip . "' 
           WHERE  pid='" . $doctor_id . "'  ";
			$query = parent::db_query ( $sql );
			return 'true';
		} else {
			
			return 'false';
		}
	}
	public function set_busy_free($doctor_id, $set_busy_free) {
		$query = parent::db_query ( "select * from #_doctors  where `pid`='" . $doctor_id . "' " );
		if (mysql_num_rows ( $query ) > 0) {
			$sql = "UPDATE #_doctors SET 
            busy_free='" . $set_busy_free . "' 
           WHERE  pid='" . $doctor_id . "'  ";
			$query = parent::db_query ( $sql );
			return 'true';
		} else {
			
			return 'false';
		}
	}
	public function set_available_date($start_date, $end_date, $locations, $status, $doctor_id) {
		$data = array ();
		$gcm = new GCM ();
		if (! empty ( $locations )) {
			$location_id = $locations;
			$modified_on = date ( 'Y-m-d H:i:s' );
			$weekday = self::today_weekday_num_find ();
			// $locations = explode ( ',', $locations );
			// insert the available in history
			$doctor_unavailable_history_details = array (
					'doctor_location_id' => $location_id,
					'doctor_id' => $doctor_id,
					'available_from' => $start_date,
					'available_to' => $end_date,
					'modified_on' => $modified_on 
			);
			// status 0 means unavailable date
			if ($status == 0) {
				$result = parent::sqlquery ( "rs", 'doctor_unavailable_history', $doctor_unavailable_history_details );
				// Cancel all the appointments comes within the unavailable date range.
				$appointment_query = parent::db_query ( "SELECT * FROM `#_appointment` WHERE doctor_id='" . $doctor_id . "' AND app_date >= '" . $start_date . "'  AND app_date <= '" . $end_date . "' AND doctor_location_id='" . $location_id . "' AND status=1  " );
				if (mysql_num_rows ( $appointment_query ) > 0) {
					$i = 0;
					while ( $appointment_data = parent::db_fetch_array ( $appointment_query ) ) {
						$appointment_id = $appointment_data ['pid'];
						$patient_id = $appointment_data ['patient_id'];
						$call_details_id = $appointment_data ['call_details_id'];
						$appointment_update_details = array (
								'status' => 4 
						);
						
						// Update Appointment status into cancel
						$udpated_id = parent::sqlquery ( "rs", 'appointment', $appointment_update_details, $update = 'pid', $id = $appointment_id );
						
						// send push notification to the patient
						$patient_gsm_tocken = parent::getSingleResult ( "select gsm_tocken from #_patients where pid='" . $patient_id . "'" );
						
						if ($patient_gsm_tocken) {
							$doctor_name = parent::getSingleResult ( "select name from #_doctors where pid=" . $doctor_id );
							$registatoin_ids = $patient_gsm_tocken;
							$message = array (
									"message" => "Appointment reference id(" . $appointment_id . ")is canceled by Dr." . $doctor_name,
									"flag" => 'appointment_cancelled' 
							);
							$is_ios_device = parent::getSingleResult ( "select is_ios_device from #_patients where pid=" . $patient_id );
							$result = $gcm->send_notification ( $registatoin_ids, $message, "Appointment Canceled", $doctor_id, 'D', $patient_id, 'P',$is_ios_device );
						}
						// Unavailable date appointment cancel
						$doctor_user_id = parent::getSingleResult ( "select user_id from #_doctors where pid='" . $doctor_id . "'" );
						$doctor_gsm_tocken = parent::getSingleResult ( "select gsm_tocken from #_gcmuser where user_id='" . $doctor_user_id . "'" );
						if ($doctor_gsm_tocken) {
							$doctor_name = parent::getSingleResult ( "select name from #_doctors where pid='" . $doctor_id . "'" );
							$message = array (
									"message" => "Appointment reference id(" . $appointment_id . ")is canceled in the unavailable dates.",
									"flag" => 'appointment_cancelled' 
							);
							$is_ios_device = parent::getSingleResult ( "select is_ios_device from #_doctors where pid=" . $doctor_id );
							$result = $gcm->send_notification ( $doctor_gsm_tocken, $message, "Appointment Canceled", $patient_id, 'P', $doctor_id, 'D',$is_ios_device );
						}
						$i ++;
					}
				}
			}
			
			$query = parent::db_query ( "select * from #_doctors_locations  where `pid`='" . $location_id . "' and doctor_id= '" . $doctor_id . "' and status=1 and isDeleted=0  " );
			if (mysql_num_rows ( $query ) > 0) {
				
				$sql = "UPDATE #_doctors_locations SET 
            available_from='" . $start_date . "' , available_to='" . $end_date . "' , available_status='" . $status . "' 
           WHERE  pid='" . $location_id . "' and doctor_id= '" . $doctor_id . "' and status=1 and isDeleted=0  ";
				
				$query = parent::db_query ( $sql );
				$quer_status = 'true';
			} else {
				
				// $missing_location_ids [] = $location_id;
				$quer_status = 'false';
			}
		} else {
			$quer_status = 'false'; // NO Location ids are given as input
		}
		$data ['status'] = $quer_status;
		// $data ['missing_location_ids'] = $missing_location_ids;
		
		return $data;
	}
	public function check_available_unavailable($doctor_id, $location_id) {
		if (! empty ( $location_id ) && ! empty ( $doctor_id )) {
			// date_default_timezone_set("Asia/Karachi");
			date_default_timezone_set ( "Asia/Yangon" );
			$today_date = date ( "Y-m-d" );
			$current_time = date ( "H:i:s" );
			$today_weekday = date ( "l" );
			$weekday = self::today_weekday_num_find ();
			
			// check whether the doctor is free or busy
			$query_busy_free = parent::db_query ( "select * from #_doctors  where `pid`='" . $doctor_id . "'  and busy_free=1 " );
			if (mysql_num_rows ( $query_busy_free ) > 0) {
				
				// check whether the location is available
				$query = parent::db_query ( "select * from #_doctors_locations  where `pid`='" . $location_id . "' and doctor_id= '" . $doctor_id . "' and status=1 and isDeleted=0  " );
				if (mysql_num_rows ( $query ) > 0) {
					
					$available_from_from_db = parent::getSingleResult ( "select available_from from #_doctors_locations where pid='" . $location_id . "' and status=1 and isDeleted=0  " );
					$available_to_from_db = parent::getSingleResult ( "select available_to from #_doctors_locations where pid='" . $location_id . "' and status=1 and isDeleted=0 " );
					if (($available_from_from_db != NULL) || ($available_to_from_db != NULL)) {
						
						// check given time is available in time slot
						$sql = "SELECT * FROM `#_doctors_locations` WHERE pid='" . $location_id . "' AND doctor_id='" . $doctor_id . "' AND(available_from <= '" . $today_date . "' AND available_to >= '" . $today_date . "') and available_status=0 and status=1 and isDeleted=0  ";
						$query_2 = parent::db_query ( $sql );
						if (mysql_num_rows ( $query_2 ) == 0) {
							
							// check current date is inbetween the availbele date
							$sql_2 = "SELECT * FROM `#_doctors_locations_time_slots` WHERE location_id='" . $location_id . "' AND weekdays='" . $weekday . "' AND(start_time <= '" . $current_time . "' AND end_time >= '" . $current_time . "') and available_status=1 and status=1 and isDeleted=0  ";
							
							$query_3 = parent::db_query ( $sql_2 );
							if (mysql_num_rows ( $query_3 ) > 0) {
								
								$data ['msg'] = 'Doctor is available';
								$data ['status'] = 'true';
							} else {
								
								$data ['msg'] = 'Doctor is not available in this location now';
								$data ['status'] = 'false';
							}
						} else {
							$data ['msg'] = 'Doctor is not available in this location on today';
							$data ['status'] = 'false';
						}
					} else {
						
						// check current date is inbetween the availbele date
						$sql_2 = "SELECT * FROM `#_doctors_locations_time_slots` WHERE location_id='" . $location_id . "' AND weekdays='" . $weekday . "' AND(start_time <= '" . $current_time . "' AND end_time >= '" . $current_time . "') and status=1 and isDeleted=0  ";
						
						$query_3 = parent::db_query ( $sql_2 );
						if (mysql_num_rows ( $query_3 ) > 0) {
							
							$data ['msg'] = 'Doctor is available';
							$data ['status'] = 'true';
						} else {
							$data ['msg'] = 'Doctor is not available in this location now';
							$data ['status'] = 'false';
						}
					}
				} else {
					$data ['msg'] = 'Doctor id and location id not matched';
					$data ['status'] = 'false';
				}
			} else {
				$data ['msg'] = 'Doctor is busy now';
				$data ['status'] = 'false';
			}
		} else {
			$data ['msg'] = 'Doctor id and location id are empty';
			$data ['status'] = 'false';
		}
		
		return $data;
	}
	public function check_available_unavailable_for_given_date_time($doctor_id, $location_id, $today_date, $current_time) {
		if (! empty ( $location_id ) && ! empty ( $doctor_id )) {
			date_default_timezone_set ( "Asia/Yangon" );
			// date_default_timezone_set("Asia/Karachi");
			// $today_date = date("Y-m-d");
			// $current_time = date("H:i:s");
			// $today_weekday = date("l");
			$timestamp = strtotime ( $today_date );
			$weekday = date ( 'l', $timestamp );
			
			$weekday = self::given_weekday_num_find ( $weekday );
			
			// check whether the doctor is free or busy
			$query_busy_free = parent::db_query ( "select * from #_doctors  where `pid`='" . $doctor_id . "'  and busy_free=1 " );
			if (mysql_num_rows ( $query_busy_free ) > 0) {
				// check whether the location is available
				$query = parent::db_query ( "select * from #_doctors_locations  where `pid`='" . $location_id . "' and doctor_id= '" . $doctor_id . "' and status=1 and isDeleted=0  " );
				if (mysql_num_rows ( $query ) > 0) {
					
					// check available from and to date is null in db?
					$available_from_from_db = parent::getSingleResult ( "select available_from from #_doctors_locations where pid='" . $location_id . "' and status=1 and isDeleted=0  " );
					$available_to_from_db = parent::getSingleResult ( "select available_to from #_doctors_locations where pid='" . $location_id . "' and status=1 and isDeleted=0 " );
					if (($available_from_from_db != NULL) || ($available_to_from_db != NULL)) {
						// check given time is available in time slot
						$sql = "SELECT * FROM `#_doctors_locations` WHERE pid='" . $location_id . "' AND doctor_id='" . $doctor_id . "' AND(available_from <= '" . $today_date . "' AND available_to >= '" . $today_date . "') and available_status=1 and status=1 and isDeleted=0  ";
						$query_2 = parent::db_query ( $sql );
						if (mysql_num_rows ( $query_2 ) > 0) {
							
							// check current date is inbetween the availbele date
							$sql_2 = "SELECT * FROM `#_doctors_locations_time_slots` WHERE location_id='" . $location_id . "' AND weekdays='" . $weekday . "' AND(start_time <= '" . $current_time . "' AND end_time >= '" . $current_time . "') and available_status=1 and status=1 and isDeleted=0  ";
							
							$query_3 = parent::db_query ( $sql_2 );
							if (mysql_num_rows ( $query_3 ) > 0) {
								
								$data ['msg'] = 'Doctor is available';
								$data ['status'] = 'true';
							} else {
								$data ['msg'] = 'Doctor is not available in this location now';
								$data ['status'] = 'false';
							}
						} else {
							$data ['msg'] = 'Doctor is not available in this location on today';
							$data ['status'] = 'false';
						}
					} else {
						// check current date is inbetween the availbele date
						$sql_2 = "SELECT * FROM `#_doctors_locations_time_slots` WHERE location_id='" . $location_id . "' AND weekdays='" . $weekday . "' AND(start_time <= '" . $current_time . "' AND end_time >= '" . $current_time . "')  and status=1 and isDeleted=0  ";
						
						$query_3 = parent::db_query ( $sql_2 );
						if (mysql_num_rows ( $query_3 ) > 0) {
							
							$data ['msg'] = 'Doctor is available';
							$data ['status'] = 'true';
						} else {
							$data ['msg'] = 'Doctor is not available in this location now';
							$data ['status'] = 'false';
						}
					}
				} else {
					$data ['msg'] = 'Doctor id and location id not matched';
					$data ['status'] = 'false';
				}
			} else {
				$data ['msg'] = 'Doctor is busy now';
				$data ['status'] = 'false';
			}
		} else {
			$data ['msg'] = 'Doctor id and location id are empty';
			$data ['status'] = 'false';
		}
		
		return $data;
	}
	public function check_chat_available($doctor_id) {
		$query = parent::db_query ( "select * from #_doctors where pid='" . $doctor_id . "'  and status=1 " );
		if (mysql_num_rows ( $query ) > 0) {
			
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				
				$isChatNeeded = parent::getSingleResult ( "select isChatNeeded from #_doctors where pid='" . $doctor_id . "'" );
				if ($isChatNeeded == 1) {
					$data ['status'] = 'true';
					$data ['msg'] = 'Connecting...';
				} else {
					$data ['status'] = 'false';
					$data ['msg'] = 'Doctor is not available for chat';
				}
			}
		} else {
			$data ['status'] = 'false';
			$data ['msg'] = 'Doctor is not available for chat';
		}
		return $data;
	}
	public function set_chat_available_unavailable($doctor_id, $isChatNeeded) {
		$query = parent::db_query ( "select * from #_doctors  where `pid`='" . $doctor_id . "'  " );
		if (mysql_num_rows ( $query ) > 0) {
			$sql = "UPDATE #_doctors SET 
            isChatNeeded='" . $isChatNeeded . "' 
           WHERE  pid='" . $doctor_id . "'  ";
			
			$query = parent::db_query ( $sql );
			return 'true';
		} else {
			
			return 'false';
		}
	}
	public function random_password($length = 8) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+:,.?";
		$password = substr ( str_shuffle ( $chars ), 0, $length );
		return $password;
	}
	public function update_new_password($new_password, $email) {
		$tablename = 'users_login';
		$email_exist = self::email_check ( $tablename, $email );
		
		if (($email_exist == true)) {
			$password = md5 ( $new_password );
			$password = base64_encode ( $password );
			// Update pass
			$update_pass_query = parent::db_query ( "update  #_" . $tablename . "  SET password ='" . $password . "' where  email='" . $email . "' " );
			return true;
		} else {
			return false;
		}
	}
	public function specialities($lang_flag) {
		$data = array ();
		$query = parent::db_query ( "select * from #_specialities  where status=1 order by name asc" );
		if (mysql_num_rows ( $query ) > 0) {
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				$data [$i] ['pid'] = $row ['pid'];
				if ($lang_flag == "en") {
					$data [$i] ['name'] = $row ['name'];
				} else {
					$data [$i] ['name'] = $row ['name_my'];
				}
				$data [$i] ['description'] = $row ['description'];
				$i ++;
			}
		} else {
			return 'false';
		}
		
		return $data;
	}
	public function get_all_admin_user_details() {
		$data = array ();
		$query = parent::db_query ( "select * from #_admin_users  where status=1 AND user_type='administrat' order by name asc" );
		if (mysql_num_rows ( $query ) > 0) {
			$i = 0;
			while ( $row = parent::db_fetch_array ( $query ) ) {
				$data [$i] ['pid'] = $row ['pid'];
				$data [$i] ['user_id'] = $row ['user_id'];
				$data [$i] ['email'] = $row ['email'];
				$data [$i] ['user_type'] = $row ['user_type'];
				$data [$i] ['name'] = $row ['name'];
				$i ++;
			}
		} else {
			return 'false';
		}
		
		return $data;
	}
	public function upload_images($file_name, $upload_path) {
		$data = array ();
		$target_dir = $upload_path;
		$tempname = 'doctor_image_' . rand ( 1, 10000000000 );
		$new_file_name = $tempname . '' . $_FILES [$file_name] ["name"];
		$target_file = $target_dir . basename ( $new_file_name );
		$uploadOk = 1;
		$upload_error = '';
		$imageFileType = strtolower ( pathinfo ( $target_file, PATHINFO_EXTENSION ) );
		// Check if image file is a actual image or fake image
		if (isset ( $_POST ["submit"] )) {
			$check = getimagesize ( $_FILES [$file_name] ["tmp_name"] );
			if ($check !== false) {
				// echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				$upload_error = "File is not an image.";
				$uploadOk = 0;
			}
		}
		// Check if file already exists
		if (file_exists ( $target_file )) {
			$upload_error = " file already exists.";
			$uploadOk = 0;
		}
		// Check file size
		/*
		 * if ($check > 1000000) {
		 * $upload_error = "Sorry, your file is too large.";
		 * $uploadOk = 0;
		 * }
		 */
		// Allow certain file formats
		if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
			$upload_error = " only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			// $upload_error = "Sorry, your file was not uploaded.";
			// if everything is ok, try to upload file
		} else {
			if (move_uploaded_file ( $_FILES [$file_name] ["tmp_name"], $target_file )) {
				// echo "The file ". basename( $_FILES["idcard_file"]["name"]). " has been uploaded.";
			} else {
				$upload_error = " there was an error uploading your file.";
			}
		}
		
		if ($uploadOk == 1 && $upload_error == '') {
			$data ['doctor_image_name'] = $new_file_name;
			$data ['status'] = 'true';
		} else {
			$data ['status'] = 'false';
			$data ['msg'] = $upload_error;
		}
		return $data;
	}
	public function getAppointmentList($pid, $user_type) {
		$data = array ();
		if ($user_type == 'hospital') {
			$doctor_query = parent::db_query ( "select * from #_doctors  where  hospital_id ='" . $pid . "'" );
		} else if ($user_type == 'clinics') {
			
			$doctor_query = parent::db_query ( "select * from #_doctors  where  clinic_id ='" . $pid . "'" );
		} else if ($user_type == 'doctors') {
			
			$doctor_query = parent::db_query ( "select * from #_doctors  where  pid ='" . $pid . "'" );
		}
		
		if (mysql_num_rows ( $doctor_query ) > 0) {
			
			$doctor_array = array ();
			while ( $doctor_data = parent::db_fetch_array ( $doctor_query ) ) {
				// $doctor_array[] = $doctor_data['user_id'];
				$doctor_array [] = $doctor_data ['pid'];
			}
			
			$query = parent::db_query ( "select * from #_appointment  where doctor_id  in (" . implode ( ',', $doctor_array ) . ") order by pid desc " );
			
			if (mysql_num_rows ( $query ) > 0) {
				
				$i = 0;
				
				while ( $row = parent::db_fetch_array ( $query ) ) {
					$data [$i] ['pid'] = $row ['pid'];
					$data [$i] ['doctor_id'] = $row ['doctor_id'];
					$data [$i] ['patient_id'] = $row ['patient_id'];
					$data [$i] ['call_details_id'] = $row ['call_details_id'];
					$data [$i] ['app_date'] = $row ['app_date'];
					$data [$i] ['app_time'] = $row ['app_time'];
					$data [$i] ['comments'] = ($row ['comments'] != '') ? $row ['comments'] : '';
					$data [$i] ['status'] = $row ['status'];
					
					$doctor_query = parent::db_query ( "select * from #_doctors where user_id='" . $row ['doctor_id'] . "'" );
					$doctor_row = parent::db_fetch_array ( $doctor_query );
					
					$speciality = parent::getSingleResult ( "select name from #_specialities where pid='" . $doctor_row ['speciality'] . "'" );
					
					$clinic = parent::getSingleResult ( "select name from #_clinics where pid='" . $doctor_row ['clinic_id'] . "'" );
					
					$hospital = parent::getSingleResult ( "select name from #_hospitals where pid='" . $doctor_row ['hospital_id'] . "'" );
					
					$patient_query = parent::db_query ( "select * from #_patients where pid='" . $row ['patient_id'] . "'" );
					$patient_row = parent::db_fetch_array ( $patient_query );
					
					$division_query = parent::db_query ( "select * from #_division where pid='" . $patient_row ['division'] . "'" );
					$division_row = parent::db_fetch_array ( $division_query );
					
					$township_query = parent::db_query ( "select * from #_township where division_id='" . $division_row ['pid'] . "'" );
					$township_row = parent::db_fetch_array ( $township_query );
					
					$data [$i] ['doctor_name'] = $doctor_row ['name'];
					$data [$i] ['doctor_speciality'] = $speciality;
					$data [$i] ['doctor_sex'] = $doctor_row ['sex'];
					$data [$i] ['doctor_address'] = $doctor_row ['address'];
					$data [$i] ['clinic_id'] = $doctor_row ['clinic_id'];
					$data [$i] ['hospital_id'] = $doctor_row ['hospital_id'];
					$data [$i] ['clinic'] = ($clinic != '') ? $clinic : '';
					$data [$i] ['patient_id'] = $patient_row ['patient_id'];
					$data [$i] ['patient_name'] = $patient_row ['name'];
					$data [$i] ['patient_age'] = $patient_row ['age'];
					$data [$i] ['patient_sex'] = $patient_row ['sex'];
					$data [$i] ['patient_phone'] = $patient_row ['phone'];
					$data [$i] ['patient_address'] = $patient_row ['address'];
					$data [$i] ['patient_postal_code'] = $patient_row ['postal_code'];
					$data [$i] ['patient_township'] = $township_row ['name'];
					$data [$i] ['patient_division'] = $division_row ['name'];
					$data [$i] ['patient_consent'] = parent::getSingleResult ( "select patient_consent from #_call_details where pid='" . $data [$i] ['call_details_id'] . "'" );
					$data [$i] ['hospital'] = ($hospital != '') ? $hospital : '';
					
					$i ++;
				}
			}
		}
		return $data;
	}
	public function getDoctorAppointmentList($doctor_id, $category = 1, $start_offset = NULL, $end_limit = NULL, $is_today = 0) {
		// print_r($category);print_r($is_today);exit;
		$where_clause = '';
		$limit_range = '';
		$appointment_category_array = array ();
		if ($category) {
			if ($category == 1 && ! $is_today) {
				$where_clause = " AND app.status=" . $category . " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')>='" . date ( 'Y-m-d' ) . "'";
			} else if ($category == 1 && $is_today) {
				//$where_clause = " AND app.status=" . $category . " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')='" . date ( 'Y-m-d' ) . "' AND STR_TO_DATE(app.app_time,'%H:%i')>='" . date ( 'H:i' ) . "'";
				$where_clause = " AND app.status=" . $category . " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')='" . date ( 'Y-m-d' ) . "'";
				
			} else if ($category == 3) {
				$where_clause = " AND (app.status=3 OR app.status=2)";
			} else {
				$where_clause = " AND app.status=" . $category;
			}
		}
		if ($start_offset || $end_limit) {
			$limit_range = " Limit " . $start_offset . "," . $end_limit;
		}
		/*
		 * if ($is_today) {
		 * $where_clause .= " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')='" . date('Y-m-d') . "'";
		 * }
		 */
		
		$query = "select app.pid as appointment_id,cd.pid as call_details_id,cd.patient_id as patient_id,app.app_date as app_date,app.app_time as app_time,dl.location as location_address,dl.location_type as location_type,dl.hospital_id as hospital_id,dl.clinic_id as clinic_id,cd.create_by as refferal_by,cd.create_by_type as refferal_type from #_appointment as app left join #_call_details as cd ON app.call_details_id=cd.pid left join #_doctors_locations as dl on dl.pid=app.doctor_location_id where app.doctor_id=" . $doctor_id . " AND app.appointment_with='D' " . $where_clause . " order by cd.pid desc " . $limit_range;
		
		$doctor_refferal_details = parent::db_query ( $query );
		if (mysql_num_rows ( $doctor_refferal_details ) > 0) {
			$i = 0;
			
			while ( $doctor_data = parent::db_fetch_array ( $doctor_refferal_details ) ) {
				// $doctor_refferal_array[] = $doctor_data['user_id'];
				// $doctor_refferal_array[$i]['referralDoctorId'] = $doctor_data['referral_doctor_id'];
				$appointment_category_array [$i] ['appointmentId'] = $doctor_data ['appointment_id'];
				$appointment_category_array [$i] ['callDetailsId'] = ($doctor_data ['call_details_id'] === NULL) ? '' : $doctor_data ['call_details_id'];
				$appointment_category_array [$i] ['locationType'] = ($doctor_data ['location_type'] === NULL) ? '' : $doctor_data ['location_type'];
				$appointment_category_array [$i] ['locationAddress'] = ($doctor_data ['location_address'] === NULL) ? '' :$doctor_data ['location_address'];
				$appointment_category_array [$i] ['locationName'] = ($doctor_data ['location_type'] === NULL) ? '' : (($doctor_data ['location_type'] == 1) ? parent::getSingleResult ( "select name from #_clinics where pid='" . $doctor_data ['clinic_id'] . "'" ) . "(Clinic)" : parent::getSingleResult ( "select name from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'" ) . "(Hospital)");
				$appointment_category_array [$i] ['isReferral'] = ($doctor_data ['refferal_by'] > 0 && $doctor_data ['refferal_type'] == 'Doctor') ? '1' : '0';
				$appointment_category_array [$i] ['appDate'] = $doctor_data ['app_date'];
				$appointment_category_array [$i] ['appTime'] = date ( 'h:i a', strtotime ( $doctor_data ['app_time'] ) );
				$appointment_category_array [$i] ['patientId'] = ($doctor_data ['patient_id'] === NULL) ? '' : $doctor_data ['patient_id'];
				$appointment_category_array [$i] ['patientName'] = ($doctor_data ['patient_id'] === NULL) ? '' : parent::getSingleResult ( "select name from #_patients where pid='" . $doctor_data ['patient_id'] . "'" );
				$appointment_category_array [$i] ['patientProfileImage'] = (parent::getSingleResult ( "select profile_pic from #_patients where pid='" . $doctor_data ['patient_id'] . "'" )=='')?'':SITE_PATH . "uploaded_files/profile_pictures/" . parent::getSingleResult ( "select profile_pic from #_patients where pid='" . $doctor_data ['patient_id'] . "'" );
				$i ++;
			}
		}
		
		return $appointment_category_array;
	}
	public function getDoctorAppointmentListBasedLoccation($doctor_id, $category = 1, $start_offset = NULL, $end_limit = NULL, $is_today = 0, $location_id) {
		$where_clause = '';
		$limit_range = '';
		$appointment_category_array = array ();
		if ($category) {
			if ($category == 1 && ! $is_today) {
				$where_clause = " AND app.status=" . $category . " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')>='" . date ( 'Y-m-d' ) . "'";
			} else if ($category == 1 && $is_today) {
				//$where_clause = " AND app.status=" . $category . " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')='" . date ( 'Y-m-d' ) . "' AND STR_TO_DATE(app.app_time,'%H:%i')>='" . date ( 'H:i' ) . "'";
				$where_clause = " AND app.status=" . $category . " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')='" . date ( 'Y-m-d' ) . "'";
			} else if ($category == 3) {
				$where_clause = " AND (app.status=3 OR app.status=2)";
			} else {
				$where_clause = " AND app.status=" . $category;
			}
		}
		if ($start_offset || $end_limit) {
			$limit_range = " Limit " . $start_offset . "," . $end_limit;
		}
		/*
		 * if ($is_today) {
		 * $where_clause .= " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')='" . date('Y-m-d') . "' AND STR_TO_DATE(app.app_time,'%H:%i')>='" . date('H:i')."'";
		 * }
		 */
		if (! empty ( $location_id )) {
			$where_clause .= " AND app.doctor_location_id=" . $location_id;
		}
		
		$query = "select app.pid as appointment_id,cd.pid as call_details_id,cd.patient_id as patient_id,app.app_date as app_date,app.app_time as app_time,dl.location as location_address,dl.location_type as location_type,dl.hospital_id as hospital_id,dl.clinic_id as clinic_id,cd.create_by as refferal_by,cd.create_by_type as refferal_type from #_appointment as app left join #_call_details as cd ON app.call_details_id=cd.pid left join #_doctors_locations as dl on dl.pid=app.doctor_location_id where app.doctor_id=" . $doctor_id . " AND app.appointment_with='D' " . $where_clause . " order by cd.pid desc " . $limit_range;
		
		$doctor_refferal_details = parent::db_query ( $query );
		if (mysql_num_rows ( $doctor_refferal_details ) > 0) {
			$i = 0;
			
			while ( $doctor_data = parent::db_fetch_array ( $doctor_refferal_details ) ) {
				// $doctor_refferal_array[] = $doctor_data['user_id'];
				// $doctor_refferal_array[$i]['referralDoctorId'] = $doctor_data['referral_doctor_id'];
				$appointment_category_array [$i] ['appointmentId'] = $doctor_data ['appointment_id'];
				$appointment_category_array [$i] ['callDetailsId'] = ($doctor_data ['call_details_id'] === NULL) ? '' : $doctor_data ['call_details_id'];
				$appointment_category_array [$i] ['locationType'] = ($doctor_data ['location_type'] === NULL) ? '' : $doctor_data ['location_type'];
				$appointment_category_array [$i] ['locationAddress'] = ($doctor_data ['location_address'] === NULL) ? '' : $doctor_data ['location_address'];
				$appointment_category_array [$i] ['locationName'] = ($doctor_data ['location_type'] === NULL) ? '' : (($doctor_data ['location_type'] == 1) ? parent::getSingleResult ( "select name from #_clinics where pid='" . $doctor_data ['clinic_id'] . "'" ) . "(Clinic)" : parent::getSingleResult ( "select name from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'" ) . "(Hospital)");
				$appointment_category_array [$i] ['isReferral'] = ($doctor_data ['refferal_by'] > 0 && $doctor_data ['refferal_type'] == 'Doctor') ? '1' : '0';
				$appointment_category_array [$i] ['appDate'] = $doctor_data ['app_date'];
				$appointment_category_array [$i] ['appTime'] = date ( 'h:i a', strtotime ( $doctor_data ['app_time'] ) );
				$appointment_category_array [$i] ['patientId'] = ($doctor_data ['patient_id'] === NULL) ? '' : $doctor_data ['patient_id'];
				$appointment_category_array [$i] ['patientName'] = ($doctor_data ['patient_id'] === NULL) ? '' : parent::getSingleResult ( "select name from #_patients where pid='" . $doctor_data ['patient_id'] . "'" );
				$i ++;
			}
		}
		
		return $appointment_category_array;
	}
	public function getPatientAppointmentHistory($patient_id, $category, $start_offset = NULL, $end_limit = NULL, $is_today = 0) {
		$where_clause = '';
		$limit_range = '';
		$appointment_category_array = array ();
		if ($category) {
			$where_clause = " AND app.status=" . $category; // Current
		}
		if ($start_offset || $end_limit) {
			$limit_range = " Limit " . $start_offset . "," . $end_limit;
		}
		if ($is_today) {
			$where_clause .= " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')='" . date ( 'Y-m-d' ) . "'";
		}
		
		// $query = "select app.pid as appointment_id,app.doctor_id as doctor_id,d.name as doctor_name,d.speciality as speciality,d.education as education,cd.pid as call_details_id,cd.chief_complaint as chief_complaint,cd.history_of_present_illness as history_of_present_illness,app.doctor_id as doctor_id,app.app_date as app_date,app.app_time as app_time,dl.location as location_address,dl.location_type as location_type,dl.hospital_id as hospital_id,dl.clinic_id as clinic_id,cd.create_by as refferal_by,cd.create_by_type as refferal_type from #_appointment as app left join #_call_details as cd ON app.call_details_id=cd.pid left join #_doctors_locations as dl on dl.pid=cd.doctor_location_id left join #_doctors as d on d.pid=app.doctor_id where app.patient_id=" . $patient_id . " " . $where_clause . " order by cd.pid desc " . $limit_range;
		$query = "select app.pid as appointment_id,app.doctor_id as doctor_id,d.name as doctor_name,d.speciality as speciality,d.education as education,cd.pid as call_details_id,cd.chief_complaint as chief_complaint,cd.history_of_present_illness as history_of_present_illness,app.doctor_id as doctor_id,app.app_date as app_date,app.app_time as app_time,cd.create_by as refferal_by,cd.create_by_type as refferal_type from #_appointment as app left join #_call_details as cd ON app.call_details_id=cd.pid left join #_doctors as d on d.pid=app.doctor_id where app.patient_id=" . $patient_id . " " . $where_clause . " order by app.pid desc " . $limit_range;
		
		$doctor_refferal_details = parent::db_query ( $query );
		if (mysql_num_rows ( $doctor_refferal_details ) > 0) {
			$i = 0;
			
			while ( $doctor_data = parent::db_fetch_array ( $doctor_refferal_details ) ) {
				// $doctor_refferal_array[] = $doctor_data['user_id'];
				// $doctor_refferal_array[$i]['referralDoctorId'] = $doctor_data['referral_doctor_id'];
				$appointment_category_array [$i] ['appointmentId'] = $doctor_data ['appointment_id'];
				$appointment_category_array [$i] ['callDetailsId'] = ($doctor_data ['call_details_id'] === NULL) ? '' : $doctor_data ['call_details_id'];
				
				$appointment_category_array [$i] ['doctorId'] = ($doctor_data ['doctor_id'] === NULL) ? '' : $doctor_data ['doctor_id'];
				$appointment_category_array [$i] ['doctorName'] = ($doctor_data ['doctor_name'] === NULL) ? '' : $doctor_data ['doctor_name'];
				if($doctor_data ['speciality']==0){
					$appointment_category_array [$i] ['doctorSpeciality'] = 'General Practitioner';
				}else{
					$appointment_category_array [$i] ['doctorSpeciality'] = ($doctor_data ['speciality'] === NULL) ? '' : parent::getSingleResult ( "select name from #_specialities where pid='" . $doctor_data ['speciality'] . "'" );
				}
				
				$appointment_category_array [$i] ['doctorEducation'] = ($doctor_data ['education'] === NULL) ? '' : $doctor_data ['education'];
				$appointment_category_array [$i] ['patient_consent'] = ($doctor_data ['education'] == 1) ? '1' : '0';
				$appointment_category_array [$i] ['chiefComplaint'] = ($doctor_data ['chief_complaint'] === NULL) ? '' : $doctor_data ['chief_complaint'];
				$appointment_category_array [$i] ['historyOfPresentIllness'] = ($doctor_data ['history_of_present_illness'] === NULL) ? '' : $doctor_data ['history_of_present_illness'];
				
				$appointment_category_array [$i] ['isReferral'] = ($doctor_data ['refferal_by'] > 0 && $doctor_data ['refferal_type'] == 'Doctor') ? '1' : '0';
				$appointment_category_array [$i] ['appDate'] = date ( 'd-M-Y', strtotime ( $doctor_data ['app_date'] ) );
				$appointment_category_array [$i] ['appTime'] = date ( 'h:i a', strtotime ( $doctor_data ['app_time'] ) );
				$appointment_category_array [$i] ['patientId'] = ($doctor_data ['patient_id'] === NULL) ? '' : $doctor_data ['patient_id'];
				$appointment_category_array [$i] ['patientName'] = ($doctor_data ['patient_id'] === NULL) ? '' : parent::getSingleResult ( "select name from #_patients where pid='" . $doctor_data ['patient_id'] . "'" );
				$appointment_category_array [$i] ['patientProfileImage'] = (parent::getSingleResult ( "select profile_pic from #_patients where pid='" . $doctor_data ['patient_id'] . "'" )=='')?'':SITE_PATH . "uploaded_files/profile_pictures/" . parent::getSingleResult ( "select profile_pic from #_patients where pid='" . $doctor_data ['patient_id'] . "'" );
				$i ++;
			}
		}
		
		return $appointment_category_array;
	}
	public function getPatientDetails($patient_id) {
		
		/* $query = "select p.pid as patient_id,p.name as name,p.phone as phone,p.sex as sex,p.date_of_birth as date_of_birth,p.blood_group as blood_group,p.age as age,p.height as height,p.weight as weight,p.created_on registred_date,p.township as township,p.division as division,p.address as address,p.profile_pic as profile_pic from #_patients as p where p.pid=".$patient_id; */
		$query = parent::db_query ( "select p.pid as patient_id,p.name as name,p.phone as phone,p.sex as sex,p.date_of_birth as date_of_birth,p.occupation as occupation,p.blood_group as blood_group,p.age as age,p.height as height,p.weight as weight,p.created_on registred_date,p.township as township,p.division as division,p.address as address,p.profile_pic as profile_pic from #_patients as p where p.pid=" . $patient_id );
		
		// $patient_details =parent::db_query($query);
		if (mysql_num_rows ( $query ) > 0) {
			
			$i = 0;
			while ( $doctor_data = parent::db_fetch_array ( $query ) ) {
				
				$patient_details [$i] ['patientId'] = ($doctor_data ['patient_id'] === NULL) ? '' : $doctor_data ['patient_id'];
				$patient_details [$i] ['patientName'] = ($doctor_data ['name'] === NULL) ? '' : $doctor_data ['name'];
				$patient_details [$i] ['email'] = ($doctor_data ['email'] === NULL) ? '' : $doctor_data ['email'];
				$patient_details [$i] ['phone'] = ($doctor_data ['phone'] === NULL) ? '' : $doctor_data ['phone'];
				$patient_details [$i] ['sex'] = ($doctor_data ['sex'] === NULL) ? '' : $doctor_data ['sex'];
				$dateOfBirth = $doctor_data ['date_of_birth'];
				$today = date ( "Y-m-d" );
				$diff = date_diff ( date_create ( $dateOfBirth ), date_create ( $today ) );
				$age = $diff->format ( '%y' );
				$patient_details [$i] ['age'] = $age;
				$patient_details [$i] ['is_drugAlergy'] = 'No';
				$patient_details [$i] ['occupation'] = ($doctor_data ['occupation'] === NULL) ? '' : $doctor_data ['occupation'];
				$patient_details [$i] ['blood_group'] = ($doctor_data ['blood_group'] === NULL) ? '' : $doctor_data ['blood_group'];
				$patient_details [$i] ['height'] = ($doctor_data ['height'] === NULL) ? '' : $doctor_data ['height'];
				$patient_details [$i] ['weight'] = ($doctor_data ['weight'] === NULL) ? '' : $doctor_data ['weight'];
				$patient_details [$i] ['address'] = ($doctor_data ['address'] === NULL) ? '' : $doctor_data ['address'];
				$patient_details [$i] ['profile_pic'] = ($doctor_data ['profile_pic'] === NULL) ? '' : SITE_PATH . "uploaded_files/profile_pictures/" . $doctor_data ['profile_pic'];
				$patient_details [$i] ['registred_date'] = ($doctor_data ['registred_date'] === NULL) ? '' : $doctor_data ['registred_date'];
				$patient_details [$i] ['township'] = ($doctor_data ['township'] === NULL) ? '' : parent::getSingleResult ( "select name from #_township where pid='" . $doctor_data ['township'] . "'" );
				$patient_details [$i] ['division'] = ($doctor_data ['division'] === NULL) ? '' : parent::getSingleResult ( "select name from #_division where pid='" . $doctor_data ['division'] . "'" );
				
				$i ++;
			}
		}
		
		return $patient_details;
	}
	public function getHospitalList($latitude, $longitude, $distance, $emergency_provider = 0) {
		$where_clause = '';
		$hospital_list = array ();
		if ($emergency_provider) {
			$where_clause .= " AND h.ambulance_service='Yes'";
		}
		$query = "SELECT h.pid As 'id',h.name as name,SUBSTRING_INDEX(h.latlong, ',', 1) as latitude,SUBSTRING_INDEX(h.latlong, ',', -1) as longitude,h.address as address,h.township as township,h.division as division,h.ambulance_service as ambulanceService,
				h.home_visit_available_start_time as startTime,h.home_visit_available_end_time as endTime,Count(dl.pid) as doctorCount,h.healthcare_services as healthcare_services,h.phone as phone,
				Round(6371 * acos( cos( radians(" . $latitude . ") ) * cos( radians(SUBSTRING_INDEX(h.latlong, ',', 1)) ) * cos( radians(SUBSTRING_INDEX(h.latlong, ',',-1)) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians(SUBSTRING_INDEX(h.latlong, ',', 1)))),2) AS 'distance'
				From crm_hospitals as h left join crm_doctors_locations as dl on dl.hospital_id=h.pid where dl.location_type=2 AND h.is_verified=1 AND h.status=1 " . $where_clause . " GROUP BY h.pid HAVING distance <=" . $distance . " ORDER BY h.pid desc";
		// print_r($query);exit();
		$query = parent::db_query ( $query );
		
		if (mysql_num_rows ( $query ) > 0) {
			
			$i = 0;
			while ( $data = parent::db_fetch_array ( $query ) ) {
				$hospital_list [$i] ['id'] = ($data ['id'] === NULL) ? '' : $data ['id'];
				$hospital_list [$i] ['name'] = ($data ['name'] === NULL) ? '' : $data ['name'];
				$hospital_list [$i] ['address'] = ($data ['address'] === NULL) ? '' : $data ['address'];
				$hospital_list [$i] ['locationType'] = '2';
				$hospital_list [$i] ['startTime'] = ($data ['startTime'] === NULL) ? '' : $data ['startTime'];
				$hospital_list [$i] ['endTime'] = ($data ['endTime'] === NULL) ? '' : $data ['endTime'];
				$hospital_list [$i] ['latitude'] = ($data ['latitude'] === NULL) ? '' : $data ['latitude'];
				$hospital_list [$i] ['longitude'] = ($data ['longitude'] === NULL) ? '' : $data ['longitude'];
				$hospital_list [$i] ['totalDoctors'] = ($data ['doctorCount'] === NULL) ? '' : $data ['doctorCount'];
				$hospital_list [$i] ['distanceAway'] = ($data ['distance'] === NULL) ? '' : $data ['distance'];
				$hospital_list [$i] ['ambulanceService'] = ($data ['ambulanceService'] === NULL || $data ['ambulanceService'] == 'No' ) ? '0' : '1';
				$hospital_list [$i] ['healthcareServices'] = ($data ['healthcare_services'] === NULL ||$data ['healthcare_services'] == 0) ? '0' : '1';
				$hospital_list [$i] ['phone'] = ($data ['phone'] === NULL) ? '' : $data ['phone'];
				$hospital_list [$i] ['township'] = ($data ['township'] === NULL || parent::getSingleResult ( "select name from #_township where pid='" . $data ['township'] . "'" ) == '') ? '' : parent::getSingleResult ( "select name from #_township where pid='" . $data ['township'] . "'" );
				$hospital_list [$i] ['division'] = ($data ['division'] === NULL || parent::getSingleResult ( "select name from #_division where pid='" . $data ['division'] . "'" ) == '') ? '' : parent::getSingleResult ( "select name from #_division where pid='" . $data ['division'] . "'" );
				
				$i ++;
			}
		}
		
		return $hospital_list;
	}
	public function getClinicList($latitude, $longitude, $distance) {
		$clinic_list = array ();
		$query = "SELECT c.pid As 'id',c.name as name,SUBSTRING_INDEX(c.latlong, ',', 1) as latitude,SUBSTRING_INDEX(c.latlong, ',', -1) as longitude,c.address as address,c.township as township,c.division as division,Count(dl.pid) as doctorCount,
				c.clinic_open_time as startTime,c.clinic_close_time as endTime,c.phone as phone,
				Round(6371 * acos( cos( radians(" . $latitude . ") ) * cos( radians(SUBSTRING_INDEX(c.latlong, ',', 1)) ) * cos( radians(SUBSTRING_INDEX(c.latlong, ',',-1)) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians(SUBSTRING_INDEX(c.latlong, ',', 1)))),2) AS 'distance'
				From crm_clinics as c left join crm_doctors_locations as dl on dl.clinic_id=c.pid where dl.location_type=1 AND c.is_verified=1 AND c.status=1 GROUP BY c.pid HAVING distance <=" . $distance . " ORDER BY c.pid desc";
		$query = parent::db_query ( $query );
		if (mysql_num_rows ( $query ) > 0) {
			
			$i = 0;
			while ( $data = parent::db_fetch_array ( $query ) ) {
				
				$clinic_list [$i] ['id'] = ($data ['id'] === NULL) ? '' : $data ['id'];
				$clinic_list [$i] ['name'] = ($data ['name'] === NULL) ? '' : $data ['name'];
				$clinic_list [$i] ['address'] = ($data ['address'] === NULL) ? '' : $data ['address'];
				$clinic_list [$i] ['locationType'] = '1';
				$clinic_list [$i] ['startTime'] = ($data ['startTime'] === NULL) ? '' : $data ['startTime'];
				$clinic_list [$i] ['endTime'] = ($data ['endTime'] === NULL) ? '' : $data ['endTime'];
				$clinic_list [$i] ['latitude'] = ($data ['latitude'] === NULL) ? '' : $data ['latitude'];
				$clinic_list [$i] ['longitude'] = ($data ['longitude'] === NULL) ? '' : $data ['longitude'];
				$clinic_list [$i] ['totalDoctors'] = ($data ['doctorCount'] === NULL) ? '' : $data ['doctorCount'];
				$clinic_list [$i] ['distanceAway'] = ($data ['distance'] === NULL) ? '' : $data ['distance'];
				// $clinic_list [$i] ['ambulanceService'] = ($data ['ambulanceService']===NULL)?'':$data ['ambulanceService'];
				$clinic_list [$i] ['township'] = ($data ['township'] === NULL || parent::getSingleResult ( "select name from #_township where pid='" . $data ['township'] . "'" ) == '') ? '' : parent::getSingleResult ( "select name from #_township where pid='" . $data ['township'] . "'" );
				$clinic_list [$i] ['division'] = ($data ['division'] === NULL || parent::getSingleResult ( "select name from #_division where pid='" . $data ['division'] . "'" )) ? '' : parent::getSingleResult ( "select name from #_division where pid='" . $data ['division'] . "'" );
				
				$i ++;
			}
		}
		
		return $clinic_list;
	}
	public function getPatientAppointmentList($patient_id, $category = 1, $start_offset = NULL, $end_limit = NULL, $is_today = 0) {
		$where_clause = '';
		$limit_range = '';
		$appointment_category_array = array ();
		if ($category) {
			if ($category == 1 && ! $is_today) {
				$where_clause = " AND app.status=" . $category . " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')>='" . date ( 'Y-m-d' ) . "'";
			} else if ($category == 1 && $is_today) {
				//$where_clause = " AND app.status=" . $category . " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')='" . date ( 'Y-m-d' ) . "' AND STR_TO_DATE(app.app_time,'%H:%i')>='" . date ( 'H:i' ) . "'";
				$where_clause = " AND app.status=" . $category . " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')='" . date ( 'Y-m-d' ) . "'";
			}
			
			if ($category == 3) {
				$where_clause = " AND (app.status=2 OR app.status=3)";
			} else {
				$where_clause = " AND app.status=" . $category;
			}
		}
		if ($start_offset || $end_limit) {
			$limit_range = " Limit " . $start_offset . "," . $end_limit;
		}
		/*
		 * if ($is_today) {
		 * $where_clause .= " AND STR_TO_DATE(app.app_date, '%Y-%m-%d')='" . date('Y-m-d') . "'";
		 * }
		 */
		
		$query = "select app.pid as appointment_id,app.appointment_with as appointment_with,app.doctor_id as doctor_id,app.doctor_location_id as doctor_location_id,cd.pid as call_details_id,cd.patient_id as patient_id,cd.patient_rated as patient_rated,app.app_date as app_date,app.app_time as app_time,cd.create_by as refferal_by,cd.create_by_type as refferal_type from #_appointment as app left join #_call_details as cd ON app.call_details_id=cd.pid  where app.patient_id=" . $patient_id . " " . $where_clause . " order by cd.pid desc " . $limit_range;
		// print_r($query);exit;
		$patient_appointment_details = parent::db_query ( $query );
		if (mysql_num_rows ( $patient_appointment_details ) > 0) {
			$i = 0;
			
			while ( $doctor_data = parent::db_fetch_array ( $patient_appointment_details ) ) {
				// $doctor_refferal_array[] = $doctor_data['user_id'];
				// $doctor_refferal_array[$i]['referralDoctorId'] = $doctor_data['referral_doctor_id'];
				$appointment_category_array [$i] ['appointmentId'] = $doctor_data ['appointment_id'];
				$appointment_category_array [$i] ['callDetailsId'] = ($doctor_data ['call_details_id'] === NULL) ? '' : $doctor_data ['call_details_id'];
				$appointment_category_array [$i] ['locationType'] = '';
				$appointment_category_array [$i] ['locationAddress'] = '';
				$appointment_category_array [$i] ['locationName'] = '';
				$appointment_category_array [$i] ['isReferral'] = ($doctor_data ['refferal_by'] > 0 && $doctor_data ['refferal_type'] == 'Doctor') ? '1' : '0';
				$appointment_category_array [$i] ['isRated'] = ($doctor_data ['patient_rated'] > 0) ? '1' : '0';
				$appointment_category_array [$i] ['appDate'] = date ( 'd-M-Y', strtotime ( $doctor_data ['app_date'] ) );
				$appointment_category_array [$i] ['appTime'] = date ( 'h:i a', strtotime ( $doctor_data ['app_time'] ) );
				$appointment_category_array [$i] ['patientId'] = ($doctor_data ['patient_id'] === NULL) ? '' : $doctor_data ['patient_id'];
				$appointment_category_array [$i] ['doctorEducation'] = '';
				$appointment_category_array [$i] ['doctorProfileImage'] = '';
				$appointment_category_array [$i] ['doctorExperience'] = '';
				$appointment_category_array [$i] ['doctorSpeciality'] = '';
				if ($doctor_data ['appointment_with'] == 'D') {
					
					$appointment_category_array [$i] ['doctorName'] = parent::getSingleResult ( "select name from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" ) . "(Doctor)";
					$appointment_category_array [$i] ['doctorEducation'] = parent::getSingleResult ( "select name from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['doctorProfileImage'] = (parent::getSingleResult ( "select profile_image from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'")=='')?'':SITE_PATH . "uploaded_files/doctors/profile/" . parent::getSingleResult ( "select profile_image from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['doctorExperience'] = parent::getSingleResult ( "select years_of_experience from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
					$doc_speciality=parent::getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
					if($doc_speciality==0){
						$appointment_category_array [$i] ['doctorSpeciality']='General Practitioner';
					}else{
						$appointment_category_array [$i] ['doctorSpeciality'] = parent::getSingleResult ( "select name from #_specialities where pid='" . parent::getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" ) . "'" );
					}
					
					$location_type = parent::getSingleResult ( "select location_type from #_doctors_locations where pid='" . $doctor_data ['doctor_location_id'] . "' and doctor_id='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['locationType'] = $location_type;
					$appointment_category_array [$i] ['locationAddress'] = ($location_type === NULL) ? '' : parent::getSingleResult ( "select location from #_doctors_locations where pid='" . $doctor_data ['doctor_location_id'] . "' and doctor_id='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['locationName'] = ($location_type === NULL) ? '' : (($location_type == 1) ? parent::getSingleResult ( "select name from #_clinics where pid='" . parent::getSingleResult ( "select clinic_id from #_doctors_locations where pid='" . $doctor_data ['doctor_location_id'] . "' and doctor_id='" . $doctor_data ['doctor_id'] . "'" ) . "'" ) . "(Clinic)" : parent::getSingleResult ( "select name from #_hospitals where pid='" . parent::getSingleResult ( "select hospital_id from #_doctors_locations where pid='" . $doctor_data ['doctor_location_id'] . "' and doctor_id='" . $doctor_data ['doctor_id'] . "'" ) . "'" ) . "(Hospital)");
				} else if ($doctor_data ['appointment_with'] == 'L') {
					$appointment_category_array [$i] ['doctorName'] = parent::getSingleResult ( "select name from #_labs where pid='" . $doctor_data ['doctor_id'] . "'" ) . "(Lab)";
					$appointment_category_array [$i] ['locationAddress'] = parent::getSingleResult ( "select address from #_labs where pid='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['locationName'] = parent::getSingleResult ( "select name from #_labs where pid='" . $doctor_data ['doctor_id'] . "'" ) . "(Lab)";
				} else if ($doctor_data ['appointment_with'] == 'H') {
					$appointment_category_array [$i] ['doctorName'] = parent::getSingleResult ( "select name from #_healthcare_organization where pid='" . $doctor_data ['doctor_id'] . "'" ) . "(Health care)";
					$appointment_category_array [$i] ['locationAddress'] = parent::getSingleResult ( "select address from #_healthcare_organization where pid='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['locationName'] = parent::getSingleResult ( "select name from #_healthcare_organization where pid='" . $doctor_data ['doctor_id'] . "'" ) . "(Health care)";
				} else {
					$appointment_category_array [$i] ['doctorName'] = "NA";
				}
				
				$i ++;
			}
		}
		
		return $appointment_category_array;
	}
	public function getAppointmentDetails($appointment_id) {
		$where_clause = '';
		$limit_range = '';
		$appointment_category_array = array ();
		
		$query = "select app.pid as appointment_id,app.status as status,app.appointment_with as appointment_with,app.doctor_id as doctor_id,app.doctor_location_id as doctor_location_id,cd.pid as call_details_id,cd.patient_id as patient_id,cd.patient_rated as patient_rated,app.app_date as app_date,app.app_time as app_time,cd.create_by as refferal_by,cd.create_by_type as refferal_type from #_appointment as app left join #_call_details as cd ON app.call_details_id=cd.pid  where app.pid=" . $appointment_id . " " . $where_clause . " order by app.pid desc " . $limit_range;
		
		$doctor_refferal_details = parent::db_query ( $query );
		if (mysql_num_rows ( $doctor_refferal_details ) > 0) {
			$i = 0;
			
			while ( $doctor_data = parent::db_fetch_array ( $doctor_refferal_details ) ) {
				// $doctor_refferal_array[] = $doctor_data['user_id'];
				// $doctor_refferal_array[$i]['referralDoctorId'] = $doctor_data['referral_doctor_id'];
				$appointment_category_array [$i] ['appointmentId'] = $doctor_data ['appointment_id'];
				$appointment_category_array [$i] ['callDetailsId'] = ($doctor_data ['call_details_id'] === NULL) ? '' : $doctor_data ['call_details_id'];
				$appointment_category_array [$i] ['locationType'] = '';
				$appointment_category_array [$i] ['locationAddress'] = '';
				$appointment_category_array [$i] ['locationName'] = '';
				$appointment_category_array [$i] ['isReferral'] = ($doctor_data ['refferal_by'] > 0 && $doctor_data ['refferal_type'] == 'Doctor') ? '1' : '0';
				$appointment_category_array [$i] ['isRated'] = ($doctor_data ['patient_rated'] > 0) ? '1' : '0';
				$appointment_category_array [$i] ['appDate'] = date ( 'd-M-Y', strtotime ( $doctor_data ['app_date'] ) );
				$appointment_category_array [$i] ['appTime'] = date ( 'h:i a', strtotime ( $doctor_data ['app_time'] ) );
				$appointment_category_array [$i] ['appointmentStatus'] = ($doctor_data ['status'] === NULL) ? '' : $doctor_data ['status'];
				$appointment_category_array [$i] ['appointmentStatusName'] = ($doctor_data ['status'] === NULL) ? '' : ($doctor_data ['status'] == 1) ? 'New' : (($doctor_data ['status'] == 2) ? 'Payment Done' : (($doctor_data ['status'] == 3) ? 'Appointment Completed' : 'Appointment Canclled'));
				$appointment_category_array [$i] ['patientId'] = ($doctor_data ['patient_id'] === NULL) ? '' : $doctor_data ['patient_id'];
				$appointment_category_array [$i] ['patientId'] = ($doctor_data ['patient_id'] === NULL) ? '' : parent::getSingleResult ( "select name from #_patients where pid='" . $doctor_data ['patient_id'] . "'" );
				$appointment_category_array [$i] ['doctorName'] = ($doctor_data ['doctor_name'] === NULL) ? '' : $doctor_data ['doctor_name'];
				$appointment_category_array [$i] ['doctorEducation'] = '';
				$appointment_category_array [$i] ['doctorProfileImage'] = '';
				$appointment_category_array [$i] ['doctorExperience'] = '';
				$appointment_category_array [$i] ['doctorSpeciality'] = '';
				if ($doctor_data ['appointment_with'] == 'D') {
						
					$appointment_category_array [$i] ['doctorName'] = parent::getSingleResult ( "select name from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" ) . "(Doctor)";
					$appointment_category_array [$i] ['doctorEducation'] = parent::getSingleResult ( "select name from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['doctorProfileImage'] = SITE_PATH . "uploaded_files/doctors/profile/" . parent::getSingleResult ( "select profile_image from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['doctorExperience'] = parent::getSingleResult ( "select years_of_experience from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
					$doc_speciality=parent::getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
					if($doc_speciality==0){
						$appointment_category_array [$i] ['doctorSpeciality'] = 'General Practitioner';
					}else{
						$appointment_category_array [$i] ['doctorSpeciality'] = parent::getSingleResult ( "select name from #_specialities where pid='" . parent::getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" ) . "'" );
					}
					
					$location_type = parent::getSingleResult ( "select location_type from #_doctors_locations where pid='" . $doctor_data ['doctor_location_id'] . "' and doctor_id='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['locationType'] = $location_type;
					$appointment_category_array [$i] ['locationAddress'] = ($location_type === NULL) ? '' : parent::getSingleResult ( "select location from #_doctors_locations where pid='" . $doctor_data ['doctor_location_id'] . "' and doctor_id='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['locationName'] = ($location_type === NULL) ? '' : (($location_type == 1) ? parent::getSingleResult ( "select name from #_clinics where pid='" . parent::getSingleResult ( "select clinic_id from #_doctors_locations where pid='" . $doctor_data ['doctor_location_id'] . "' and doctor_id='" . $doctor_data ['doctor_id'] . "'" ) . "'" ) . "(Clinic)" : parent::getSingleResult ( "select name from #_hospitals where pid='" . parent::getSingleResult ( "select hospital_id from #_doctors_locations where pid='" . $doctor_data ['doctor_location_id'] . "' and doctor_id='" . $doctor_data ['doctor_id'] . "'" ) . "'" ) . "(Hospital)");
				} else if ($doctor_data ['appointment_with'] == 'L') {
					$appointment_category_array [$i] ['doctorName'] = parent::getSingleResult ( "select name from #_labs where pid='" . $doctor_data ['doctor_id'] . "'" ) . "(Lab)";
					$appointment_category_array [$i] ['locationAddress'] = parent::getSingleResult ( "select address from #_labs where pid='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['locationName'] = parent::getSingleResult ( "select name from #_labs where pid='" . $doctor_data ['doctor_id'] . "'" ) . "(Lab)";
				} else if ($doctor_data ['appointment_with'] =='H') {
					$appointment_category_array [$i] ['doctorName'] = parent::getSingleResult ( "select name from #_healthcare_organization where pid='" . $doctor_data ['doctor_id'] . "'" ) . "(Health care)";
					$appointment_category_array [$i] ['locationAddress'] = parent::getSingleResult ( "select address from #_healthcare_organization where pid='" . $doctor_data ['doctor_id'] . "'" );
					$appointment_category_array [$i] ['locationName'] = parent::getSingleResult ( "select name from #_healthcare_organization where pid='" . $doctor_data ['doctor_id'] . "'" ) . "(Health care)";
				} else {
					$appointment_category_array [$i] ['doctorName'] = "NA";
				}
				$i ++;
			}
		}
		
		return $appointment_category_array;
	}
	public function getPaymentHistoryDetails($user_id, $user_type) {
		$where_clause = '';
		$payment_history_list = array ();
		if ($user_type == 'P') {
			$where_clause = " aph.patient_id=" . $user_id;
		} else if ($user_type == 'D') {
			$where_clause = " aph.doctor_id=" . $user_id;
		}
		
		$query = "select aph.pid as payment_history_id,aph.appointment_id as appointment_id,aph.call_details_id as call_details_id,aph.doctor_id as doctor_id,aph.patient_id as patient_id,aph.payment_mode as payment_mode,aph.payment_amount as payment_amount,aph.doctor_transaction_type as doctor_transaction_type,aph.patient_transaction_type as patient_transaction_type,aph.created_on as payment_date_time from #_app_payment_history as aph Where " . $where_clause . " order by aph.pid desc";
		
		$payment_history_details = parent::db_query ( $query );
		if (mysql_num_rows ( $payment_history_details ) > 0) {
			$i = 0;
			
			while ( $payment_data = parent::db_fetch_array ( $payment_history_details ) ) {
				// $doctor_refferal_array[] = $doctor_data['user_id'];
				// $doctor_refferal_array[$i]['referralDoctorId'] = $doctor_data['referral_doctor_id'];
				$payment_history_list [$i] ['paymentHistoryId'] = $payment_data ['payment_history_id'];
				$payment_history_list [$i] ['appointmentId'] = $payment_data ['appointment_id'];
				$payment_history_list [$i] ['callDetailsId'] = ($payment_data ['call_details_id'] === NULL) ? '' : $payment_data ['call_details_id'];
				$payment_history_list [$i] ['paymentMode'] = ($payment_data ['payment_mode'] === NULL) ? '' : $payment_data ['payment_mode'];
				$payment_history_list [$i] ['transactionMode'] = ($user_type == 'P') ? $payment_data ['patient_transaction_type'] : $payment_data ['doctor_transaction_type'];
				$payment_history_list [$i] ['paymentAmout'] = ($payment_data ['payment_amount'] === NULL) ? '' : $payment_data ['payment_amount'];
				$payment_history_list [$i] ['paymentDateTime'] = ($payment_data ['payment_date_time'] === NULL) ? '' : $payment_data ['payment_date_time'];
				
				$payment_history_list [$i] ['patientId'] = ($payment_data ['patient_id'] === NULL) ? '' : $payment_data ['patient_id'];
				$payment_history_list [$i] ['patientName'] = ($payment_data ['patient_id'] === NULL || parent::getSingleResult ( "select name from #_patients where pid='" . $payment_data ['patient_id'] . "'" ) == '') ? '' : parent::getSingleResult ( "select name from #_patients where pid='" . $payment_data ['patient_id'] . "'" );
				$payment_history_list [$i] ['patientProfileImage'] = ($payment_data ['patient_id'] === NULL || parent::getSingleResult ( "select profile_pic from #_patients where pid='" . $payment_data ['patient_id'] . "'" ) === '') ? '' : SITE_PATH . "uploaded_files/profile_pictures/" . parent::getSingleResult ( "select profile_pic from #_patients where pid='" . $payment_data ['patient_id'] . "'" );
				
				$payment_history_list [$i] ['doctorId'] = ($payment_data ['doctor_id'] === NULL) ? '' : $payment_data ['doctor_id'];
				$payment_history_list [$i] ['doctorName'] = ($payment_data ['doctor_id'] === NULL) ? '' : parent::getSingleResult ( "select name from #_doctors where pid='" . $payment_data ['doctor_id'] . "'" );
				$payment_history_list [$i] ['doctorEducation'] = ($payment_data ['doctor_id'] === NULL || parent::getSingleResult ( "select education from #_doctors where pid='" . $payment_data ['doctor_id'] . "'" ) == '') ? '' : parent::getSingleResult ( "select education from #_doctors where pid='" . $payment_data ['doctor_id'] . "'" );
				$payment_history_list [$i] ['doctorProfileImage'] = ($payment_data ['doctor_id'] === NULL || parent::getSingleResult ( "select profile_image from #_doctors where pid='" . $payment_data ['doctor_id'] . "'" ) == '') ? '' : SITE_PATH . "uploaded_files/doctors/" . parent::getSingleResult ( "select profile_image from #_doctors where pid='" . $payment_data ['doctor_id'] . "'" );
				$payment_history_list [$i] ['doctorExperience'] = ($payment_data ['doctor_id'] === NULL || parent::getSingleResult ( "select years_of_experience from #_doctors where pid='" . $payment_data ['doctor_id'] . "'" ) == '') ? '' : parent::getSingleResult ( "select years_of_experience from #_doctors where pid='" . $payment_data ['doctor_id'] . "'" );
				$payment_history_list [$i] ['doctorSpeciality'] = ($payment_data ['doctor_id'] === NULL || parent::getSingleResult ( "select name from #_specialities where pid='" . parent::getSingleResult ( "select speciality from #_doctors where pid='" . $payment_data ['doctor_id'] . "'" ) . "'" ) == '') ? '' : parent::getSingleResult ( "select name from #_specialities where pid='" . parent::getSingleResult ( "select speciality from #_doctors where pid='" . $payment_data ['doctor_id'] . "'" ) . "'" );
				$i ++;
			}
		}
		$query = "UPDATE crm_notification_details set un_read=0 WHERE title Like '%Wallet%' And receiver_id='".$user_id."' and receiver_type='".$user_type."'";
		$updated_wallet_un_read = parent::db_query ( $query );
		return $payment_history_list;
	}
	public function getDoctorsListOnLocation($location_id, $location_type) {
		$where_clause = '';
		$doctor_list = array ();
		if ($location_type == 1) {
			$where_clause = " dl.clinic_id=" . $location_id;
		} else if ($location_type == 2) {
			$where_clause = "dl.hospital_id=" . $location_id;
		}
		
      //  $query = "select dl.pid as location_id,dl.doctor_id as doctor_id from #_doctors_locations as dl Where " . $where_clause . " and dl.is_verified=1 and dl.status=1 GROUP BY dl.doctor_id";
	    $query = "SELECT dl.pid as location_id,dl.doctor_id as doctor_id
FROM #_doctors_locations as dl
INNER JOIN #_doctors as D ON D.pid = dl.doctor_id Where " . $where_clause . " and dl.is_verified=1 and dl.status=1 GROUP BY dl.doctor_id ORDER BY D.name";

		
		$payment_history_details = parent::db_query ( $query );
		if (mysql_num_rows ( $payment_history_details ) > 0) {
			$i = 0;
			
			while ( $doctor_data = parent::db_fetch_array ( $payment_history_details ) ) {
				// $doctor_refferal_array[] = $doctor_data['user_id'];
				// $doctor_refferal_array[$i]['referralDoctorId'] = $doctor_data['referral_doctor_id'];
				$doctor_list [$i] ['locationId'] = $doctor_data ['location_id'];
				
				$doctor_list [$i] ['doctorId'] = ($doctor_data ['doctor_id'] === NULL) ? '' : $doctor_data ['doctor_id'];
				$doctor_list [$i] ['doctorName'] = ($doctor_data ['doctor_id'] === NULL || parent::getSingleResult ( "select name from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" ) == '') ? '' : parent::getSingleResult ( "select name from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
				$doctor_list [$i] ['doctorEmail'] = ($doctor_data ['doctor_id'] === NULL || parent::getSingleResult ( "select email from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" ) == '') ? '' : parent::getSingleResult ( "select email from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
				$doctor_list [$i] ['doctorEducation'] = ($doctor_data ['doctor_id'] === NULL || parent::getSingleResult ( "select education from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" ) == '') ? '' : parent::getSingleResult ( "select education from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
				$doctor_list [$i] ['doctorProfileImage'] = ($doctor_data ['doctor_id'] === NULL || parent::getSingleResult ( "select profile_image from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" ) == '') ? '' : SITE_PATH . "uploaded_files/doctors/profile/" . parent::getSingleResult ( "select profile_image from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
				$doctor_list [$i] ['doctorExperience'] = ($doctor_data ['doctor_id'] === NULL || parent::getSingleResult ( "select years_of_experience from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" ) == '') ? '' : parent::getSingleResult ( "select years_of_experience from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
				$doc_speciality=parent::getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" );
				if($doc_speciality==0){
					$doctor_list [$i] ['doctorSpeciality']='General Practitioner';
				}else{
					$doctor_list [$i] ['doctorSpeciality'] = ($doctor_data ['doctor_id'] === NULL || parent::getSingleResult ( "select name from #_specialities where pid='" . parent::getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" ) . "'" ) == '') ? '' : parent::getSingleResult ( "select name from #_specialities where pid='" . parent::getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_data ['doctor_id'] . "'" ) . "'" );
				}
				
				$i ++;
			}
		}
		
		return $doctor_list;
	}
	public function get_address($table_name, $id, $filter_name) {
		$data = array ();
		$wh = '';
		/*
		 * if ($filter_name == 'doctors') {
		 * if (!empty($id)) {
		 * $pid = $id;
		 *
		 * $wh = " and D.pid ='" . $pid . "'";
		 *
		 * }
		 * $query = parent::db_query("select * from #_" . $table_name . " as D where 1=1 " . $wh . " ");
		 * if (mysql_num_rows($query) > 0) {
		 * $i = 0;
		 * while ($row = parent::db_fetch_array($query)) {
		 * $data [$i] ['pid'] = $row ['pid'];
		 * $data [$i] ['address'] = $row ['address'];
		 * $i++;
		 * }
		 * }
		 *
		 * }
		 */
		if (($filter_name == 'hospitals') || ($filter_name == 'labs') || ($filter_name == 'clinics')) {
			if (! empty ( $id )) {
				$pid = $id;
				$wh = " and H.pid ='" . $pid . "'";
			}
			$query = parent::db_query ( "select * from #_" . $table_name . " as H where 1=1 " . $wh . "  " );
			if (mysql_num_rows ( $query ) > 0) {
				$i = 0;
				while ( $row = parent::db_fetch_array ( $query ) ) {
					$data [$i] ['pid'] = $row ['pid'];
					$data [$i] ['name'] = $row ['name'];
					$name = $row ['name'];
					$address = $row ['address'];
					$nearest_land_mark = $row ['nearest_land_mark'];
					$nearest_bus_stop = $row ['nearest_bus_stop'];
					$township = $row ['township'];
					$division = $row ['division'];
					$township = parent::getSingleResult ( "select name from #_township where pid='" . $township . "'" );
					$division = parent::getSingleResult ( "select name from #_division where pid='" . $division . "'" );
					$address_from_db = $row ['address'];
					if (! empty ( $name )) {
						$address = $name;
					}
					if (! empty ( $township )) {
						$address = $name . ' ' . $township;
					}
					$latlong = self::get_lat_long ( $address ); // create a function with the name "get_lat_long" given as below
					if ($latlong == ',') {
						$address = $name . ' ' . $division;
						$latlong = self::get_lat_long ( $address ); // create a function with the name "get_lat_long" given as below
					}
					if ($latlong == ',') {
						$address = $name . ' ' . $nearest_land_mark;
						$latlong = self::get_lat_long ( $address ); // create a function with the name "get_lat_long" given as below
					}
					if ($latlong == ',') {
						$address = $name . ' ' . $nearest_bus_stop;
						$latlong = self::get_lat_long ( $address ); // create a function with the name "get_lat_long" given as below
					}
					if ($latlong == ',') {
						$address = $name . ' ' . $address_from_db;
						$latlong = self::get_lat_long ( $address ); // create a function with the name "get_lat_long" given as below
					}
					if ($latlong == ',') {
						$address = $address_from_db;
						$latlong = self::get_lat_long ( $address ); // create a function with the name "get_lat_long" given as below
					}
					if ($latlong == ',') {
						$address = $township;
						$latlong = self::get_lat_long ( $address ); // create a function with the name "get_lat_long" given as below
					}
					if ($latlong == ',') {
						$address = $division;
						$latlong = self::get_lat_long ( $address ); // create a function with the name "get_lat_long" given as below
					}
					if ($latlong == ',') {
						$address = $name;
						$latlong = self::get_lat_long ( $address ); // create a function with the name "get_lat_long" given as below
					}
					
					$data [$i] ['latlong'] = $latlong;
					$latlong_details = array (
							'latlong' => $latlong 
					);
					$latlong_from_db = $row ['latlong'];
					if ($filter_name == 'hospitals') {
						if (($latlong_from_db == '') || ($latlong_from_db == 'null')) {
							$user_id = parent::sqlquery ( "rs", 'hospitals', $latlong_details, $update = 'pid', $id = $row ['pid'] );
						}
					}
					if ($filter_name == 'labs') {
						if (($latlong_from_db == '') || ($latlong_from_db == 'null')) {
							$user_id = parent::sqlquery ( "rs", 'labs', $latlong_details, $update = 'pid', $id = $row ['pid'] );
						}
					}
					if ($filter_name == 'clinics') {
						if (($latlong_from_db == '') || ($latlong_from_db == 'null')) {
							$user_id = parent::sqlquery ( "rs", 'clinics', $latlong_details, $update = 'pid', $id = $row ['pid'] );
						}
					}
					
					$i ++;
				}
			}
		}
		return $data;
	}
	
	// function to get the address
	public function get_lat_long($address) {
		$address = str_replace ( " ", "+", $address );
		$url = "https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=AIzaSyAFcge70nYYfzdjJ7IloOyA9zJIZrxJQb0&region=Myanmar";
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, CURLOPT_PROXYPORT, 3128 );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		$response = curl_exec ( $ch );
		curl_close ( $ch );
		$response_a = json_decode ( $response );
		$lat = $response_a->results [0]->geometry->location->lat;
		$long = $response_a->results [0]->geometry->location->lng;
		return $lat . ',' . $long;
	}
	
	public function TimeIsBetweenTwoTimes($start, $end, $current_time) {
	$current_time=date('H:i',strtotime($current_time));
	$start = DateTime::createFromFormat('H:i', $start);
	$end = DateTime::createFromFormat('H:i', $end);
	$current_time = DateTime::createFromFormat('H:i', $current_time);
	if ($start > $end) $end->modify('+1 day');
	return ($start <= $current_time && $current_time <= $end) || ($start <= $current_time->modify('+1 day') && $current_time <= $end);
}
}