<?php
/*
# @package BSC CMS

# Copy right code 
# The base configurations of the BSC CMS.
# This file has the following configurations: MySQL settings, Table Prefix,
# by visiting {config.inc.php} Codex page. You can get the MySQL settings from your web host.

# MySQL settings - You can get this info from your web host
# The name of the database for BSC CMS

# Developer by Krishna Sharma
# eMail: sse.krishna@gmail.com
*/

class FuncsAmd {  
	private $var;
	
	public function adminsbox(){
		return '';
	}
	
	public function adminebox(){
		return '';
	}
	
	public function breadcrumb($com){
		if($com=='pages'){
			return "CMS";	
		} else if($com=='bg'){
			return "Background";		
		}else if($com=='dt'){
			return "Device type";		
		}else if($com=='top'){
			return "Type of product";		
		} 
		else if($com=='adm'){
			return "CMS User";		
		}else if($com=='salesorders'){
			return "Sales Orders";		
		}
		else if($com=='sales'){
			return "Sales user";		
		}
		else if($com=='cate'){
			return "Category";		
		}
		else if($com=='category'){
			return "Category";		
		} 
		
		else if($com=='member'){
			return "Customers";		
		}else if($com=='setting'){
			if($mode=="payment"){
				return "Payment Gateway";		
			}
		} 
		else{
			return ucfirst($com);		
		}
	}
	public function admincids($val){
		$arr = explode(",",$val);
		$newarr = array();
		foreach($arr as $k => $v) {
			if($v)
				$newarr[] = $v;
		}
		return $newarr;
	}
	public function calcinch($f, $i){
		$inch = ($f * (12)) + $i;
		return $inch;
	}
	public function sessset($val, $msg=""){
		$_SESSION['sessmsg'] = $val;
		$_SESSION['alert'] = $msg;
	}
	
	public function adminpublish($val){
		if($val == 'Active'){
			return "Deactivate";
		} else {
			return "Activate";
		}
	}
	public function secure(){
		
		 if (!$_SESSION["AMD"][0] and !$_SESSION["AMD"][2])
		 {
			//header('Location: '.SITE_PATH_ADM.'login.php');
			echo '
			<script type="text/javascript">
			<!--
			window.location.href="'.SITE_PATH_ADM.'";
			-->
			</SCRIPT>'
			;
			exit;	 
		}
	}
	public function url($file){
		return SITE_PATH_ADM.$file.".php";
	}
	public function iurl($comp,$mode='', $id='', $action=''){
		return SITE_PATH_ADM."home.php?comp=".$comp.(($mode)?"&mode=".$mode:'').(($id)?"&uid=".$id:'').(($action)?"&action=".$action:'');
	}
	public function furl($file){
		return SITE_PATH_ADM.$file."/";
	}

	public function alert(){
		if($_SESSION['alert']=='e'){
			echo $this->error();
			unset($_SESSION['sessmsg']);$_SESSION['sessmsg']='';
		}
		if($_SESSION['alert']=='w'){
			echo $this->warning();	
			unset($_SESSION['sessmsg']);$_SESSION['sessmsg']='';
		}
		if($_SESSION['alert']=='s'){
			echo $this->success();	
			unset($_SESSION['sessmsg']);$_SESSION['sessmsg']='';
		}
	}
	
	public function alertfront(){
		if($_SESSION['alert']=='e'){
			echo $this->error2();
			unset($_SESSION['sessmsg']);$_SESSION['sessmsg']='';
		}
		if($_SESSION['alert']=='w'){
			echo $this->warning();	
			unset($_SESSION['sessmsg']);$_SESSION['sessmsg']='';
		}
		if($_SESSION['alert']=='s'){
			echo $this->success();	
			unset($_SESSION['sessmsg']);$_SESSION['sessmsg']='';
		}
	}
	
	public function info() {
		if($_SESSION['sessmsg']){
			return '<div class="alertMessage info SE">'.$_SESSION['sessmsg'].'</div>';		
		}
	}
	
	public function error() {
		if($_SESSION['sessmsg']){
			return '<div class="error-msg2 alertMessage error SE">'.$_SESSION['sessmsg'].'</div>';		
		}
	}
	
	public function error2() {
		if($_SESSION['sessmsg']){
			return '<div class="error-msg2 alertMessage error2 SE">'.$_SESSION['sessmsg'].'</div>';		
		}
	}
	
	public function warning() {
		if($_SESSION['sessmsg']){
			return '<div class="error-msg2 alertMessage warning SE">'.$_SESSION['sessmsg'].'</div>';		
		}	
	}
	
	public function success() {
		if($_SESSION['sessmsg']){
			return '<div class="error-msg alertMessage success SE">'.$_SESSION['sessmsg'].'</div>';	
		}	
	}
	
	public function rowerror($n) {
		return '<tr class="grey"><td align="center" colspan="'.$n.'"><b>Sorry! No record in databse.</b></td></tr>';	
	} 
	
	public function even_odd($vars){
			if($vars%2==1){
				return ' class="grey"';		
			}
	}
	
	public function orders($vars, $files=''){
		return $vars;
		//return '<a href="'.(($files)?$PHP_SELF:'javascript:void(0);').'">'.$vars.'</a>';
	}
	public function norders($vars){
		return $vars;
	}
	
	public function check_all(){
		return '<input name="check_all" type="checkbox" id="check_all" value="1" onClick="checkall(this.form)">';
	}
	
	public function check_input($vars){
		return '<input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="'.$vars.'">';
	}
	public function actiond($comp, $pid){		return '<a class="deletefile" href="'.SITE_PATH_ADM.'home.php?comp='.$comp.'&uid='.$pid.'&action=del" onclick="return confirm(\'Do you want delete this record?\');"><i class="fa fa-times"></i></a>';	}
	public function actionSame($comp, $pid, $pgn){
		return '<a href="'.SITE_PATH_ADM.'index.php?comp='.$comp.'&mode=add&uid='.$pid.'&start='.$pgn.'"><img src="'.SITE_PATH_ADM.'images/icon_edit.png" alt="Edit Record" title="Edit Record"/></a><a href="'.SITE_PATH_ADM.'index.php?comp='.$comp.'&uid='.$pid.'&action=del&start='.$pgn.'" onclick="return confirm(\'Do you want delete this record?\');"><img src="'.SITE_PATH_ADM.'images/delete-icon.png"  alt="Delete Record"  title="Delete Record"/></a>';
	}
	public function action($comp, $pid){
		
		
		return '<a class="editfile" href="'.SITE_PATH_ADM.'home.php?comp='.$comp.'&mode=add&uid='.$pid.'"><i class="fa fa-pencil"></i></a><a class="deletefile" href="'.SITE_PATH_ADM.'home.php?comp='.$comp.'&uid='.$pid.'&action=del" onclick="return confirm(\'Do you want delete this record?\');"><i class="fa fa-times"></i></a>';
	}
    public function verify_action($comp, $pid){

        return '<a class="editfile" href="'.SITE_PATH_ADM.'home.php?comp='.$comp.'&mode=add&uid='.$pid.'"><i class="fa fa-pencil"></i></a>';
    }
    public function verify_user_action($comp, $verification_crm_url){

        return '<a class="editfile" href="'.$verification_crm_url.'"><i class="fa fa-pencil"></i></a>';
    }
	public function verify_user_action_with_del($comp, $verification_crm_url,$pid){

        return '<a class="editfile" href="'.$verification_crm_url.'"><i class="fa fa-pencil"></i></a><a class="deletefile" href="'.SITE_PATH_ADM.'home.php?comp='.$comp.'&uid='.$pid.'&action=del" onclick="return confirm(\'Do you want delete this record?\');"><i class="fa fa-times"></i></a>';
    }
	public function Eaction($comp, $pid){
		$eurl = SITE_PATH_ADM.'index.php?comp='.$comp.'&mode=add&uid='.$pid;
		return ' onclick="location.href=\''.$eurl.'\';" ';
	}
	public function Caction($comp, $pid){
		$eurl = SITE_PATH_ADM.'index.php?comp='.$comp.'&mode=customquotesupdate&uid='.$pid;
		return ' onclick="location.href=\''.$eurl.'\';" ';
	}
	public function cataction($vars, $ids, $tags=''){
		return '<table border="0" cellpadding="0" cellspacing="0"><tr><td><a href="'.$vars.'?id='.$ids.'"><img src="'.SITE_PATH_ADM.'images/edit-btn.jpg" alt="Edit record" title="Edit record"/></a></td><td style="padding-left:15px;"><a href="'.SITE_PATH_ADM.(($tags)?$tags:CPAGE).'?id='.$ids.'&action=del&view=true" onclick="return confirm(\'Do you want delete this record?\');"><img src="'.SITE_PATH_ADM.'images/delete-btn.jpg" alt="Delete record" title="Delete record"/></a></td></tr></table>';
	}
	public function action2($comp, $pid){
		return '<a href="'.SITE_PATH_ADM.'index.php?comp='.$comp.'&mode=add&uid='.$pid.'"><img src="'.SITE_PATH_ADM.'images/icon_edit.png" alt=""></a><a href="javascript:void(0);"><img  style="opacity:0.8;color:#333; " src="'.SITE_PATH_ADM.'images/delete-icon.png" alt=""></a>';
	}
	public function actionEX($comp, $pid){
		return '<div align="center"><a href="'.SITE_PATH_ADM.'index.php?comp='.$comp.'&mode=add&uid='.$pid.'"><img src="'.SITE_PATH_ADM.'images/icon_edit.png" alt=""></a></div>';
	}
	
	public function action3($comp, $pid){
		return '<a href="'.SITE_PATH_ADM.'index.php?comp='.$comp.'&mode=add&uid='.$pid.'"><img src="'.SITE_PATH_ADM.'images/icon_edit.png" alt=""></a>';
	}
	public function action4($comp, $pid){
		return '<a href="'.SITE_PATH_ADM.'index.php?comp='.$comp.'&mode=add&uid='.$pid.'"><img src="'.SITE_PATH_ADM.'images/view-icon.png" alt=""></a>';
	}
	public function viewmode($comp, $pid){
		return '<a href="'.SITE_PATH_ADM.'index.php?comp='.$comp.'&mode=view&uid='.$pid.'"><img src="'.SITE_PATH_ADM.'images/view-icon.png" alt=""></a>';
	}
	
	public function h1_tag($vars, $others='&nbsp;'){
		return '<h1><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%" align="left">'.$vars.'</td><td width="50%">'.$others.'</td></tr></table></h1>';
	}
	
	public function heading($vars){
		return '<h2>'.$vars.'</h2>';
	}
	public function get_editor($fld, $vals, $path='', $w='900', $h='350'){
		return '<textarea name="'.$fld.'" id="'.$fld.'"  rows=""  cols="" class="textareas">'.$vals.'</textarea><script type="text/javascript">
				window.onload = function(){
					var editor=CKEDITOR.replace(\''.$fld.'\',{
			        uiCoor : \'#9AB8F3\',
					width : \''.$w.'px\',
					height : \''.$h.'px\'
    			} );
				CKFinder.setupCKEditor( editor, \''.SITE_SUB_PATH.'lib/ckfinder/\' );};</script>';
	}
	/*public function get_editor($fld, $vals, $path='', $w='80', $h='10'){
		return '<textarea name="'.$fld.'" id="'.$fld.'"  rows="$w"  cols="$h" class="textareas">'.$vals.'</textarea><script type="text/javascript">
//<![CDATA[
    CKEDITOR.replace(\''.$fld.'\',
	{
     	fullPage : true,
            extraPlugins : \'\'
    });
//]]>
</script>';
	}*/
	
	public function get_editor_s($fld, $vals, $w='45', $h='7'){
		return '<textarea cols="'.$w.'" id="'.$fld.'" name="'.$fld.'" rows="'.$h.'">'.$vals.'</textarea>
		<script type="text/javascript">
		//<![CDATA[

			// Replace the <textarea id="editor"> with an CKEditor
			// instance, using default configurations.
			CKEDITOR.replace( \''.$fld.'\',
				{
					extraPlugins : \'uicolor\',
					toolbar :
					[
						[ \'Bold\', \'Italic\', \'-\', \'NumberedList\', \'BulletedList\', \'-\', \'Link\', \'Unlink\' ],
						[ \'UIColor\' ]
					]
				});

		//]]>
		</script>';
	}
	
	public function imageurl($string){
		
		$string=strtolower($string);
		$string=preg_replace('/\s+/',' ',$string);
		$string=trim($string);
		//$string = str_replace(' ', '-', $string);
		$string =preg_replace('/[^A-Za-z0-9\-]/', '', $string);
		$string = preg_replace('/-+/', '-', $string);
		
		$string = str_replace(" ", "",trim(strtolower($string)));
		$string = str_replace("/", " ",$string);
		$string = str_replace('\\', " ",$string);
		$string = str_replace("(", "",$string);
		$string = str_replace(")", "",$string);
		$string = str_replace("&", "",$string);
		$string = str_replace("#", "",$string);
		$string = str_replace("---", "",$string);
		$string = str_replace("--", "",$string);
		$string = str_replace("-", "",$string);
		$string = str_replace("&shy;", "",$string);
		$string = str_replace("&minus;", "",$string);
		$string = str_replace("'", "",$string);
		$string = str_replace('"', "",$string);
		$string = str_replace(" –", "",$string);
		$string = str_replace("+", "",$string);
		$string = str_replace(",", "",$string);
		$string = str_replace("   ", " ",$string);
		$string = str_replace("  ", " ",$string);
		
		return $string;
	}
	
	
	public function imagename($designers_sku_code,$design_title,$color_name,$materials,$sub_materials,$speciel_category,$die_type_name,$die_name,$brand_name,$series_name,$model_name)
	{
	     $imagename='';
		 if($designers_sku_code!='')
		 {
		   $imagename =$this->imageurl(trim($designers_sku_code));	 
		 }
		 if($design_title!='')
		 {
		   $imagename .=' ('.$this->imageurl(trim($design_title)).') ';	 
		 }
		 
		 if($color_name!='')
		 {
		   $imagename .= $this->imageurl(trim($color_name)).' ';	 
		 }
		 
		 if($materials!='')
		 {
		   $imagename .= $this->imageurl(trim($materials)).' ';	 
		 }
		 
		 if($sub_materials!='')
		 {
		   $imagename .= $this->imageurl(trim($sub_materials)).' ';	 
		 }
		 
		 if($speciel_category!='')
		 {
		   $imagename .=$this->imageurl(trim($speciel_category)).' ';	 
		 }
		 
		 if($die_type_name!='')
		 {
		   $imagename .=$this->imageurl(trim($die_type_name)).' ';	 
		 }
		 
		 if($die_name!='')
		 {
		   $imagename .=$this->imageurl(trim($die_name)).' ';	 
		 }
		 
		 if($brand_name!='')
		 {
		   $imagename .='for '.$this->imageurl(trim($brand_name)).' ';	 
		 }
		 
		 if($series_name!='')
		 {
		   $imagename .=$this->imageurl(trim($series_name)).' ';	 
		 }
		 
		 if($model_name!='')
		 {
		   $imagename .=$this->imageurl($model_name);	 
		 }
		 
		 return $imagename;
		 
	}
	
	public function mk_prdname($color_name,$materials,$sub_materials,$speciel_category,$die_type_name,$die_name,$brand_name,$series_name,$model_name)
	{
		// $color_name.' '.$materials.' '.$sub_materials.' '.$speciel_category.' '.$die_type_name.' '.$die_name.' for '.$brand_name.' '.$series_name.' '.$model_name;
	     $imagename; 
	   
	     if($color_name!='')
		 {
		   $imagename .= (trim($color_name)).' ';	 
		 }
		 
		 if($materials!='')
		 {
		   $imagename .= (trim($materials)).' ';	 
		 }
		 
		 if($sub_materials!='')
		 {
		   $imagename .= (trim($sub_materials)).' ';	 
		 }
		 
		 if($speciel_category!='')
		 {
		   $imagename .=(trim($speciel_category)).' ';	 
		 }
		 
		 if($die_type_name!='')
		 {
		   $imagename .=(trim($die_type_name)).' ';	 
		 }
		 
		 if($die_name!='')
		 {
		   $imagename .=(trim($die_name)).' ';	 
		 }
		 
		 if($brand_name!='')
		 {
		   $imagename .='for '.(trim($brand_name)).' ';	 
		 }
		 
		 if($series_name!='')
		 {
		   $imagename .=(trim($series_name)).' ';	 
		 }
		 
		 if($model_name!='')
		 {
		   $imagename .=($model_name);	 
		 }
	   return $imagename;	
	}  
	
	
	public function baseurl($string){
		
		$string=strtolower($string);
		$string=preg_replace('/\s+/',' ',$string);
		$string=trim($string);
		$string = str_replace(' ', '-', $string);
		$string =preg_replace('/[^A-Za-z0-9\-]/', '', $string);
		$string = preg_replace('/-+/', '-', $string);
		
		/*$vals = str_replace(" ", "",trim(strtolower($vals)));
		$vals = str_replace("/", "",$vals);
		$vals = str_replace("(", "",$vals);
		$vals = str_replace(")", "",$vals);
		$vals = str_replace("&", "",$vals);
		$vals = str_replace("#", "",$vals);
		$vals = str_replace("---", "",$vals);
		$vals = str_replace("--", "",$vals);
		$vals = str_replace("-", "",$vals);
		$vals = str_replace("&shy;", "",$vals);
		$vals = str_replace("&minus;", "",$vals);
		$vals = str_replace("'", "",$vals);
		$vals = str_replace('"', "",$vals);
		$vals = str_replace(" –", "",$vals);
		$vals = str_replace("+", "",$vals);
		$vals = str_replace(",", "",$vals);
		*/
		return $string;
	}
	
	public function spl($vals){
		$vals = str_replace("'", "&#039;",trim($vals));
		$vals = str_replace('"', "&quot;",trim($vals));
		return $vals;
	}
	public function spl1($vals){
		$vals = str_replace("'", "&#039;",trim($vals));
		return $vals;
	}
	public function viewimage($path	, $img){
		if($img and file_exists(UP_FILES_FS_PATH."/".$path."/".$img)){
			return '<a href="'.SITE_PATH.'uploaded_files/'.$path.'/'.$img.'" target="_blank">View</a>';
		} else{
			return "N/A";
		}
	} 
	public function textcount($one,$two, $num){
		return 'onKeyDown="textCounter(document.aforms.'.$one.',document.aforms.'.$two.','.$num.')" onKeyUp="textCounter(document.aforms.'.$one.',document.aforms.'.$two.','.$num.')"';
	}
	public function maxnum($name, $num){
		return '<input readonly type="text" name="'.$name.'" size="3" maxlength="3" value="'.$num.'">';
	}
	
	public function compname($name)
	{
		$name = str_replace('_',' ',$name);
		$name = ucfirst($name);
		return $name;
	}
    public function send_verification_mail()
    {

    }
    public function display_is_verified_status($status)
    {
        switch ($status) {
            case 0:
                $val ='Not Done';
                break;
            case 1:
                $val ='Done';
                break;


            default:
                //code to be executed if n is different from all labels;
        }

        return $val;
    }
	public function displaystatus($status)
	{
	   switch ($status) {
						case 0:
								 $val ='Inactive';
								 break;
						case 1:
								 $val ='Active';
								 break;
								 
							case 2:
								 $val ='Processing';
								 break;
								 
						case 3:
								 $val ='Finished';
								 break;
						
						
						default:
							//code to be executed if n is different from all labels;
                      }	
					  
		return $val;
	}
	
	public function user_type($user)
	{
	   switch ($user) {
						
						case 1:
								 $val ='Super Admin';
								 break;
						
						default:
							//code to be executed if n is different from all labels;
                      }	
					  
		return $val;
	}


}
?>