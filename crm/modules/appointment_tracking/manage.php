<?php
//include(FS_ADMIN ."app/class/doctor.php");
include(FS_ADMIN . _MODS . "/appointment_tracking/user.inc.php");
$US = new Users();

if ($action) {

    if ($uid > 0 || !empty($arr_ids)) {

        switch ($action) {
			
			 case "del":
						 $OP->delete($uid);
						 $ADMIN->sessset('Record has been deleted', 'e'); 
						 break;
						 
		  case "Delete":
						 $OP->delete($arr_ids);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Deleted', 'e');
						 break;
						 

            case "Active":
                $US->status($arr_ids, 1);
                $ADMIN->sessset(count($arr_ids) . ' Item(s) Active', 's');
                break;

            case "Inactive":
                $US->status($arr_ids, 0);
                $ADMIN->sessset(count($arr_ids) . ' Item(s) Inactive', 's');
                break;


            default:
        }
        $BSC->redir($ADMIN->iurl($comp), true);
    }
}

$start = intval($start);
$pagesize = intval($pagesize) == 0 ? (($_SESSION["totpaging"]) ? $_SESSION["totpaging"] : DEF_PAGE_SIZE) : $pagesize;

list($result, $reccnt) = $US->display($start, $pagesize, $fld, $otype, $search_data);

?>
<style>
    .controls-button li {
        display: none;
    }
    .controls-button li:first-child {
        display: none;
    }

    .controls-button li:last-child {
        display: none;
    }
</style>
<!--right section panel-->
<div class="vd_content-section clearfix">
    <div class="row">

        <div class="col-md-12">
            <?= $ADMIN->alert() ?>
            <div class="info-call-details">
                <ul>
                    <li>  <input type="text"  name="search_data"  value="<?=$search_data?>"  placeholder="Search"/></li>
                    <li><input type="submit" class="records-search greenbutton inputsearch" value="Search"></li>
                </ul>
            </div>

            <div class="panel-heading vd_bg-green white">

                <h3 class="panel-title">Appointment Details  </h3>
            </div>
            <div class="section-body">




                <div class="col-md-12">
                    <!--edit table-->
                    <div class="table-responsive " id="ordrz">
                        <table class="table data-tbl custom-style table-striped">
                            <thead>
                            <tr class="tbl-head">
                                <th><?= $ADMIN->check_all() ?></th>
                                <th>No.</th>
                                <th>Appointment With</th>
                                <th>Doctor/ Lab/ Healthcare Name</th>
                                <th>Patient Name</th>
                                <th>Patient Phone</th>
                                <th>
                                    <a href="<?= $ADMIN->iurl($comp) ?>&fld=app_date<?= (($otype == 'asc') ? "&otype=desc" : '&otype=asc') ?>" <?= (($fld == 'app_date') ? 'class="selectedTab"' : '') ?>><span <?= (($otype == 'asc') ? 'class="des"' : 'class="asc"') ?>>Appointment Date</span></a>
                                </th>
                                <th>Appointment Time</th>
                                <th>Location Name</th>
                                <th>Location Phone</th>
                                <th>Location Address</th>
                                <th>Status</th>
                            </tr>
                            </thead>

                            <!--</table>
                                 <ul class="sortable-liststyle" style="list-style:none;">
                      -->
                            <tbody>
                            <?php if ($reccnt) {
                                $nums = (($start) ? $start + 1 : 1);
                                $k = 0;
                                $doc_lab_helth_name='';
                                while ($line = $PDO->db_fetch_array($result)) {
                                    @extract($line);
                                    $k++;
                                    $css = ($k % 2 != 0) ? 'success' : '';


                                    ?>
                                    <!--<li style="list-style:none;" id="recordsArray_<?= $pid ?>">
			<table class="table data-tbl custom-style">
			 <thead>-->
                                    <tr data-item-id=1 class="item <?= $css ?>">
                                        <th><?= $ADMIN->check_input($pid) ?></th>
                                        <th><?= $nums ?></th>
                                        <?php
                                        $doc_lab_helth_name='';
                                        //http://192.168.0.123/DoctorOnCall/crm/home.php?comp=doctors&mode=add&uid=8400&hospital_id=106
                                        if($appointment_with=='D'){
                                            $appointment_with_name='Doctor';
                                            $doc_lab_helth_name= $DoctorName;
                                            $location_address = $PDO->getSingleResult("select location from #_doctors_locations where pid='" . $doctor_location_id . "'");
                                        }
                                        if($appointment_with=='H'){
                                            $appointment_with_name='Health Care';
                                            $doc_lab_helth_name= $healthcareName;
                                            $location_address = $PDO->getSingleResult("select address from #_healthcare_organization where pid='" . $doctor_id . "'");
                                        }
                                        if($appointment_with=='L'){
                                            $appointment_with_name='Lab';
                                            $doc_lab_helth_name= $LabName;
                                            $location_address = $PDO->getSingleResult("select address from #_labs where pid='" . $doctor_id . "'");
                                        }
                                        if($appointment_with=='O'){
                                            $appointment_with_name='Others';
                                            $location_address = '';

                                        }
                                        if($appointment_with=='N'){
                                            $appointment_with_name='None';
                                            $location_address = '';
                                        }
                                        if($status==1){
                                            $app_status='New';
                                        }
                                        if($status==2){
                                            $app_status='Completed With Payment';
                                        }
                                        if($status==3){
                                            $app_status='Completed Without Payment';
                                        }
                                        if($status==4){
                                            $app_status='Cancelled';
                                        }
                                        if($status==0){
                                            $app_status='No Status';
                                        }
                                       $patient_name = $PDO->getSingleResult("select name from #_patients where pid='" . $patient_id . "'");
									   $patient_phone = $PDO->getSingleResult("select phone from #_patients where pid='" . $patient_id . "'");

                                       $location_address=explode('~',$location_address);
                                       ?>
                                        <th>
										
                                           <?= ucwords($appointment_with_name) ?>
                                        </th>
                                        <th><?= $doc_lab_helth_name ?></th>
                                        <th><?= $patient_name ?></th>
                                        <th><?= $patient_phone ?></th>
                                        <th><?= $app_date ?></th>
                                        <th><?= date('h:i a ', strtotime($app_time)); ?></th>
                                        <th><?php
                                        if($locationType==1){
                                            echo $location_name = $PDO->getSingleResult("select name from #_clinics where pid='" . $clinicId . "'")."("."Clinics".")";
                                        }
                                        if($locationType==2){
                                           echo $location_name= $PDO->getSingleResult("select name from #_hospitals where pid='" . $hospitalId . "'")."("."Hospitals".")";
                                        }?>
                                        </th>
                                        <th>
										<?php
                                        if($locationType==1){                                            
                                            echo $location_phone= $PDO->getSingleResult("select phone from #_clinics where pid='" . $clinicId . "'");
                                        }
                                        if($locationType==2){
                                            echo $location_phone= $PDO->getSingleResult("select phone from #_hospitals where pid='" . $hospitalId . "'");
                                        }?>
                                        </th>
                                        <th><?= ( $location_address!='') ? $location_address[0] .' '. $location_address[1] : '' ; ?></th>
                                        <th><?= $app_status ?></th>
										
                                    </tr>
                                    <!-- </thead>
                                    </table>
                                    </li>-->
                                    <?php $nums++;
                                } ?>


                            <?php } else {
                                echo '<div align="center" class="norecord">No Record Found</div>';
                            } ?>

                            <!-- </ul>-->
                            </tbody>
                        </table>

                        <?php include("cuts/paging.inc.php"); ?>
                        <div class="pull-right pagination" style="display:none;">
                            <ul class="pagination">
                                <li class="page-pre"><a href="#">‹</a></li>
                                <li class="page-number active"><a href="#">1</a></li>
                                <li class="page-number"><a href="#">2</a></li>
                                <li class="page-number"><a href="#">3</a></li>
                                <li class="page-number"><a href="#">4</a></li>
                                <li class="page-number"><a href="#">5</a></li>
                                <li class="page-last-separator disabled"><a href="#">...</a></li>
                                <li class="page-last"><a href="#">80</a></li>
                                <li class="page-next"><a href="#">›</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- close edit table-->
                </div>
            </div>

            <!--next button-->

            <!--Close next button-->
        </div>
    </div>
</div>
<!--Close right section panel-->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
    jQuery(document).ready(function () {
        jQuery(function () {
            jQuery("#ordrz ul").sortable({
                opacity: 0.6, cursor: 'move', update: function () {
                    var order = jQuery(this).sortable("serialize") + '&tbl=<?=tblName?>&field=pid';
                    $.post("<?=SITE_PATH_ADM?>modules/orders.php", order, function (theResponse) {
                    });
                }
            });
        });
    });
</script>