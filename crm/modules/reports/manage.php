<?php 
include(FS_ADMIN._MODS."/reports/class.inc.php");
$OP = new Options();


$search_data = @$_GET['search'];

$start = intval($start);
$pagesize = intval($pagesize)==0?(($_SESSION["totpaging"])?$_SESSION["totpaging"]:DEF_PAGE_SIZE):$pagesize;
list($result,$reccnt) = $OP->display($start,$pagesize,$fld,$otype,$search_data);

?>
<!--right section panel-->
		<div class="vd_content-section clearfix">
		  	<div class="row">
			<style>
			.sarch-box-records{
				margin:5px;
			}
				.btn-records{
						z-index:999999;
						
				}
				.sarch-box-records:focus{
					
						background:#fff;
				}
				.records-search{
						padding: 7px 13px;
						background:#1FAE66;
						color:#fff;
						border:0px;
						margin:5px;
						
				}
				.records-search:active{
					padding: 7px 13px;
						background:#1FAE66;
						color:#fff;
						border:0px;
						margin:5px;
				}
				.records-search:focus{
					padding: 7px 13px;
						background:#1FAE66;
						color:#fff;
						border:0px;
						margin:5px;
				}
			</style>
              <div class="col-md-12">
			  <?=$ADMIN->alert()?>
			  <div class="form-group">
					<div class="col-md-8 col-md-offset-2">
					<div class="col-md-8"><input type="text" value="<?=$search_data?>" class="sarch-box-records" name="search" id="search" placeholder="Serch Records By Patient Id" onkeypress="handle(event)"> </div>
					<div class="col-md-3"><input type="button" value="Search" class="records-search"></div>
					</div>
					</div>
<div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-pencil"></i> </span> Patient Call Details </h3>
                  	</div>
              		<div class="section-body">
              			
						
			<!--edit table-->
                    <div class="table-responsive ">
                    <table class="table data-tbl custom-style table-striped" id="sortable">
                    <thead>
                      <tr class="tbl-head">
                        <th><?=$ADMIN->check_all()?></th>
                        <th>No.</th>
						 <th>Patient Name</th>
						 <th>Patient ID</th>
						 <th>Service Type</th>
						<!-- <th><a href="<?=$ADMIN->iurl($comp)?>&fld=email<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='email')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>>Email</span></a></th>-->
                        <th>Action</th>
                      </tr>
               </thead>
					<tbody>
					<!--</table>
					 <ul class="sortable-liststyle" style="list-style:none;">-->
          
          
            <?php if($reccnt)
			      { 
			
			        $nums = (($start)?$start+1:1); 
					$k = 0;
					
				    while ($line = $PDO->db_fetch_array($result))
				    {
					    $patient_id = $line['pid'];
						$k++;
						if($_SESSION[AMD][2]=='clinics')
						{
							$wh =" and book_type='Clinics'";
							
						}else if($_SESSION[AMD][2]=='healthcare_organisation'){
							$wh =" and book_type='Healthcare'";
						}else if($_SESSION[AMD][2]=='labs'){
							$wh =" and book_type='Lab'";
						}
						
					   $sql = "select b.* from #_booking as a inner join #_call_details as b on a.call_id=b.pid where 1=1 and b.patient_id='{$patient_id}' and  a.book_id='".$_SESSION[AMD][0]."' ".$wh." group by a.call_id  order by a.pid desc ";	
				if($_SESSION[AMD][2]=="doctors"){
					$rs = $OP->get('call_details',$patient_id,'patient_id');
				}else{
				$rs = $PDO->db_query($sql);	
				}
						while ($line1 = $PDO->db_fetch_array($rs))
						{
							@extract($line1);
						
			
			?>
          <!--  <li style="list-style:none;" id="recordsArray_<?=$pid?>">
			<table class="table data-tbl custom-style">
			 <thead>-->
				<tr data-item-id=1 class="item <?=$css?>">
                  <th><?=$ADMIN->check_input($pid)?></th>
                  <th><?=$nums?></th>
                <!--  <th><?=ucwords($PDO->getSingleResult("select name from #_patients where pid='{$patient_id}'"))?></th>-->
				  <th><?=ucwords($PDO->getSingleResult("select name from #_patients where pid='{$patient_id}'"))?></th>
                  <th><?=ucwords($PDO->getSingleResult("select patient_id from #_patients where pid='{$patient_id}'"))?></th>
                  <th><?=$service_type?></th>
                  <th><a style="color:red;" href="<?=$ADMIN->iurl('reports','upload')?>&uid=<?=$pid?>"><i class="fa fa-eye"></i>  </a></th>
            </tr>
			<!-- </thead>
			</table>-->
            </li>
						<?php $nums++; }} ?>
            
            
          
           <?php  }else { echo '<tr><td colspan="7"><div align="center" class="norecord">No Record Found</div></td></tr>'; } ?>
           
         <!--  </ul>-->
		 </tbody>
		   </table>
		   <?php include("cuts/paging.inc.php");?>
						<div class="pull-right pagination" style="display:none;">
                        <ul class="pagination">
                        <li class="page-pre"><a href="#">‹</a></li>
                        <li class="page-number active"><a href="#">1</a></li>
                        <li class="page-number"><a href="#">2</a></li>
                        <li class="page-number"><a href="#">3</a></li>
                        <li class="page-number"><a href="#">4</a></li>
                        <li class="page-number"><a href="#">5</a></li>
                        <li class="page-last-separator disabled"><a href="#">...</a></li>
                        <li class="page-last"><a href="#">80</a></li>
                        <li class="page-next"><a href="#">›</a></li>
                        </ul>
                        </div>	
                        </div>
			<!-- close edit table-->
              		</div>

            <!--next button-->
           
			<!--Close next button-->
              </div>
            </div>
		</div>
  <!--Close right section panel-->
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
jQuery(document).ready(function(){ 
	jQuery(function() {
		jQuery("#ordrz ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = jQuery(this).sortable("serialize") + '&tbl=<?=tblName?>&field=pid'; 
			 $.post("<?=SITE_PATH_ADM?>modules/orders.php", order, function(theResponse){ }); 															
		}								  
		});
	});
	$('.records-search').click(function(){
		var value = $('#search').val();
		window.location.href="<?php echo SITE_PATH; ?>home.php?comp=reports&search="+value;
	});
	
	
	
	
});	
$('#formID').bind("keyup keypress", function(e) {
var code = e.keyCode || e.which; 
if (code == 13) { 
e.preventDefault();
var value = $('#search').val();
		window.location.href="<?php echo SITE_PATH; ?>home.php?comp=reports&search="+value;
}
});
</script>