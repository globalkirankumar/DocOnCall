<!-- Modal popup-->
<div id="medicalreferencedownload" class="modal medicalreference-download fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="append-records">
			<table class="table table-border">
			<tr><td><strong>Title</strong></td><td><strong>Download File</strong></td></tr>
			<?php while ($file = $PDO->db_fetch_array($medicalreference_query)){  ?>
			<tr>
				<th><?php echo ucwords($file['title']);?></th>
				<th><a href="<?=SITE_PATH?>/modules/reports/ajax.php?file=<?php echo $file['file_name'];?>" target="_new()" class="" data-href="<?php echo $file['file_name'];?>">DOWNLOAD</a></th>
			</tr>	
			<?php } ?>
			</table>
	   </div>

    </div>
  </div>
</div>
<!--close modal popup-->
<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {padding: 8px 15px;	text-align: left !important;}
</style>