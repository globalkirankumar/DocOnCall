<?php 
include(FS_ADMIN._MODS."/call_details/class.inc.php");
$OP = new Options();

if($action)
{
  
  if($uid >0  || !empty($arr_ids))
  {
   
	switch($action)
	{
		  case "del":
						 $OP->delete($uid);
						 $ADMIN->sessset('Record has been deleted', 'e'); 
						 break;
						 
		  case "Delete":
						 $OP->delete($arr_ids);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Deleted', 'e');
						 break;
						 
						 
		  case "Active":
						 $OP->status($arr_ids,1);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Active', 's');
						 break;
						 
		  case "Inactive":
						 $US->status($arr_ids,0);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Inactive', 's');
						 break;
					 
		  
		  default:
	}
    $BSC->redir($ADMIN->iurl($comp), true);
  }
}
if($BSC->is_post_back())
{
   $path = UP_FILES_FS_PATH."/reports";
   if($_FILES['reports']['name'])
   {
		$allowed =  array('gif','png' ,'jpg','jpeg','bmp','txt','pdf','doc','docx');
		$filename = $_FILES['reports']['name'];
		$size = $_FILES['reports']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ) {
			echo "<script>alert('File Type Mismatch')</script>";
		}else if($size>2097152){
			echo "<script>alert('Maximum file size is 2M')</script>";
		}else{
			$_POST['reports'] = $BSC->uploadFile2($path,$_FILES['reports']['name'],'reports');
			$OP->upload();
			echo "<script>alert('Reports Uploaded')</script>";
		}
	 
   }

}
$start = intval($start);
$pagesize = intval($pagesize)==0?(($_SESSION["totpaging"])?$_SESSION["totpaging"]:DEF_PAGE_SIZE):$pagesize;
list($result,$reccnt) = $OP->display($start,$pagesize,$fld,$otype,$search_data);



?>
<!--right section panel-->
<div class="vd_content-section clearfix">
		  	<div class="row">
			
              <div class="col-md-12">
			  <?=$ADMIN->alert()?>
              
              <div class="info-call-details">
               
              <?php $call_search_type = (!empty($call_search_type))?$call_search_type:array(); ?>
              
					<ul>
                  <li><span class="add-line-height"><input type="checkbox" name="call_search_type[]" value="follow up call"  <?=in_array('follow up call',$call_search_type)?'checked="checked"':''?> /> Follow up call</span></li>
				  
                  <li><span class="add-line-height"><input type="checkbox" name="call_search_type[]" value="schedule follow up call" <?=in_array('schedule follow up call',$call_search_type)?'checked="checked"':''?>  /> Scheduled follow up call</span></li>
				  
                  <li><span class="add-line-height"><input type="checkbox" name="call_search_type[]" value="subscription call"  <?=in_array('subscription call',$call_search_type)?'checked="checked"':''?> /> Subscription call</span></li>
				 
				 </ul>
                 <div style="clear:both"></div>
                 <ul>
				  
                   <?php  if($_SESSION['AMD'][2]=='administrator' || $_SESSION['AMD'][2]=='supervisor') {?>
                     
                      <li>
                        <select name="search_doctor_agents">
                         <option value="">---Doctor Agents---</option>
                         <?php
						  $dotagt_query=$PDO->db_query("select * from #_doctor_agents where status='1' order by name asc "); 
						  while ($dotagt_data = $PDO->db_fetch_array($dotagt_query))
				          {
						 ?>
                           <option value="<?=$dotagt_data['user_id']?>"   <?=($dotagt_data['user_id']==$search_doctor_agents)?'selected="selected"':''?>><?=$dotagt_data['name']?> (<?=$dotagt_data['email']?>)</option>
                         <?php } ?>
                        </select>
                      
                      </li>
                   
                   <?php } ?>
                   
                   <li>
                        <select name="search_sub_centers">
                         <option value="">---Doctor Sub Centers---</option>
                         <?php
						  $dotagt_query=$PDO->db_query("select * from #_sub_centers where status='1' order by name asc "); 
						  while ($dotagt_data = $PDO->db_fetch_array($dotagt_query))
				          {
						 ?>
                           <option value="<?=$dotagt_data['pid']?>"   <?=($dotagt_data['pid']==$search_sub_centers)?'selected="selected"':''?>><?=$dotagt_data['name']?></option>
                         <?php } ?>
                        </select>
                      
                      </li>
                   
                      <li>  <input type="text"  name="search_patients_name"  value="<?=$search_patients_name?>"  placeholder="Patient Name " /></li>
                      <li>  <input type="text"  name="search_patients_id"  value="<?=$search_patients_id?>"  placeholder="Patient ID " /></li>
                      <li>  <input type="text"  name="search_phone_number"  value="<?=$search_phone_number?>"  placeholder="Phone Number" /></li>
                      <li>  <input type="text" class="datepicker" name="search_date"  value="<?=$search_date?>"  placeholder="Date" /></li>
                     
                      <li><input type="submit" class="records-search greenbutton inputsearch" value="Search"></li>
                      
                   
                      <!--<li><input type="submit" class="records-search greenbutton inputsearch" value="Search"></li>-->
				  </ul>
              </div>
              		<div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title">Call Details </h3>
                  	</div>
              		<div class="section-body">
              			
						
			<!--edit table-->
                    <div class="table-responsive ">
                    <table class="table data-tbl custom-style table-striped" id="sortable">
                    <thead>
                      <tr class="tbl-head">
                        <th><?=$ADMIN->check_all()?></th>
                          <th>No.</th>
                          
						   <th>Patient Name</th>
						   <th>Patient Phone</th>
                         
						   <th>Patient ID</th>
                              <?php  if($_SESSION['AMD'][2]=='administrator' || $_SESSION['AMD'][2]=='supervisor') {?>
                           <th>Doctor Agent</th>
                           <?php } ?>
						   <th>Service Type</th>
                           <th>Date</th>
						<!--  <th>Call Type</th>
						<th><a href="<?=$ADMIN->iurl($comp)?>&fld=email<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='email')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>>Email</span></a></th>
                        <th>Status</th>-->
                        <th>Action</th>
                      </tr>
               </thead>
					<tbody>
					<!--</table>
					 <ul class="sortable-liststyle" style="list-style:none;">-->
          
          
            <?php if($reccnt)
			      { 
			
			        $nums = (($start)?$start+1:1); 
					$k = 0;
				    while ($line = $PDO->db_fetch_array($result))
				    {
					    @extract($line);
						$k++;
						$css =($k%2!=0)?'success':'';
			
			
			?>
        
				<tr data-item-id=1 class="item <?=$css?>">
                  <th><?=$ADMIN->check_input($pid)?></th>
                  <th><?=$nums?></th>
                  
				  <th  style="text-align:left"><a href="javascript:;" data-toggle="modal" data-target="#timelineModal"  onclick="patient_timeline(<?php echo $patient_id;?>)"><?=ucwords($PDO->getSingleResult("select name from #_patients where pid='{$patient_id}'"))?></a></th>
				  <th><?php echo $PDO->getSingleResult("select phone from #_patients where pid='{$patient_id}'");?></th>
                  <th><?=ucwords($PDO->getSingleResult("select patient_id from #_patients where pid='{$patient_id}'"))?></th>
                   <?php  if($_SESSION['AMD'][2]=='administrator' || $_SESSION['AMD'][2]=='supervisor') {?>
                           <th>
						    <?php if($create_by_type=='doctoragents')
							      {
						           
								        echo ucwords($PDO->getSingleResult("select name from #_doctor_agents where user_id='{$create_by}'"));
								  
								  }else if($create_by_type=='administrator'){
									  
									    echo ucwords($PDO->getSingleResult("select name from #_admin_users where user_id='{$create_by}'"));  
								  }
									
							  ?></th>
                           <?php } ?>
                  <th><?=$service_type?></th>
                 
                  <th><?=date('d M Y h:i a', strtotime($created_on))?></th>
                 <!-- <th><?=$call_type?></th>
                  <th><?=$ADMIN->displaystatus($status)?></th>-->
                  <th> <a href="<?=$ADMIN->iurl('call_details','view')?>&uid=<?=$pid?>" style="color:red" title="Explore"> <i class="fa fa-eye"></i> </a> &nbsp;&nbsp;&nbsp;<a href="#"  data-toggle="modal" data-target=".modal-edits" title="Upload"><i class="fa fa-upload"></i></a>
				 
				 </th>
            </tr>
			
            </li>
            <?php $nums++; } ?>
            
            
          
           <?php  }else { echo '<tr><td colspan="8"><div align="center" class="norecord">No Record Found</div></td></tr>'; }  ?>
           
         <!--  </ul>-->
		 </tbody>
		   </table>
		   <?php include("cuts/paging.inc.php");?>
						<div class="pull-right pagination" style="display:none;">
                        <ul class="pagination">
                        <li class="page-pre"><a href="#">‹</a></li>
                        <li class="page-number active"><a href="#">1</a></li>
                        <li class="page-number"><a href="#">2</a></li>
                        <li class="page-number"><a href="#">3</a></li>
                        <li class="page-number"><a href="#">4</a></li>
                        <li class="page-number"><a href="#">5</a></li>
                        <li class="page-last-separator disabled"><a href="#">...</a></li>
                        <li class="page-last"><a href="#">80</a></li>
                        <li class="page-next"><a href="#">›</a></li>
                        </ul>
                        </div>	
                        </div>
			<!-- close edit table-->
              		</div>

            <!--next button-->
           
			<!--Close next button-->
              </div>
            </div>
		</div>
  <!--Close right section panel-->
  <?php include('upload-model.php');?>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
jQuery(document).ready(function(){ 
	jQuery(function() {
		jQuery("#ordrz ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = jQuery(this).sortable("serialize") + '&tbl=<?=tblName?>&field=pid'; 
			 $.post("<?=SITE_PATH_ADM?>modules/orders.php", order, function(theResponse){ }); 															
		}								  
		});
	});
});	
</script>