<!-- Modal popup-->
<div id="subscriptionDetail" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="append-records">
			<table class="table">
			<?php 
			    	$subscription_data = $PDO->db_fetch_array($subscription_query);
			        $subscription =(array) json_decode($subscription_data['subscription_detail']);
			      //  print_r($subscription);
			?>
			<tr>
				<th>Subscriber ID number:</th>
				<th><?=$subscription['subscriber_id_number']?></th>
			</tr>
            <tr>
				<th>Subscribed date and time:</th>
				<th><?=$subscription['subscribed_date']?></th>
			</tr>
             <tr>
				<th>thhealth plan category (silver / gold / diamond membership plan ):</th>
				<th><?=$subscription['health_plan_category']?></th>
			</tr>	
            
              <tr>
				<th>health plan expired date:</th>
				<th><?=$subscription['health_plan_expired_date']?></th>
			</tr>	
            
             <tr>
				<th>name of subscriber:</th>
				<th><?=$subscription['name_of_subscriber']?></th>
			</tr>	
            
            <tr>
				<th>date of birth , age , sex:</th>
				<th><?=$subscription['date_of_birth_age_sex']?></th>
			</tr>	
            
             <tr>
				<th>national registration card number (NRC number):</th>
				<th><?=$subscription['national_registration_card_number']?></th>
			</tr>
            
             <tr>
				<th>father’s name:</th>
				<th><?=$subscription['fathers_name']?></th>
			</tr>
            
             <tr>
				<th>permanent address:</th>
				<th><?=$subscription['permanent_address']?></th>
			</tr>
            
             <tr>
				<th>current address:</th>
				<th><?=$subscription['current_address']?></th>
			</tr>	
            
              <tr>
				<th>contact ph number:</th>
				<th><?=$subscription['contact_ph_number']?></th>
			</tr>		
			
            <tr>
				<th>email address:</th>
				<th><?=$subscription['email_address']?></th>
			</tr>	
            
             <tr>
				<th>occupation:</th>
				<th><?=$subscription['occupation']?></th>
			</tr>
            
            <tr>
				<th>marital status:</th>
				<th><?=$subscription['marital_status']?></th>
			</tr>			
			
            
             <tr>
				<th>medical history (health record):</th>
				<th><?=$subscription['medical_history']?></th>
			</tr>
            
             <tr>
				<th>choose payment method for subscription:</th>
				<th><?=$subscription['payment_method_for_subscription']?></th>
			</tr>	
            	
           </table>
	   </div>

    </div>
  </div>
</div>
<!--close modal popup-->