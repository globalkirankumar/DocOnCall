<?php 
include(FS_ADMIN._MODS."/add_banner/class.inc.php");
$OP = new Options();

if($action)
{
  
  if($uid >0  || !empty($arr_ids))
  {
   
	switch($action)
	{
		  case "del":
						 $OP->delete($uid);
						 $ADMIN->sessset('Record has been deleted', 'e'); 
						 break;
						 
		  case "Delete":
						 $OP->delete($arr_ids);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Deleted', 'e');
						 break;
						 
						 
		  case "Active":
						 $OP->status($arr_ids,1);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Active', 's');
						 break;
						 
		  case "Inactive":
						 $OP->status($arr_ids,0);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Inactive', 's');
						 break;
					 
		  
		  default:
	}
    $BSC->redir($ADMIN->iurl($comp).(($pid)?'&pid='.$pid:''), true);
  }
}

$start = intval($start);

$pagesize = intval($pagesize)==0?(($_SESSION["totpaging"])?$_SESSION["totpaging"]:DEF_PAGE_SIZE):$pagesize;
list($result,$reccnt) = $OP->display($start,$pagesize,$fld,$otype,$search_data,'division_id',$division_id);

?>
<!--right section panel-->
		<div class="vd_content-section clearfix">
		  	<div class="row">
			
              <div class="col-md-12">
			  <?=$ADMIN->alert()?>
              
              <div class="info-call-details">
			    <ul>
				    <li><input type="text"  name="search_data"  value="<?=$search_data?>"  placeholder="Search" /></li>
                    <li><input type="submit" class="records-search greenbutton inputsearch" value="Search"></li>
                  </ul>
              </div>
              		<div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title"> Banner List </h3>
                  	</div>
              		<div class="section-body">
              			
						
			<!--edit table-->
                    <div class="table-responsive ">
                    <table class="table data-tbl custom-style table-striped" id="sortable">
                    <thead>
                      <tr class="tbl-head">
                        <th><?=$ADMIN->check_all()?></th>
                        <th>No.</th>
                        
                         <th><a href="<?=$ADMIN->iurl($comp)?>&fld=banner_page<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='banner_page')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>> Banner Page Type </span></a></th>
                         
                          <th><a href="<?=$ADMIN->iurl($comp)?>&fld=banner_type<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='banner_type')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>> Banner Type </span></a></th>
                          
                         <th><a href="<?=$ADMIN->iurl($comp)?>&fld=title<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='title')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>> Title </span></a></th>
                         
                         <th><a href="<?=$ADMIN->iurl($comp)?>&fld=reports<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='reports')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>> Image </span></a></th>
                        
						 <th><a href="<?=$ADMIN->iurl($comp)?>&fld=date_from<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='date_from')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>> Date From</span></a></th>
                         
                         <th><a href="<?=$ADMIN->iurl($comp)?>&fld=date_to<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='date_to')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>> Date To </span></a></th>
                                             
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
               </thead>
					<tbody>
					<!--</table>
					 <ul class="sortable-liststyle" style="list-style:none;">-->
          
          
            <?php if($reccnt)
			      { 
			
			        $nums = (($start)?$start+1:1); 
					$k = 0;
					
				    while ($line = $PDO->db_fetch_array($result))
				    {
					    @extract($line);
						$k++;
						$css =($k%2!=0)?'success':'';
			
			
			?>
          <!--  <li style="list-style:none;" id="recordsArray_<?=$pid?>">
			<table class="table data-tbl custom-style">
			 <thead>-->
				<tr data-item-id=1 class="item <?=$css?>">
                  <td><?=$ADMIN->check_input($pid)?></td>
                  <td><?=$nums?></td>
                  <td><?=$banner_page?></td>
                  <td><?=$banner_type?></td>	
                  <th><?=$title?></th>
                  <th><?php if($reports!='' && file_exists(UP_FILES_FS_PATH."/banner/".$reports) ) {?>
                     <a href="<?=SITE_PATH."uploaded_files/banner/".$reports?>" class="image" target="_blank">
                     <img src="<?=SITE_PATH."uploaded_files/banner/".$reports?>" width="100px;" height="50px;"/></a>
              	     <?php } ?>
                  </th>
                  <th><?=$date_from?></th>
                  <th><?=$date_to?></th>
                  <td><?=$ADMIN->displaystatus($status)?></td>
                  <td>
				  <?php   if($_SESSION['AMD'][2]=='doctoragents') {  ?>
                  
                  
                          <a href="<?=SITE_PATH?>/modules/reports/ajax.php?file=<?php echo $file_name?>"  title="Downlaod"> <i class="fa fa-upload"></i> </a>
                               
                     <?php   }else{
						        echo $ADMIN->action($comp, $pid.'&pid='.$pid);
						   
				          }
						   ?></td>
            </tr>
			<!-- </thead>
			</table>-->
            </li>
            <?php $nums++; } ?>
            
            
          
           <?php  }else { echo '<div align="center" class="norecord">No Record Found</div>'; } ?>
           
         <!--  </ul>-->
		 </tbody>
		   </table>
		   <?php include("cuts/paging.inc.php");?>
						<div class="pull-right pagination" style="display:none;">
                        <ul class="pagination">
                        <li class="page-pre"><a href="#">‹</a></li>
                        <li class="page-number active"><a href="#">1</a></li>
                        <li class="page-number"><a href="#">2</a></li>
                        <li class="page-number"><a href="#">3</a></li>
                        <li class="page-number"><a href="#">4</a></li>
                        <li class="page-number"><a href="#">5</a></li>
                        <li class="page-last-separator disabled"><a href="#">...</a></li>
                        <li class="page-last"><a href="#">80</a></li>
                        <li class="page-next"><a href="#">›</a></li>
                        </ul>
                        </div>	
                        </div>
			<!-- close edit table-->
              		</div>

            <!--next button-->
           
			<!--Close next button-->
              </div>
            </div>
		</div>
  <!--Close right section panel-->
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
jQuery(document).ready(function(){ 
	jQuery(function() {
		jQuery("#ordrz ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = jQuery(this).sortable("serialize") + '&tbl=<?=tblName?>&field=pid'; 
			 $.post("<?=SITE_PATH_ADM?>modules/orders.php", order, function(theResponse){ }); 															
		}								  
		});
	});
});	
</script>