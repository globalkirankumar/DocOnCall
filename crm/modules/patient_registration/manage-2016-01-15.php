<?php 
include(FS_ADMIN._MODS."/patient_registration/class.inc.php");
$US = new Users();


if($BSC->is_post_back())
{

	if(isset($health_consultation))
	{
		$PDO->sqlquery("rs",'call_details',array('health_consultation'=>$health_consultation),'pid',$_SESSION['call_id']);
			
	}
	unset($_SESSION['patient_id']);
	unset($_SESSION['call_id']);
	unset($_SESSION['subscription_details_id']);
	unset($_SESSION['patient_id']);
	$ADMIN->sessset('Call has been completed successfully', 'e'); 
	$BSC->redir($ADMIN->iurl('call_details'), true);

}
?>
<script language="javascript">
function callerInformation()
{
	 if($('#caller_information_same').is(':checked'))
	 {
		   	
           
		   $("input[name=caller_phone]").val($("input[name=patient_phone]").val());
		   $("input[name=caller_name]").val($("input[name=patient_name]").val());
		   $("input[name=caller_age]").val($("input[name=patient_age]").val());
		   $("input[name=caller_address]").val($("input[name=patient_address]").val());
		   $("input[name=caller_postal_code]").val($("input[name=patient_postal_code]").val());
		 	   
		   var patient_sex = $("#patient_sex option:selected" ).val();
		   $('#caller_sex option').removeAttr('selected').filter('[value='+patient_sex+']').attr('selected', true)
		   
		   var patient_sex = $("#patient_sex option:selected" ).val();
		   $('#caller_sex option').removeAttr('selected').filter('[value='+patient_sex+']').attr('selected', true);
		   
		   
		  $("input[name=caller_address]").val($("input[name=patient_address]").val());
		  $("input[name=caller_postal_code]").val($("input[name=patient_postal_code]").val());
		   
		   
		  // var patient_township = $("#patient_township option:selected" ).val();
		    $( "#caller_township option:selected" ).text($("#patient_township option:selected" ).text());
		    $( "#caller_township option:selected" ).val($("#patient_township option:selected" ).val());
		   //$('#caller_township option').removeAttr('selected').filter('[value='+patient_township+']').attr('selected', true);
		 	 
		  // $('.cat2').trigger('change');
			   
		   var patient_division = $("#patient_division option:selected" ).val();
		   
		   $('#caller_division option').removeAttr('selected').filter('[value='+patient_division+']').attr('selected', true)
		   
		 
	 }else{
		 
		   $("input[name=caller_phone]").val('');
		   $("input[name=caller_name]").val('');
		   $("input[name=caller_age]").val('');
		   $("input[name=caller_address]").val('');
		   $("input[name=caller_postal_code]").val('');
		 
	 }
}

function InformationForm()
{
  //formID	
 //  $('.controls-button').hide();
  
   $('#errormsg').html(''); 
   var str = $("#formID").serialize();
  
  /* if($("input:radio[name='call_type']").is(":checked")==false) 
   {
	   
		 $('#errormsg').html('Call type is required!'); 
		 return false;     
   }
   */
   if($("input[name=patient_phone]").val()=='')
   {
	    //alert('Patient phone is required!');
		 $('#errormsg').html('Patient phone is required!'); 
		return false;  
   }
   
   if($("input[name=patient_address]").val()=='')
   {
	   // alert('Patient address is required!');
		 $('#errormsg').html('Patient address is required!'); 
		return false;  
   }
   
   if($("input[name=patient_postal_code]").val()=='')
   {
	   // alert('Patient postal code is required!');
		 $('#errormsg').html('Patient postal code is required!'); 
		return false;  
   }
  
   if($("#patient_sex option:selected").val()=='')
   {
	   // alert('Patient sex is required!');
		 $('#errormsg').html('Patient sex  is required!'); 
		return false;  
   }
   if($("#patient_division option:selected").val()=='')
   {
	   // alert('Patient division is required!');
		 $('#errormsg').html('Patient division is required!'); 
		return false;  
   }
   
   if($("#patient_township option:selected").val()=='')
   {
	   // alert('Patient township is required!');
		 $('#errormsg').html('Patient township is required!'); 
		return false;  
   }
   
   // caller
    if($("input[name=caller_phone]").val()=='')
   {
	   // alert('Caller phone is required!');
		 $('#errormsg').html('Caller phone is required!'); 
		return false;  
   }
  
   if($("#caller_sex option:selected").val()=='')
   {
	    alert('Caller sex is required!');
		 $('#errormsg').html('Call type is required!'); 
		return false;  
   }
   
   if($("input[name=caller_address]").val()=='')
   {
	    //alert('Caller address is required!');
		 $('#errormsg').html('Caller address is required!'); 
		return false;  
   }
   
    if($("input[name=caller_postal_code]").val()=='')
   {
	   // alert('Caller postal code is required!');
		 $('#errormsg').html('Caller postal code is required!'); 
		return false;  
   }
   
   if($("#caller_division option:selected").val()=='')
   {
	    //alert('Caller division is required!');
		 $('#errormsg').html('Caller division is required!'); 
		return false;  
   }
   
   if($("#caller_township option:selected").val()=='')
   {
	   // alert('Caller township is required!');
		 $('#errormsg').html('Caller townshipis required!'); 
		return false;  
   }
   
   
   
   $.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=services",
		           data: str,
		           cache: false,
		           success: function(html){
					    // alert(html)
						 $('#PatientDiv').html(html);
			            // $("#property_type").html(html);
						
		         }});
   
   
}

function services(type)
{
	
	 $("input[name=service_type]").val(type)
     var str = $("#formID").serialize(); 
	 $.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=history",
		           data: str,
		           cache: false,
		           success: function(html){
					    // alert(html)
						 $('#PatientDiv').html(html);
						 $('.timePicker').timepicker({
								minuteStep: 1,
								template: false,
								showSeconds: true,
								showMeridian: false,
						});	
			            // $("#property_type").html(html);
						
	}});
}


function history(type)
{
	 var flag =1;
	 $('#errormsg').html(''); 
	 if($("input:radio[name='service_provided']").is(":checked")==false) 
	 {
		  flag ='please select service provided'; 
		 
	 }else {
		  
		  var service_provided = $('input[name=service_provided]:checked', '#formID').val();
		  if($("input:radio[name='referral_service_to']").is(":checked")==false &&  service_provided =='referral service to')
	      {
		     flag ='please select referral service to'; 
	      } 
	 }
	 
	 if(flag==1)
	 {	 
	     var str = $("#formID").serialize(); 
		 $.ajax({type: "POST",
				 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=service_provided",
				 data: str,
				 cache: false,
				 success: function(html){ 
				    $('#PatientDiv').html(html);	
				 }
		});
	 }else {  $('#errormsg').html(flag); }
	
}


function SubscriptionCall()
{
       var str = $("#SubscriptionForm").serialize();
	   $.ajax({type: "POST",
				 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=SubscriptionCall",
				 data: str,
				 cache: false,
				 success: function(html){ 
				   // alert(html)
				    $('#SubscriptionMsg').html('Subscription call detail added sucessfully.');	
				 }
		});	
}

function serach_patient()
{ 
      var search_mobile =  $("input[name=search_mobile]").val();
      var serach_patientid =  $("input[name=serach_patientid]").val();
	  
	  if(search_mobile=='' && serach_patientid=='')
	  {
		    $('#searchbox_div').html('<div style="color:#E60000; text-align:center">Please enter Mobile No./Patient ID</div> ');	
	  
	  }else {
	  
		   var str ='search_mobile='+search_mobile+'&serach_patientid='+serach_patientid;
		  $.ajax({type: "POST",
					 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=SerachPatient",
					 data: str,
					 cache: false,
					 success: function(html){ 
					    //alert(html)
					   $('#searchbox_div').html(html);	
					 }
			});	
		
	  }

}


function chanagepreferences(preferences, data)
{
	
	var str ="preferences="+preferences;
     $.ajax({type: "POST",
					 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=Preferences",
					 data: str,
					 cache: false,
					 success: function(html){ 
					     $('#PatientDiv').html(html);	
					 }
			});		
}


function patient_detail(patient_id)
{
	
  	  	$.ajax({type: "POST",
					 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=patient_detail",
					 dataType: "json",
					 data: { "patient_id": patient_id},
					 cache: false,
					 success: function(data){
						  
						   console.log(data)
					       $("input[name=patient_phone]").val(data.phone);
						   $("input[name=patient_name]").val(data.name);
						   $("input[name=patient_age]").val(data.age);
						   $("input[name=patient_address]").val(data.address);
						   $("input[name=patient_postal_code]").val(data.postal_code);
						   $('#patient_sex option').removeAttr('selected').filter('[value='+data.sex+']').attr('selected', true);
						   $('#patient_division option').removeAttr('selected').filter('[value='+data.division+']').attr('selected', true)
						   $("#patient_township option:selected" ).text(data.township_text);
		                   $("#patient_township option:selected" ).val(data.township);
					  	
					 }
			});	
}

	
function searchdata()
{
	
	var services_search =$("#services_search option:selected").val();
	var home_services_search =$("#home_services_search option:selected").val();
	var hospital_services_search =$("#hospital_services_search option:selected").val();
	var speciality_search =$("#speciality_search option:selected").val();
	
	var str ='services_search='+services_search+'&home_services_search='+home_services_search+'&hospital_services_search='+hospital_services_search+'&speciality_search='+speciality_search;
	
	 $.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=searchdata",
		           data: str,
		           cache: false,
		           success: function(html){
					      // alert(html)
						$('#PatientDiv').html(html);
			           
						
	}});	
	
	
	
}

	
	
function patient_timeline(patient_id)
{
	
	var str ='patient_id='+patient_id;
	
	$.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/timeline.php",
		           data: str,
		           cache: false,
		           success: function(html){
					      // alert(html)
						  $('#timelineModalDate').html(html);
			           
						
	}});	
	
 	
}

</script>
 
<!--right section panel-->
<div class="vd_content-section clearfix" >
		  	<div class="row">
              <div class="col-md-12">
              		  <div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-pencil"></i> </span> Patient 	/ Caller Information </h3>
                  	</div>
              
                      <div id="PatientDiv">		
                        <div class="section-body">
                            <div class="searchbox">
                            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                         
                            <div class="col-md-5"><input name="search_mobile" id="search_mobile" type="text" class="inputsearch" placeholder="Mobile No." ></div>
                            <div class="col-md-5"><input name="serach_patientid" id="serach_patientid" type="text" class="inputsearch" placeholder="Patient ID"></div>
                           
                            <div class="col-md-2"><button type="button" class="greenbutton inputsearch" onclick="serach_patient()">Search</button></div>
                          
                            </div>
                            
                            <div style="clear:both"></div>
                            
                             <div id="searchbox_div"></div>
                            </div>
                            
                <!--Patient/caller Information-->
                            <div class="information">
                            
                            <!--Patient Information-->
                                <div class="col-md-6">
                                <div class="panel-heading vd_bg-green white">
                                <h3 class="panel-title align-center"> <span class="menu-icon"> </span>Patient Information </h3>
                                </div>
                                    <div class="form-box">
                                    <div class="form-group">
                                        <span class="checkbox_callinfo"><input type="checkbox"  name="call_type" value="follow up call" ><label class="check_box">Is this a follow up call?</label></span>
                                        <span class="checkbox_callinfo"><input type="checkbox" name="subscription_call"  value="Yes"  data-toggle="modal" data-target="#Subscription"><label class="check_box">Is this a subscription call? </label></span>
                                    </div>
                                    
                                    
                                  
                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Phone No.<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text" name="patient_phone"  value="<?=$patient_phone?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Name</label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text" name="patient_name" value="<?=$patient_name?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Sex <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select  name="patient_sex" id="patient_sex" class="input_box_client_select">
                                          <option value="" >---select---</option>
                                         <option  <?=($patient_sex=='Male')?'selected="selected"':''?> >Male</option>
                                         <option <?=($patient_sex=='Female')?'selected="selected"':''?>>Female</option>
                                        </select>
                                        </div>
                                        </div>
                                        
                                        <div class="form-group">
                                        <label class="col-sm-4 control-label">Age</label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text"  name="patient_age" value="<?=$patient_age?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    
                                        <div class="form-group">
                                        <label class="col-sm-4 control-label">Address<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text"  name="patient_address" value="<?=$patient_address?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    
                                       <div class="form-group">
                                        <label class="col-sm-4 control-label">Postal Code<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text"  name="patient_postal_code" value="<?=$patient_postal_code?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Division <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select name="patient_division" id="patient_division" class="input_box_client_select cat2"  data-name="patient_township" data-folder="doctor_agents">
                                         <option value="" >---select---</option>
                                         <?php 
										        $record=$PDO->db_query("select * from #_division where status =1  order by name");
										        while($res=mysql_fetch_array($record))
												{
										  ?>
					                 <option value="<?=$res['pid']?>" <?php if($patient_division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						              <?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                        
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Township <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select name="patient_township" id="patient_township" class="input_box_client_select add_records">
                                         <option value="" >---select---</option>
                                        <?php $record=$PDO->db_query("select * from #_township where status =1 and division_id='".$division."' order by name");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($patient_township==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						<?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                </div>
    
                            <!--Caller Information-->
                                <div class="col-md-6">
                                <div class="panel-heading vd_bg-green white">
                                <h3 class="panel-title align-center"> <span class="menu-icon"> </span>Caller Information </h3>
                                </div>
                                <div class="form-box">
                                    <div class="form-group">
                                        <span class="checkbox_sameinfo"><input type="checkbox"   name="caller_information_same" id="caller_information_same"  onclick="callerInformation()" value="Yes"><label class="check_box">Click here if information is same </label></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Phone No.<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text" name="caller_phone"  value="<?=$caller_phone?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Name</label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text" name="caller_name"  value="<?=$caller_name?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Sex <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select name="caller_sex" id="caller_sex"  class="input_box_client_select">
                                          <option value="" >---select---</option>
                                          <option  <?=($caller_sex=='Male')?'selected="selected"':''?> >Male</option>
                                          <option <?=($caller_sex=='Female')?'selected="selected"':''?>>Female</option>
                                        </select>
                                        </div>
                                        </div>
                                        
                                      <div class="form-group">
                                        <label class="col-sm-4 control-label">Age</label>
                                         <div class="col-sm-7 controls">
                                            <div class="vd_input-wrapper light-theme no-icon">
                                                <input type="text" name="caller_age" value="<?=$caller_age?>"  class="input_box_client" placeholder="">
                                           </div>
                                        </div>
                                    </div>
                                     
                                      <div class="form-group">
                                        <label class="col-sm-4 control-label">Address<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text"  name="caller_address" value="<?=$caller_address?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    
                                       <div class="form-group">
                                        <label class="col-sm-4 control-label">Postal Code<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text"  name="caller_postal_code" value="<?=$caller_postal_code?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    
                                     
                                     <div class="form-group">
                                        <label class="col-sm-4 control-label">Division <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select name="caller_division" id="caller_division" class="input_box_client_select cat2" data-name="caller_township" data-folder="doctor_agents">
                                            <option value="" >---select---</option>
                                          <?php 
										        $record=$PDO->db_query("select * from #_division where status =1 order by name ");
										        while($res=mysql_fetch_array($record))
												{
										  ?>
					                 <option value="<?=$res['pid']?>" <?php if($caller_division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						              <?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                        
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Township <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select name="caller_township" id="caller_township" class="input_box_client_select add_records">
                                        <option value="" >---select---</option>
                                       <?php $record=$PDO->db_query("select * from #_township where status =1 and division_id='".$division."' order by name");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						<?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                </div>
                        
                            </div>
                             <div id="errormsg" style="color:#F00; text-align:center"></div>
                <!-- close Patient/caller Information-->
                        </div>
                        <!--next button-->
                        <div class="col-sm-12"> 
                            <a class="btn nextbutton  next pull-right greenbutton" href="javascript:void()" onclick="InformationForm()">Next <span class="menu-icon"><i class="fa fa-fw fa-chevron-circle-right"></i></span></a> 
                        </div>
                        <!--Close next button-->
                     </div>
              </div>
            </div>
		</div>
        
        

<!-- Modal popup-->
<div id="timelineModal" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
    <div id="timelineModalDate"></div>
  
  <!-- <div class="modal-footer">
     <a class="btn nextbutton  next pull-right greenbutton" data-dismiss="modal" href="screen2.html">Close </a>
  </div> -->
</div>
</div>
</div>
<!--close modal popup-->



