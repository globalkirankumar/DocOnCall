<?php 
include(FS_ADMIN._MODS."/healthguide_subcat/class.inc.php");


$OP = new Options();

if($BSC->is_post_back())
{
   if($uid)
   {
	   $_POST['updateid']=$uid;
       $flag = $OP->update($_POST);
  
   }else {
	 
	   $flag = $OP->add($_POST);
	   
	     
   }
   
   if($flag==1)
   {
   
     $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($healthguide_id)?'&healthguide_id='.$healthguide_id:'')).$dlr, true);
   }
}


if($uid)
{
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row" id="form-basic">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Add/Health <?=$ADMIN->compname($comp)?> </h3>
                </div>
              		
              		<div class="panel-body">
				

              			
						
			<!--add-update form-->
                <!--    <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">-->
              
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Title <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" data-errormessage-value-missing="township is required!" name="name" id="name"  value="<?=$name?>" type="text">
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="control-label  col-sm-4">Title(MY) <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" data-errormessage-value-missing="township is required!" name="name_my" id="name_my"  value="<?=$name_my?>" type="text">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Description <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                         <textarea name="description" rows="35" cols="10"><?=$description?></textarea>
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="control-label  col-sm-4">Description(MY) <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          
						  <textarea name="description_my" rows="35" cols="10"><?=$description_my?></textarea>
                        </div>
                    </div>
					
					
					<!-- End Services -->
					<div class="form-group">
                      <label class="control-label col-sm-4 col-xs-12">Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls col-sm-6 col-xs-12">
                          <select name="status" id="status" class="validate [required]" data-errormessage-value-missing="Status is required!">
                              <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?>  >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                    </div>
                  </div>
                
                  
                  <div class="form-group form-actions">
                    <div class="col-sm-6"></div>
                    <div class="col-md-6">
					 <input type="hidden" name="healthguide_id" value="<?=$healthguide_id?>" />
					 <input type="hidden" name="update_id" value="<?=$uid?>" />
                       <button id="submit-register" name="submit-register" class="btn vd_btn vd_bg-green vd_white uibutton loading" onclick="send_notification1()" type="button"><i class="icon-ok" ></i> Save</button>
                          <button onclick="location.reload();" class="btn vd_btn" type="button">Cancel</button>
                    </div>
                  </div>
             <!--   </form>-->
			<!-- close add-update form-->
              		</div>
              </div>
            </div>
		</div>
<script src="<?=SITE_PATH?>validation/js/jquery-1.8.2.min.js"></script>
<script src="<?=SITE_PATH?>validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=SITE_PATH?>validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>

function send_notification1()
{     $("#submit-register").attr('disabled', 'disabled');
      var healthguide_id =  $("input[name=healthguide_id]").val();
	  var update_id =  $("input[name=update_id]").val();
      var name =  $("#name").val();
	  var name_my =  $("#name_my").val();
	  var status =  $("#status").val();
	  var description =  $("textarea[name=description]").val();
	  
	  var description_my =  $("textarea[name=description_my]").val();
	  
	  var str ='name_my='+name_my+'&name='+name+'&status='+status+'&healthguide_id='+healthguide_id+'&description='+description+'&update_id='+update_id+'&description_my='+description_my;
	  
		  $.ajax({
			         type: "POST",
					 url: "<?=SITE_PATH_ADM?>/modules/manage_notification.php?flag=healthguidecontent",
					 data: str,
					 cache: false,
					 success: function(response){ 
					 
					 if(response==1)
					 {   
						 window.location.href='<?=SITE_PATH_ADM?>/home.php?comp=healthguide_subcat&healthguide_id='+healthguide_id;
					 }
					 
					 }
			});	
}
jQuery(document).ready(function(){
	// binds form submission and fields to the validation engine
	jQuery("#formID").validationEngine();
});
$('.hide_show').change(function(){
	var id = $(this).attr("data-id");
	if($(this).val()=="Available")
	{
		$('#'+id).show();
	}else{
		$('#'+id).hide();
	}
});
$('.services').click(function(){
	var id = $(this).attr("data-id");
	if(this.checked)
	{
		$('#'+id).show();
	}else{
		$('#'+id).hide();
	}
});
</script>