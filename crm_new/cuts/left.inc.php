<div class="content">
  <div class="container">
    <div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left  ">
	
	<div class="navbar-menu clearfix">
        <div class="vd_panel-menu hidden-xs">
            <span data-original-title="Expand All" data-toggle="tooltip" data-placement="bottom" data-action="expand-all" class="menu" data-intro="<strong>Expand Button</strong><br/>To expand all menu on left navigation menu." data-step=4 >
                <i class="fa fa-sort-amount-asc"></i>
            </span>                   
        </div>
    	<h3 class="menu-title hide-nav-medium hide-nav-small">Menu</h3>
        <div class="vd_menu">
        	 <ul>
	<?php $user_type = @$_SESSION['AMD'][2];
    $user_id = @$_SESSION['AMD'][0];?>
	<?php if(strtolower($user_type)=="doctors"){?>
	<!--    Doctor    -->
	<li>
        <a href="<?=$ADMIN->iurl('appointment')?>">
			<span class="menu-icon"><i class="fa fa-dashboard"></i></span> 
            <span class="menu-text">Appointment</span>  
        </a>
    </li>
	<!-- <li>
			<a href="<?=$ADMIN->iurl('reports')?>">
				<span class="menu-icon"><i class="fa fa-hospital-o"></i></span> 
				<span class="menu-text">Upload Reports</span>  
			</a>
		</li> -->
	<?php }else if(strtolower($user_type)=="doctoragents"){?>
		<!--    Doctor Agents    -->
		 <li>
			<a href="<?=$ADMIN->iurl('patient_registration')?>">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span> 
				<span class="menu-text">Creating Appointment</span>  
			</a>
		</li>
		<li>
			<a href="<?=$ADMIN->iurl('call_details')?>">
				<span class="menu-icon"><i class="fa fa-hospital-o"></i></span> 
				<span class="menu-text">MY Call Details</span>  
			</a>
		</li>
        
         <li>
        <a href="<?=$ADMIN->iurl('medicalreference')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Download Medical Reference</span>  
        </a>
    </li>
    
    <li>
            <a href="<?=$ADMIN->iurl('emergency')?>">
                <span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
                <span class="menu-text">Emergency <span id="emergency_location" >(<?=$PDO->getSingleResult("select count(*) from #_emergency  where status=0")?>)</span></span>  
            </a>
      </li>
    
		<!--<li>
			<a href="<?=$ADMIN->iurl('reports')?>">
				<span class="menu-icon"><i class="fa fa-hospital-o"></i></span> 
				<span class="menu-text">Upload Reports</span>  
			</a>
		</li>-->
        
        
	<?php }else if(strtolower($user_type)=="administrator" or strtolower($user_type)=="supervisor"){?>
	<!--    Administrator And Supervisor    -->
	<li>
        <a href="<?=$ADMIN->iurl('admin_users')?>">
			<span class="menu-icon"><i class="fa fa-user"></i></span> 
            <span class="menu-text">Admin Users</span>  
        </a>
    </li>
    <li>
        <a href="<?=$ADMIN->iurl('clinics')?>">
			<span class="menu-icon"><i class="fa fa-stethoscope"></i></span> 
            <span class="menu-text">Clinics</span>  
        </a>
    </li>
    <li>
        <a href="<?=$ADMIN->iurl('hospitals')?>">
			<span class="menu-icon"><i class="fa fa-hospital-o"></i></span> 
            <span class="menu-text">Hospital</span>  
        </a>
    </li> 	
	<li>
        <a href="<?=$ADMIN->iurl('labs')?>">
			<span class="menu-icon"><i class="fa fa-flask"></i></span> 
            <span class="menu-text">Labs</span>  
        </a>
    </li>
	<li>
        <a href="<?=$ADMIN->iurl('services_cat')?>">
			<span class="menu-icon"><i class="fa fa-flask"></i></span> 
            <span class="menu-text">Lab Services</span>  
        </a>
    </li>
	<li>
        <a href="<?=$ADMIN->iurl('healthcare_organization')?>">
			<span class="menu-icon"><i class="fa fa-ambulance"></i></span> 
            <span class="menu-text">Home Healthcare</span>  
        </a>
    </li>
	<li>
        <a href="<?=$ADMIN->iurl('doctor_agents')?>">
			<span class="menu-icon"><i class="fa fa-user-md"></i></span> 
            <span class="menu-text">Doctor Agents</span>  
        </a>
    </li>
    <li>
        <a href="<?=$ADMIN->iurl('patient_registration')?>">
			<span class="menu-icon"><i class="fa fa-phone"></i></span> 
            <span class="menu-text">Creating Appointment</span>  
        </a>
    </li>	<li>        <a href="<?=$ADMIN->iurl('custom_notification')?>">			<span class="menu-icon"><i class="fa fa-bell"></i></span>             <span class="menu-text">Notification</span>          </a>    </li>
     
	 <li>
        <a href="<?=$ADMIN->iurl('voip_call_details')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">VOIP Call Details</span>  
        </a>
     </li>
	 
	 <li>
        <a href="<?=$ADMIN->iurl('healthguide')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Health Guide</span>  
        </a>
    </li>
    
    <li>
        <a href="<?=$ADMIN->iurl('call_details')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Call Details</span>  
        </a>
    </li>
    
    <li>
        <a href="<?=$ADMIN->iurl('appointment')?>">
			<span class="menu-icon"><i class="fa fa-clock-o"></i></span> 
            <span class="menu-text">Doctor's Appointment</span>  
        </a>
    </li>
	
    
    <li>
        <a href="<?=$ADMIN->iurl('appointment&mode=schedule')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Lab/Imaging Appointment</span>  
        </a>
    </li>
    
      <li>
        <a href="<?=$ADMIN->iurl('specialities')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Specialities</span>  
        </a>
    </li>
    
      <li>
        <a href="<?=$ADMIN->iurl('division')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Division</span>  
        </a>
    </li>
    
      <li>
        <a href="<?=$ADMIN->iurl('add_banner')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Add Banner</span>  
        </a>
    </li>
	
     <li>
        <a href="<?=$ADMIN->iurl('sponsored')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Sponsored Doctor</span>  
        </a>
    </li>
	
	 <li>
        <a href="<?=$ADMIN->iurl('sponsore_clinics')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Sponsored Clinics</span>  
        </a>
    </li>
	
	 <li>
        <a href="<?=$ADMIN->iurl('sponsore_hospital')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Sponsored Hospital</span>  
        </a>
    </li>
	
	<li>
        <a href="<?=$ADMIN->iurl('sponsore_healthcare')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Sponsored HealthCare</span>  
        </a>
    </li>
    
     <li>
        <a href="<?=$ADMIN->iurl('sub_centers')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Sub Centers List</span>  
        </a>
    </li>
    
      <li>
        <a href="<?=$ADMIN->iurl('medicalreference')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Uload Medical Reference</span>  
        </a>
    </li>
    
     <li>
        <a href="<?=$ADMIN->iurl('health_tips')?>">
			<span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
            <span class="menu-text">Health Tips</span>  
        </a>
    </li>
   
     <li>
            <a href="<?=$ADMIN->iurl('emergency')?>">
                <span class="menu-icon"><i class="fa fa-phone-square"></i></span> 
                <span class="menu-text">Emergency <span id="emergency_location" >(<?=$PDO->getSingleResult("select count(*) from #_emergency  where status=0")?>)</span></span>  
            </a>
      </li>
    
	<?php }else{ ?>
	<!--   Clinics,Labs,Hospital And Healthcare     -->
	<?php if(strtolower($user_type)=="clinics" or strtolower($user_type)=="hospital" or strtolower($user_type)=="manager" or strtolower($user_type)=="executive"){         
        $hospital_id = $PDO->getSingleresult("select pid from #_hospitals where user_id ='".$user_id."' ");
        $clinic_id = $PDO->getSingleresult("select pid from #_clinics where user_id ='".$user_id."' "); 
        $created_id = $PDO->getSingleresult("select createdby_userid from #_manager_executives where user_id ='".$user_id."' ");
        $created_type = $PDO->getSingleresult("select createdby_user_type from #_manager_executives where user_id ='".$user_id."' ");     
        ?>
	<li>
        <a href="<?=$ADMIN->iurl('appointment')?>">
			<span class="menu-icon"><i class="fa fa-clock-o"></i></span> 
            <span class="menu-text">Doctor's Appointment</span>  
        </a>
    </li>
<?php if(($hospital_id && strtolower($user_type)=="hospital") || (($created_id && strtolower($created_type)=="hospital") && strtolower($user_type)=="manager")){
    $manage_hos_id = $PDO->getSingleresult("select pid from #_hospitals where user_id ='".$created_id."' ");
    $hospital_id = ($created_type=="hospital")?$manage_hos_id:$hospital_id;

    ?>
    <li>
        <a href="home.php?comp=doctors&hospital_id=<?=$hospital_id?>">
            <span class="menu-icon"><i class="fa fa-user-md"></i></span> 
            <span class="menu-text">Doctors</span>  
        </a>
    </li>
<?php }?>
<?php if(($clinic_id && strtolower($user_type)=="clinics") || ($created_id && strtolower($created_type)=="clinics") && strtolower($user_type)=="manager"){
    $mclinic_id = $PDO->getSingleresult("select pid from #_clinics where user_id ='".$created_id."' ");
    $clinic_id = ($created_type=="clinics")?$mclinic_id:$clinic_id;
    ?>
    <li>
        <a href="home.php?comp=doctors&clinic_id=<?=$clinic_id?>">
            <span class="menu-icon"><i class="fa fa-user-md"></i></span> 
            <span class="menu-text">Doctors</span>  
        </a>
    </li>
<?php }?>
    <li>
        <a href="<?=$ADMIN->iurl('patient_registration')?>">
            <span class="menu-icon"><i class="fa fa-phone"></i></span> 
            <span class="menu-text">Creating Appointment</span>  
        </a>
    </li>
<?php if(strtolower($user_type)!="executive"){?>
    <li>
        <a href="<?=$ADMIN->iurl('manager_executives')?>">
            <span class="menu-icon"><i class="fa fa-user"></i></span> 
            <span class="menu-text">Manage Users</span>  
        </a>
    </li>

	<?php } } ?>
		<li>
			<a href="<?=$ADMIN->iurl('appointment','schedule')?>">
				<span class="menu-icon"><i class="fa fa-hospital-o"></i></span> 
				<span class="menu-text">Lab/Imaging Appointment</span>  
			</a>
		</li>
    <?php if(strtolower($user_type)!="clinics" && strtolower($user_type)=="hospital" && strtolower($user_type)=="manager" && strtolower($user_type)=="executive"){?>
	<li>
			<a href="<?=$ADMIN->iurl('reports')?>">
				<span class="menu-icon"><i class="fa fa-hospital-o"></i></span> 
				<span class="menu-text">Upload Reports</span>  
			</a>
	</li>
    <?php }?>
	
	

	
	<?php } ?>

    <?php if(strtolower($user_type)=="clinics"){
      $clinic_id = $PDO->getSingleresult("select pid from #_clinics where user_id ='".$user_id."' ");?>
      <li> <a href="home.php?comp=clinics&mode=add&uid=<?=$clinic_id?>"> <div class="menu-icon"><i class="fa fa-user-md"></i></div>  <div class="menu-text">Update Lab Services</div> </a> </li>
    <?php }else if(strtolower($user_type)=="hospital"){
      $hospital_id = $PDO->getSingleresult("select pid from #_hospitals where user_id ='".$user_id."' ");?>
      <li> <a href="home.php?comp=hospitals&mode=add&uid=<?=$hospital_id?>"> <div class="menu-icon"><i class="fa fa-user-md"></i></div>  <div class="menu-text">Update Lab Services</div> </a> </li> 
    <?php }?>
    
    
    
    <li style="display:none;">
    	<a href="javascript:void(0);" data-action="click-trigger">
        	<span class="menu-icon"><i class="fa fa-dashboard"></i></span> 
            <span class="menu-text">Menu</span>  
            <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
       	</a>
     	<div class="child-menu"  data-action="click-target">
		<ul>
            <li>
                <a href="<?=$ADMIN->iurl('admin_users')?>">
                    <span class="menu-text">Admin Users</span>  
                </a>
            </li> 
            <!-- <ul>
                <li>
                    <a href="index.html">
                        <span class="menu-text">Default Dashboard</span>  
                    </a>
                </li>              
                <li>
                    <a href="index-ecommerce.html">
                        <span class="menu-text">E-Commerce Dashboard</span>  
                    </a>
                </li> 
                <li>
                    <a href="index-analytics.html">
                        <span class="menu-text">Analytics Dashboard</span>  
                    </a>
                </li> 
                <li>
                    <a href="index-blogging.html">
                        <span class="menu-text">Blogging Dashboard</span>  
                    </a>
                </li>  
                <li>
                    <a href="index-event-management.html">
                        <span class="menu-text">Event Management Dashboard</span>  
                        <span class="menu-badge"><span class="badge vd_bg-yellow">NEW</span></span>
                    </a>
                </li>                                                                                                  
            </ul>  -->  
      	</div>
    </li>  
 	  
                
    
               
         
   
   
   
   
        
   
    
                 
</ul>
<!-- Head menu search form ends -->         </div>             
    </div>
    <div class="navbar-spacing clearfix">
    </div>
    <div class="vd_menu vd_navbar-bottom-widget">
        <ul>
            <li>
                <a href="<?=SITE_PATH?>logout.php">
                    <span class="menu-icon"><i class="fa fa-sign-out"></i></span>          
                    <span class="menu-text">Logout</span>             
                </a>
                
            </li>
        </ul>
    </div>     
</div>    
   