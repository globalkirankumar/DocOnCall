<?php
/*
# @package BSC CMS
# Copy right code 
# The base configurations of the BSC CMS.
# This file has the following configurations: MySQL settings, Table Prefix,
# MySQL settings - You can get this info from your web host
# The name of the database for BSC CMS

# Developer by jugal kishore kiroriwal
*/

	
	

	if (LOCAL_MODE || $HTTP_HOST == 'jugal' || $HTTP_HOST == '192.168.0.2')
	{
		/*Localhost database detail*/
		$ARR_DBS["dbs"]['host'] = 'localhost';
		$ARR_DBS["dbs"]['name'] = 'mayanmar_crm3'; 
    	$ARR_DBS["dbs"]['user'] = 'root';
		$ARR_DBS["dbs"]['password'] = '';
		define('SITE_SUB_PATH', '/crm/');
	  
	} else { 
		/* live database connection */
		$ARR_DBS["dbs"]['host'] = 'localhost';
		$ARR_DBS["dbs"]['name'] = 'crm'; 
    	$ARR_DBS["dbs"]['user'] = 'crm_db_user';
		$ARR_DBS["dbs"]['password'] = 'db@doccrm';
		define('SITE_SUB_PATH', '/crm_new/');
		
 	}
	
	date_default_timezone_set('Asia/Rangoon'); 
	
	// Database table prefix //
	define('tb_Prefix', 'crm_');
	 
	
	/* Site Path */
	define('SITE_PATH', 'http://'.$HTTP_HOST.SITE_SUB_PATH); 
	/* Admin Path */
	define('ADMIN_DIR', '');
	define('SITE_PATH_ADM', 'http://'.$HTTP_HOST.SITE_SUB_PATH.ADMIN_DIR);
	
	 /* plugins Path */
	define('PLUGINS_DIR', 'lib/plugins');
	
	 /* file upload Path */
	define('UP_FILES_FS_PATH', SITE_FS_PATH.'/uploaded_files');
	
	define('FS_ADMIN', SITE_FS_PATH.'/'.ADMIN_DIR);
	
	// Define Module folder name //
	define('_MODS', "modules");
	
	
	/* Powered by Blue Sapphire Creations */
	define('PWDBYL', 'http://www.digitalabs.in/');
	define('PWDBY', 'iDreams Digitalabs Pvt. Ltd.');
		
		
	// pagination defalut limt
	define('DEF_PAGE_SIZE', 25);


	// define table name ///
	define('tblName', $comp);
	
?>