 <form class="form-horizontal" action="#" role="form">
                      <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="col-sm-5 control-label">Doctor Type <span class="vd_red">*</span></label>
                        <div class="col-sm-7">
                          <select name="doctor_type" class="hide_show validate[required]" data-id="special" data-errormessage-value-missing="Doctor Type is required!">
                            <option value=''>---Select Doctor Type---</option>
							<?php $record=$PDO->db_query("select * from #_doctor_type where status=1");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($doctor_type==$res['pid']){echo "selected";}?>><?=ucfirst($res['name'])?></option>
						<?php } ?>
                          </select>
                        </div>
                      </div>
					    <div class="form-group col-md-6 col-sm-6 col-xs-12" style="<?php if($doctor_type!=2){?>display:none;<?php } ?>" id="special">
                        <label class="col-sm-5 control-label">Specialities</label>
                        <div class="col-sm-7">
                          <select name="speciality" class="validate[required]" data-errormessage-value-missing="Specialities is required!" >
                            <option>---Select Specialities---</option>
                           <?php $record=$PDO->db_query("select * from #_specialities where status=1");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($speciality==$res['pid']){echo "selected";}?>><?=ucfirst($res['name'])?></option>
						<?php } ?>
                          </select>
                        </div>
                      </div>
					  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="col-sm-5 control-label">Doctor Name <span class="vd_red">*</span></label>
                        <div class="col-sm-7">
                          <input name="name" value="<?=$name;?>" placeholder="Doctor Name" class="validate[required]" data-errormessage-value-missing="Doctor Name is required!" type="text">
                         </div>
                      </div>					  <div class="form-group col-md-6 col-sm-6 col-xs-12">                        <label class="col-sm-5 control-label">Doctor Name(MY)<span class="vd_red">*</span></label>                        <div class="col-sm-7">                          <input name="name_my" value="<?=$name_my;?>" placeholder="Doctor Name (MY)" class="validate[required]" data-errormessage-value-missing="Doctor Name is required!" type="text">                         </div>                      </div>
					    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="col-sm-5 control-label">Registration Number <span class="vd_red"></span></label>
                        <div class="col-sm-7">
                          <input name="rs_number" value="<?=$rs_number;?>" placeholder="Doctor Registration Number" type="text">
                          </div>
                      </div>
					  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="col-sm-5 control-label">Email <span class="vd_red">*</span></label>
                        <div class="col-sm-7">
                          <input name="email" value="<?=$email;?>" placeholder="Doctor Email" class="validate[required,custom[email]]" data-errormessage-value-missing="Email is required!" title="Please enter valid email." type="text">
                          </div>
                      </div>
					   <?php if($pid!=''){?>
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label  col-sm-5">Change Password <span class="vd_red"></span></label>
                        <div class="col-sm-7">
						 <div class="vd_checkbox checkbox-danger checkbox-addcss">
                            <input type="checkbox" name="change_pwd"  class="change_pwd"  value="1" id="checkbox-3">
                            <label for="checkbox-3"></label>
                         </div>
						</div> 
                    </div>
					<?php } ?>
					<div class="form-group col-md-6 col-sm-6 col-xs-12 pwd" style="<?php if($pid!=''){ echo 'display:none;';}?>">
                        <label class="control-label  col-sm-5">Password <span class="vd_red"></span></label>
                        <div  class="col-sm-7">
                          <input  name="password" id="password"  value="" type="password">
                        </div>
                    </div>
					  
					  
					  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="col-sm-5 control-label">Education</label>
                        <div class="col-sm-7">
                          <input name="education" value="<?=$education;?>" placeholder="Education" class=""  type="text">
                         </div>
                      </div>
					 <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="col-sm-5 control-label">Fees</label>
                        <div class="col-sm-7">
                          <input name="fees" value="<?=$fees;?>" placeholder="Doctor Fee" type="text">
                        </div>
                      </div>
					 <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="col-sm-5 control-label">Mobile Number <span class="vd_red">*</span></label>
                        <div class="col-sm-7 border-none">
                            <input type="text" name="phone_code"  value="+95" readonly="readonly" class="phonecode" >
                            <input name="phone" value="<?=$phone;?>" placeholder="Contact Number" class="validate[required,custom[integer]] input_box_client" data-errormessage-value-missing="Contact No. is required!" type="text">
                           <span   class="ppnotes">  Please do not start the number with a 0.</span>
                         </div>
                      </div>
					  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="col-sm-5 control-label">Division <span class="vd_red">*</span></label>
                        <div class="col-sm-7">
                          <select name="division" data-name="township" data-folder="doctor_agents" class="validate[required] cat" data-errormessage-value-missing="Division is required!">
							<option value="">Select Division</option>
							<?php $record=$PDO->db_query("select * from #_division");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						<?php } ?>
							
						</select>	
                        </div>
                      </div>
					 <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="col-sm-5 control-label">Township <span class="vd_red">*</span></label>
                        <div class="col-sm-7">
                         <select name="township"  id="township" class="add_records validate[required]" data-errormessage-value-missing="Township is required!" onchange="getpincode('township','area_code')">
							<option value="">Select Township</option>
							<?php if($township!='' and $township!=0){?>
							<option value="<?php echo $township;?>" selected><?php echo $PDO->getSingleresult("select name from #_township where pid='".$township."'");?></option>
							<?php }?>
						</select>	
                        </div>
                      </div>
					  
					   <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="col-sm-5 control-label">Location</label>
                        <div class="col-sm-7">
                          <input name="location" value="<?=$location;?>" placeholder="Location"  type="text">
                          </div>
                      </div>					  <div class="form-group col-md-6 col-sm-6 col-xs-12">                        <label class="col-sm-5 control-label">Location (MY)</label>                        <div class="col-sm-7">                          <input name="location_my" value="<?=$location_my;?>" placeholder="Location (MY)"  type="text">                          </div>                      </div>
					  <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="col-sm-5 control-label">Postal Code <span class="vd_red"></span></label>
                        <div class="col-sm-7">
                          <input name="area_code" id="area_code" value="<?=$area_code;?>" placeholder="Postal Code"  type="text">
                          </div>
                      </div>
					  <div class="form-group col-md-6 col-sm-6 col-xs-12">
						<label class="col-sm-5 control-label">Status <span class="vd_red">*</span></label>
                      
                      <div  class="col-sm-7">
                          <select name="status" class="validate[required]" data-errormessage-value-missing="Status is required!">
                              <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?>  >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                        </div>
                  </div>
					    <!---------time sloat---------->
	<div class="form-group col-md-12 col-sm-12 col-xs-12 time-sloat">
		<label class="col-md-2 col-sm-2 col-xs-12 control-label">Time Slot</label>
		<div class="col-md-10 col-sm-10 col-xs-12 add-fields">
		<?php $dates = (array)json_decode($dates); //echo "<pre>"; print_r($dates);?>
			<!-- Monday -->
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="cols-kk ">
				<div class="col-sm-12">
					<label class="control-label">Days</label>
					<input name="day[]" readonly="" value="Monday" placeholder="Readonly input here..." disabled="" type="text">
				</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="cols-kk2">
						<!------------time-------->
						 <?php
						 
						 /* echo '<pre>';
						  print_r($dates); 
						 echo '</pre>';*/
						 
						 $monday = (array)$dates['monday'];  
						 
						 
						 
						 
						 ?>
						 <?php $tot = count($monday['from']);?>
						  <?php 
						  if($tot<2)
						  {  
							$monday['from'][0]=$monday['from'][0];  
							$monday['to'][0]=$monday['to'][0];
							$tot = 1;
					 	 }
						 ?>
						 <?php for($i=0;$i<$tot;$i++){ ?>
						<div class="cols-kk3 col-md-">
						<div class="col-sm-4  controls">
							<label class="control-label">Time From</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[monday][from][]" value="<?php echo $monday['from'][$i];?>" class="timepicker-default" placeholder="Time From" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-4  controls">
							<label class="control-label">Time To</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[monday][to][]" value="<?php echo $monday['to'][$i];?>" class="timepicker-default" placeholder="Time To" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-3">
							<input value="<?php echo $i+1;?>" class="hidden-val" name="tot-time[]" type="hidden">
							<input value="monday" class="hidden-val1"  type="hidden">
							<button style="display: inline-block;" data-value="<?php echo $i+1;?>" type="button" class="btn vd_btn vd_bg-green vd_white btn-2 add-field" <?=($i>0)?'onclick="remove_field(this)"':''?> ><i class="fa <?php if($i==0){?>fa-plus-square<?php }else{?>fa-minus-square<?php } ?>"></i></button>
                            
                           
						</div>
						</div>
						 <?php } ?>
						<!--- end time ---------->
					</div>	
			</div>
			
			<!-- Tuesday -->
		
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="cols-kk ">
				<div class="col-sm-12">
					<label class="control-label"></label>
					<input name="day[]" readonly="" value="Tuesday" placeholder="Readonly input here..." disabled="" type="text">
				</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="cols-kk2">
						<!------------time-------->
						 <?php $tuesday = (array)$dates['tuesday']; ?>
						 <?php $tot = count($tuesday['from']);?>
						 <?php if($tot<2){  
							$tuesday['from'][0]=$tuesday['from'][0];  
							$tuesday['to'][0]=$tuesday['to'][0];
							$tot = 1;
								}
						?>
						 <?php for($i=0;$i<$tot;$i++){?>
					<div class="cols-kk3 col-md-">
						<div class="col-sm-4  controls">
							<label class="control-label">Time From</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[tuesday][from][]" value="<?php echo $tuesday['from'][$i];?>" class="timepicker-default" placeholder="Time From" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-4  controls">
							<label class="control-label">Time To</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[tuesday][to][]" value="<?php echo $tuesday['to'][$i];?>" class="timepicker-default" placeholder="Time To" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-3">
							<input value="<?php echo $i+1;?>" class="hidden-val" name="tot-time[]" type="hidden">
							<input value="tuesday" class="hidden-val1"  type="hidden">
							<button style="display: inline-block;" data-value="<?php echo $i+1;?>" type="button" class="btn vd_btn vd_bg-green vd_white btn-2 add-field" <?=($i>0)?'onclick="remove_field(this)"':''?>  ><i class="fa <?php if($i==0){?>fa-plus-square<?php }else{?>fa-minus-square<?php } ?>"></i></button>
						</div>
					</div>
					<?php } ?>
					<!--- end time ---------->
				</div>	
			</div>
			<!-- Wednesday -->
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="cols-kk ">
				<div class="col-sm-12">
					<label class="control-label"></label>
					<input name="day[]" readonly="" value="Wednesday" placeholder="Readonly input here..." disabled="" type="text">
				</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="cols-kk2">
						<!------------time-------->
					<?php $wednesday = (array)$dates['wednesday']; ?>
					<?php $tot = count($wednesday['from']);?>
					<?php if($tot<2){  
					$wednesday['from'][0]=$wednesday['from'][0];  
					$wednesday['to'][0]=$wednesday['to'][0];
					$tot =1;
						}?>
					<?php for($i=0;$i<$tot;$i++){?>	
					<div class="cols-kk3 col-md-">
						<div class="col-sm-4  controls">
							<label class="control-label">Time From</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[wednesday][from][]" value="<?php echo $wednesday['from'][$i];?>" class="timepicker-default" placeholder="Time From" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-4  controls">
							<label class="control-label">Time To</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[wednesday][to][]" value="<?php echo $wednesday['to'][$i];?>" class="timepicker-default" placeholder="Time To" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-3">
							<input value="<?php echo $i+1;?>" class="hidden-val" name="tot-time[]" type="hidden">
							<input value="wednesday" class="hidden-val1"  type="hidden">
							<button style="display: inline-block;" data-value="<?php echo $i+1;?>" type="button" class="btn vd_btn vd_bg-green vd_white btn-2 add-field" <?=($i>0)?'onclick="remove_field(this)"':''?>  ><i class="fa <?php if($i==0){?>fa-plus-square<?php }else{?>fa-minus-square<?php } ?>"></i></button>
						</div>
					</div>
					<?php } ?>
					<!--- end time ---------->
				</div>	
			</div>
			<!-- Thursday -->
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="cols-kk ">
				<div class="col-sm-12">
					<label class="control-label"></label>
					<input name="day[]" readonly="" value="Thursday" placeholder="Readonly input here..." disabled="" type="text">
				</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="cols-kk2">
						<!------------time-------->
					<?php $thursday = (array)$dates['thursday']; ?>
					<?php $tot = count($thursday['from']);?>
					<?php if($tot<2){  
					$thursday['from'][0]=$thursday['from'][0];  
					$thursday['to'][0]=$thursday['to'][0];
					$tot = 1;	
					}?>
					<?php for($i=0;$i<$tot;$i++){?>		
					<div class="cols-kk3 col-md-">
						<div class="col-sm-4  controls">
							<label class="control-label">Time From</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[thursday][from][]" value="<?php echo $thursday['from'][$i];?>" class="timepicker-default" placeholder="Time From" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-4  controls">
							<label class="control-label">Time To</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[thursday][to][]" value="<?php echo $thursday['to'][$i];?>" class="timepicker-default" placeholder="Time To" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-3">
							<input value="<?php echo $i+1;?>" class="hidden-val" name="tot-time[]" type="hidden">
							<input value="thursday" class="hidden-val1"  type="hidden">
							<button style="display: inline-block;" data-value="<?php echo $i+1;?>" type="button" class="btn vd_btn vd_bg-green vd_white btn-2 add-field" <?=($i>0)?'onclick="remove_field(this)"':''?>  ><i class="fa <?php if($i==0){?>fa-plus-square<?php }else{?>fa-minus-square<?php } ?>"></i></button>
						</div>
					</div>
					<?php } ?>
					<!--- end time ---------->
				</div>	
			</div>
			<!-- Friday -->
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="cols-kk ">
				<div class="col-sm-12">
					<label class="control-label"></label>
					<input name="day[]" readonly="" value="Friday" placeholder="Readonly input here..." disabled="" type="text">
				</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="cols-kk2">
						<!------------time-------->
					<?php $friday = (array)$dates['friday']; ?>
					<?php $tot = count($friday['from']);?>
					<?php if($tot<2){  
					$friday['from'][0]=$friday['from'][0];  
					$friday['to'][0]=$friday['to'][0];
					$tot = 1;
						}?>
					<?php for($i=0;$i<$tot;$i++){?>	
					<div class="cols-kk3 col-md-">
						<div class="col-sm-4  controls">
							<label class="control-label">Time From</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[friday][from][]" value="<?php echo $friday['from'][$i];?>" class="timepicker-default" placeholder="Time From" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-4  controls">
							<label class="control-label">Time To</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[friday][to][]" value="<?php echo $friday['to'][$i];?>" class="timepicker-default" placeholder="Time To" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-3">
							<input value="<?php echo $i+1;?>" class="hidden-val" name="tot-time[]" type="hidden">
							<input value="friday" class="hidden-val1"  type="hidden">
							<button style="display: inline-block;" data-value="<?php echo $i+1;?>" type="button" class="btn vd_btn vd_bg-green vd_white btn-2 add-field" <?=($i>0)?'onclick="remove_field(this)"':''?>  ><i class="fa <?php if($i==0){?>fa-plus-square<?php }else{?>fa-minus-square<?php } ?>"></i></button>
						</div>
					</div>
					<?php } ?>
					<!--- end time ---------->
				</div>	
			</div>
			<!-- Saturday -->
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="cols-kk ">
				<div class="col-sm-12">
					<label class="control-label"></label>
					<input name="day[]" readonly="" value="Saturday" placeholder="Readonly input here..." disabled="" type="text">
				</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="cols-kk2">
						<!------------time-------->
					<?php $saturday = (array)$dates['saturday']; ?>
					<?php 	$tot = count($saturday['from']);?>
					<?php if($tot<2){  
					$saturday['from'][0]=$saturday['from'][0];  
					$saturday['to'][0]=$saturday['to'][0];
					$tot = 1;
						}
					
					
					?>
					<?php for($i=0;$i<$tot;$i++){?>	
					<div class="cols-kk3 col-md-">
						<div class="col-sm-4  controls">
							<label class="control-label">Time From</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[saturday][from][]" value="<?php echo $saturday['from'][$i];?>" class="timepicker-default" placeholder="Time From" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-4  controls">
							<label class="control-label">Time To</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[saturday][to][]" value="<?php echo $saturday['to'][$i];?>" class="timepicker-default" placeholder="Time To" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-3">
							<input value="1" class="hidden-val" name="tot-time[]" type="hidden">
							<input value="saturday" class="hidden-val1"  type="hidden">
							<button style="display: inline-block;" data-value="1" type="button" class="btn vd_btn vd_bg-green vd_white btn-2 add-field" <?=($i>0)?'onclick="remove_field(this)"':''?>  ><i class="fa <?php if($i==0){?>fa-plus-square<?php }else{?>fa-minus-square<?php } ?>"></i></button>
						</div>
					</div>
					<?php } ?>
					<!--- end time ---------->
				</div>	
			</div>
			<!-- Sunday -->
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="cols-kk ">
				<div class="col-sm-12">
					<label class="control-label"></label>
					<input name="day[]" readonly="" value="Sunday" placeholder="Readonly input here..." disabled="" type="text">
				</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="cols-kk2">
						<!------------time-------->
					<?php $sunday = (array)$dates['sunday']; ?>
					<?php $tot = count($sunday['from']);?>
					<?php if($tot<2){  
					$sunday['from'][0]=$sunday['from'][0];  
					$sunday['to'][0]=$sunday['to'][0];
					$tot = 1;	
					}?>
					<?php for($i=0;$i<$tot;$i++){  ?>	
					<div class="cols-kk3 col-md-">
						<div class="col-sm-4  controls">
							<label class="control-label">Time From</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[sunday][from][]" value="<?php echo $sunday['from'][$i];?>" class="timepicker-default" placeholder="Time From" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-4  controls">
							<label class="control-label">Time To</label>
							<div class="input-group bootstrap-timepicker">
								<input name="dates[sunday][to][]" value="<?php echo $sunday['to'][$i];?>" class="timepicker-default" placeholder="Time To" type="text">
								<span id="timepicker-default-span" class="input-group-addon">
									<i class="fa fa-clock-o"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-3">
							<input value="1" class="hidden-val" name="tot-time[]" type="hidden">
							<input value="sunday" class="hidden-val1"  type="hidden">
							<button style="display: inline-block;" data-value="1" type="button" class="btn vd_btn vd_bg-green vd_white btn-2 add-field" <?=($i>0)?'onclick="remove_field(this)"':''?> ><i class="fa <?php if($i==0){?>fa-plus-square<?php }else{?>fa-minus-square<?php } ?>"></i></button>
						</div>
					</div>
					<?php } ?>
					<!--- end time ---------->
				</div>	
			</div>
		</div>
    </div>
	<!---------end time sloat------>
					  
					 
                     
                   
                 