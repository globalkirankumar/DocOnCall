<!-- Modal popup-->
<div id="myModal" class="modal modal-edit<?php echo $i;?> fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="append-records">
				<!-- content -->
				   <div class="question_ans">
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">Chief Complaint</span>  </li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $medical['chief_complaint'];?>" readonly></span></li>
                  </ul>
                  </div>
				  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">History of present illness </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="<?php echo $medical['history_of_present_illness'];?>" readonly></textarea> </span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                  <span class="que_heading_medical">Past history  </span>
                  <div class="nastingul">
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">past medical  history</span></li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $medical['past_medical_history'];?>" readonly></span></li>
                  </ul>
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">Past surgical history</span></li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $medical['past_surgical_history'];?>" readonly></span></li>
                  </ul>
                  </div>
                  </div>
				   <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Social history  </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $medical['social_history'];?>" readonly> </span></li>
                    </ul>
                  </div>
                  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Family history </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $medical['family_history'];?>" readonly> 
                      </span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                      <ul>
                        <li><span class="que_heading_medical">Medication History  </span>  </li>
                        <li class="editli">
							<!-- <img src="img/que.jpg"> -->
							<span class="que_inner">history of drug allergy </span>  
						</li>
                        <li class="editli">
							<!-- <img src="img/ans.jpg"> -->
							<span class="que_inner"> 
								<span class="checkbox_callinfo">
									<input value="2" id="checkbox-4" type="checkbox" <?php if(strtolower($medical['history_of_drug_allergy'])=="yes"){ echo 'checked';}?> disabled>
									<label class="check_box">Yes</label>
								</span>
								<span class="checkbox_callinfo">
									<input value="2" id="checkbox-4" type="checkbox" <?php if(strtolower($medical['history_of_drug_allergy'])=="no"){ echo 'checked';}?> disabled>
									<label class="check_box">No</label>
								</span>
							</span>
						</li>
                      <br>
                      <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">if yes <input type="text" class="que_input_box" placeholder="<?php echo $medical['drug_allergy_comment'];?>" readonly > </span>  </li>
                        <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">Regular taking medication <input type="text" class="que_input_box" placeholder="<?php echo $medical['drug_allergy_regular_taking_medication'];?>" readonly > </span>  </li>
                       
                      </ul>
                    </div>
					  <div class="question_ans">
                      <ul class="libox">

                        <li><span class="text">BP- </span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $medical['bp'];?>" readonly></span> </li>
                        <li><span class="text">PR-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $medical['pr'];?>" readonly></span> </li>
                       <li><span class="text">SaO2-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $medical['sao'];?>" readonly></span> </li>
                       <li><span class="text">Weight-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $medical['weight'];?>" readonly></span> </li>
                        <li><span class="text">RBS-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $medical['rbs'];?>" readonly></span> </li>
                       
                      </ul>
                    </div>
					 <div class="question_ans">
                      <ul>
                        <li><img src="img/que.jpg"><span class="que_inner">provisional diagnosis   </span>  </li>
                        <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $medical['provisional_diagnosis'];?>" readonly> </span></li>
                      </ul>
                    </div>
					 <div class="question_ans">
                      <ul>
                        <li><img src="img/que.jpg"><span class="que_inner">Service provided </span>  </li>
                        <li><img src="img/ans.jpg"><span class="que_inner">
                        <div id="email-input-wrapper" class="controls col-sm-6">
                        <div class="vd_radio radio-success">
                        <input value="medical advice and counseling" id="optionsRadios5" name="service_provided" type="radio" <?php if(strtolower($medical['service_provided'])=="medical advice and counseling"){ echo 'checked';}?> disabled>
                          <label for="optionsRadios5"> medical advice and counseling </label>
                        </div>

                         <div class="vd_radio radio-success">
                        <input  value="health education and information" id="optionsRadios4" name="service_provided" type="radio"  <?php if(strtolower($medical['service_provided'])=="health education and information"){ echo 'checked';}?>  disabled>
                          <label for="optionsRadios4"> Health education and information </label>
                        </div>

                        <div class="vd_radio radio-success">
                       <input  value="referral service to" id="optionsRadios6" name="service_provided" type="radio"  <?php if(strtolower($medical['service_provided'])=="referral service to"){ echo 'checked';}?> disabled >
                          <label for="optionsRadios6"> Referral service to</label>                       
                            <span class="checkbox-inline">
								<input type="radio" name="type" id="track" value="track" <?php if(strtolower($medical['referral_service_to'])=="gp"){ echo 'checked';}?> disabled />
								<label for="track">GP</label>
							</span>
							<span class="checkbox-inline">
								<input type="radio" name="type" id="track" value="track" <?php if(strtolower($medical['referral_service_to'])=="specialist"){ echo 'checked';}?> disabled />
								<label for="track">Specialist</label>
							</span>
							<span class="checkbox-inline">
								<input type="radio" name="type" id="track" value="track" <?php if(strtolower($medical['referral_service_to'])=="lab"){ echo 'checked';}?> disabled />
								<label for="track">Lab</label>
							</span>
							<span class="checkbox-inline">
								<input type="radio" name="type" id="track" value="track" <?php if(strtolower($medical['referral_service_to'])=="imaging"){ echo 'checked';}?> disabled />
								<label for="track">Imaging</label>
							</span>
							<span class="checkbox-inline">
								<input type="radio" name="type" id="track" value="track" <?php if(strtolower($medical['referral_service_to'])=="healthcare"){ echo 'checked';}?>  disabled />
								<label for="track">Home visit</label>
							</span>
                        </div>
                        

                      </div>
                      
                      <div style="margin-top:20px;"><span class="que_inner"><input type="checkbox" name="follow_up_call_schedule"  value="Yes" <?=($medical['follow_up_call_schedule']=='Yes')?'checked="checked"':''?> onclick="follow_up_call_date_div()" >&nbsp;Schedule Follow up call </span> 
                       
                       <div id="follow_up_call_date_div" style="display: <?=($medical['follow_up_call_schedule']=='Yes')?'block"':'none'?>">
                        <div class="col-md-6 col-sm-6 col-xs-6"><input type="text" class="datepicker" name="follow_up_call_date"  placeholder="Date" value="<?=$medical['follow_up_call_date']?>" /></div>
                        <div class="col-md-6 col-sm-6 col-xs-6"><input type="text" name="follow_up_call_time" class="timePicker"  placeholder="Time"  value="<?=$medical['follow_up_call_time']?>"  /></div>
                       
                       </div>
                       </div>
                        </li>
                      </ul>
                    </div>
                  
       
				<!-- end content -->
      </div>

    </div>
  </div>
</div>
<!--close modal popup-->