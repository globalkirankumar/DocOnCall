<?php 
include(FS_ADMIN._MODS."/health_tips/class.inc.php");


$OP = new Options();

if($BSC->is_post_back())
{
	
    $path = UP_FILES_FS_PATH."/health_tips/";
	   
	$allowed =  array('gif','png','jpg','jpeg');
	$flag =1;
	
	if($_FILES['image']['name']!='')
	{
	
		$filename = $_FILES['image']['name'];
		$size = $_FILES['image']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) )
		{
			 $flag =0;
             $PDO->sessset('File Type Mismatch', 'e');
		}else if($size>2097152){
			 $flag =0;
			 $PDO->sessset('Maximum file size is 2M', 'e');
		}else{
			
			 $_POST['image'] = $BSC->uploadFile2($path,$_FILES['image']['name'],'image');
			
		}	
		
	}
	
	
	
	if($flag ==1)
	{
		   if($uid)
		   {
			   $_POST['updateid']=$uid;
			   $flag = $OP->update($_POST);
		  
		   }else {
			 
			   $flag = $OP->add($_POST);
			   
				 
		   }
		   
		   if($flag==1)
		   {
		   
			 $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($galleryid)?'&galleryid='.$galleryid:'')).$dlr, true);
		   }
   }
}


if($uid)
{
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row" id="form-basic">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Add/Health <?=$ADMIN->compname($comp)?> </h3>
                </div>
              		
              		<div class="panel-body">
				

              			
						
			<!--add-update form-->
                <!--    <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">-->
              
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Title <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" data-errormessage-value-missing="Title is required!" name="name" id="name"  value="<?=$name?>" type="text">
                        </div>
                    </div>
                    
                    <!--<div class="form-group">
                        <label class="control-label  col-sm-4">Description <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                         
                          
                           <textarea class="validate[required]" data-errormessage-value-missing="Description is required!" name="description" id="description" ><?=$description?></textarea>
                        </div>
                    </div>-->
					
					<!-- End Services -->
					
                  
                  <!--<div class="form-group">
                        <label class="control-label  col-sm-4">Upload Image <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-6">
                         <?php if($image!='' && file_exists(UP_FILES_FS_PATH."/health_tips/".$image) ) {?>
                  					 <div class="tool"><a href="<?=SITE_PATH."uploaded_files/health_tips/".$image?>" target="_blank">View</a>
                                     </div>
                   					  <a href="" class="image"> <span></span>
                                      <img src="<?=SITE_PATH."uploaded_files/health_tips/".$image?>" alt="" width="200" height="100"></a>
              					<?php } ?>
                            <input type="file" name="image" <?=($uid==0)?'class="validate[required]"':''?> data-errormessage-value-missing="images is required!">
                             <label style="color:#D20000; font-size:11px">NOTE: Image type allowed("jpeg,jpg,png only"). Maximum images size 2 MB</label>
           				  
                        </div>
                        
                    </div>-->
                  <div class="form-group">
                      <label class="control-label col-sm-4 col-xs-12">Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls col-sm-6 col-xs-12">
                          <select name="status" class="validate [required]" data-errormessage-value-missing="Status is required!">
                                <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?>  >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                    </div>
                  </div>
                  
                  <div class="form-group form-actions">
                    <div class="col-sm-6"></div>
                    <div class="col-md-6">
					 <input type="hidden" name="pid" value="<?=$user_id?>" />
                       <button id="submit-register" name="submit-register" class="btn vd_btn vd_bg-green vd_white uibutton loading" type="submit"><i class="icon-ok" ></i> Save</button>
                          <button onclick="location.reload();" class="btn vd_btn" type="button">Cancel</button>
                    </div>
                  </div>
             <!--   </form>-->
			<!-- close add-update form-->
              		</div>
              </div>
            </div>
		</div>
<script src="<?=SITE_PATH?>validation/js/jquery-1.8.2.min.js"></script>
<script src="<?=SITE_PATH?>validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=SITE_PATH?>validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#formID").validationEngine();
		});
		$('.hide_show').change(function(){
			var id = $(this).attr("data-id");
			if($(this).val()=="Available")
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
		$('.services').click(function(){
			var id = $(this).attr("data-id");
			if(this.checked)
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
</script>