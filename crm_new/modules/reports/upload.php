<?php 
include(FS_ADMIN._MODS."/reports/class.inc.php");
$OP = new Options();
$uid = @$_GET['uid'];
$result = $OP->get('call_details',$uid);

if($BSC->is_post_back())
{
   $path = UP_FILES_FS_PATH."/reports";
   if($_FILES['reports']['name'])
   {
		$allowed =  array('gif','png' ,'jpg','jpeg','bmp','txt','pdf','doc','docx');
		$filename = $_FILES['reports']['name'];
		$size = $_FILES['reports']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ) {
			echo "<script>alert('File Type Mismatch')</script>";
		}else if($size>2097152){
			echo "<script>alert('Maximum file size is 2M')</script>";
		}else{
			 $_POST['reports'] = $BSC->uploadFile2($path,$_FILES['reports']['name'],'reports');
			$OP->upload();
			echo "<script>alert('Reports Uploaded')</script>";
		}
	 
   }

}

?>
<?php include('upload-model.php');?>

<!--right section panel-->	<div class="vd_content-section clearfix">
		  	<div class="row">
              <div class="col-md-12">
              		
              		<div class="section-body">
                        <section >
                <!--TIMELINE FIRST SECTION START-->
				<?php 
						//$line = $PDO->db_fetch_array($result);
						//$pt_id = $line['patient_id'];
						//$patient = $OP->get('call_details',$pt_id,'patient_id','desc');
						
						$k=0; while ($line1 = $PDO->db_fetch_array($result))
						{
							
							@extract($line1);
							$k++;
							$rs = $PDO->db_fetch_array($OP->get('patients',$patient_id));
				?>	
                      <article>
                        <div class="inner">
                         <!-- <span class="step"><?=$k?></span>-->
                          <div style="text-align:left;">
                          <div class="paneledit widget">
                              <div class="panel-heading vd_bg-green">
                                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-info-circle"></i> </span>Patient / Caller Information</h3>
                                <div class="vd_panel-menu">
                                    <div data-original-title="Config" data-toggle="tooltip" data-placement="bottom" class="nav-medium-button menu entypo-icon smaller-font">
                                    </div>
                                </div>
                              </div>
                              <div class="panel-body2 overflow-hidden">
                                <div class="row mgbt-xs-0">
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span>Patient ID:</span><?=$rs['patient_id']?></li>
                                    <li><span>Age:</span><?=$rs['age']?></li>
                                    </ul>
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span>Name:</span><?=$rs['name']?></li>
                                    <li><span>Sex:</span><?=$rs['sex']?></li>
                                    </ul>
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span>Phone No:</span><?=$rs['phone']?></li>
									<?php $township = $rs['township']; ?>
                                    <li><span>Township:</span><?=$PDO->getSingleResult("select name from #_township where pid='{$township}'")?></li>
                                    </ul>
                                  </div>
                                </div>
                                <div class="history-btn">
								<?php if($call_type!=''){?>
                                    <span class="patient-history"><?=$call_type?></span>
								<?php } ?>
								<?php include("model.php");?>
                                    <span class="patient-history"><a href="#"  data-toggle="modal" data-target=".modal-edit<?php echo $k;?>">Medical History</a></span>
									<?php $files = $OP->get('reports',$pid,'call_details_id');
									if($OP->count_val($files)>0){
									?>
									<?php include('download-model.php');?>
                                    <span class="patient-history"><a href="#"  data-toggle="modal" data-target=".modal-download">Download Reports</a></span>
									<?php } ?>
									 <span class="patient-history"><a href="#"  data-toggle="modal" data-target=".modal-edits">Upload Reports</a></span>
                                </div>
                                
                                
                                
                                
                                <?php 
									
								  
								  
								  
								  						 				 
								
								 $app = $OP->getappointment('appointment',$pid,'call_details_id');
								 if($OP->count_val($app)>0)
								 {
									 
									 ?>
							<div class="app">
                                <span class="doctor-heading">Doctor Appointment(s)</span>
                                 <div class="table-responsive">
                                 <table class="table table-bordered align-left">
                              
                                    <thead>
                                        <tr>
                                            <th><span>Hospital/Clinic Name</span></th>
                                            <th><span>Appointment With Dr.</th>
                                            <th><span>Appointment Date</span></th>
                                            <th><span>Appointment Time</span></th>
                                        </tr>
                                    </thead>
								<?php
									while ($line2 = $PDO->db_fetch_array($app))
									{
										$did = $line2['doctor_id'];
									?>
                                    
                                    <tr>
                                            <td><?php $id = $PDO->getSingleResult("select hospital_id from #_doctors where user_id='{$did}' and hospital_id>0");
                                                                $id1 = $PDO->getSingleResult("select clinic_id from #_doctors where user_id='{$did}' and clinic_id>0");
                                                                if($id1!=''){ echo $PDO->getSingleResult("select name from #_clinics where pid='{$id1}'"); }
                                                                if($id!=''){ echo $PDO->getSingleResult("select name from #_hospitals where pid='{$id}'"); }
                                                                ?></td>
                                            <td><?=$PDO->getSingleResult("select name from #_doctors where user_id='{$did}'")?></td>
                                            <td><?=date('d F Y', strtotime($line2['app_date']))?></td>
                                            <td><?=$line2['app_time']?></td>
                                        </tr>
                               
                                  <?php } ?>
                                   
                                </table>
                                 <div class="clearfix"></div>
									</div>
                                    </div>
                                
                               
							<?php  }
							
							       if($_SESSION['AMD'][2]!='doctors')
								   {
								
								  $find = $OP->get2('booking',$pid,'call_id','service_type','Lab');
								
								 if($OP->count_val($find)>0){ 
								 ?>
								  <div class="app">
                                <span class="doctor-heading">Laboratory Appointment(s)</span>
                                  <div class="table-responsive">
                                 <table class="table table-bordered align-left">
                                    <thead>
                                        <tr>
                                            <td><span>Name</span></td>
                                            <td><span>Appointment Date</span></td>
                                            <td><span>Appointment Time</span></td>
                                           
                                        </tr>
                                    </thead>
								<?php
									while ($line2 = $PDO->db_fetch_array($find))
									{
										$book_type= $line2['book_type'];
										$book_id = $line2['book_id'];
									if(strtolower($book_type)=="lab"){
										$book_name = $PDO->getSingleResult("select name from #_labs where user_id='{$book_id}'");
										
									}else if(strtolower($book_type)=="hospital"){
										$book_name = $PDO->getSingleResult("select name from #_hospitals where user_id='{$book_id}'");
									}else if(strtolower($book_type)=="clinics"){
										
										$book_name = $PDO->getSingleResult("select name from #_clinics where user_id='{$book_id}'");
									}else if(strtolower($book_type)=="healthcare"){
										$book_name = $PDO->getSingleResult("select name from #_healthcare_organization where user_id='{$book_id}'");
									}
										//echo "select name from #_labs where user_id='{$book_id}'";
										
										
								?>
                                
                                    <tr>
                                            <td><?=ucfirst($book_name)?></td>
                                            <td><?=ucfirst($line2['book_date'])?></td>
                                            <td><?=ucfirst($line2['book_time'])?></td>
                                          
                                        </tr>
                                
								   
									
								<?php } ?>
                               </table>
                                 <div class="clearfix"></div>
									</div>
                                    </div>
                                
                                 <?php } 
									
								//    clinics
								$find = $OP->get2('booking',$pid,'call_id','service_type','Imaging');
								
								 if($OP->count_val($find)>0){ 
								 ?>
								  <div class="app">
                                <span class="doctor-heading">Imaging Appointment(s)</span>
                                <div class="table-responsive">
                                 <table class="table table-bordered align-left">
                                    <thead>
                                        <tr>
                                            <td><span>Name</span></td>
                                            <td><span>Appointment Date</span></td>
                                            <td><span>Appointment Time</span></td>
                                           
                                        </tr>
                                    </thead>
								<?php
									while ($line2 = $PDO->db_fetch_array($find))
									{
										$book_type = $line2['book_type'];
										$book_id = $line2['book_id'];
										if(strtolower($book_type)=="lab"){
										$book_name = $PDO->getSingleResult("select name from #_labs where user_id='{$book_id}'");
										
									}else if(strtolower($book_type)=="hospital"){
										$book_name = $PDO->getSingleResult("select name from #_hospitals where user_id='{$book_id}'");
									}
									//	$book_name = $PDO->getSingleResult("select name from #_clinics where user_id='{$book_id}'");
										
								?>
                                
                                <tr>
                                            <td><?=ucfirst($book_name)?></td>
                                            <td><?=ucfirst($line2['book_date'])?></td>
                                            <td><?=ucfirst($line2['book_time'])?></td>
                                          
                                        </tr>
                                
								
								<?php } ?>
                                
                                   </table>
                                    <div class="clearfix"></div>
									</div>
                                    </div>
                                <?php 
								 }
									
								
									
								//    healthcare
								$find = $OP->get2('booking',$pid,'call_id','service_type','Healthcare');
								
								 if($OP->count_val($find)>0){ 
								 ?>
								  <div class="app">
                                <span class="doctor-heading">Healthcare Appointment(s)</span>
                                <div class="table-responsive">
                                 <table class="table table-bordered align-left">
                                    <thead>
                                        <tr>
                                            <td><span>Name</span></td>
                                            <td><span>Appointment Date</span></td>
                                            <td><span>Appointment Time</span></td>
                                           
                                        </tr>
                                    </thead>
								<?php
									while ($line2 = $PDO->db_fetch_array($find))
									{
										$book_id = $line2['book_id'];
										$book_name = $PDO->getSingleResult("select name from #_healthcare_organization where user_id='{$book_id}'");
										
								?>
                                  
                                <tr>
                                            <td><?=ucfirst($book_name)?></td>
                                            <td><?=ucfirst($line2['book_date'])?></td>
                                            <td><?=ucfirst($line2['book_time'])?></td>
                                          
                                        </tr>
								
								<?php } ?>  
                                  </table>
                                    <div class="clearfix"></div>
									</div>
                                  </div>
                                  <?php } } ?>
                                    
                                    
                                 
                              </div>
                            </div>
                          </div>
                        </div>
                      </article>
						<?php } ?>
  <!--TIMELINE FIRST SECTION CLOSE-->
					
</section>
              		</div>
              </div>
            </div>
		</div>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
/*
jQuery(document).ready(function(){ 
	jQuery(function() {
		jQuery(".send_file").click(function() {
			var data= $('#formID').serialize();
			alert(data);
			 $.post("<?=SITE_PATH_ADM?>modules/reports/ajax.php", data, function(theResponse){ 
			 
			 alert(theResponse);
			 }); 															
		});								  
		});
	});
	*/
	$(function(){
		$('.download_reports').click(function(){
			var href= $(this).attr("data-href");
			var data = "file="+href;
			$.post("<?=SITE_PATH_ADM?>modules/reports/ajax.php", data, function(theResponse){ 
			 
			 }); 															
		});
	});
</script>
<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {padding: 8px 15px;	text-align: left !important;}
</style>