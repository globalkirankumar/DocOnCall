<?php 
include(FS_ADMIN._MODS."/admin_users/user.inc.php");


$US = new Users();

if($BSC->is_post_back())
{
   if($uid)
   {
	   $_POST['updateid']=$uid;
       $flag = $US->update($_POST);
  
   }else {
	 
	   $flag = $US->add($_POST);
	   
	     
   }
   
   if($flag==1)
   {
   
     $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($galleryid)?'&galleryid='.$galleryid:'')).$dlr, true);
   }
}


if($uid)
{
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row">
              <div class="col-md-12">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title">Forms</h3>
                  	</div>
              		<div class="section-body">
                  <h2 class="form-heading">Add/Update - <?=$ADMIN->compname($comp)?></h2>
				<div class="cl"></div>
				

              			
						
			<!--add-update form-->
                <!--    <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">-->
              
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Name <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input  type="text"  name="name" id="name"  value="<?=$name?>" class="validate[required] text-input" data-errormessage-value-missing="Name is required!" >
                        </div>
                      </div>
                    </div>
					<div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Email <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input class="validate[required,custom[email]]" data-errormessage-value-missing="Email is required!" name="email" id="email"  value="<?=$email?>" type="text">
                        </div>
                      </div>
                    </div>
					<?php if($pid!=''){?>
					<div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Change Password <span class="vd_red"></span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input  name="change_pwd" id="change_pwd"  value="1" type="checkbox">
                        </div>
                      </div>
                    </div>
					<?php } ?>
					<div class="form-group pwd" style="<?php if($pid!=''){ echo 'display:none;';}?>">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Password <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input class="validate[required]" data-errormessage-value-missing="Password is required!" name="password" id="password"  value="" type="password">
                        </div>
                      </div>
                    </div>
                   
					<div class="form-group">
                    <div class="col-md-12">
                      <label class="control-label col-sm-2 col-xs-12">Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls col-sm-4 col-xs-12">
                          <select name="status" class="validate[required]" data-errormessage-value-missing="Status is required!">
                              <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?>  >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                        </div>
                    </div>
                  </div>
                
                  <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-md-6 mgbt-xs-10 mgtp-20">
                     
                      
                      <div class="mgtp-10">
					   <input type="hidden" name="pid" value="<?=$pid?>" />
                        <button class="btn vd_bg-green vd_white greenbutton uibutton loading" type="submit" id="submit-register" name="submit-register">Submit</button>
                         <button onclick="location.reload();" class="btn  orng-btn" type="submit" id="submit-register" name="submit-register">Clear Form</button>
                      </div>
                    </div>
                    <div class="col-md-12 mgbt-xs-5"> </div>
                  </div>
             <!--   </form>-->
			<!-- close add-update form-->
              		</div>
              </div>
            </div>
		</div>

<script>
     $('#change_pwd').click(function(){
			if(this.checked)
			{
				$('.pwd').show();
			}else{
				$('.pwd').hide();
			}
	});
</script>