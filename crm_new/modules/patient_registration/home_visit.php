<?php

$patient_query=$PDO->db_query("select * from #_patients where pid ='".$_SESSION['patient_id']."'  "); 
$patient_data = $PDO->db_fetch_array($patient_query);  

$caller_query=$PDO->db_query("select * from #_call_details where pid ='".$_SESSION['call_id']."'  "); 
$caller_data = $PDO->db_fetch_array($caller_query);  
?>
<div class="row" id="form-basic">
              <div class="col-md-12">
               <div class="panel widget">
                  <div class="panel-body-list">
                   
                      <div id="wizard-3" class="form-wizard condensed">
                        <div class="tab-content no-bd pd-25">
                          <div class="tab-pane active" id="tab31">
                             <div class="row">
              
                <div class="col-sm-12" > 
                          <a class="btn nextbutton  next pull-right greenbutton" href="javascript:health_consultation()" onclick="$('#formID' ).submit();">Save  and Finish<span class="menu-icon"><i class="fa fa-fw fa-chevron-circle-right"></i></span></a> 
			</div>
            </div> 
                            <!---user information-->
							<div class="paneledit widget">
							  <div class="panel-heading vd_bg-green">
								<h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-info-circle"></i> </span>Patient Information</h3>
								<div class="vd_panel-menu">
									<div data-original-title="Config" data-toggle="tooltip" data-placement="bottom" class="nav-medium-button menu entypo-icon smaller-font">
									</div>
								</div>
							  </div>
							  <div class="panel-body2">
								<div class="row mgbt-xs-0">
								  <div class="col-md-4 col-sm-4 col-xs-12">
									<ul class="information">
									<li><span>Patient ID:</span><?=$patient_data['patient_id']?></li>
									<li><span>Age:</span><?=$patient_data['age']?></li>
									</ul>
								  </div>
								  <div class="col-md-4 col-sm-4 col-xs-12">
									<ul class="information">
									<li><span>Name:</span><?=$patient_data['name']?></li>
									<li><span>Sex:</span><?=$patient_data['sex']?></li>
									</ul>
								  </div>
								  <div class="col-md-4 col-sm-4 col-xs-12">
									<ul class="information">
									<li><span>Phone No:</span><?=$patient_data['phone']?></li>
									<li><span>Township:</span><?=$PDO->getSingleresult("select name from #_township where pid='".$patient_data['township']."' ")?></li>
									</ul>
								  </div>
								</div>
							  </div>
							</div>  
							<!---Close user information-->
														
                         <?php
						 
						   $services_search='Healthcare';
						 
					       include('search.php');
				       
					       if($home_services_search!='')
					       {
						      $home_services_search_wh="  and home_visit_service='".$home_services_search."'";   
					       }
					   
					       if($hospital_services_search!='')
						   {
							  $hospital_wh=" and pid='".$hospital_services_search."'";	
						  }

						  if($user_type=='hospital' || $user_type=='clinics'){
						  	$township_wh = " and user_id='".$user_id."'";
						  }
						  if($user_type=='manager' || $user_type=='executive'){
							$created_id = $PDO->getSingleresult("select createdby_userid from #_manager_executives where user_id ='".$user_id."' ");
							$township_wh = " and user_id='".$created_id."'";
						  }
										  
				          if($hospital_services_search=='')
						  {
					        include('healthcare_detail.inc.php');
                          }
						  
						  $services_like_data ='"healthcare":"1"'; 
						
						  include('hospitals_detail.inc.php');
						 					 
						 ?>  
                    
                         
                      </div>
                  
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>
            <!-- row -->
           <div class="row">
              
                <div class="col-sm-12" > 
                          <a class="btn nextbutton  next pull-right greenbutton" href="javascript:health_consultation()" onclick="$('#formID' ).submit();">Save  and Finish<span class="menu-icon"><i class="fa fa-fw fa-chevron-circle-right"></i></span></a> 
			</div>
            </div> 
            </div>
            
            
          </div>
          
             <div id="bookedDiv" class="modal fade myModa13" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="bookedclosebutton">&times;</button>
      </div>
      <div class="modal-body">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	  <h3 class="appointment-date">Appointment Date</h3>
	  <div id="bookederror"  style="color:#CA0000"></div>
	  </div>
	  <div class="col-md-6 col-sm-6 col-xs-12 popup-margin">
            <input type="hidden"  name="book_type" id="book_type"   value=""  />
               <input type="hidden"  name="service_type" id="service_type"   value=""  />
            <input type="hidden"  name="book_id" id="book_id"   value=""  />
            <input type="hidden"  name="call_detail_id" id="call_detail_id"   value="<?=$_SESSION['call_id']?>"  />
            <input type="hidden"  name="patient_id"  id="patient_id" value="<?=$_SESSION['patient_id']?>"  />
            <input type="text"  name="appointment_date" id="appointment_date"  class="datepicker"   placeholder="Appointment Date" />
             <span>eg: <?=date('m/d/Y')?></span>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12 popup-margin">
            <input type="text"  name="appointment_time" id="appointment_time"   placeholder="Appointment Time" />
            <span>eg: <?=date('h:i a')?></span>
		</div>
        
        
          <div class="col-md-12 col-sm-12 col-xs-12 popup-php">
               <textarea name="booking_comments"  id="booking_comments"  placeholder="comments" ><?=$booking_comments?></textarea>
           </div>
           
            <div class="col-md-12 col-sm-12 col-xs-12 popup-php">
               Home Visit Service: <input type="radio" name="home_visit_service" id="home_visit_service_yes" value="Yes" />Yes 
               &nbsp;&nbsp;&nbsp;  <input type="radio" name="home_visit_service" value="No" checked="checked" />No
           </div>
        
        
           <div class="col-md-12 col-sm-12 col-xs-12 popup-php"><a href="javascript:void(0)" onclick="bookedleb2()">Save</a></div>
		   </div>
    </div>
  </div>
</div>	
 <script>
function showslider(show,hide)
{
		$("#"+hide).hide("slow");
		//$("#see"+hide).html("See Here");
		$("#"+show).slideToggle("slow");
		//$("#see"+show).html("Close Here");	
}


function schedulebox(book_type,service_type,book_id)
{
	 $("input[name=book_type]").val(book_type);
	  $("input[name=service_type]").val(service_type);
	 $("input[name=book_id]").val(book_id);
	 $("input[name=appointment_date]").val('');
	 $("input[name=appointment_time]").val('');
	 
}

function bookedleb2()
{
	 var book_type = $("input[name=book_type]").val();
	   var service_type = $("input[name=service_type]").val();
	 var book_id = $("input[name=book_id]").val();
	 var call_detail_id = $("input[name=call_detail_id]").val();
	 var patient_id = $("input[name=patient_id]").val();
	 var appointment_date = $("input[name=appointment_date]").val();
	 var appointment_time = $("input[name=appointment_time]").val();
	  var home_visit_service=$('input[name=home_visit_service]:checked').val();
     var booking_comments = $('textarea#booking_comments').val();
	
	 
	 if(appointment_date=='' || appointment_time=='')
	 {
	    $('#bookederror').html('Please enter appointment date/time!');
	 
	 }else{
	
	 //var str ='book_type='+book_type+'&book_id='+book_id+'&call_detail_id='+call_detail_id+'&patient_id='+patient_id+'&date='+appointment_date+'&time='+appointment_time
	 	var str ='book_type='+book_type+'&service_type='+service_type+'&book_id='+book_id+'&call_detail_id='+call_detail_id+'&patient_id='+patient_id+'&date='+appointment_date+'&time='+appointment_time+'&booking_comments='+booking_comments+'&home_visit_service='+home_visit_service;
	 $.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=booked",
		           data: str,
		           cache: false,
		           success: function(html)
				   {
					 
					        $('#bookedclosebutton').trigger( "click");
						    $('#schedule'+book_type+book_id).html('Booked Successfully ');
							 $('#schedule'+book_type+book_id).parent().removeClass('greenbutton');
							$('#schedule'+book_type+book_id).parent().addClass('redbutton');
							//$('#schedule'+book_type+book_id).parent( ".greenbutton" ).css("background-color", "red");
							// $("#property_type").html(html);
						
	              }});	
	 }
}


function bookedleb(type,book_id,call_id)
{
      var str ='book_type='+type+'&book_id='+book_id+'&call_id='+call_id
	 $.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=booked",
		           data: str,
		           cache: false,
		           success: function(html){
					      // alert(html)
						   $('#'+type+book_id).html('Booked Successfully ');
						   $('#schedule'+book_type+book_id).parent().removeClass('greenbutton');
							$('#schedule'+book_type+book_id).parent().addClass('redbutton');
						  

			            // $("#property_type").html(html);
						
	}});	
}

 $('.datepicker').datepicker({
						  changeMonth: true,//this option for allowing user to select month
						  changeYear: true //this option for allowing user to select from year range
						});


						
						
						
</script>
     