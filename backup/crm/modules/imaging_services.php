<div class="Laboratory">
	<h5>Imaging Centre</h5>
	<div class="col-lg-12">
		<!-- IMAGIN & OTHER INVESTIGATIONS -->	
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>IMAGIN & OTHER INVESTIGATIONS</h4>
				<ul>	
					<li>
						<span class="Laboratory_span">Prime Aquilion(Toshiba)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Prime Aquilion(Toshiba) is required!" name="imagin_services[Prime_Aquilion]"   value="<?=$imagin_services['Prime_Aquilion']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Bone Densitometer(Ultrasound)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Bone Densitometer(Ultrasound) is required!" name="imagin_services[Bone_Densitometer]"   value="<?=$imagin_services['Bone_Densitometer']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Computerized Spirometer(lung function test)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Computerized Spirometer is required!" name="imagin_services[Computerized_Spirometer]"   value="<?=$imagin_services['Computerized_Spirometer']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Colposcopy</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Colposcopy is required!" name="imagin_services[Colposcopy]"   value="<?=$imagin_services['Colposcopy']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">24hr ECG(Holter)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="24hr ECG(Holter) is required!" name="imagin_services[24hr_ECG]"   value="<?=$imagin_services['24hr_ECG']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">2D Echocardiogram</span>
						<input class="Laboratory_input" data-errormessage-value-missing="2D Echocardiogram is required!" name="imagin_services[2D_Echocardiogram]"   value="<?=$imagin_services['2D_Echocardiogram']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Colour Doppler Imaging(Carotid,Lower Limb,Umbilical Cord)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Colour Doppler Imaging(Carotid,Lower Limb,Umbilical Cord) is required!" name="imagin_services[Colour_Doppler_Imaging]"   value="<?=$imagin_services['Colour_Doppler_Imaging']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Stress Test(Treadmill)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Stress Test(Treadmill) is required!" name="imagin_services[Stress_Test]"   value="<?=$imagin_services['Stress_Test']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Ultrasound (Thyorid,Breast,Abdomen,pelvis,TVS, Sonohysterosalpingogram)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Ultrasound(Thyorid,Breast,Abdomen,pelvis,TVS,Sonohysterosalpingogram) is required!" name="imagin_services[Ultrasound]"   value="<?=$imagin_services['Ultrasound']?>" type="text">
					</li>
				</ul>
			</div>
		</div>	
		<!-- End IMAGIN & OTHER INVESTIGATIONS -->	
	</div>
</div>	