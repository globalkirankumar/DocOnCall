<?php 
include("../lib/open.inc.php");

$pt_sql = $PDO->db_query("select * from #_patients where pid='{$patient_id}'");
$patient = $PDO->db_fetch_array($pt_sql);
class Options extends dbc{
 public function get($table=NULL,$pid=NULL,$field='pid',$order='asc')
	 {
		 if($table!=NULL){
			 $sql = "select * from #_".$table." Where 1";
			 if($pid!=NULL){
			 $sql.=" and {$field}='{$pid}'";
			 }
			 $sql.=" order by pid ".$order." ";
			$result = parent::db_query($sql); 
			return $result;
		 }
		 
	 }
	 public function get2($table=NULL,$pid=NULL,$field='pid',$field1=NULL,$field_val1=NULL,$order='asc')
	 {
		 if($table!=NULL){
			 $sql = "select * from #_".$table." Where 1";
			 if($pid!=NULL){
			 $sql.=" and {$field}='{$pid}'";
			 }
			 if($field1!=NULL and $field_val1!=NULL){
				$sql.=" and {$field1}='{$field_val1}'"; 
			 }
			 $sql.=" order by pid ".$order." ";
			$result = parent::db_query($sql); 
			return $result;
		 }
		 
	 }
	 public function count_val($resource=NULL)
	 {
		 if($resource!='')
		 {
			 $tot = mysql_num_rows($resource);
			 return ($tot)?$tot:0;
		 }
	 }
}
$OP = new Options();
	 
?>
<!--right section panel-->
	<div class="vd_content-section clearfix add-timeline-php">
		  	<div class="row">
              <div class="col-md-12">
              		
              		<div class="section-body">
                        <section id="timeline">
                        
                        <div class="paneledit widget">
                              <div class="panel-heading vd_bg-green">
                                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-info-circle"></i> </span>Patient / Caller Information</h3>
                                <div class="vd_panel-menu">
                                    <div data-original-title="Config" data-toggle="tooltip" data-placement="bottom" class="nav-medium-button menu entypo-icon smaller-font">
                                    </div>
                                </div>
                              </div>
                              <div class="panel-body2 overflow-hidden">
                                <div class="row mgbt-xs-0">
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span class="timeline-span">Patient ID:</span><?=$patient['patient_id']?></li>
                                    <li><span class="timeline-span">Age:</span><?=$patient['age']?></li>
                                    </ul>
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span class="timeline-span">Name:</span><?=$patient['name']?></li>
                                    <li><span class="timeline-span">Sex:</span><?=$patient['sex']?></li>
                                    </ul>
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span class="timeline-span">Phone No:</span><?=$patient['phone']?></li>
									<?php $township = $patient['township']; ?>
                                    <li><span class="timeline-span">Township:</span><?=$PDO->getSingleResult("select name from #_township where pid='{$township}'")?></li>
                                    </ul>
                                  </div>
                                </div>
                                
                                
                              </div>
                            </div>
                            
                            
                            
                <!--TIMELINE FIRST SECTION START-->
                 <?php
								 
								  $medsels = $PDO->db_query("select * from inv_sale_subscription where patcient_id='".$patient_id."' and  created_on  >= '".date('Y-m-d', strtotime($medsels_data['created_on']))."'  order by created_on desc ");
				                 
								  if(mysql_num_rows($medsels)>0)
								  {
								  ?>
                                     <div class="app">
                                   <span class="doctor-heading">Subscription Plan</span>
                                   <div class="table-responsive">
                                
                                    
                                    <table class="table table-bordered table-striped">
                        <thead>
                          <tr class="item">
                            <th>No.</th>
                            <th>Subscription Plan </th>
                            <th>Validity</th>
                           <th>Description</th>
                            <th>Amount Taken</th>
							<th>Date</th>
                          </tr>
                        </thead>
                    <?php        
					            
								$i=0;
								 while($medselsline = $PDO->db_fetch_array($medsels)) {  $i++; 
								 
								  $plan_query =	$PDO->db_query("select * from inv_subscription_plan where id='".$medselsline['subscription_id']."' "); 
								  $plan_data = $PDO->db_fetch_array($plan_query);
								 ?>
                                
                            <tbody>
                              <tr>
                                <td><?php echo $i;?></td>
                                <td><?=$plan_data['name']?></td>
                                <td><?=$plan_data['validity_duration']?> <?=$plan_data['validity_type']?></td>
                                <td><?=$plan_data['description']?></td>
                                <td><?php echo $medselsline['amount_taken']?></td>
                                <td><?=date('d F Y', strtotime($medselsline['created_on']))?></td>
                              </tr>
                            </tbody>
                           <?php } ?>
                      </table>
                                
                                    <div class="clearfix"></div>
									</div>
                                  </div>
                                  
                                  <?php } ?> 
                                  
                                  
                                  
                                   <?php
								 
								  $medsels = $PDO->db_query("select * from inv_sale_service where patcient_id='".$patient_id."' and  created_on  >= '".date('Y-m-d', strtotime($medsels_data['created_on']))."'  order by created_on desc ");
				                 
								  if(mysql_num_rows($medsels)>0)
								  {
								  ?>
                                     <div class="app">
                                   <span class="doctor-heading">Service Plan</span>
                                   <div class="table-responsive">
                                
                                    
                                    <table class="table table-bordered table-striped">
                        <thead>
                          <tr class="item">
                            <th>No.</th>
                            <th>Service Plan</th>
                            <th>Validity</th>
                            <th>Description</th>	
                            <th>Amount Taken</th>
							<th>Date</th>
                          </tr>
                        </thead>
                    <?php 
								$i=0;
								 while($medselsline = $PDO->db_fetch_array($medsels))
								 { 
								    
									  $plan_query =	$PDO->db_query("select * from inv_services where id='".$medselsline['service_id']."' "); 								                                       $plan_data = $PDO->db_fetch_array($plan_query);
								  
								  $i++; 
							?>
                                  
                            <tbody>
                              <tr>
                                <td><?php echo $i;?></td>
                                <td><?=$plan_data['name']?></td>
                                 <td><?=$plan_data['validity_duration']?> <?=$plan_data['validity_type']?></td>
                                <td><?=$plan_data['description']?></td>
                                <td><?php echo $medselsline['amount_taken']?></td>
                                
                                <td><?=date('d F Y', strtotime($medselsline['created_on']))?></td>
                              </tr>
                            </tbody>
                           <?php } ?>
                      </table>
                                
                                                   
                                    <div class="clearfix"></div>
									</div>
                                  </div>
                                  
                                  <?php } ?> 
                
                
				<?php
				
				     $date_arr=array();
				     $date_query = $PDO->db_query("select created_on from #_call_details where patient_id='{$patient_id}' order by pid desc");
					 while ($date_data = $PDO->db_fetch_array($date_query))
				     {
						 $date_arr[] =date('Y-m-d',strtotime($date_data['created_on']));
					 }
					 
					// echo "select * from inv_sales where patient_id='".$patient_id."' order by created_on desc ";
					 $medsels_query = $PDO->db_query("select * from inv_sales where patient_id='".$patient_id."' order by created_on desc ");
					 while ($medsels_data = $PDO->db_fetch_array($medsels_query))
				     {
						 $date_arr[] = $medsels_data['created_on'];
					 }
					 
					 $k=count($date_arr);
					 $date_arr = array_unique($date_arr);
					 rsort($date_arr);			 
					// echo '<pre>';  print_r($date_arr);
					
					
					$m=0;
					foreach($date_arr as $calldate)
					{
					   
					  
					   $medsels_query = $PDO->db_query("select * from inv_sales where patient_id='".$patient_id."' and created_on ='".$calldate."'  group by  created_on  order by created_on desc ");
					   while ($medsels_data = $PDO->db_fetch_array($medsels_query))
				       {
						   $k--;
						?>   
                        
						    <article>
                        <div class="inner">
                          <span class="step"><?=$k?></span>
                          <div style="text-align:left;">
                          <div class="paneledit widget">
                              <div class="panel-heading vd_bg-green">
                                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-info-circle"></i> </span><?=date('d F Y', strtotime($medsels_data['created_on']))?></h3>
                                <div class="vd_panel-menu">
                                    <div data-original-title="Config" data-toggle="tooltip" data-placement="bottom" class="nav-medium-button menu entypo-icon smaller-font">
                                    </div>
                                </div>
                              </div>
                              <div class="panel-body2 overflow-hidden">
                               
                              <?php
										
								  $medsels = $PDO->db_query("select * from inv_sales where patient_id='".$patient_id."' and  created_on  = '".date('Y-m-d', strtotime($medsels_data['created_on']))."'  order by created_on desc ");
				                  
								  if(mysql_num_rows($medsels)>0)
								  {
					             
								 ?>                               
                                             <div class="app">
                                   <span class="doctor-heading">Medicine</span>
                                   <div class="table-responsive">
                                <?php  while($medselsdat = $PDO->db_fetch_array($medsels)) {  ?>
                                
                              <!-- <strong> Date : <?=date('d F Y', strtotime($medselsdat['created_on']))?></strong>-->
                                <table class="table table-bordered table-striped">
                        <thead>
                          <tr class="item">
                           <th>No.</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Generic</th>
                            <th>Form Medicine</th>
                            <th>Strength</th>
                            <th>Usage</th>
							<th>Quantity</th>
                            <th>Price</th>
                          </tr>
                        </thead>
                        <?php 
						 $medqry = $PDO->db_query("select * from inv_sales where patient_id='".$patient['pid'] ."' and  ses_id = '".$medselsdat['ses_id']."'");
						   $i=0;
						   $grandtotal=0;
				           while($medata = $PDO->db_fetch_array($medqry)) { $i++;?>
                                  
                           <tbody>
                          <tr>
                          <td><?php echo $i;?></td>
                           <td><?=$PDO->getSingleResult("select name from inv_category where id='".$medata['category_id']."' ")?> </td>
                            <td><?=$PDO->getSingleResult("select name from inv_brand where id ='".$medata['brand_id']."' ")?></td>
                            <td><?=$PDO->getSingleResult("select generic_name from inv_generic_name where id='".$medata['generic_id']."' ")?></td>
                            <td><?=$PDO->getSingleResult("select form_of_medicine_name from inv_form_of_medicine where id='".$medata['formof_id']."' ")?></td>
                            <td><?=$PDO->getSingleResult("select strength_of_medicine_name from inv_strength_of_medicine where id='".$medata['strength_id']."' ")?> </td>
                            <td><?=$PDO->getSingleResult("select usage_category_name from inv_usage_category where id='".$medata['usage_id']."' ")?> </td>
							 <td><?php echo $medata['qty'];?></td>
                            <td><?php  echo $medata['sale_price'];?></td>
                          </tr>
                        </tbody>        
                         <?php
						            $total=$medata['qty']*$medata['sale_price'];
								  
								    $grandtotal += $total;
						 
						  } ?>
          		
					  <tr>
							<td colspan="9" > <span style="float:right" ><b>Grand Total : <?=$grandtotal?>  </b></span></td>
							
						</tr>
                      </table>
                      
                                  <?php } ?>
                              
                                    <div class="clearfix"></div>
									</div>
                                  </div>
                                  
                                  <?php } ?> 
								 
							   </div>
                            </div>
                          </div>
                        </div>
                      </article>  
                        
                        <?php   
					   }
					
				
				       // echo "select * from #_call_details where patient_id='{$patient_id}'";
					  $result = $PDO->db_query("select * from #_call_details where patient_id='{$patient_id}' and created_on >='".$calldate." 00:00:00'   and created_on <='".$calldate." 23:59:59' order by pid desc");
				      while ($line = $PDO->db_fetch_array($result))
				      {
					     @extract($line);
						 $k--;
						
						//print_r($line);
						
				?>		
                      <article>
                        <div class="inner">
                          <span class="step"><?=$k?></span>
                          <div style="text-align:left;">
                          <div class="paneledit widget">
                              <div class="panel-heading vd_bg-green">
                                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-info-circle"></i> </span><?=date('d F Y', strtotime($line['created_on']))?>,   <?=date('h:i a', strtotime($line['created_on']))?></h3>
                                <div class="vd_panel-menu">
                                    <div data-original-title="Config" data-toggle="tooltip" data-placement="bottom" class="nav-medium-button menu entypo-icon smaller-font">
                                    </div>
                                </div>
                              </div>
                              <div class="panel-body2 overflow-hidden">
                                
                                <div class="history-btn">
								<?php $call_type = $PDO->getSingleResult("select call_type from #_call_details where pid='{$call_details_id}'");?>
								<?php if($call_type!=''){?>
                                    <span class="patient-history"><?=$call_type?></span>
								<?php } ?>	
								
								<?php include('model.php');?>
                                    <span class="patient-history"><a href="#"  data-toggle="modal" data-target=".modal-timeline<?php echo $k;?>">Medical History</a></span>
                                   <?php $files = $OP->get('reports',$pid,'call_details_id');
									if($OP->count_val($files)>0){
									?>
									<?php include('download-model.php');?>
                                    <span class="patient-history"><a href="#"  data-toggle="modal" data-target=".modal-download-t<?php echo $k;?>">Download Reports</a></span>
									<?php } ?>
                                    
                                     <?php
									
									  $medicalreference_query=$PDO->db_query("select * from #_medicalreference where patient_id ='".$patient_id."' and status='1' ");
									  if(mysql_num_rows($medicalreference_query) > 0)
									  {
									 
									   include('download-medicalreference.php');?>
                                       <span class="patient-history"><a href="#"  data-toggle="modal" data-target=".medicalreference-download">Medical Reference</a></span>
                                     <?php } ?>
                                     
                                     <!--  <span class="patient-history"><a href="#"  data-toggle="modal" data-target=".medicine-timeline<?php echo $k;?>">Medicine</a></span>                    <?php // include('medicine-timeline.php'); ?>-->
                                </div>
                                
                                <?php if($services_comment!='') {?>
                                 <div class="app">
                                <span class="doctor-heading">Services Comments</span>
                                  <div>  <?=$services_comment?></div>
                                  </div>
                                <?php }  if($health_consultation!='') {?>
                                 <div class="app">
                                <span class="doctor-heading">Health Consultation</span>
                                  <div>  <?=$health_consultation?></div>
                                  </div>
                                <?php } 
									
								
								
								$app = $OP->get('appointment',$pid,'call_details_id');
								if($OP->count_val($app)>0)
								{?>
							<div class="app">
                                <span class="doctor-heading">Doctor Appointment(s)</span>
					<div class="table-responsive">
                    <table class="table table-bordered align-left">
					<thead>
						<tr>
							<th><span>Hospital/Clinic Name</span></th>
							<th><span>Appointment With Dr.</th>
							<th><span>Appointment Date</span></th>
							<th><span>Appointment Time</span></th>
						</tr>
					</thead>
					<tbody>	
								<?php
									while ($line2 = $PDO->db_fetch_array($app))
									{
										$did = $line2['doctor_id'];
									?>
				
					<tr>
                        <td><?php $id = $PDO->getSingleResult("select hospital_id from #_doctors where user_id='{$did}' and hospital_id>0");
											$id1 = $PDO->getSingleResult("select clinic_id from #_doctors where user_id='{$did}' and clinic_id>0");
											if($id1!=''){ echo $PDO->getSingleResult("select name from #_clinics where pid='{$id1}'"); }
											if($id!=''){ echo $PDO->getSingleResult("select name from #_hospitals where pid='{$id}'"); }
											?></td>
                        <td><?=$PDO->getSingleResult("select name from #_doctors where user_id='{$did}'")?></td>
                        <td><?=$line2['app_date']?></td>
						<td><?=$line2['app_time']?></td>
                    </tr>
                    
                               <?php if($line2['comments']!='') {?>
                                    <tr>
                                       <td colspan="4"><strong>Doctor Comment:</strong> <?=$line2['comments']?></td>
                                    </tr>
				
								
								<?php  } }
									echo "</tbody> </table></div></div>";
								}  ?>
                                   
                               
                                
                               
							<?php 
								
								//    labs
								$find = $OP->get2('booking',$pid,'call_id','service_type','Lab');
								
								 if($OP->count_val($find)>0){ 
								 ?>
								  <div class="app">
                                <span class="doctor-heading">Laboratory Appointment(s)</span>
                                  <div class="table-responsive">
                                 <table class="table table-bordered align-left">
                                    <thead>
                                        <tr>
                                            <td><span>Name</span></td>
                                             <td><span>Test</span></td>
                                            <td><span>Appointment Date</span></td>
                                            <td><span>Time</span></td>
                                           
                                        </tr>
                                    </thead>
								<?php
									while ($line2 = $PDO->db_fetch_array($find))
									{
										$book_type= $line2['book_type'];
										$book_id = $line2['book_id'];
									if(strtolower($book_type)=="lab"){
										$book_name = $PDO->getSingleResult("select name from #_labs where user_id='{$book_id}'");
										
									}else if(strtolower($book_type)=="hospital"){
										$book_name = $PDO->getSingleResult("select name from #_hospitals where user_id='{$book_id}'");
									}else if(strtolower($book_type)=="clinics"){
										
										$book_name = $PDO->getSingleResult("select name from #_clinics where user_id='{$book_id}'");
									}else if(strtolower($book_type)=="healthcare"){
										$book_name = $PDO->getSingleResult("select name from #_healthcare_organization where user_id='{$book_id}'");
									}
										//echo "select name from #_labs where user_id='{$book_id}'";
									  $lab_test =explode('!,',trim($line2['lab_test'],'!')); 		
										
								?>
                                
                                    <tr>
                                            <td><?=ucfirst($book_name)?></td>
                                             <td>                                            
                                            <?php 
											     for($i=0;$i < count($lab_test); $i++)
												 {
													 $lab_arr =explode(':',$lab_test[$i]);
													 echo $lab_arr[1].'<br>';
												 }
										     ?>
                                            </td>
                                            <td><?=ucfirst($line2['book_date'])?></td>
                                            <td><?=ucfirst($line2['book_time'])?></td>
                                          
                                        </tr>
                                
								   
									
								<?php } ?>
                               </table>
                                 <div class="clearfix"></div>
									</div>
                                    </div>
                                
                                 <?php } 
									
								//    clinics
								$find = $OP->get2('booking',$pid,'call_id','service_type','Imaging');
								
								 if($OP->count_val($find)>0){ 
								 ?>
								  <div class="app">
                                <span class="doctor-heading">Imaging Appointment(s)</span>
                                <div class="table-responsive">
                                 <table class="table table-bordered align-left">
                                    <thead>
                                        <tr>
                                            <td><span>Name</span></td>
                                             <td><span>Test</span></td>
                                            <td><span>Appointment Date</span></td>
                                            <td><span>Appointment Time</span></td>
                                           
                                        </tr>
                                    </thead>
								<?php
									while ($line2 = $PDO->db_fetch_array($find))
									{
										$book_type = $line2['book_type'];
										$book_id = $line2['book_id'];
										if(strtolower($book_type)=="lab"){
										$book_name = $PDO->getSingleResult("select name from #_labs where user_id='{$book_id}'");
										
									}else if(strtolower($book_type)=="hospital"){
										$book_name = $PDO->getSingleResult("select name from #_hospitals where user_id='{$book_id}'");
									}
									//	$book_name = $PDO->getSingleResult("select name from #_clinics where user_id='{$book_id}'");
									 $lab_test =explode('!,',trim($line2['lab_test'],'!')); 	
								?>
                                
                                <tr>
                                            <td><?=ucfirst($book_name)?></td>
                                             <td>                                            
                                            <?php 
											     for($i=0;$i < count($lab_test); $i++)
												 {
													 $lab_arr =explode(':',$lab_test[$i]);
													 echo $lab_arr[1].'<br>';
												 }
										     ?>
                                            </td>
                                            <td><?=ucfirst($line2['book_date'])?></td>
                                            <td><?=ucfirst($line2['book_time'])?></td>
                                          
                                        </tr>
                                
								
								<?php } ?>
                                
                                   </table>
                                    <div class="clearfix"></div>
									</div>
                                    </div>
                                <?php 
								 }
									
								/*
								$find = $OP->get2('booking',$pid,'call_id','book_type','Hospital');
								
								 if($OP->count_val($find)>0){ 
								 ?>
								  <div class="app">
                                <span class="doctor-heading">Hospital Appointment(s)</span>
                                <div class="table-responsive">
                                 <table class="table table-bordered align-left">
                                    <thead>
                                        <tr>
                                            <td><span>Appointment Service</span></td>
                                            <td><span>Appointment</span></td>
                                            <td><span>Date</span></td>
                                           
                                        </tr>
                                    </thead>
								<?php
									while ($line2 = $PDO->db_fetch_array($find))
									{
										$book_id = $line2['book_id'];
										$book_name = $PDO->getSingleResult("select name from #_hospitals where user_id='{$book_id}'");
										
								?>
                                
                                <tr>
                                            <td><?=ucfirst($book_name)?></td>
                                            <td><?=ucfirst($line2['book_date'])?></td>
                                            <td><?=ucfirst($line2['book_time'])?></td>
                                          
                                        </tr>
                                
								
								<?php } ?>
                                  </table>
                                    <div class="clearfix"></div>
									</div>
                                  </div>
								<?php 
								 } */
									
								//    healthcare
								$find = $OP->get2('booking',$pid,'call_id','service_type','Healthcare');
								
								 if($OP->count_val($find)>0){ 
								 ?>
								  <div class="app">
                                <span class="doctor-heading">Healthcare Appointment(s)</span>
                                <div class="table-responsive">
                                 <table class="table table-bordered align-left">
                                    <thead>
                                        <tr>
                                            <td><span>Name</span></td>
                                            <td><span>Appointment Date</span></td>
                                            <td><span>Appointment Time</span></td>
                                           
                                        </tr>
                                    </thead>
								<?php
									while ($line2 = $PDO->db_fetch_array($find))
									{
										$book_id = $line2['book_id'];
										$book_name = $PDO->getSingleResult("select name from #_healthcare_organization where user_id='{$book_id}'");
										
								?>
                                  
                                <tr>
                                            <td><?=ucfirst($book_name)?></td>
                                            <td><?=ucfirst($line2['book_date'])?></td>
                                            <td><?=ucfirst($line2['book_time'])?></td>
                                          
                                        </tr>
								
								<?php } ?>  
                                  </table>
                                    <div class="clearfix"></div>
									</div>
                                  </div>
								 <?php } ?>
								 
								
								 
                               
							   </div>
                            </div>
                          </div>
                        </div>
                      </article>
					<?php } 
					
					
					}
					?>
  <!--TIMELINE FIRST SECTION CLOSE-->
						
</section>
              		</div>
              </div>
            </div>
		</div>
<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {padding: 8px 15px;	text-align: left !important;}
</style>
<script>
$(function(){
	$('.close1').click(function(){
		window.location.href=window.location.href;
	});
});
</script>										