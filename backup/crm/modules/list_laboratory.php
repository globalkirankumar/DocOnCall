<div class="Laboratory">
	<h5>Lab</h5>
	<div class="col-lg-12">
		<!-- MOLECULAR DIAGNOSIS -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>MOLECULAR DIAGNOSIS</h4>
				<ul>
					<li>
						<span class="Laboratory_span">HBV DNA (VIAL LOAD TEST)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="HBV DNA (VIAL LOAD TEST) is required!" name="left_services[hbv_dna]"   value="<?=$left_services['hbv_dna']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">HCV RNA (VIAL LOAD TEST)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="HCV RNA (VIAL LOAD TEST) is required!" name="left_services[hcv_rna]"   value="<?=$left_services['hcv_rna']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">HIV RNA (VIAL LOAD TEST)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="HIV RNA (VIAL LOAD TEST) is required!" name="left_services[hiv_rna]"   value="<?=$left_services['hiv_rna']?>" type="text">
					</li>
				</ul>
			</div>
		</div>	
		<!-- End MOLECULAR DIAGNOSIS -->
		<!-- BIOCHEMISTRY -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>BIOCHEMISTRY</h4>
				<ul>	
					<li>
						<span class="Laboratory_span">LIPID PROFILE ( fasting )(3cc/Plain Tube)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="LIPID PROFILE is required!" name="left_services[lipid_profile]"   value="<?=$left_services['lipid_profile']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Cholesterol(Total)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Cholesterol(Total) is required!" name="left_services[cholesterol]"   value="<?=$left_services['cholesterol']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Triglyceride</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Triglyceride is required!" name="left_services[triglyceride]"   value="<?=$left_services['triglyceride']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">HDL</span>
						<input class="Laboratory_input" data-errormessage-value-missing="HDL is required!" name="left_services[hdl]"   value="<?=$left_services['hdl']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">LDL/Direct LDL</span>
						<input class="Laboratory_input" data-errormessage-value-missing="LDL/Direct LDL is required!" name="left_services[ldl]"   value="<?=$left_services['ldl']?>" type="text">
					</li>
				</ul>
			</div>
		</div>		
		<!-- END BIOCHEMISTRY -->
		<!-- U & E(3cc/Plain) -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>U & E(3cc/Plain)</h4>
				<ul>	
					<li>
						<span class="Laboratory_span">Urea</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Urea is required!" name="left_services[urea]"   value="<?=$left_services['urea']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Sodium</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Sodium is required!" name="left_services[sodium]"   value="<?=$left_services['sodium']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Potassium</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Potassium is required!" name="left_services[potassium]"   value="<?=$left_services['potassium']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Bicarbonate(ECO2)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Bicarbonate(ECO2) is required!" name="left_services[bicarbonate]"   value="<?=$left_services['bicarbonate']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Creatinine</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Creatinine is required!" name="left_services[creatinine]"   value="<?=$left_services['creatinine']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Uric Acid</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Uric Acid is required!" name="left_services[uric_acid]"   value="<?=$left_services['uric_acid']?>" type="text">
					</li>	
					<!--<li>
						<span class="Laboratory_span">Uric Acid</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Uric Acid is required!" name="left_services[uric_acid]"   value="<?=$left_services['uric_acid']?>" type="text">
					</li>	-->
				</ul>
			</div>
		</div>
		<!-- End U & E(3cc/Plain) -->
		<!-- LIVER FUNCTION TEST(3cc/Plain) -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>LIVER FUNCTION TEST(3cc/Plain)</h4>
				<ul>
					<li>
						<span class="Laboratory_span">Alkaline Phosphatase</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Alkaline Phosphatase is required!" name="left_services[alk_phosphatase]"   value="<?=$left_services['alk_phosphatase']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">ALT/SGPT</span>
						<input class="Laboratory_input" data-errormessage-value-missing="ALT/SGPT is required!" name="left_services[alt_sgpt]"   value="<?=$left_services['alt_sgpt']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">AST/SGOT</span>
						<input class="Laboratory_input" data-errormessage-value-missing="AST/SGOT is required!" name="left_services[alt_sgot]"   value="<?=$left_services['alt_sgot']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Bilirubin(Total)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Bilirubin(Total) is required!" name="left_services[bilirubin]"   value="<?=$left_services['bilirubin']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">Gamma-GT(GGT)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Gamma-GT(GGT) is required!" name="left_services[gamma-gt]"   value="<?=$left_services['gamma-gt']?>" type="text">
					</li>	
				</ul>
			</div>
		</div>		
		<!-- End LIVER FUNCTION TEST(3cc/Plain) -->
		<div class="clearfix"></div>
		<!-- DIABETES TEST -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>DIABETES TEST</h4>
				<ul>
					<li>
						<span class="Laboratory_span">Glucose(Fasting )</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Glucose(Fasting ) is required!" name="left_services[glucose]"   value="<?=$left_services['glucose']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">HBA2C(3cc/EDTA)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="HBA2C(3cc/EDTA) is required!" name="left_services[hba2c]"   value="<?=$left_services['hba2c']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">Insulin</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Insulin is required!" name="left_services[insulin]"   value="<?=$left_services['insulin']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">RBS ( random blood sugur)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="RBS ( random blood sugur) is required!" name="left_services[rbs]"   value="<?=$left_services['rbs']?>" type="text">
					</li>	
				</ul>	
			</div>			
		</div>
		<!-- End DIABETES TEST -->
		<!-- T&DP(3cc/Plain) -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>T&DP(3cc/Plain)</h4>
				<ul>
					<li>
						<span class="Laboratory_span">Total Protein</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Total Protein is required!" name="left_services[total_protein]"   value="<?=$left_services['total_protein']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Albumin</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Albumin is required!" name="left_services[albumin]"   value="<?=$left_services['albumin']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">Globulin</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Globulin is required!" name="left_services[globulin]"   value="<?=$left_services['globulin']?>" type="text">
					</li>		
				</ul>		
			</div>				
		</div>					
		<!-- End T&DP(3cc/Plain) -->
		<!-- OTHERS(3cc/Plain) -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>OTHERS(3cc/Plain)</h4>
				<ul>
					<li>
						<span class="Laboratory_span">Amylase</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Amylase is required!" name="left_services[amylase]"   value="<?=$left_services['amylase']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">Magnesium</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Magnesium is required!" name="left_services[magnesium]"   value="<?=$left_services['magnesium']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">Calcium</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Calcium is required!" name="left_services[calcium]"   value="<?=$left_services['calcium']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">Corrected Ca</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Corrected Ca is required!" name="left_services[corrected_ca]"   value="<?=$left_services['corrected_ca']?>" type="text">
					</li>			
					<li>
						<span class="Laboratory_span">Phosphorous</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Phosphorous is required!" name="left_services[phosphorous]"   value="<?=$left_services['phosphorous']?>" type="text">
					</li>			
					<li>
						<span class="Laboratory_span">LDH</span>
						<input class="Laboratory_input" data-errormessage-value-missing="LDH is required!" name="left_services[ldh]"   value="<?=$left_services['ldh']?>" type="text">
					</li>			
					<li>
						<span class="Laboratory_span">CK</span>
						<input class="Laboratory_input" data-errormessage-value-missing="CK is required!" name="left_services[ck]"   value="<?=$left_services['ck']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">CK-MB</span>
						<input class="Laboratory_input" data-errormessage-value-missing="CK-MB is required!" name="left_services[ck_mb]"   value="<?=$left_services['ck_mb']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">CRP(Latex/hs)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="CRP(Latex/hs) is required!" name="left_services[crp_latex]"   value="<?=$left_services['crp_latex']?>" type="text">
					</li>	
				</ul>
			</div>
		</div>
		<!-- End OTHERS(3cc/Plain) -->
		<!-- IMMUNOLOGY -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>IMMUNOLOGY</h4>
				<ul>	
					<li>
						<span class="Laboratory_span">PSA(Total/Free)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="PSA(Total/Free) is required!" name="left_services[psa]"   value="<?=$left_services['psa']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">CEA</span>
						<input class="Laboratory_input" data-errormessage-value-missing="CEA is required!" name="left_services[cea]"   value="<?=$left_services['cea']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">AFP</span>
						<input class="Laboratory_input" data-errormessage-value-missing="AFP is required!" name="left_services[afp]"   value="<?=$left_services['afp']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">CA 12-5</span>
						<input class="Laboratory_input" data-errormessage-value-missing="CA 12-5 is required!" name="left_services[ca_12_5]"   value="<?=$left_services['ca_12_5']?>" type="text">
					</li>		
					<li>
						<span class="Laboratory_span">CA 125-3</span>
						<input class="Laboratory_input" data-errormessage-value-missing="CA 125-3 is required!" name="left_services[ca_125_3]"   value="<?=$left_services['ca_125_3']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">CA 19-9</span>
						<input class="Laboratory_input" data-errormessage-value-missing="CA 19-9 is required!" name="left_services[ca_19_9]"   value="<?=$left_services['ca_19_9']?>" type="text">
					</li>	
				</ul>			
			</div>
		</div>		
		<!-- End IMMUNOLOGY -->
		<div class="clearfix"></div>
		<!-- HORMONES -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>HORMONES(3cc/Plain)</h4>
				<ul>
					<li>
						<span class="Laboratory_span">T3,T4,TSH</span>
						<input class="Laboratory_input" data-errormessage-value-missing="T3,T4,TSH is required!" name="left_services[t3_t4_tsh]"   value="<?=$left_services['t3_t4_tsh']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">Free T3,T4</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Free T3,T4 is required!" name="left_services[t3_t4_free]"   value="<?=$left_services['t3_t4_free']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Anti TPO</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Anti TPO is required!" name="left_services[anti_tpo]"   value="<?=$left_services['anti_tpo']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">Estradiol(E2)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Estradiol(E2) is required!" name="left_services[estradiol]"   value="<?=$left_services['estradiol']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">FSH</span>
						<input class="Laboratory_input" data-errormessage-value-missing="FSH is required!" name="left_services[FSH]"   value="<?=$left_services['FSH']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">LH</span>
						<input class="Laboratory_input" data-errormessage-value-missing="LH is required!" name="left_services[LH]"   value="<?=$left_services['LH']?>" type="text">
					</li>			
					<li>
						<span class="Laboratory_span">Progesterone</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Progesterone is required!" name="left_services[progesterone]"   value="<?=$left_services['progesterone']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">Prolactin</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Prolactin is required!" name="left_services[prolactin]"   value="<?=$left_services['prolactin']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">Vitamin D</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Vitamin D is required!" name="left_services[vitamin_D]"   value="<?=$left_services['vitamin_D']?>" type="text">
					</li>	
					<li>
						<span class="Laboratory_span">Testosterone</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Testosterone is required!" name="left_services[testosterone]"   value="<?=$left_services['testosterone']?>" type="text">
					</li>		
					<li>
						<span class="Laboratory_span">Cortisol(Serum/24hr Urine)</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="Cortisol(Serum/24hr Urine) is required!" name="left_services[cortisol]"   value="<?=$left_services['cortisol']?>" type="text">
					</li>		
					<li>
						<span class="Laboratory_span">β HCG(quantitative)</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="β HCG(quantitative) is required!" name="left_services[β_HCG]"   value="<?=$left_services['β_HCG']?>" type="text">
					</li>		
					<li>
						<span class="Laboratory_span">β Cross Lap(10hr Fasting)</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="β Cross Lap(10hr Fasting) is required!" name="left_services[β_Cross_Lap]"   value="<?=$left_services['β_Cross_Lap']?>" type="text">
					</li>			
				</ul>			
			</div>				
		</div>
		<!-- End HORMONES -->
		<!-- THERAPEUTIC DRUG LEVEL -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>THERAPEUTIC DRUG LEVEL</h4>
				<ul>
					<li>
						<span class="Laboratory_span">Tacrolimus</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="Tacrolimus is required!" name="left_services[tacrolimus]"   value="<?=$left_services['tacrolimus']?>" type="text">
					</li>		
				</ul>
			</div>
		</div>		
		<!-- End THERAPEUTIC DRUG LEVEL -->
		<!-- RHEUMATOLOGY(3cc/Plain) -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>RHEUMATOLOGY(3cc/Plain)</h4>
				<ul>
					<li>
						<span class="Laboratory_span">ASO(titre)</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="ASO(titre) is required!" name="left_services[ASO]"   value="<?=$left_services['ASO']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">RA(titre)</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="RA(titre) is required!" name="left_services[RA]"   value="<?=$left_services['RA']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Anti CCP(quantitative)</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="Anti CCP(quantitative) is required!" name="left_services[anti_ccp]"   value="<?=$left_services['anti_ccp']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Complement 3</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="Complement 3 is required!" name="left_services[complement_3]"   value="<?=$left_services['complement_3']?>" type="text">
					</li>		
					<li>
						<span class="Laboratory_span">Complement 4</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="Complement 4 is required!" name="left_services[complement_4]"   value="<?=$left_services['complement_4']?>" type="text">
					</li>			
					<li>
						<span class="Laboratory_span">Lupus anticoagulant</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="Lupus anticoagulant is required!" name="left_services[lupus_anticoagulant]"   value="<?=$left_services['lupus_anticoagulant']?>" type="text">
					</li>			
				</ul>
			</div>
		</div>		
		<!-- End RHEUMATOLOGY(3cc/Plain) -->
		<!-- CARDIAC MARKERS -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>CARDIAC MARKERS</h4>
				<ul>
					<li>
						<span class="Laboratory_span">Troponin I (3cc/Plain)</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="Lupus anticoagulant is required!" name="left_services[lupus_anticoagulant]"   value="<?=$left_services['lupus_anticoagulant']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Troponin T (3cc/Heparin or EDTA)</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="Troponin T (3cc/Heparin or EDTA) is required!" name="left_services[troponin_T]"   value="<?=$left_services['troponin_T']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">NT-Pro BNP(2cc/Heparin)</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="NT-Pro BNP(2cc/Heparin) is required!" name="left_services[nt_pro]"   value="<?=$left_services['nt_pro']?>" type="text">
					</li>	
				</ul>
			</div>
		</div>		
		<!-- End CARDIAC MARKERS -->
		<div class="clearfix"></div>
		<!-- INFECTIOUS DISEASE(3cc/Plain) -->	
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>INFECTIOUS DISEASE(3cc/Plain)</h4>
				<ul>
					<li>
						<span class="Laboratory_span">HBV PROFILE</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="HBV PROFILE is required!" name="left_services[hbv_profile]"   value="<?=$left_services['hbv_profile']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">HBs Ag</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="HBs Ag is required!" name="left_services[HBs_Ag]"   value="<?=$left_services['HBs_Ag']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Anti HBs(quantitative)</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="Anti HBs(quantitative) is required!" name="left_services[Anti_HBs]"   value="<?=$left_services['Anti_HBs']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">HBe Ag</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="HBe Ag is required!" name="left_services[HBe_Ag]"   value="<?=$left_services['HBe_Ag']?>" type="text">
					</li>		
					<li>
						<span class="Laboratory_span">Anti HBc(Total/IgM)</span>
						<input class="Laboratory_input"  data-errormessage-value-missing="Anti HBc(Total/IgM) is required!" name="left_services[Anti_HBc]"   value="<?=$left_services['Anti_HBc']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">HIV 1 & 2 Antibody</span>
						<input class="Laboratory_input" data-errormessage-value-missing="HIV 1 & 2 Antibodyis is required!" name="left_services[hiv_1_2]"   value="<?=$left_services['hiv_1_2']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">VDRL/RPR</span>
						<input class="Laboratory_input" data-errormessage-value-missing="VDRL/RPR is required!" name="left_services[vdrl_rpr]"   value="<?=$left_services['vdrl_rpr']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">TPHA</span>
						<input class="Laboratory_input" data-errormessage-value-missing="TPHA is required!" name="left_services[TPHA]"   value="<?=$left_services['TPHA']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Dengue Ag & Antibody(IgG/IgM)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Dengue Ag & Antibody(IgG/IgM) is required!" name="left_services[Dengue_Ag_Antibody]"   value="<?=$left_services['Dengue_Ag_Antibody']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Widal Test</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Widal Test is required!" name="left_services[Widal_Test]"   value="<?=$left_services['Widal_Test']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">MF(ICT/Film)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="MF(ICT/Film) is required!" name="left_services[MF_ICT_Flim]"   value="<?=$left_services['MF_ICT_Flim']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Chikungunya (IgM)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Chikungunya (IgM) is required!" name="left_services[Chikungunya]"   value="<?=$left_services['Chikungunya']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">H.Polari(IgG)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="H.Polari(IgG) is required!" name="left_services[H_Polari]"   value="<?=$left_services['H_Polari']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Leptosprial Antibody(IgG/IgM)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Leptosprial Antibody(IgG/IgM) is required!" name="left_services[Leptosprial_Antibody]"   value="<?=$left_services['Leptosprial_Antibody']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Malaria(ICT/Film)(3cc/EDTA)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Malaria(ICT/Film)(3cc/EDTA) is required!" name="left_services[Malaria]"   value="<?=$left_services['Malaria']?>" type="text">
					</li>
				</ul>
			</div>
		</div>		
		<!-- End INFECTIOUS DISEASE(3cc/Plain) -->
		<!-- 24 HR URINE ANALYSIS -->	
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>24 HR URINE ANALYSIS</h4>
				<ul>
					<li>
						<span class="Laboratory_span">Sodium</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Sodium is required!" name="left_services[Sodium_24]"   value="<?=$left_services['Sodium_24']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Potassium</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Potassium is required!" name="left_services[Potassium_24]"   value="<?=$left_services['Potassium_24']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Protein</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Protein is required!" name="left_services[Protein_24]"   value="<?=$left_services['Protein_24']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Cortisol</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Cortisol is required!" name="left_services[Cortisol_24]"   value="<?=$left_services['Cortisol_24']?>" type="text">
					</li>
				</ul>
			</div>
		</div>	
		<!-- End 24 HR URINE ANALYSIS -->
		<!-- HAEMATOLOGY(3cc/EDTA) -->	
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>HAEMATOLOGY(3cc/EDTA)</h4>
				<ul>
					<li>
						<span class="Laboratory_span">CP(32 Parameters)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="CP(32 Parameters) is required!" name="left_services[CP_32]"   value="<?=$left_services['CP_32']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">ESR</span>
						<input class="Laboratory_input" data-errormessage-value-missing="ESR is required!" name="left_services[ESR]"   value="<?=$left_services['ESR']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">CD4 count</span>
						<input class="Laboratory_input" data-errormessage-value-missing="CD4 count is required!" name="left_services[CD4]"   value="<?=$left_services['CD4']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Hb %</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Hb % is required!" name="left_services[hb]"   value="<?=$left_services['hb']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Reticulocyte Count %</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Reticulocyte Count % is required!" name="left_services[Reticulocyte_Count]"   value="<?=$left_services['Reticulocyte_Count']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">ABO/Rh Grouping</span>
						<input class="Laboratory_input" data-errormessage-value-missing="ABO/Rh Grouping is required!" name="left_services[ABO_Rh]"   value="<?=$left_services['ABO_Rh']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Rh Antibody Titre</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Rh Antibody Titre is required!" name="left_services[Rh_Antibody]"   value="<?=$left_services['Rh_Antibody']?>" type="text">
					</li>
					<li>
						<span class="Laboratory_span">Hb Electrophoresis</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Hb Electrophoresis is required!" name="left_services[Hb_Electrophoresis]"   value="<?=$left_services['Hb_Electrophoresis']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Protein Electrophoresis</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Protein Electrophoresis is required!" name="left_services[Protein_Electrophoresis]"   value="<?=$left_services['Protein_Electrophoresis']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">HbF (Singer's Test)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="HbF (Singer's Test) is required!" name="left_services[HbF]"   value="<?=$left_services['HbF']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Hb H Inclusion</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Hb H Inclusion is required!" name="left_services[Hb_H_Inclusin]"   value="<?=$left_services['Hb_H_Inclusin']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Coombs' Test</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Coombs' Test is required!" name="left_services[Coombs_Test]"   value="<?=$left_services['Coombs_Test']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Cold/Warm Agglutination</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Cold/Warm Agglutination is required!" name="left_services[Cold_warm]"   value="<?=$left_services['Cold_warm']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">G6PD</span>
						<input class="Laboratory_input" data-errormessage-value-missing="G6PD is required!" name="left_services[G6PD]"   value="<?=$left_services['G6PD']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">PNH</span>
						<input class="Laboratory_input" data-errormessage-value-missing="PNH is required!" name="left_services[PNH]"   value="<?=$left_services['PNH']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">LAPA score</span>
						<input class="Laboratory_input" data-errormessage-value-missing="LAPA score is required!" name="left_services[LAPA_score]"   value="<?=$left_services['LAPA_score']?>" type="text">
					</li>
				</ul>
			</div>
		</div>	
		<!-- End HAEMATOLOGY(3cc/EDTA) -->
		<!-- IRON STUDY(3cc/Plain) -->	
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>IRON STUDY(3cc/Plain)</h4>
				<ul>
					<li>
						<span class="Laboratory_span">Ferritin</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Ferritin score is required!" name="left_services[Ferritin]"   value="<?=$left_services['Ferritin']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Serum Iron</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Serum Iron score is required!" name="left_services[Serum_Iron]"   value="<?=$left_services['Serum_Iron']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">TIBC</span>
						<input class="Laboratory_input" data-errormessage-value-missing="TIBC score is required!" name="left_services[TIBC]"   value="<?=$left_services['TIBC']?>" type="text">
					</li>
				</ul>
			</div>
		</div>	
		<!-- End IRON STUDY(3cc/Plain) -->	
		<div class="clearfix"></div>
		<!-- COAGULATION PROFILE -->	
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>COAGULATION PROFILE</h4>
				<ul>
					<li>
						<span class="Laboratory_span">PT/INR</span>
						<input class="Laboratory_input" data-errormessage-value-missing="PT/INR is required!" name="left_services[PT_INR]"   value="<?=$left_services['PT_INR']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">APTT</span>
						<input class="Laboratory_input" data-errormessage-value-missing="APTT is required!" name="left_services[APTT]"   value="<?=$left_services['APTT']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Fibrinogen</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Fibrinogen is required!" name="left_services[Fibrinogen]"   value="<?=$left_services['Fibrinogen']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Factor VIII</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Factor VIII is required!" name="left_services[Factor_VIII]"   value="<?=$left_services['Factor_VIII']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Factor IX</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Factor IX is required!" name="left_services[Factor_IX]"   value="<?=$left_services['Factor_IX']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">D-dimer</span>
						<input class="Laboratory_input" data-errormessage-value-missing="D-dimer is required!" name="left_services[D_dimer]"   value="<?=$left_services['D_dimer']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">FDP</span>
						<input class="Laboratory_input" data-errormessage-value-missing="FDP is required!" name="left_services[FDP]"   value="<?=$left_services['FDP']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Mixing Factor</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Mixing Factor is required!" name="left_services[Mixing_Factor]"   value="<?=$left_services['Mixing_Factor']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Von Willebrand Factor</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Von Willebrand Factor is required!" name="left_services[Von_Willebrand_Factor]"   value="<?=$left_services['Von_Willebrand_Factor']?>" type="text">
					</li>
				</ul>
			</div>
		</div>	
		<!-- End COAGULATION PROFILE -->
		<!-- BONE MARROW STUDY -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>BONE MARROW STUDY</h4>
				<ul>
					<li>
						<span class="Laboratory_span">Trephine Biopsy</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Trephine Biopsy is required!" name="left_services[Trephine_Biopsy]"   value="<?=$left_services['Trephine_Biopsy']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Aspiration</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Aspiration is required!" name="left_services[Aspiration]"   value="<?=$left_services['Aspiration']?>" type="text">
					</li>
				</ul>
			</div>
		</div>		
		<!-- End BONE MARROW STUDY -->
		<!-- HISTOPATHOLOGY -->	
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>HISTOPATHOLOGY</h4>
				<ul>
					<li>
						<span class="Laboratory_span">Fluid Cytology</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Fluid Cytology is required!" name="left_services[Fluid_Cytology]"   value="<?=$left_services['Fluid_Cytology']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Sputum Cytology</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Sputum Cytology is required!" name="left_services[Sputum_Cytology]"   value="<?=$left_services['Sputum_Cytology']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Tissue Biopsy</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Tissue Biopsy is required!" name="left_services[Tissue_Biopsy]"   value="<?=$left_services['Tissue_Biopsy']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">FNAC</span>
						<input class="Laboratory_input" data-errormessage-value-missing="FNAC is required!" name="left_services[FNAC]"   value="<?=$left_services['FNAC']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Pap Smear</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Pap Smear is required!" name="left_services[Pap_Smear]"   value="<?=$left_services['Pap_Smear']?>" type="text">
					</li>
				</ul>
			</div>
		</div>	
		<!-- End HISTOPATHOLOGY -->
		<!-- MICROBIOLOGY -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>MICROBIOLOGY</h4>
				<ul>	
					<li>
						<span class="Laboratory_span">Aspirated Fluid(CSF/Pleural/Ascitic)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Aspirated Fluid(CSF/Pleural/Ascitic) is required!" name="left_services[Aspirated_Fluid]"   value="<?=$left_services['Aspirated_Fluid']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Blood(Note Instructions)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Blood(Note Instructions) is required!" name="left_services[Blood]"   value="<?=$left_services['Blood']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Urine</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Urine is required!" name="left_services[Urine]"   value="<?=$left_services['Urine']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Sputum</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Sputum is required!" name="left_services[Sputum]"   value="<?=$left_services['Sputum']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Stool</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Stool is required!" name="left_services[Stool]"   value="<?=$left_services['Stool']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Pus</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Pus is required!" name="left_services[Pus]"   value="<?=$left_services['Pus']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Throat Swab</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Throat Swab is required!" name="left_services[Throat_Swab]"   value="<?=$left_services['Throat_Swab']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Ear Swab</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Ear Swab is required!" name="left_services[Ear_Swab]"   value="<?=$left_services['Ear_Swab']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Wound Swab</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Wound Swab is required!" name="left_services[Wound_Swab]"   value="<?=$left_services['Wound_Swab']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">High Vaginal Swab(HVS)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="High Vaginal Swab(HVS) is required!" name="left_services[High_Vaginal]"   value="<?=$left_services['High_Vaginal']?>" type="text">
					</li>
				</ul>			
			</div>
		</div>
		<!-- End MICROBIOLOGY -->
		<div class="clearfix"></div>
		<!-- ROUTINE EXAMINATION(RE) -->
		<div class="col-md-3 col-sm-4 col-xs-12">
			<div class="Laboratory_box">
				<h4>ROUTINE EXAMINATION(RE)</h4>
				<ul>	
					<li>
						<span class="Laboratory_span">Aspirated Fluid RE</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Aspirated Fluid RE is required!" name="left_services[Aspirated_Fluid_RE]"   value="<?=$left_services['Aspirated_Fluid_RE']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">(CSF/Pleural/Ascitic)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="(CSF/Pleural/Ascitic) is required!" name="left_services[CSF_Pleural_Ascitic]"   value="<?=$left_services['CSF_Pleural_Ascitic']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Urine(RE)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Urine(RE) is required!" name="left_services[Urine_RE]"   value="<?=$left_services['Urine_RE']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Urine Microalbumin</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Urine Microalbumin is required!" name="left_services[Urine_Microalbumin]"   value="<?=$left_services['Urine_Microalbumin']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Urine(HCG/UCG)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Urine(HCG/UCG) is required!" name="left_services[Urine_HCG_UCG]"   value="<?=$left_services['Urine_HCG_UCG']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Bence Jones Protein(Urine)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Bence Jones Protein(Urine) is required!" name="left_services[Bence_Jones_Protein]"   value="<?=$left_services['Bence_Jones_Protein']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Stool(RE)</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Stool(RE) is required!" name="left_services[Stool_RE]"   value="<?=$left_services['Stool_RE']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Stool For Occult Blood</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Stool For Occult Blood is required!" name="left_services[Stool_For_Occult_Blood]"   value="<?=$left_services['Stool_For_Occult_Blood']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Stool For AFB</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Stool For AFB is required!" name="left_services[Stool_For_AFB]"   value="<?=$left_services['Stool_For_AFB']?>" type="text">
					</li>
					<li> 
						<span class="Laboratory_span">Sputum AFB</span>
						<input class="Laboratory_input" data-errormessage-value-missing="Sputum AFB is required!" name="left_services[Sputum_AFB]"   value="<?=$left_services['Sputum_AFB']?>" type="text">
					</li>
				</ul>
			</div>
		</div>	
		<!-- End ROUTINE EXAMINATION(RE) -->
	</div>
</div>	