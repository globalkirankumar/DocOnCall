<?php
$caller_query=$PDO->db_query("select * from #_call_details where pid ='".$_SESSION['call_id']."'  "); 
$caller_data = $PDO->db_fetch_array($caller_query);  
@extract($caller_data);
?>
  <div>		
		  	<div class="row">
              <div class="col-md-12">
              		<div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title"><span class="menu-icon"> <i class="fa fa-pencil"></i> </span> Medical History</h3>
                  	</div>
              		<div class="section-body">
                  <div class="form-group">
                    <span class="checkbox_sameinfo"><input type="checkbox"  name="call_type" value="follow up call" id="checkbox-4"   <?=($call_type=='follow up call')?'checked="checked"':''?> ><label class="check_box"  >Is this a follow up call?</label></span>
                  </div>
                  <div class="question_ans">
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">Chief Complaint</span>  </li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" name="chief_complaint" value="<?=$chief_complaint?>" class="que_input_box" placeholder=""></span></li>
                  </ul>
                  </div>
                  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">History of present illness </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="history_of_present_illness" ><?=$history_of_present_illness?></textarea> </span></li>
                    </ul>
                  </div>
                  <div class="question_ans">
                  <span class="que_heading">Past history  </span>
                  <div class="nastingul">
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">past medical  history</span></li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder=""  name="past_medical_history" value="<?=$past_medical_history?>"></span></li>
                  </ul>
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">Past surgical history</span></li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="" name="past_surgical_history" value="<?=$past_surgical_history?>" ></span></li>
                  </ul>
                  </div>
                  </div>
                  
                  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Social history  </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="" name="social_history" value="<?=$social_history?>" > </span></li>
                    </ul>
                  </div>
                  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Family history </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder=""  name="family_history" value="<?=$family_history?>" > 
                      </span></li>
                    </ul>
                  </div>
                  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Occupational history</span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder=" " name="occupational_history" value="<?=$occupational_history?>" > 
                      </span></li>
                    </ul>
                  </div>
                    <div class="question_ans">
                      <ul>
                        <li><span class="que_heading">Medication History  </span>  </li>
                        <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">history of drug allergy </span>  </li>
                        <li class="editli"><!-- <img src="img/ans.jpg"> --><span class="que_inner"> <span class="checkbox_callinfo"><input  type="radio" name="history_of_drug_allergy" value="Yes" <?=($history_of_drug_allergy=='Yes')?'checked="checked"':''?> ><label class="check_box">Yes</label></span><span class="checkbox_callinfo"><input type="radio"  name="history_of_drug_allergy" value="No" <?=($history_of_drug_allergy=='No')?'checked="checked"':''?> ><label class="check_box">No</label></span></span></span></li>
                      <br>
                      <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">if yes <input type="text" class="que_input_box" placeholder="" name="drug_allergy_comment"  value="<?=$drug_allergy_comment?>"> </span>  </li>
                        <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">Regular taking medication <input type="text" class="que_input_box" placeholder="" name="drug_allergy_regular_taking_medication"  value="<?=$drug_allergy_regular_taking_medication?>"> </span>  </li>
                       
                      </ul>
                    </div>
                    <div class="question_ans">
                      <ul class="libox">

                        <li><span class="text">BP- </span> <span class="input"><input type="text" class="que_input_box"  name="bp" value="<?=$bp?>" ></span> </li>
                        <li><span class="text">PR-</span> <span class="input"><input type="text" class="que_input_box"  name="pr" value="<?=$pr?>" ></span> </li>
                       <li><span class="text">SaO2-</span> <span class="input"><input type="text" class="que_input_box"  name="sao" value="<?=$sao?>" ></span> </li>
                       <li><span class="text">Weight-</span> <span class="input"><input type="text" class="que_input_box"  name="weight" value="<?=$weight?>"  ></span> </li>
                        <li><span class="text">RBS-</span> <span class="input"><input type="text" class="que_input_box"  name="rbs" value="<?=$rbs?>" ></span> </li>
                          <li><span class="text">Temp-</span> <span class="input"><input type="text" class="que_input_box"  name="temp" value="<?=$temp?>" ></span> </li>
                        
                       
                       
                      </ul>
                    </div>
                    <div class="question_ans">
                      <ul>
                        <li><img src="img/que.jpg"><span class="que_inner">provisional diagnosis   </span>  </li>
                        <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder=""  name="provisional_diagnosis"  value="<?=$provisional_diagnosis?>"> </span></li>
                      </ul>
                    </div>
                    <div class="question_ans">
                      <ul>
                        <li><img src="img/que.jpg"><span class="que_inner">Service provided </span>  </li>
                        <li><img src="img/ans.jpg"><span class="que_inner">
                        <div id="email-input-wrapper" class="controls col-sm-6">
                        <div class="vd_radio radio-success">
                        <input   type="radio"  value="medical advice and counseling"  name="service_provided"  id="optionsRadios4" <?=($service_provided=='medical advice and counseling' || $caller_data['service_type']=='Health Consultation')?'checked="checked"':''?> onclick="service_provided_val()"  >
                          <label for="optionsRadios4"> Medical advice and counseling </label>
                        </div>

                         <div class="vd_radio radio-success">
                          <input  type="radio" value="health education and information"  name="service_provided" id="optionsRadios5" <?=($service_provided=='health education and information')?'checked="checked"':''?> onclick="service_provided_val()">
                          <label for="optionsRadios5"> Health education and information </label>
                        </div>

                        <div class="vd_radio radio-success">
                        <input type="radio" value="referral service to"  name="service_provided" <?=($service_provided=='referral service to'  || $caller_data['service_type']!='Health Consultation')?'checked="checked"':''?> id="optionsRadios6" onclick="service_provided_val()">
                          <label for="optionsRadios6"> Referral service to</label>
                        
                            <span class="checkbox-inline"><input type="radio"  name="referral_service_to"  value="GP" <?=($referral_service_to=='GP')?'checked="checked"':''?>  id="GP" onclick="referralserviceto()" ><label for="GP">GP</label></span>
                            <span class="checkbox-inline"><input type="radio" name="referral_service_to"  value="Specialist" <?=($referral_service_to=='Specialist')?'checked="checked"':''?> id="Specialist" onclick="referralserviceto()" ><label for="Specialist">Specialist</label></span>
                            <span class="checkbox-inline"><input type="radio" name="referral_service_to"  value="Lab" <?=($referral_service_to=='Lab')?'checked="checked"':''?> id="Lab" onclick="referralserviceto()" ><label for="Lab">Lab</label> </span>
                            <span class="checkbox-inline"><input type="radio" name="referral_service_to"  value="Imaging" <?=($referral_service_to=='Imaging')?'checked="checked"':''?> id="Imaging"  onclick="referralserviceto()"><label for="Imaging">Imaging</label></span>
                            <span class="checkbox-inline"><input type="radio" name="referral_service_to"  value="Home visit" <?=($referral_service_to=='Home visit')?'checked="checked"':''?> id="homevisit" ><label for="homevisit" onclick="referralserviceto()">Home visit</label></span>
                         
                        </div>
                        <!--<div class="vd_radio radio-success">
                        <input type="radio" value="follow up call"  id="optionsRadios3" name="service_provided" <?=($service_provided=='follow up call')?'checked="checked"':''?>>
                          <label for="optionsRadios3"> Follow up call (date time)</label>
                        </div>-->

                      </div>
                      
                       <div style="margin-top:20px;"><span class="que_inner"><input type="checkbox" name="follow_up_call_schedule"  value="Yes" <?=($caller_data['follow_up_call_schedule']=='Yes')?'checked="checked"':''?> onclick="follow_up_call_date_div()" >&nbsp;Schedule Follow up call </span> 
                       
                       <div id="follow_up_call_date_div" style="display:none">
                        <div class="col-md-6 col-sm-6 col-xs-6"><input type="text" class="datepicker" name="follow_up_call_date"  placeholder="Date" /></div>
                        <div class="col-md-6 col-sm-6 col-xs-6"><input type="text" name="follow_up_call_time" class="timePicker"  placeholder="Time"  /> <br />eg : <?=date('h:i a')?></div>
                       
                       </div>
                       </div>
                        </li>
                      </ul>
                      
                      
                      
                    </div>
                    
                    <script>
					
					    function service_provided_val()
						{
							if($("#optionsRadios6").is(':checked')==false)
							{
							     $( "#GP" ).prop( "checked", false ); 	
							     $( "#Specialist" ).prop( "checked", false );
								 $( "#Lab" ).prop( "checked", false );
								 $( "#Imaging" ).prop( "checked", false );
								 $( "#homevisit" ).prop( "checked", false );
							}
						 
						}
					
					    
						function referralserviceto()
						{
							
						    $( "#optionsRadios6" ).prop( "checked", true );  
						}
					
					
						function follow_up_call_date_div()
						{
							
							if($("input:checkbox[name='follow_up_call_schedule']").is(":checked")==true) 
							 {
								  $('#follow_up_call_date_div').show();
								 
							 }else {
								   $('#follow_up_call_date_div').hide(); 
							 }
						 }
					</script>
                  
                     <div id="errormsg" style="color:#F00; text-align:center"></div>
       

			<!-- close add-update form-->
              		</div>
                    <div class="col-sm-12"> 
							<a style="margin-left:10px;" class="btn nextbutton  next pull-right greenbutton" href="javascript:history()">Next <span class="menu-icon"><i class="fa fa-fw fa-chevron-circle-right"></i></span></a> 
				           <a class="btn nextbutton  next pull-right greenbutton" href="javascript:void()" onclick="back('comment')">Previous <span class="menu-icon"><i class="fa fa-fw fa-chevron-circle-left"></i></span></a>  
			</div>
              </div>
            </div>
		</div>
        
        