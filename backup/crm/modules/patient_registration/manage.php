<?php 
include(FS_ADMIN._MODS."/patient_registration/class.inc.php");
//include(FS_ADMIN._MODS."/pagination.php");

$US = new Users();


if($BSC->is_post_back())
{

	if(isset($health_consultation))
	{
		$PDO->sqlquery("rs",'call_details',array('health_consultation'=>$health_consultation),'pid',$_SESSION['call_id']);
			
	}
	
	$ADMIN->sessset('Call has been completed successfully', 'e'); 
	$BSC->redir($ADMIN->iurl('call_details'), true);

}
unset($_SESSION['patient_id']);
unset($_SESSION['call_id']);
unset($_SESSION['subscription_details_id']);
unset($_SESSION['patient_id']);
unset($_SESSION['caller_id']);
?>
       
   <div id="loader">
<div class="loadersmall"></div>
<style>
.loadersmall {
    border: 5px solid #f3f3f3;
    -webkit-animation: spin 1s linear infinite;
    animation: spin 1s linear infinite;
    border-top: 5px solid #555;
    border-radius: 50%;
    width: 50px;
    height: 50px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -25px;
    margin-top: -25px;
}
@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}


</style>
</div>  
<script language="javascript">
function callerInformation()
{
	 if($('#caller_information_same').is(':checked'))
	 {
		   	
           
		   $("input[name=caller_phone]").val($("input[name=patient_phone]").val());
		   $("input[name=caller_name]").val($("input[name=patient_name]").val());
		   $("input[name=caller_age]").val($("input[name=patient_age]").val());
		   $("input[name=caller_address]").val($("input[name=patient_address]").val());
		   $("input[name=caller_postal_code]").val($("input[name=patient_postal_code]").val());
		 	   
		   var patient_sex = $("#patient_sex option:selected" ).val();
		   $('#caller_sex option').removeAttr('selected').filter('[value='+patient_sex+']').attr('selected', true)
		   
		   var patient_sex = $("#patient_sex option:selected" ).val();
		   $('#caller_sex option').removeAttr('selected').filter('[value='+patient_sex+']').attr('selected', true);
		   
		   
		  $("input[name=caller_address]").val($("input[name=patient_address]").val());
		  $("input[name=caller_postal_code]").val($("input[name=patient_postal_code]").val());
		   
		   
		  // var patient_township = $("#patient_township option:selected" ).val();
		    $( "#caller_township option:selected" ).text($("#patient_township option:selected" ).text());
		    $( "#caller_township option:selected" ).val($("#patient_township option:selected" ).val());
		   //$('#caller_township option').removeAttr('selected').filter('[value='+patient_township+']').attr('selected', true);
		 	 
		  // $('.cat2').trigger('change');
			   
		   var patient_division = $("#patient_division option:selected" ).val();
		   
		   $('#caller_division option').removeAttr('selected').filter('[value='+patient_division+']').attr('selected', true)
		   
		 
	 }else{
		 
		   $("input[name=caller_phone]").val('');
		   $("input[name=caller_name]").val('');
		   $("input[name=caller_age]").val('');
		   $("input[name=caller_address]").val('');
		   $("input[name=caller_postal_code]").val('');
		 
	 }
}
var flag2=0;
function InformationForm()
{
	flag2=1;
  //formID	
 //  $('.controls-button').hide();
 
   $('#errormsg').html(''); 
   var str = $("#formID").serialize();
  
  /* if($("input:radio[name='call_type']").is(":checked")==false) 
   {
	   
		 $('#errormsg').html('Call type is required!'); 
		 return false;     
   }
   */
   
   var regExp = /^0[0-9].*$/
 
   if($('#sub_center_check').prop("checked")==true && $("#sub_centers option:selected").val()=='') 
   {  
	    $('#errormsg').html('Sub Centers is required!'); 
		return false;   
   }
   
   
   var patient_phone = $("input[name=patient_phone]").val();
   if(patient_phone=='')
   {
	   $('#errormsg').html('Patient phone is required!'); 
	   return false;  
   } 
   if(Number.isNaN(Number(patient_phone)))
   {  
       $('#errormsg').html('Patient phone should be numeric!'); 
	   return false;  
	   
   }
   
   if(regExp.test(patient_phone))
   {  
       $('#errormsg').html('Patient phone do not start the number with a 0!'); 
	   return false;  
	   
   }
   
   
   if($("#patient_sex option:selected").val()=='')
   {
	   // alert('Patient sex is required!');
		 $('#errormsg').html('Patient sex  is required!'); 
		return false;  
   }
   
    if($("input[name=patient_age]").val()=='')
   {
	   // alert('Patient address is required!');
		 $('#errormsg').html('Patient age is required!'); 
		return false;  
   }
   
   
   if($("input[name=patient_address]").val()=='')
   {
	   // alert('Patient address is required!');
		 $('#errormsg').html('Patient address is required!'); 
		return false;  
   }
   
   
  
  
   if($("#patient_division option:selected").val()=='')
   {
	   // alert('Patient division is required!');
		 $('#errormsg').html('Patient division is required!'); 
		return false;  
   }
   
   if($("#patient_township option:selected").val()=='')
   {
	   // alert('Patient township is required!');
		 $('#errormsg').html('Patient township is required!'); 
		return false;  
   }
   
   if($("input[name=patient_postal_code]").val()=='')
   {
	   // alert('Patient postal code is required!');
		 $('#errormsg').html('Patient postal code is required!'); 
		return false;  
   }
   
   
   
   var alternate_phone = $("input[name=alternate_phone]").val();
   if(alternate_phone!='')
   {
	   if(Number.isNaN(Number(alternate_phone)))
	   {  
		   $('#errormsg').html('Alternate phone should be numeric!'); 
		   return false;  
		   
	   }
	   
	   if(regExp.test(alternate_phone))
	   {  
		   $('#errormsg').html('Alternate phone do not start the number with a 0!'); 
		   return false;  
		   
	   }
   }
   
   
   var alternate_phone1 = $("input[name=alternate_phone1]").val();
   if(alternate_phone1!='')
   {
	   if(Number.isNaN(Number(alternate_phone1)))
	   {  
		   $('#errormsg').html('Alternate phone should be numeric!'); 
		   return false;  
		   
	   }
	   
	   if(regExp.test(alternate_phone1))
	   {  
		   $('#errormsg').html('Alternate phone do not start the number with a 0!'); 
		   return false;  
	   }
   }
   
    var alternate_phone2 = $("input[name=alternate_phone2]").val();
   if(alternate_phone2!='')
   {
	   if(Number.isNaN(Number(alternate_phone2)))
	   {  
		   $('#errormsg').html('Alternate phone should be numeric!'); 
		   return false;  
	   }	   
	   if(regExp.test(alternate_phone2))
	   {  
		   $('#errormsg').html('Alternate phone do not start the number with a 0!'); 
		   return false;  
	   }
   }
   
   
   
   // caller
   var caller_phone = $("input[name=caller_phone]").val();
    if(caller_phone=='')
   {
	   // alert('Caller phone is required!');
		 $('#errormsg').html('Caller phone is required!'); 
		return false;  
   }
   
   if(Number.isNaN(Number(caller_phone)))
   {  
       $('#errormsg').html('Caller phone should be numeric!'); 
	   return false;  
	   
   }
   
   if(regExp.test(caller_phone))
   {  
       $('#errormsg').html('Caller phone do not start the number with a 0!'); 
	   return false;  
	   
   }
  
   if($("#caller_sex option:selected").val()=='')
   {
	    alert('Caller sex is required!');
		 $('#errormsg').html('Call type is required!'); 
		return false;  
   }
   
   if($("input[name=caller_address]").val()=='')
   {
	    //alert('Caller address is required!');
		 $('#errormsg').html('Caller address is required!'); 
		return false;  
   }
   
    if($("input[name=caller_postal_code]").val()=='')
   {
	   // alert('Caller postal code is required!');
		 $('#errormsg').html('Caller postal code is required!'); 
		return false;  
   }
   
   if($("#caller_division option:selected").val()=='')
   {
	    //alert('Caller division is required!');
		 $('#errormsg').html('Caller division is required!'); 
		return false;  
   }
   
   if($("#caller_township option:selected").val()=='')
   {
	   // alert('Caller township is required!');
		 $('#errormsg').html('Caller townshipis required!'); 
		return false;  
   }
   
   
    $('#loader').show();
   $.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=services",
		           data: str,
		           cache: false,
		           success: function(html){
					  // alert(html)
						 $('#PatientDiv').html(html);
						  $('#loader').hide();
			            // $("#property_type").html(html);
						
		         }});
   
   
}

function services(type)
{
	$('#loader').show();
	 $("input[name=service_type]").val(type)
     var str = $("#formID").serialize(); 
	 $.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=history",
		           data: str,
		           cache: false,
		           success: function(html){
					    // alert(html)
						 $('#PatientDiv').html(html);
						 $('#loader').hide();
						 $('.datepicker').datepicker({
						  changeMonth: true,//this option for allowing user to select month
						  changeYear: true //this option for allowing user to select from year range
						});
						
						$('.timePicker').timepicker({
		minuteStep: 1,
		template: false,
		showSeconds: true,
		showMeridian: false,
	});	
			            // $("#property_type").html(html);
						
	}});
}

function back(type){
		 $('#loader').show();
	     var str = ""; 
		 $.ajax({type: "POST",
				 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag="+type,
				 data: str,
				 cache: false,
				 success: function(html){ 
				    $('#PatientDiv').html(html);	
					$('#loader').hide();
				 }
		});
	
}

function history(type)
{
	$('#loader').show();
	 var flag =1;
	 $('#errormsg').html(''); 
	 if($("input:radio[name='service_provided']").is(":checked")==false) 
	 {
		  flag ='please select service provided'; 
		 
	 }else {
		  
		  var service_provided = $('input[name=service_provided]:checked', '#formID').val();
		  if($("input:radio[name='referral_service_to']").is(":checked")==false &&  service_provided =='referral service to')
	      {
		     flag ='please select referral service to'; 
	      } 
	 }
	 
	 if(flag==1)
	 {	 
	     var str = $("#formID").serialize(); 
		 $.ajax({type: "POST",
				 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=service_provided",
				 data: str,
				 cache: false,
				 success: function(html){ 
				    $('#PatientDiv').html(html);	
					$('#loader').hide();
				 }
		});
	 }else {  $('#errormsg').html(flag); $('#loader').hide();}
	
}


function SubscriptionCall()
{
       var str = $("#SubscriptionForm").serialize();
	   $.ajax({type: "POST",
				 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=SubscriptionCall",
				 data: str,
				 cache: false,
				 success: function(html){ 
				   // alert(html)
				    $('#SubscriptionMsg').html('Subscription call detail added sucessfully.');	
				 }
		});	
}

function serach_patient()
{ 
	 $('#loader').show();	
      var search_mobile =  $("input[name=search_mobile]").val();
      var serach_patientid =  $("input[name=serach_patientid]").val();
	  
	  if(search_mobile=='' && serach_patientid=='')
	  {
		    $('#searchbox_div').html('<div style="color:#E60000; text-align:center">Please enter Mobile No./Patient ID</div> ');	
			 $('#loader').hide();
	  
	  }else {
	  
		   var str ='search_mobile='+search_mobile+'&serach_patientid='+serach_patientid;
		  $.ajax({type: "POST",
					 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=SerachPatient",
					 data: str,
					 cache: false,
					 success: function(html){ 
					    //alert(html)
					   $('#searchbox_div').html(html);	
					    $('#loader').hide();
					 }
			});	
		
	  }

}


function chanagepreferences(preferences, data)
{
	
	var str ="preferences="+preferences;
     $.ajax({type: "POST",
					 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=Preferences",
					 data: str,
					 cache: false,
					 success: function(html){ 
					     $('#PatientDiv').html(html);	
					 }
			});		
}


function patient_detail(patient_id)
{
	
  	  	$.ajax({type: "POST",
					 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=patient_detail",
					 dataType: "json",
					 data: { "patient_id": patient_id},
					 cache: false,
					 success: function(data){
						  
						   console.log(data)
					       $("input[name=patient_phone]").val(data.phone);
						   $("input[name=patient_name]").val(data.name);
						   $("input[name=patient_age]").val(data.age);
						   $("input[name=patient_address]").val(data.address);
						   $("input[name=patient_postal_code]").val(data.postal_code);
						   $('#patient_sex option').removeAttr('selected').filter('[value='+data.sex+']').attr('selected', true);
						   $('#patient_division option').removeAttr('selected').filter('[value='+data.division+']').attr('selected', true)
						   $("#patient_township option:selected" ).text(data.township_text);
		                   $("#patient_township option:selected" ).val(data.township);
					  	
					 }
			});	
}

	
function searchdata()
{
	$('#loader').show();
	var services_search =$("#services_search option:selected").val();
	var home_services_search =$("#home_services_search option:selected").val();
	var hospital_services_search =$("#hospital_services_search option:selected").val();
	var clinic_services_search =$("#clinic_services_search option:selected").val();
	var speciality_search =$("#speciality_search option:selected").val();
	var doctor_search =$("#doctor_search option:selected").val();
	var search_division =$("#search_division option:selected").val();
	var search_township =$("#search_township option:selected").val();
	
	var str ='services_search='+services_search+'&home_services_search='+home_services_search+'&hospital_services_search='+hospital_services_search+'&clinic_services_search='+clinic_services_search+'&speciality_search='+speciality_search+'&doctor_search='+doctor_search+'&search_division='+search_division+'&search_township='+search_township;
	
	 $.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=searchdata",
		           data: str,
		           cache: false,
		           success: function(html){
					       //alert(html)
						$('#PatientDiv').html(html);
						$('#loader').hide();
			           
						
	}});	
	
	
	
}




	window.onbeforeunload = function(event) {	
	if(flag2==1)	{
		event.returnValue = "If you refresh the page, you will loose all data of current call!";
	}
}


 
 
 
 $(document).ready(function(){
 
 
$('#sub_center_check').click(function(){
	
	         if($(this).prop("checked") == true)
			{
				$("#sub_centers_div").show();
               // alert("Checkbox is checked.");
            }else if($(this).prop("checked") == false){
				 $("#sub_centers_div").hide();
                //alert("Checkbox is unchecked.");
            }
});


});

</script>
 
<!--right section panel-->
<div class="vd_content-section clearfix" >
		  	<div class="row">
              <div class="col-md-12">
              		  <div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-pencil"></i> </span> Patient 	/ Caller Information </h3>
                  	</div>
              
                      <div id="PatientDiv">		
                        <div class="section-body">
                            <div class="searchbox">
                            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                         
                            <div class="col-md-5"><input name="search_mobile" id="search_mobile" type="text" class="inputsearch" placeholder="Mobile No." ></div>
                            <div class="col-md-5"><input name="serach_patientid" id="serach_patientid" type="text" class="inputsearch" placeholder="Patient ID"></div>
                           
                            <div class="col-md-2"><button type="button" class="greenbutton inputsearch" onclick="serach_patient()">Search</button></div>
                          
                            </div>
                            
                            <div style="clear:both"></div>
                            
                             <div id="searchbox_div"></div>
                            </div>
                            
                <!--Patient/caller Information-->
                            <div class="information">
                            
                            <!--Patient Information-->
                                <div class="col-md-6">
                                <div class="panel-heading vd_bg-green white">
                                <h3 class="panel-title align-center"> <span class="menu-icon"> </span>Patient Information </h3>
                                </div>
                                    <div class="form-box">
                                    <div class="form-group">
                                        <span class="checkbox_callinfo"><input type="checkbox"  name="call_type" value="follow up call" ><label class="check_box">Is this a follow up call?</label></span>
                                        <span class="checkbox_callinfo"><input type="checkbox" name="subscription_call"  value="Yes"  data-toggle="modal" data-target="#Subscription"><label class="check_box">Is this a subscription call? </label></span>
                                        
                                         <span class="checkbox_callinfo"><input type="checkbox" name="sub_center_check" id="sub_center_check"  value="Yes"   ><label class="check_box">Sub Centers  </label></span>
                                    </div>
                                    
                                    
                                    <div class="form-group" id="sub_centers_div" style="display:none">
                                        <label class="col-sm-4 control-label">Sub Centers <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select  name="sub_centers" id="sub_centers" class="input_box_client_select">
                                          <option value="" >---Select---</option>
                                        <?php 
										        $record=$PDO->db_query("select * from #_sub_centers where status =1  order by name");
										        while($res=mysql_fetch_array($record))
												{
										  ?>
					                 <option value="<?=$res['pid']?>" <?php if($sub_centers==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						              <?php } ?>
                                        </select>
                                        </div>
                                        </div>
                                    
                                  
                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Phone No.<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon clearfix border-none">
                                           <input type="text" name="phone_code"  value="+95" readonly="readonly" class="phonecode" >
                                           <input type="text" name="patient_phone"  value="<?=$patient_phone?>" class="input_box_client" placeholder="">
                                          <span   class="ppnotes">  Please do not start the number with a 0.</span>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Name</label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text" name="patient_name" value="<?=$patient_name?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Sex <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select  name="patient_sex" id="patient_sex" class="input_box_client_select">
                                          <option value="" >---select---</option>
                                         <option  <?=($patient_sex=='Male')?'selected="selected"':''?> >Male</option>
                                         <option <?=($patient_sex=='Female')?'selected="selected"':''?>>Female</option>
                                        </select>
                                        </div>
                                        </div>
                                        
                                        <div class="form-group">
                                        <label class="col-sm-4 control-label">Age <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text"  name="patient_age" value="<?=$patient_age?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    
                                        <div class="form-group">
                                        <label class="col-sm-4 control-label">Address<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text"  name="patient_address" value="<?=$patient_address?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    
                                       
                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Division <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select name="patient_division" id="patient_division" class="input_box_client_select cat2"  data-name="patient_township" data-folder="doctor_agents">
                                         <option value="" >---select---</option>
                                         <?php 
										        $record=$PDO->db_query("select * from #_division where status =1  order by name");
										        while($res=mysql_fetch_array($record))
												{
										  ?>
					                 <option value="<?=$res['pid']?>" <?php if($patient_division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						              <?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                        
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Township <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select name="patient_township" id="patient_township" class="input_box_client_select add_records" onchange="getpincode('patient_township','patient_postal_code')">
                                         <option value="" >---select---</option>
                                        <?php $record=$PDO->db_query("select * from #_township where status =1 and division_id='".$division."' order by name");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($patient_township==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						<?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Postal Code<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text"  name="patient_postal_code" id="patient_postal_code" value="<?=$patient_postal_code?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Alternate Phone No.</label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon border-none">
                                        <input type="text" name="phone_code"  value="+95" readonly="readonly" class="phonecode" >
                                        <input type="text" name="alternate_phone"  value="<?=$alternate_phone?>" class="input_box_client" placeholder="">
                                        <span   class="ppnotes">  Please do not start the number with a 0.</span>
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Alternate Phone No.</label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon border-none">
                                        <input type="text" name="phone_code"  value="+95" readonly="readonly" class="phonecode" >
                                        <input type="text" name="alternate_phone1"  value="<?=$alternate_phone1?>" class="input_box_client" placeholder="">
                                        <span   class="ppnotes">  Please do not start the number with a 0.</span>
                                        </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Alternate Phone No.</label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon border-none">
                                        <input type="text" name="phone_code"  value="+95" readonly="readonly" class="phonecode" >
                                        <input type="text" name="alternate_phone2"  value="<?=$alternate_phone2?>" class="input_box_client" placeholder="">
                                        <span   class="ppnotes">  Please do not start the number with a 0.</span>
                                        </div>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                </div>
    
                            <!--Caller Information-->
                                <div class="col-md-6">
                                <div class="panel-heading vd_bg-green white">
                                <h3 class="panel-title align-center"> <span class="menu-icon"> </span>Caller Information </h3>
                                </div>
                                <div class="form-box">
                                    <div class="form-group">
                                        <span class="checkbox_sameinfo"><input type="checkbox"   name="caller_information_same" id="caller_information_same"  onclick="callerInformation()" value="Yes"><label class="check_box">Click here if information is same </label></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Phone No.<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon border-none">
                                        <input type="text" name="phone_code"  value="+95" readonly="readonly" class="phonecode" >
                                        <input type="text" name="caller_phone"  value="<?=$caller_phone?>" class="input_box_client" placeholder="">
                                        <span class="ppnotes">Please do not start the number with a 0.</span>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Name</label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text" name="caller_name"  value="<?=$caller_name?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Sex <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select name="caller_sex" id="caller_sex"  class="input_box_client_select">
                                          <option value="" >---select---</option>
                                          <option  <?=($caller_sex=='Male')?'selected="selected"':''?> >Male</option>
                                          <option <?=($caller_sex=='Female')?'selected="selected"':''?>>Female</option>
                                        </select>
                                        </div>
                                        </div>
                                        
                                      <div class="form-group">
                                        <label class="col-sm-4 control-label">Age</label>
                                         <div class="col-sm-7 controls">
                                            <div class="vd_input-wrapper light-theme no-icon">
                                                <input type="text" name="caller_age" value="<?=$caller_age?>"  class="input_box_client" placeholder="">
                                           </div>
                                        </div>
                                    </div>
                                     
                                      <div class="form-group">
                                        <label class="col-sm-4 control-label">Address<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text"  name="caller_address" value="<?=$caller_address?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                    
                                       
                                    
                                     
                                     <div class="form-group">
                                        <label class="col-sm-4 control-label">Division <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select name="caller_division" id="caller_division" class="input_box_client_select cat2" data-name="caller_township" data-folder="doctor_agents">
                                            <option value="" >---select---</option>
                                          <?php 
										        $record=$PDO->db_query("select * from #_division where status =1 order by name ");
										        while($res=mysql_fetch_array($record))
												{
										  ?>
					                 <option value="<?=$res['pid']?>" <?php if($caller_division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						              <?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                        
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Township <span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <select name="caller_township" id="caller_township" class="input_box_client_select add_records" onchange="getpincode('caller_township','caller_postal_code')">
                                        <option value="" >---select---</option>
                                       <?php $record=$PDO->db_query("select * from #_township where status =1 and division_id='".$division."' order by name");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						<?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                    
                                     <div class="form-group">
                                        <label class="col-sm-4 control-label">Postal Code<span class="star">*</span></label>
                                        <div class="col-sm-7 controls">
                                        <div class="vd_input-wrapper light-theme no-icon">
                                        <input type="text"  name="caller_postal_code" id="caller_postal_code" value="<?=$caller_postal_code?>" class="input_box_client" placeholder="">
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                        
                            </div>
                             <div id="errormsg" style="color:#F00; text-align:center"></div>
                <!-- close Patient/caller Information-->
                        </div>
                        <!--next button-->
                        <div class="col-sm-12"> 
							
                            <a  class="btn nextbutton  next pull-right greenbutton" href="javascript:void()" onclick="InformationForm()">Next <span class="menu-icon"><i class="fa fa-fw fa-chevron-circle-right"></i></span></a> 
							 
                        </div>
                        <!--Close next button-->
                     </div>
              </div>
            </div>
		</div>
 
   





