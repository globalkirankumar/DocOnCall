 <?php
$left_services = (array)json_decode($lab_data['left_services'], true);
$imagin_services = (array)json_decode($lab_data['imagin_services'], true);
?>
<style>
#Hlab2 {
    border: 1px solid #E9CECE !important;
    padding: 10px 10px;
}
.edit-table-php span{
	font-size: 12px !important;
	font-weight: 400 !important;
	padding: 6px 4px !important;
}
.Laboratory_box h5{
	font-weight: bold;
	color: #1FAE66;
}
.edit-table-php label{
	margin-bottom: 5px !important;
}

</style>
<div id="<?=$labdiv?>"  style="margin-top:20px; display:none">
                            <div class="custom-heading">Available Lab Test List</div>
                          
                             <div class="Laboratory_box">
                                   <h5>MOLECULAR DIAGNOSIS</h5>
                                      <table class="lab table-bordered edit-table-php" width="100%">
                                    <thead>
                                        <tr>
                                            <?php $tr=0; if($left_services['hbv_dna']!=''){ $tr++; ?>
                                            <th   width="33%"><span><input type="hidden" name="lab_test[]" value="hbv_dna" />&nbsp;HBV DNA (VIAL LOAD TEST) : <?=$left_services['hbv_dna']?><?=CURN?></span> </th>
                                            <?php }   if($left_services['hcv_rna']!=''){ $tr++; ?>
                                            <th  width="33%"><span><input type="hidden" name="lab_test[]" value="hcv_rna" />&nbsp;HCV RNA (VIAL LOAD TEST) : <?=$left_services['hcv_rna']?><?=CURN?></span></th>
                                            <?php }   if($left_services['hiv_rna']!=''){ $tr++; ?>
                                            <th  width="33%"><span><input type="hidden" name="lab_test[]" value="hiv_rna" />&nbsp;HIV RNA (VIAL LOAD TEST) : <?=$left_services['hiv_rna']?><?=CURN?></span> </th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                </table>
    
                            </div>
                            
                            <div class="Laboratory_box">
                                <h5>BIOCHEMISTRY</h5>
                                
                                    <table  width="100%"class="lab table-bordered edit-table-php">
                                    <thead>
                                        <tr>
                                           <?php $tr=0; if($left_services['lipid_profile']!=''){ $tr++; ?>
                                            
                                               <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="lipid_profile" />&nbsp;LIPID PROFILE ( fasting )(3cc/Plain Tube) : <?=$left_services['lipid_profile']?><?=CURN?></span> </label></th>
                                           
                                             <?php } if($left_services['cholesterol']!=''){  if($tr%3==0){ echo '</tr><tr>';}   $tr++; ?>
                                           
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="cholesterol" />&nbsp;Cholesterol(Total) : <?=$left_services['cholesterol']?><?=CURN?></span></label></th>
                                            
                                             <?php } if($left_services['triglyceride']!=''){ 	 $tr++; if($tr%3==0){ echo '</tr><tr>';}  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="triglyceride" />&nbsp;Triglyceride : <?=$left_services['triglyceride']?><?=CURN?></span> </label></th>
                                            
                                           <?php } if($left_services['hdl']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                           
                                            <th><label><span><input type="hidden" name="lab_test[]" value="hdl" />&nbsp;HDL : <?=$left_services['hdl']?><?=CURN?></span> </label></th>  <?php } if($left_services['ldl']!=''){  if($tr%3==0){ echo '</tr><tr>';}  $tr++;   ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="ldl" />&nbsp;LDL/Direct LDL : <?=$left_services['ldl']?><?=CURN?></span></label></th>
                                              <?php }?>
                                           
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            
                            <div class="Laboratory_box">
                                <h5>U & E(3cc/Plain)</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                          <?php $tr=0; if($left_services['urea']!=''){ $tr++; ?>
                                               <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="urea" />&nbsp;Urea : <?=$left_services['urea']?><?=CURN?></span> </label></th>
                                               <?php } if($left_services['sodium']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="sodium" />&nbsp;Sodium : <?=$left_services['sodium']?><?=CURN?></span></label></th>
                                               <?php } if($left_services['potassium']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="potassium" />&nbsp;Potassium : <?=$left_services['potassium']?><?=CURN?></span> </label></th>
                                         <?php } if($left_services['bicarbonate']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                              <th><label><span><input type="hidden" name="lab_test[]" value="bicarbonate" />&nbsp;Bicarbonate(ECO2) : <?=$left_services['bicarbonate']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['creatinine']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="creatinine" />&nbsp;Creatinine : <?=$left_services['creatinine']?><?=CURN?></span></label></th>
                                              <?php } if($left_services['uric_acid']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                             <th><label><span><input type="hidden" name="lab_test[]" value="uric_acid" />&nbsp;Uric Acid : <?=$left_services['uric_acid']?><?=CURN?></span></label></th>
                                             <?php } ?>
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            
                            <div class="Laboratory_box">
                                <h5>LIVER FUNCTION TEST(3cc/Plain)</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['alk_phosphatase']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="alk_phosphatase" />&nbsp;Alkaline Phosphatase : <?=$left_services['alk_phosphatase']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['alt_sgpt']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="alt_sgpt" />&nbsp;ALT/SGPT : <?=$left_services['alt_sgpt']?><?=CURN?></span></label></th>
                                         <?php } if($left_services['alt_sgot']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="alt_sgot" />&nbsp;AST/SGOT : <?=$left_services['alt_sgot']?><?=CURN?></span> </label></th>
                                         <?php } if($left_services['bilirubin']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="bilirubin" />&nbsp;Bilirubin(Total) : <?=$left_services['bilirubin']?><?=CURN?></span> </label></th>
                                           <?php } if($left_services['gamma-gt']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="gamma-gt" />&nbsp;Gamma-GT(GGT) : <?=$left_services['gamma-gt']?><?=CURN?></span></label></th>
                                          <?php } ?>
                                                
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            
                            <div class="Laboratory_box">
                                <h5>DIABETES TEST</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['glucose']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="glucose" />&nbsp;Glucose(Fasting ) : <?=$left_services['glucose']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['hba2c']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="hba2c" />&nbsp;HBA2C(3cc/EDTA) : <?=$left_services['hba2c']?><?=CURN?></span></label></th>
                                         <?php } if($left_services['insulin']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="insulin" />&nbsp;Insulin : <?=$left_services['insulin']?><?=CURN?></span> </label></th>
                                         <?php } if($left_services['rbs']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="rbs" />&nbsp;RBS ( random blood sugur): <?=$left_services['rbs']?><?=CURN?></span> </label></th>
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            
                            <div class="Laboratory_box">
                                <h5>T&DP(3cc/Plain)</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['total_protein']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="total_protein" />&nbsp;Total Protein : <?=$left_services['total_protein']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['albumin']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="albumin" />&nbsp;Albumin : <?=$left_services['albumin']?><?=CURN?></span></label></th>
                                             
                                         <?php } if($left_services['globulin']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="globulin" />&nbsp;Globulin: <?=$left_services['globulin']?><?=CURN?></span> </label></th>
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                              <div class="Laboratory_box">
                                <h5>OTHERS(3cc/Plain)</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['amylase']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="amylase" />&nbsp;Amylase : <?=$left_services['amylase']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['magnesium']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="magnesium" />&nbsp;Magnesium : <?=$left_services['magnesium']?><?=CURN?></span></label></th>
                                         <?php } if($left_services['calcium']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="calcium" />&nbsp;Calcium : <?=$left_services['calcium']?><?=CURN?></span> </label></th>
                                         <?php } if($left_services['corrected_ca']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="corrected_ca" />&nbsp;Corrected Ca: <?=$left_services['corrected_ca']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['phosphorous']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="phosphorous" />&nbsp;Phosphorous: <?=$left_services['phosphorous']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['ldh']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="ldh" />&nbsp;LDH: <?=$left_services['ldh']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['ck']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="ck" />&nbsp;CK: <?=$left_services['ck']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['ck_mb']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="ck_mb" />&nbsp;CK-MB: <?=$left_services['ck_mb']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['crp_latex']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="crp_latex" />&nbsp;CRP(Latex/hs): <?=$left_services['crp_latex']?><?=CURN?></span> </label></th>
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            <div class="Laboratory_box">
                                <h5>IMMUNOLOGY</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['psa']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="psa" />&nbsp;PSA(Total/Free) : <?=$left_services['psa']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['cea']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="cea" />&nbsp;CEA : <?=$left_services['cea']?><?=CURN?></span></label></th>
                                             
                                         <?php } if($left_services['afp']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="afp" />&nbsp;AFP: <?=$left_services['afp']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['ca_12_5']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="ca_12_5" />&nbsp;CA 12-5: <?=$left_services['ca_12_5']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['ca_125_3']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="ca_125_3" />&nbsp;CA 125-3: <?=$left_services['ca_125_3']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['ca_19_9']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="ca_19_9" />&nbsp;CA 19-9: <?=$left_services['ca_19_9']?><?=CURN?></span> </label></th>
                                            
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            <div class="Laboratory_box">
                                <h5>HORMONES(3cc/Plain)</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['t3_t4_tsh']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="t3_t4_tsh" />&nbsp;T3,T4,TSH : <?=$left_services['t3_t4_tsh']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['t3_t4_free']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="t3_t4_free" />&nbsp;Free T3,T4 : <?=$left_services['t3_t4_free']?><?=CURN?></span></label></th>
                                             
                                         <?php } if($left_services['anti_tpo']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="anti_tpo" />&nbsp;Anti TPO: <?=$left_services['anti_tpo']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['estradiol']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="estradiol" />&nbsp;Estradiol(E2): <?=$left_services['estradiol']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['FSH']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="FSH" />&nbsp;FSH: <?=$left_services['FSH']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['LH']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="LH" />&nbsp;LH: <?=$left_services['LH']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['progesterone']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="progesterone" />&nbsp;Progesterone: <?=$left_services['progesterone']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['prolactin']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="prolactin" />&nbsp;Prolactin: <?=$left_services['prolactin']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['vitamin_D']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="vitamin_D" />&nbsp;Vitamin D: <?=$left_services['vitamin_D']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['testosterone']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="testosterone" />&nbsp;Testosterone: <?=$left_services['testosterone']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['cortisol']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="cortisol" />&nbsp;Cortisol(Serum/24hr Urine): <?=$left_services['cortisol']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['β_HCG']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="β_HCG" />&nbsp;β HCG(quantitative): <?=$left_services['β_HCG']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['β_Cross_Lap']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="β_Cross_Lap" />&nbsp;β Cross Lap(10hr Fasting): <?=$left_services['β_Cross_Lap']?><?=CURN?></span> </label></th>
                                            
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            <div class="Laboratory_box">
                                <h5>THERAPEUTIC DRUG LEVEL</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['tacrolimus']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="tacrolimus" />&nbsp;Tacrolimus : <?=$left_services['tacrolimus']?><?=CURN?></span> </label></th>
                                       
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            <div class="Laboratory_box">
                                <h5>RHEUMATOLOGY(3cc/Plain)</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['ASO']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="ASO" />&nbsp;ASO(titre) : <?=$left_services['ASO']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['RA']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="RA" />&nbsp;RA(titre) : <?=$left_services['RA']?><?=CURN?></span></label></th>
                                             
                                         <?php } if($left_services['anti_ccp']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="anti_ccp" />&nbsp;Anti CCP(quantitative): <?=$left_services['anti_ccp']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['complement_3']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="complement_3" />&nbsp;Complement 3: <?=$left_services['complement_3']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['complement_4']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="complement_4" />&nbsp;Complement 4: <?=$left_services['complement_4']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['lupus_anticoagulant']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="lupus_anticoagulant" />&nbsp;Lupus anticoagulant: <?=$left_services['lupus_anticoagulant']?><?=CURN?></span> </label></th>
                                            
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            <div class="Laboratory_box">
                                <h5>CARDIAC MARKERS</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['lupus_anticoagulant']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="lupus_anticoagulant" />&nbsp;Troponin I (3cc/Plain): <?=$left_services['lupus_anticoagulant']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['troponin_T']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="troponin_T" />&nbsp;Troponin T (3cc/Heparin or EDTA) : <?=$left_services['troponin_T']?><?=CURN?></span></label></th>
                                             
                                         <?php } if($left_services['nt_pro']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="nt_pro" />&nbsp;NT-Pro BNP(2cc/Heparin): <?=$left_services['nt_pro']?><?=CURN?></span> </label></th>
                                            
                                          
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                          
                          
                           <div class="Laboratory_box">
                                <h5>INFECTIOUS DISEASE(3cc/Plain)</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['hbv_profile']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="hbv_profile" />&nbsp;HBV PROFILE: <?=$left_services['hbv_profile']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['HBs_Ag']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="HBs_Ag" />&nbsp;HBs Ag : <?=$left_services['HBs_Ag']?><?=CURN?></span></label></th>
                                             
                                         <?php } if($left_services['Anti_HBs']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Anti_HBs" />&nbsp;NT-Pro Anti HBs(quantitative): <?=$left_services['Anti_HBs']?><?=CURN?></span> </label></th>
                                            
                                          
                                             <?php } if($left_services['HBe_Ag']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="HBe_Ag" />&nbsp;NT-Pro HBe Ag<: <?=$left_services['HBe_Ag']?><?=CURN?></span> </label></th>
                                            
                                          
                                          
                                             <?php } if($left_services['Anti_HBc']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Anti_HBc" />&nbsp;Anti HBc(Total/IgM): <?=$left_services['Anti_HBc']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['hiv_1_2']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="hiv_1_2" />&nbsp;HIV 1 & 2 Antibody: <?=$left_services['hiv_1_2']?><?=CURN?></span> </label></th>
                                            
                                            
                                             <?php } if($left_services['vdrl_rpr']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="vdrl_rpr" />&nbsp; VDRL/RPR: <?=$left_services['vdrl_rpr']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['TPHA']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="TPHA" />&nbsp;TPHA: <?=$left_services['TPHA']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['Dengue_Ag_Antibody']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Dengue_Ag_Antibody" />&nbsp;Dengue Ag & Antibody(IgG/IgM): <?=$left_services['Dengue_Ag_Antibody']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['Widal_Test']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Widal_Test" />&nbsp;Widal Test: <?=$left_services['Widal_Test']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['MF_ICT_Flim']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="MF_ICT_Flim" />&nbsp;MF(ICT/Film): <?=$left_services['MF_ICT_Flim']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['Chikungunya']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Chikungunya" />&nbsp;Chikungunya (IgM): <?=$left_services['Chikungunya']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['H_Polari']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="H_Polari" />&nbsp;H.Polari(IgG): <?=$left_services['H_Polari']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['Leptosprial_Antibody']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Leptosprial_Antibody" />&nbsp;Leptosprial Antibody(IgG/IgM): <?=$left_services['Leptosprial_Antibody']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['Malaria']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Malaria" />&nbsp;Malaria(ICT/Film)(3cc/EDTA: <?=$left_services['Malaria']?><?=CURN?></span> </label></th>
                                            
                                            
                                            
                                          
                                          
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                             <div class="Laboratory_box">
                                <h5>24 HR URINE ANALYSIS</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['Sodium_24']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="Sodium_24" />&nbsp;Sodium: <?=$left_services['Sodium_24']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['Potassium_24']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Potassium_24" />&nbsp;Potassium : <?=$left_services['Potassium_24']?><?=CURN?></span></label></th>
                                             
                                         <?php } if($left_services['Protein_24']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Protein_24" />&nbsp;NT-Pro Protein: <?=$left_services['Protein_24']?><?=CURN?></span> </label></th>
                                            
                                          
                                            <?php } if($left_services['Cortisol_24']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Cortisol_24" />&nbsp;NT-Pro Cortisol: <?=$left_services['Cortisol_24']?><?=CURN?></span> </label></th>
                                            
                                          
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            
                            <div class="Laboratory_box">
                                <h5>HAEMATOLOGY(3cc/EDTA)</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['CP_32']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="CP_32" />&nbsp;CP(32 Parameters): <?=$left_services['CP_32']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['ESR']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="ESR" />&nbsp;ESR : <?=$left_services['ESR']?><?=CURN?></span></label></th>
                                             
                                         <?php } if($left_services['CD4']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="CD4" />&nbsp;CD4 count: <?=$left_services['CD4']?><?=CURN?></span> </label></th>
                                            
                                          
                                            <?php } if($left_services['hb']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="hb" />&nbsp;Hb %: <?=$left_services['hb']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['Reticulocyte_Count']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Reticulocyte_Count" />&nbsp; Reticulocyte Count %: <?=$left_services['Reticulocyte_Count']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['ABO_Rh']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="ABO_Rh" />&nbsp;ABO/Rh Grouping %: <?=$left_services['ABO_Rh']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['Rh_Antibody']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Rh_Antibody" />&nbsp;Rh Antibody Titre: <?=$left_services['Rh_Antibody']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['Hb_Electrophoresis']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Hb_Electrophoresis" />&nbsp;Hb Electrophoresis: <?=$left_services['Hb_Electrophoresis']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['Protein_Electrophoresis']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Protein_Electrophoresis" />&nbsp;Protein Electrophoresis: <?=$left_services['Protein_Electrophoresis']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['HbF']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="HbF" />&nbsp;HbF (Singer's Test): <?=$left_services['HbF']?><?=CURN?></span> </label></th>
                                            
                                             <?php } if($left_services['Hb_H_Inclusin']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Hb_H_Inclusin" />&nbsp;Hb H Inclusion: <?=$left_services['Hb_H_Inclusin']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['Coombs_Test']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Coombs_Test" />&nbsp;Coombs' Test: <?=$left_services['Coombs_Test']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['Cold_warm']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Cold_warm" />&nbsp;Cold/Warm Agglutination: <?=$left_services['Cold_warm']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['G6PD']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="G6PD" />&nbsp;G6PD: <?=$left_services['G6PD']?><?=CURN?></span> </label></th> <?php } if($left_services['PNH']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="PNH" />&nbsp;PNH: <?=$left_services['PNH']?><?=CURN?></span> </label></th>
                                             <?php } if($left_services['LAPA_score']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="LAPA_score" />&nbsp;HLAPA score: <?=$left_services['LAPA_score']?><?=CURN?></span> </label></th>
                                            
                                            
                                          
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                               <div class="Laboratory_box">
                                <h5>IRON STUDY(3cc/Plain)</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['Ferritin']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="Ferritin" />&nbsp;Ferritin: <?=$left_services['Ferritin']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['Serum_Iron']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Serum_Iron" />&nbsp;Serum Iron : <?=$left_services['Serum_Iron']?><?=CURN?></span></label></th>
                                             
                                         <?php } if($left_services['TIBC']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="TIBC" />&nbsp;TIBC: <?=$left_services['TIBC']?><?=CURN?></span> </label></th>
                                            
                                          
                                           
                                          
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            
                            <div class="Laboratory_box">
                                <h5>COAGULATION PROFILE</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['PT_INR']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="PT_INR" />&nbsp;PT/INR: <?=$left_services['PT_INR']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['APTT']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="APTT" />&nbsp;APTT : <?=$left_services['APTT']?><?=CURN?></span></label></th>
                                             
                                         <?php } if($left_services['Fibrinogen']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Fibrinogen" />&nbsp;Fibrinogen: <?=$left_services['Fibrinogen']?><?=CURN?></span> </label></th>
                                            
                                            
                                               <?php } if($left_services['Factor_VIII']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Factor_VIII" />&nbsp;Factor VIII: <?=$left_services['Factor_VIII']?><?=CURN?></span> </label></th>
                                            
                                            
                                               <?php } if($left_services['Factor_IX']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Factor_IX" />&nbsp;Factor IX: <?=$left_services['Factor_IX']?><?=CURN?></span> </label></th>
                                            
                                            
                                              <?php } if($left_services['D_dimer']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="D_dimer" />&nbsp;D-dimer: <?=$left_services['D_dimer']?><?=CURN?></span> </label></th>
                                            
                                              <?php } if($left_services['FDP']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="FDP" />&nbsp;FDP: <?=$left_services['FDP']?><?=CURN?></span> </label></th>
                                            
                                               <?php } if($left_services['Mixing_Factor']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Mixing_Factor" />&nbsp;Mixing Factor: <?=$left_services['Mixing_Factor']?><?=CURN?></span> </label></th>
                                            
                                               <?php } if($left_services['Von_Willebrand_Factor']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th><label><span><input type="hidden" name="lab_test[]" value="Von_Willebrand_Factor" />&nbsp;Von Willebrand Factor: <?=$left_services['Von_Willebrand_Factor']?><?=CURN?></span> </label></th>
                                            
                                          
                                           
                                          
                                           
                                          <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                                 <div class="Laboratory_box">
                                <h5>BONE MARROW STUDY</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['Trephine_Biopsy']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="Trephine_Biopsy" />&nbsp;Trephine Biopsy: <?=$left_services['Trephine_Biopsy']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['Aspiration']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Aspiration" />&nbsp;Aspiration: <?=$left_services['Aspiration']?><?=CURN?></span></label></th>
                                             
                                           <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            
                            <div class="Laboratory_box">
                                <h5>HISTOPATHOLOGY</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['Fluid_Cytology']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="Fluid_Cytology" />&nbsp;Fluid Cytology: <?=$left_services['Fluid_Cytology']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['Sputum_Cytology']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Sputum_Cytology" />&nbsp;Sputum Cytology: <?=$left_services['Sputum_Cytology']?><?=CURN?></span></label></th>
                                            
                                             <?php } if($left_services['Tissue_Biopsy']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Tissue_Biopsy" />&nbsp;Tissue Biopsy: <?=$left_services['Tissue_Biopsy']?><?=CURN?></span></label></th>
                                            
                                             <?php } if($left_services['FNAC']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="FNAC" />&nbsp;FNAC: <?=$left_services['FNAC']?><?=CURN?></span></label></th>
                                            
                                            
                                             <?php } if($left_services['Pap_Smear']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Pap_Smear" />&nbsp;Pap Smear: <?=$left_services['Pap_Smear']?><?=CURN?></span></label></th>
                                                                                                   
                                             
                                           <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            <div class="Laboratory_box">
                                <h5>MICROBIOLOGY</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['Aspirated_Fluid']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="Aspirated_Fluid" />&nbsp;Aspirated Fluid(CSF/Pleural/Ascitic): <?=$left_services['Aspirated_Fluid']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['Blood']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Blood" />&nbsp;Blood(Note Instructions): <?=$left_services['Blood']?><?=CURN?></span></label></th>
                                            
                                             <?php } if($left_services['Urine']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Urine" />&nbsp;Urine: <?=$left_services['Urine']?><?=CURN?></span></label></th>
                                            
                                             <?php } if($left_services['Sputum']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Sputum" />&nbsp;Sputum: <?=$left_services['Sputum']?><?=CURN?></span></label></th>
                                            
                                            
                                             <?php } if($left_services['Stool']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Stool" />&nbsp;Stool: <?=$left_services['Stool']?><?=CURN?></span></label></th>
                                            <?php } if($left_services['Pus']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Pus" />&nbsp;Pus: <?=$left_services['Pus']?><?=CURN?></span></label></th>
                                            <?php } if($left_services['Throat_Swab']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Throat_Swab" />&nbsp;Throat Swab: <?=$left_services['Throat_Swab']?><?=CURN?></span></label></th>
                                            <?php } if($left_services['Ear_Swab']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Ear_Swab" />&nbsp;Ear Swab: <?=$left_services['Ear_Swab']?><?=CURN?></span></label></th>
                                            <?php } if($left_services['Wound_Swab']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Wound_Swab" />&nbsp;Wound Swab: <?=$left_services['Wound_Swab']?><?=CURN?></span></label></th>
                                            <?php } if($left_services['High_Vaginal']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="High_Vaginal" />&nbsp;High Vaginal Swab(HVS): <?=$left_services['High_Vaginal']?><?=CURN?></span></label></th>
                                                                                                   
                                             
                                           <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                            
                            <div class="Laboratory_box">
                                <h5>ROUTINE EXAMINATION(RE)</h5>
                                
                                    <table  width="100%"class="lab table-bordered">
                                    <thead>
                                        <tr>
                                        <?php $tr=0; if($left_services['Aspirated_Fluid_RE']!=''){ $tr++; ?>
                                            <th    width="33%"><label><span><input type="hidden" name="lab_test[]" value="Aspirated_Fluid_RE" />&nbsp;Aspirated Fluid RE: <?=$left_services['Aspirated_Fluid_RE']?><?=CURN?></span> </label></th>
                                        <?php } if($left_services['CSF_Pleural_Ascitic']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="CSF_Pleural_Ascitic" />&nbsp;(CSF/Pleural/Ascitic): <?=$left_services['CSF_Pleural_Ascitic']?><?=CURN?></span></label></th>
                                            
                                             <?php } if($left_services['Urine_RE']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Urine_RE" />&nbsp;Urine(RE): <?=$left_services['Urine_RE']?><?=CURN?></span></label></th>
                                            
                                             <?php } if($left_services['Urine_Microalbumin']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Urine_Microalbumin" />&nbsp;Urine Microalbumin: <?=$left_services['Urine_Microalbumin']?><?=CURN?></span></label></th>
                                            
                                            
                                             <?php } if($left_services['Urine_HCG_UCG']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Urine_HCG_UCG" />&nbsp;Urine(HCG/UCG): <?=$left_services['Urine_HCG_UCG']?><?=CURN?></span></label></th>
                                            <?php } if($left_services['Bence_Jones_Protein']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Bence_Jones_Protein" />&nbsp;Bence Jones Protein(Urine): <?=$left_services['Bence_Jones_Protein']?><?=CURN?></span></label></th>
                                            <?php } if($left_services['Stool_RE']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Stool_RE" />&nbsp;Stool(RE): <?=$left_services['Stool_RE']?><?=CURN?></span></label></th>
                                            <?php } if($left_services['Stool_For_Occult_Blood']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Stool_For_Occult_Blood" />&nbsp;Stool For Occult Blood: <?=$left_services['Stool_For_Occult_Blood']?><?=CURN?></span></label></th>
                                            <?php } if($left_services['Stool_For_AFB']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Stool_For_AFB" />&nbsp;Stool For AFB: <?=$left_services['Stool_For_AFB']?><?=CURN?></span></label></th>
                                            <?php } if($left_services['Sputum_AFB']!=''){   if($tr%3==0){ echo '</tr><tr>';} $tr++;  ?>
                                            <th  width="33%"><label><span><input type="hidden" name="lab_test[]" value="Sputum_AFB" />&nbsp;Sputum AFB: <?=$left_services['Sputum_AFB']?><?=CURN?></span></label></th>
                                                                                                   
                                             
                                           <?php } ?>
                                            
                                        </tr>
                                    </thead>
                                    </table>
                                
                            </div>
                            
                        </div>
                        
<div id="<?=$imagingdiv?>"  style="margin-top:20px; display:none">
                            <div class="custom-heading">AVAILABLE IMAGIN & OTHER INVESTIGATIONS</div>
                            <div class="Laboratory_box">

    <ul>
        <?php if($imagin_services['Prime_Aquilion']!=''){  ?>	
        <li>
              <span class="Laboratory_span">Prime Aquilion(Toshiba) : <?=$imagin_services['Prime_Aquilion']?><?=CURN?> </span>
            
        </li>
        <?php } if($imagin_services['Bone_Densitometer']!=''){  ?>	
        <li> 
            <span class="Laboratory_span">Bone Densitometer(Ultrasound) : <?=$imagin_services['Bone_Densitometer']?><?=CURN?></span>
            
        </li>
          <?php } if($imagin_services['Computerized_Spirometer']!=''){  ?>	
        <li> 
            <span class="Laboratory_span">Computerized Spirometer(lung function test) : <?=$imagin_services['Computerized_Spirometer']?><?=CURN?></span>
        
        </li>
          <?php } if($imagin_services['Colposcopy']!=''){  ?>	
        <li> 
            <span class="Laboratory_span">Colposcopy  : <?=$imagin_services['Colposcopy']?><?=CURN?></span>
            <
        </li>
          <?php } if($imagin_services['24hr_ECG']!=''){  ?>	
        <li> 
            <span class="Laboratory_span">24hr ECG(Holter) : <?=$imagin_services['24hr_ECG']?><?=CURN?></span>
            
        </li>
          <?php } if($imagin_services['2D_Echocardiogram']!=''){  ?>	
        <li> 
            <span class="Laboratory_span">2D Echocardiogram : <?=$imagin_services['2D_Echocardiogram']?><?=CURN?></span>
            
        </li>
          <?php } if($imagin_services['Colour_Doppler_Imaging']!=''){  ?>	
        <li> 
            <span class="Laboratory_span">Colour Doppler Imaging(Carotid,Lower Limb,Umbilical Cord) : <?=$imagin_services['Colour_Doppler_Imaging']?><?=CURN?></span>
            
        </li>
          <?php } if($imagin_services['Stress_Test']!=''){  ?>	
        <li> 
            <span class="Laboratory_span">Stress Test(Treadmill) : <?=$imagin_services['Stress_Test']?><?=CURN?></span>
            
        </li>
          <?php } if($imagin_services['Ultrasound']!=''){  ?>	
        <li> 
            <span class="Laboratory_span">Ultrasound (Thyorid,Breast,Abdomen,pelvis,TVS, Sonohysterosalpingogram) : <?=$imagin_services['Ultrasound']?><?=CURN?></span>
            
        </li>
        <?php } ?>
    </ul>
</div>
                          </div>