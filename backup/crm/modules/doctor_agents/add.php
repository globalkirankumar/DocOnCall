<?php 
include(FS_ADMIN._MODS."/doctor_agents/class.inc.php");


$OP = new Options();

if($BSC->is_post_back())
{
   if($uid)
   {
	   $_POST['updateid']=$uid;
       $flag = $OP->update($_POST);
  
   }else {
	 
	   $flag = $OP->add($_POST);
	   
	     
   }
   
   if($flag==1)
   {
   
     $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($galleryid)?'&galleryid='.$galleryid:'')).$dlr, true);
   }
}


if($uid)
{
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row" id="form-basic">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Add/Health <?=$ADMIN->compname($comp)?> </h3>
					</div>
              		
              		<div class="panel-body">
				

              			
						
			<!--add-update form-->
                <!--    <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">-->
              
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Name <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input class="validate[required]" data-errormessage-value-missing="Name is required!" name="name" id="name"  value="<?=$name?>" type="text">
                        </div>
                      </div>
                    </div>
					<div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Email <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input class="validate[required, custom[email]]" data-errormessage-value-missing="Email is required!" name="email" id="email"  value="<?=$email?>" type="text">
                        </div>
                      </div>
                    </div>
					<?php if($pid!=''){?>
					<div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Change Password <span class="vd_red"></span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input  name="change_pwd" id="change_pwd"  value="1" type="checkbox">
                        </div>
                      </div>
                    </div>
					<?php } ?>
                    
					<div class="form-group pwd" style="<?php if($pid!=''){ echo 'display:none;';}?>">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Password <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input  name="password" id="password"  value="" type="password"  class="validate[required]" data-errormessage-value-missing="Password is required!">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">phone <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input class="validate[required]" data-errormessage-value-missing="Phone No. is required!" name="phone" id="phone"  value="<?=$phone?>" type="text">
                        </div>
                      </div>
                    </div>
					<div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">sex <span class="vd_red"></span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
						<select name="sex" data-errormessage-value-missing="sex is required!">
							<option value="Male" <?php if($sex=="Male"){echo 'selected';}?>>Male</option>
							<option value="Female" <?php if($sex=="Female"){echo 'selected';}?>>Female</option>
						</select>	
                         
                        </div>
                      </div>
                    </div>
					 <div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Date Of Birth <span class="vd_red"></span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input class="datepicker" data-errormessage-value-missing="Date Of Birth is required!" name="date_of_birth" id="date_of_birth"  value="<?=$date_of_birth?>" type="text">
                        </div>
                      </div>
                    </div>
					<div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Address <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input class="validate[required]" data-errormessage-value-missing="Address is required!" name="address" id="address"  value="<?=$address?>" type="text">
                        </div>
                      </div>
                    </div>
					
					<div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Division <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
						<select name="division"  data-name="township" data-folder="doctor_agents" class="validate[required] cat" data-errormessage-value-missing="Division is required!">
							<option value="">---Select Division---</option>
							<?php $record=$PDO->db_query("select * from #_division");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						<?php } ?>
							
						</select>	
                         
                        </div>
                      </div>
                    </div>
					
					<div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Township <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <select name="township" id="township"  class="add_records validate[required]" data-errormessage-value-missing="Township is required!"  onchange="getpincode('township','zipcode')">
							<option value="">---Select Township---</option>
							<?php if($township!='' and $township!=0){?>
							<option value="<?php echo $township;?>" selected><?php echo $PDO->getSingleresult("select name from #_township where pid='".$township."'");?></option>
							<?php }?>
						</select>	
                        </div>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="col-md-12">
                        <label class="control-label  col-sm-2">Postal Code </label>
                        <div id="first-name-input-wrapper" class="controls col-sm-8">
                          <input  name="zipcode" id="zipcode"  value="<?=$zipcode?>" type="text">
                        </div>
                      </div>
                    </div>
					
					<div class="form-group">
                    <div class="col-md-12">
                      <label class="control-label col-sm-2 col-xs-12">Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls col-sm-4 col-xs-12">
                          <select name="status" class="validate[required]" data-errormessage-value-missing="Status is required!">
                              <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?>  >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                        </div>
                    </div>
                  </div>
                
                  <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-md-6 mgbt-xs-10 mgtp-20">
                     
                      
                      <div class="mgtp-10">
					   <input type="hidden" name="pid" value="<?=$user_id?>" />
                        <button class="btn vd_bg-green vd_white greenbutton uibutton loading" type="submit" id="submit-register" name="submit-register">Submit</button>
                         <button onclick="location.reload();" class="btn  orng-btn" type="submit" id="submit-register" name="submit-register">Clear Form</button>
                      </div>
                    </div>
                    <div class="col-md-12 mgbt-xs-5"> </div>
                  </div>
             <!--   </form>-->
			<!-- close add-update form-->
              		</div>
              </div>
            </div>
		</div>
