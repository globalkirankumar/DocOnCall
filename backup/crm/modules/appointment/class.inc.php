<?php
/*
# @package BSC CMS
# Copy right code 
# The base configurations of the BSC CMS.
# manage admin user add update and delete
# Developer by jugal kishore kiroriwal

*/

class Options extends dbc
{
	
	 public function  add($data)
	 {
		    @extract($data);
			$tblName = "users_login";
	        $query=parent::db_query("select * from #_".$tblName." where email ='".$email."' "); 
	        if(mysql_num_rows($query)==0)
	        {
				$data['password']=$this->password($data['password']);
				$data['user_type'] = "administrator";
				$data[user_id] = parent::sqlquery("rs",'users_login',$data);
				$data['created_on']=@date('Y-m-d H:i:s');
				$data['create_by']=$_SESSION["AMD"][0];
				$data['shortorder']=parent::getSingleresult("select max(shortorder) as shortorder from #_".tblName." where 1=1 ")+1;
				parent::sqlquery("rs",'admin_users',$data);
		        parent::sessset('Record has been added', 's');
	            $flag =1;
		    }else {
			    parent::sessset('Record has already added', 'e');
				$flag =0;
			}
			
			return $flag; 
	 }
	 
	  public function getappointment($table=NULL,$pid=NULL,$field='pid',$order='asc')
	 {
		    if($table!=NULL)
			{
			  $sql = "select * from #_".$table." Where 1";
			  if($pid!=NULL)
			  {
			   $sql.=" and {$field}='{$pid}'";
			  }
			  
			  if($_SESSION['AMD'][2]=='clinics')
			  {
			  
				  $clinic_id = parent::getSingleResult("select pid from #_clinics where user_id='".$_SESSION['AMD'][0]."' ");
				  $dtr_query=parent::db_query("select * from #_doctors where clinic_id ='".$clinic_id."'  "); 
				  $drr_arr =array();
				  while($dtr_data =parent::db_fetch_array($dtr_query))
				  {
					  $drr_arr[] = $dtr_data['user_id']; 
				  }
				  
				  if(!empty( $drr_arr))
				  {
					  $sql.=" and doctor_id in (".implode(',',$drr_arr).")";  
				  }else{
					  $sql.=" and doctor_id =0 ";   
				  }
				  
				 
			  }
			  
			  if($_SESSION['AMD'][2]=='hospital')
			  {
			  
				  $hospital_id = parent::getSingleResult("select pid from #_hospitals where user_id='".$_SESSION['AMD'][0]."' ");
				  $dtr_query=parent::db_query("select * from #_doctors where hospital_id ='".$hospital_id."'  "); 
				  $drr_arr =array();
				  while($dtr_data =parent::db_fetch_array($dtr_query))
				  {
					  $drr_arr[] = $dtr_data['user_id']; 
				  }
				  
				  if(!empty( $drr_arr))
				  {
					  $sql.=" and doctor_id in (".implode(',',$drr_arr).")";  
				  }else{
					  $sql.=" and doctor_id =0 ";   
				  }
				  
				 
			  }
			  
			  if($_SESSION['AMD'][2]=='healthcare_organization')
			  {
				    $sql.=" and doctor_id =0 ";  
			  }
			  
			  if($_SESSION['AMD'][2]=='labs')
			  {
				    $sql.=" and doctor_id =0 ";  
			  }
			  
			  if($_SESSION['AMD'][2]=='doctors')
			  {
				    $sql.=" and doctor_id ='".$_SESSION['AMD'][0]."' ";  
			  }
			  
			  //print_r($_SESSION);
					
			  $sql.=" order by pid ".$order." ";
			  $result = parent::db_query($sql); 
			  return $result; 
		   }
		 
	 }
	 
	 
	 public function  update($data)
	 {
		   @extract($data);
		   $tblName = "users_login";
		   // pid is a user id  And Update id is  pid of module table
	       $query=parent::db_query("select * from #_".tblName." where pid='".$updateid."' "); 
		   
		   $querydata =mysql_fetch_array($query);
	       if($querydata['email']==$email)
	       {
			   if($data['password']!='' and $data['change_pwd']!='')
			   {
				   $data['password']=$this->password($data['password']);
                   parent::sqlquery("rs",'users_login',array('password'=>$data['password']),'pid',$pid);				   
			   }
				 parent::sqlquery("rs",'users_login',array('user_type'=>$data['user_type']),'pid',$pid); 
		        
				 $data['modified_on']=@date('Y-m-d H:i:s');
			     parent::sqlquery("rs",'admin_users',$data,'pid',$updateid);
		         parent::sessset('Record has been updated', 's');
	             $flag =1;
		    }else {
				$query2 = db_query("select * from #_".$tblName." where email='".$email."' ");
				if(mysql_num_rows($query)==0)
				{
					parent::sqlquery("rs",'users_login',array('email'=>$data['email'],'user_type'=>$data['user_type']),'pid',$pid);
					$flag =1;
				}else{					
			    parent::sessset('Record has already added', 'e');
				$flag =0;
				}
			}
			
			return $flag;  
		 
	 }
	 
	 public function  delete($updateid)
	 {
		   if(is_array($updateid))
		   {
			   $updateid=implode(',',$updateid);
		   }
		   $tblName = "users_login";
		    $id = parent::getSingleresult("select user_id  from #_".tblName." where pid in ($updateid)");
		    parent::db_query("delete from #_".tblName." where pid in ($updateid)");
		    parent::db_query("delete from #_".$tblName." where pid in ($id)");
		   
		   
	 }
	 
	 public function status($updateid,$status)
	 {
		   if(is_array($updateid))
		   {
			   $updateid=implode(',',$updateid);
		   }
		   
		   parent::db_query("update  #_".tblName." set status='".$status."' where pid in ($updateid)");
		   
	 }
	 
	  
	 
	 public function  display($start,$pagesize,$fld,$otype,$search_data)
	 {
		$start = intval($start); 
	   	$columns = "select * "; 
		$wh="";
		if(trim($search_data)!='')
		{
		   $wh=" and (username like '%".parent::parse_input($search_data)."%' or email like '%".parent::parse_input($search_data)."%' or user_type like '%".parent::parse_input($search_data)."%') ";	
		}
		//$wh.= " ".$this->sql();
		/* start filter */
		
		$user_type = @$_SESSION['AMD'][2];
		$ID = @$_SESSION['AMD'][0];
		/*  filter by doctor */
		$table = tblName;
		
		$reccnt_flag=1;
		
		if(strtolower($user_type)=="doctor" or strtolower($user_type)=="doctors")
		{
			 $wh.=" and doctor_id='{$ID}'";
			 $table = tblName;
			 
		}else if(strtolower($user_type)=="labs"){
			$table = "booking";
			$wh=" and book_type like '%Lab%' ";
		}else if(strtolower($user_type)=="hospital"){
			$table = tblName;
			
			$hospital_id =  parent::getSingleresult("select pid from #_hospitals where user_id='{$ID}'");
			$doctor_id =  parent::db_query("select * from #_doctors where hospital_id='{$hospital_id}' and hospital_id>0");
			$d='';
			$i=0;
			while($rs = @mysql_fetch_array($doctor_id))
			{
				$d.=$rs['user_id'].',';
				$i++;
			}
			$d = substr($d,0,-1);
			if($d!='')
			{
		      $wh.=" and doctor_id in ({$d}) ";	
			}else { $reccnt_flag=0; }
		}
		else if(strtolower($user_type)=="clinics")
		{
			$table = tblName;
			//echo $ID;
			$clinic_id =  parent::getSingleresult("select pid from #_clinics where user_id='{$ID}'");
			$doctor_id =  parent::db_query("select * from #_doctors where clinic_id='{$clinic_id}' and clinic_id>0");
			$d='';
			$i=0;
			while($rs = @mysql_fetch_array($doctor_id))
			{
				$d.=$rs['user_id'].',';
				$i++;
			}
			$d = substr($d,0,-1);
			
			if($d!='')
			{
			    $wh.=" and doctor_id in ({$d}) ";	
			}else { $reccnt_flag=0; }
			
		}else if(strtolower($user_type)=="healthcare_organization"){
			$table = "booking";
			$wh=" and book_type like '%Healthcare%' ";
		}else{
			$table = tblName;
		}
		
		
		if($_POST['search_doctor_agents']!='')
		{
		  $wh .=" and created_by ='".$_POST['search_doctor_agents']."'   ";	
		}
		
		
		if($_POST['search_date']!='')
		{
		  $wh .=" and created_on like '%".date('Y-m-d', strtotime($_POST['search_date']))."%' ";	
		}
		
		if($_POST['search_patients_id']!='')
		{
		     $patients_id=parent::getSingleresult("select pid from #_patients where patient_id = '".$_POST['search_patients_id']."' ");
			 $wh .=" and patient_id = '".$patients_id."' ";
		}
		
		if($_POST['search_phone_number']!='')
		{
			 $caller_patients= parent::db_query("select * from #_patients where phone = '".$_POST['search_phone_number']."'  "); 
             
			 $whp ='';
			 while($caller_data =  parent::db_fetch_array($caller_patients))
			 {
				  $whp .= $caller_data['pid'].',';
			 }
			 if($whp!='')
			 {
		         $wh .=" and patient_id in (".trim($whp,',').") ";
			 }
		}
		
		if($_POST['search_patients_name']!='')
		{
			 
			
			
			 $caller_patients= parent::db_query("select * from #_patients where name like '%".$_POST['search_patients_name']."%'  "); 
             
			 $whp ='';
			 while($caller_data =  parent::db_fetch_array($caller_patients))
			 {
				  $whp .= $caller_data['pid'].',';
			 }
			 if($whp!='')
			 {
		       $wh .=" and patient_id in (".trim($whp,',').") ";
			 }
		}
	
		/* end filter */
		$sql = " from #_".$table." where 1 ".$zone.$mtype.$extra.$extra1.$extra2.$wh;
		
		$order_by == '' ? $order_by = (($ord)?'orders':(($fld)?$fld:'shortorder')) : true;
        $order_by2 == '' ? $order_by2 = (($otype)?$otype:'DESC') : true;
		$sql_count = "select count(*) ".$sql; 
		$sql .= "order by $order_by $order_by2 ";
		$sql .= "limit $start, $pagesize ";
		$sql = $columns.$sql;
	
		$result = parent::db_query($sql);
		if($reccnt_flag==1)
		{
		   $reccnt = parent::db_scalar($sql_count);
		}else { $reccnt =0; }
		return array($result,$reccnt);
	 }
	public function sql()
	{
		$user_type = @$_SESSION['AMD'][2];
		$ID = @$_SESSION['AMD'][0];
		if(strtolower($user_type)=="doctor" or strtolower($user_type)=="doctors")
		{
			 $sql = "and doctor_id='{$ID}'";
			 return $sql;
		}else if(strtolower($user_type)=="labs"){
			$sql = "and doctor_id='{$ID}'";
			return $sql;
		}else if(strtolower($user_type)=="hospital"){
			$sql = "and doctor_id='{$ID}'";
			return $sql;
		}else if(strtolower($user_type)=="clinics"){
			$sql = "and doctor_id='{$ID}'";
			return $sql;
		}else if(strtolower($user_type)=="healthcare_organization"){
			$sql = "and doctor_id='{$ID}'";
			return $sql;
		}
	}	
	 
	 public function password($password)
	 {
	        $password=md5($password); 
		    $password=base64_encode($password); 	
		    return $password;
	 }
	  // 13 jan 2016
	 public function get($table=NULL,$pid=NULL,$field='pid',$order='asc')
	 {
		 if($table!=NULL){
			 $sql = "select * from #_".$table." Where 1";
			 if($pid!=NULL){
			 $sql.=" and {$field}='{$pid}'";
			 }
			 $sql.=" order by pid ".$order." ";
			$result = parent::db_query($sql); 
			return $result; 
		 }
		 
	 }
	 public function count_val($resource=NULL)
	 {
		 if($resource!='')
		 {
			 $tot = mysql_num_rows($resource);
			 return ($tot)?$tot:0;
		 }
	 }
	 public function ajax()
	 {
		 print_r($_FILES);
		 exit();
	 }
	 
	 public function upload()
	 {
		
		parent::sqlquery("rs",'reports',$_POST);
		parent::sessset('File has been Uploaded', 's');
	 }
}








?>