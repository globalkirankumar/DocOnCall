<?php 
include(FS_ADMIN._MODS."/appointment/class.inc.php");
$OP = new Options();
if($action)
{
  
  if($uid >0  || !empty($arr_ids))
  {
   
	switch($action)
	{
		  case "del":
						 $OP->delete($uid);
						 $ADMIN->sessset('Record has been deleted', 'e'); 
						 break;
						 
		  case "Delete":
						 $OP->delete($arr_ids);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Deleted', 'e');
						 break;
						 
						 
		  case "Active":
						 $OP->status($arr_ids,1);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Active', 's');
						 break;
						 
		  case "Inactive":
						 $OP->status($arr_ids,0);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Inactive', 's');
						 break;
					 
		  
		  default:
	}
    $BSC->redir($ADMIN->iurl($comp), true);
  }
}


if($_SESSION[AMD][2]=='clinics')
{
	$wh =" and book_id='".$_SESSION[AMD][0]."'  and book_type='Clinics'";
	
}else if($_SESSION[AMD][2]=='healthcare_organisation'){
	
	$wh =" and book_id='".$_SESSION[AMD][0]."' and book_type='Healthcare'";

}else if($_SESSION[AMD][2]=='labs'){
	$wh =" and book_id='".$_SESSION[AMD][0]."' and book_type='Lab'";
}else if($_SESSION[AMD][2]=='hospital'){
	$wh =" and book_id='".$_SESSION[AMD][0]."' and book_type='Hospital'";
}

if($_POST['search_date']!='')
{
  $wh .=" and book_date = '".date('Y-m-d', strtotime($_POST['search_date']))."' ";	
}

if($_POST['search_patients_id']!='')
{
	 $patients_id=$PDO->getSingleresult("select pid from #_patients where patient_id = '".$_POST['search_patients_id']."' ");
	 $wh .=" and patient_id = '".$patients_id."' ";
}

if($_POST['search_phone_number']!='')
{
	 $caller_patients= $PDO->db_query("select * from #_patients where phone = '".$_POST['search_phone_number']."'  "); 
	 
	 $whp ='';
	 while($caller_data =  $PDO->db_fetch_array($caller_patients))
	 {
		  $whp .= $caller_data['pid'].',';
	 }
	 if($whp!='')
	 {
		 $wh .=" and patient_id in (".trim($whp,',').") ";
	 }
}

if($_POST['search_patients_name']!='')
{
	 
	
	
	 $caller_patients= $PDO->db_query("select * from #_patients where name like '%".$_POST['search_patients_name']."%'  "); 
	 
	 $whp ='';
	 while($caller_data =  $PDO->db_fetch_array($caller_patients))
	 {
		  $whp .= $caller_data['pid'].',';
	 }
	 if($whp!='')
	 {
	   $wh .=" and patient_id in (".trim($whp,',').") ";
	 }
}

$start = intval($start);
$pagesize = intval($pagesize)==0?(($_SESSION["totpaging"])?$_SESSION["totpaging"]:DEF_PAGE_SIZE):$pagesize;
$columns = "select * "; 
$sql = " from #_booking where 1 ".$wh;
$sql_count = "select count(*) ".$sql; 
$sql .= "order by pid desc ";
$sql .= "limit $start, $pagesize ";
$sql = $columns.$sql;

$result = $PDO->db_query($sql);
$reccnt =$PDO->db_scalar($sql_count);
?>
<!--right section panel-->
		<div class="vd_content-section clearfix">
		  	<div class="row">
			
              <div class="col-md-12">
			  <?=$ADMIN->alert()?>
               <div class="info-call-details">
					
                
                 <ul>
				  
                  
                   
                  
                   
                      <li>  <input type="text"  name="search_patients_name"  value="<?=$search_patients_name?>"  placeholder="Patient Name " /></li>
                      <li>  <input type="text"  name="search_patients_id"  value="<?=$search_patients_id?>"  placeholder="Patient ID " /></li>
                      <li>  <input type="text"  name="search_phone_number"  value="<?=$search_phone_number?>"  placeholder="Phone Number" /></li>
                      <li>  <input type="text" class="datepicker" name="search_date"  value="<?=$search_date?>"  placeholder="Date" /></li>
                     
                     <li><input type="submit" class="records-search greenbutton inputsearch" value="Search"></li>
                      
                   
                      <!--<li><input type="submit" class="records-search greenbutton inputsearch" value="Search"></li>-->
				  </ul>
              </div>
              		<div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title">Schedule List </h3>
                  	</div>
              		<div class="section-body">
              			
						
			<!--edit table-->
                    <div class="table-responsive ">
                    <table class="table data-tbl custom-style table-striped" id="sortable">
                    <thead>
                      <tr class="tbl-head">
                        <th><?=$ADMIN->check_all()?></th>
                        <th>No.</th>
						<th>Patient Name</th>
						<th>Patient ID</th>
						<th>Book Date</th>
						<th>Book Time</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
               </thead>
					<tbody>
					<!--</table>
					 <ul class="sortable-liststyle" style="list-style:none;">-->
          
          
            <?php if($reccnt)
			      { 
			
			        $nums = (($start)?$start+1:1); 
					$k = 0;
				    while ($line = $PDO->db_fetch_array($result))
				    {
					    @extract($line);
						$k++;
						$css =($k%2!=0)?'success':'';
			
			
			?>
          <!--  <li style="list-style:none;" id="recordsArray_<?=$pid?>">
			<table class="table data-tbl custom-style">
			 <thead>-->
				<tr data-item-id=1 class="item <?=$css?>">
                  <th><?=$ADMIN->check_input($pid)?></th>
                  <th><?=$nums?></th>
				  <th><?=ucwords($PDO->getSingleResult("select name from #_patients where pid='{$patient_id}'"))?></th>
                  <th><?=ucwords($PDO->getSingleResult("select patient_id from #_patients where pid='{$patient_id}'"))?></th>
                  <th><?=$book_date?></th>
                  <th><?=$book_time?></th>
                  <th><?=$ADMIN->displaystatus($status)?></th>
                  <th><a href="<?=$ADMIN->iurl('appointment','view')?>&uid=<?=$pid?>&sc=schedule" style="color:red" title="Explore"> <i class="fa fa-eye"></i>  </a>&nbsp;&nbsp;&nbsp;<a href="<?=$ADMIN->iurl('reports')?>" title="Upload Reports "> <i class="fa fa-upload"></i></a>
				  </th>
            </tr>
			<!-- </thead>
			</table>-->
            </li>
            <?php  ++$nums;} ?>
            
            
          
           <?php  }else { echo '<tr><td colspan="8"><div align="center" class="norecord">No Record Found</div></td></tr>'; }  ?>
           
         <!--  </ul>-->
		 </tbody>
		   </table>
		   <?php include("cuts/paging.inc.php");?>
						<div class="pull-right pagination" style="display:none;">
                        <ul class="pagination">
                        <li class="page-pre"><a href="#">�</a></li>
                        <li class="page-number active"><a href="#">1</a></li>
                        <li class="page-number"><a href="#">2</a></li>
                        <li class="page-number"><a href="#">3</a></li>
                        <li class="page-number"><a href="#">4</a></li>
                        <li class="page-number"><a href="#">5</a></li>
                        <li class="page-last-separator disabled"><a href="#">...</a></li>
                        <li class="page-last"><a href="#">80</a></li>
                        <li class="page-next"><a href="#">�</a></li>
                        </ul>
                        </div>	
                        </div>
			<!-- close edit table-->
              		</div>

            <!--next button-->
           
			<!--Close next button-->
              </div>
            </div>
		</div>
  <!--Close right section panel-->
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
jQuery(document).ready(function(){ 
	jQuery(function() {
		jQuery("#ordrz ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = jQuery(this).sortable("serialize") + '&tbl=<?=tblName?>&field=pid'; 
			 $.post("<?=SITE_PATH_ADM?>modules/orders.php", order, function(theResponse){ }); 															
		}								  
		});
	});
});	
</script>