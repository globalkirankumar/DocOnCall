<!-- Modal popup-->
<div id="doctorhistoryview" class="modal modal-edit fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="append-records">
				<!-- content -->
				   <div class="question_ans">
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">Chief Complaint</span>  </li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $history_data['chief_complaint'];?>" readonly></span></li>
                  </ul>
                  </div>
				  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">History of present illness </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="<?php echo $history_data['history_of_present_illness'];?>" readonly></textarea> </span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                  <span class="que_heading_medical">Past history  </span>
                  <div class="nastingul">
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">past medical  history</span></li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $history_data['past_medical_history'];?>" readonly></span></li>
                  </ul>
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">Past surgical history</span></li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $history_data['past_surgical_history'];?>" readonly></span></li>
                  </ul>
                  </div>
                  </div>
				   <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Social history  </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $history_data['social_history'];?>" readonly> </span></li>
                    </ul>
                  </div>
                  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Family history </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $history_data['family_history'];?>" readonly> 
                      </span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                      <ul>
                        <li><span class="que_heading_medical">Medication History  </span>  </li>
                        <li class="editli">
							<!-- <img src="img/que.jpg"> -->
							<span class="que_inner">history of drug allergy </span>  
						</li>
                        <li class="editli">
							<!-- <img src="img/ans.jpg"> -->
							<span class="que_inner"> 
								<span class="checkbox_callinfo">
									<input value="2" id="checkbox-4" type="checkbox" <?php if(strtolower($history_data['history_of_drug_allergy'])=="yes"){ echo 'checked';}?> disabled>
									<label class="check_box">Yes</label>
								</span>
								<span class="checkbox_callinfo">
									<input value="2" id="checkbox-4" type="checkbox" <?php if(strtolower($history_data['history_of_drug_allergy'])=="no"){ echo 'checked';}?> disabled>
									<label class="check_box">No</label>
								</span>
							</span>
						</li>
                      <br>
                      <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">if yes <input type="text" class="que_input_box" placeholder="<?php echo $history_data['drug_allergy_comment'];?>" readonly > </span>  </li>
                        <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">Regular taking medication <input type="text" class="que_input_box" placeholder="<?php echo $history_data['drug_allergy_regular_taking_medication'];?>" readonly > </span>  </li>
                       
                      </ul>
                    </div>
					  <div class="question_ans">
					  <ul> <li><span class="que_heading">Physical examination findings</span>  
                      <ul class="libox">
                        <li><span class="text">BP- </span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $history_data['bp'];?>" readonly></span> </li>
                        <li><span class="text">PR-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $history_data['pr'];?>" readonly></span> </li>
                       <li><span class="text">SaO2-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $history_data['sao'];?>" readonly></span> </li>
                       <li><span class="text">Weight-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $history_data['weight'];?>" readonly></span> </li>
                        <li><span class="text">RBS-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $history_data['rbs'];?>" readonly></span> </li>
                       
                      </ul>
                    </div>
					<div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Heart </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="heart" ><?=$history_data['heart']?></textarea> </span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Lung </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="lung" ><?=$history_data['lung']?></textarea> </span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Abdomen </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="abdomen" ><?=$history_data['abdomen']?></textarea> </span></li>
                    </ul>
                  </div>
				   <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Extremities </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="extremities" ><?=$history_data['extremities']?></textarea> </span></li>
                    </ul>
                  </div>
					 <div class="question_ans">
                      <ul>
                        <li><img src="img/que.jpg"><span class="que_inner">provisional diagnosis   </span>  </li>
                        <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box" placeholder="<?php echo $history_data['provisional_diagnosis'];?>" readonly> </span></li>
                      </ul>
                    </div>
					   <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Investigations and treatments given  </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><textarea class="que_input_box" placeholder="" name="investigations_and_treatments" ><?=$history_data['investigations_and_treatments']?></textarea> </span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Next follow up appointment on </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><input type="text" class="que_input_box datepicker" placeholder=""  name="next_follow_up_appointment"  value="<?=$doctors_history_data['next_follow_up_appointment']?>"></span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Imaging and investigation results </span>  </li>
					  <?php if($doctors_history_data['imaging_investigation1']!=''){?>
                      <li><img src="img/ans.jpg"><span class="que_inner"><a href="<?=SITE_PATH?>/modules/appointment/ajax.php?file=<?php echo $doctors_history_data['imaging_investigation1'];?>&folder=history" target="_new()" class="" data-href="<?php echo $doctors_history_data['imaging_investigation1'];?>"><?php echo $doctors_history_data['imaging_investigation1'];?></a></span></li>
					  <?php }?>
					  <?php if($doctors_history_data['imaging_investigation2']!=''){?>
                      <li><img src="img/ans.jpg"><span class="que_inner"><a href="<?=SITE_PATH?>/modules/appointment/ajax.php?file=<?php echo $doctors_history_data['imaging_investigation2'];?>&folder=history" target="_new()" class="" data-href="<?php echo $doctors_history_data['imaging_investigation2'];?>"><?php echo $doctors_history_data['imaging_investigation2'];?></a></span></li>
					  <?php }?>
					  <?php if($doctors_history_data['imaging_investigation3']!=''){?>
                      <li><img src="img/ans.jpg"><span class="que_inner"><a href="<?=SITE_PATH?>/modules/appointment/ajax.php?file=<?php echo $doctors_history_data['imaging_investigation3'];?>&folder=history" target="_new()" class="" data-href="<?php echo $doctors_history_data['imaging_investigation3'];?>"><?php echo $doctors_history_data['imaging_investigation3'];?></a></span></li>
					  <?php }?>
					  <?php if($doctors_history_data['imaging_investigation4']!=''){?>
                      <li><img src="img/ans.jpg"><span class="que_inner"><a href="<?=SITE_PATH?>/modules/appointment/ajax.php?file=<?php echo $doctors_history_data['imaging_investigation4'];?>&folder=history" target="_new()" class="" data-href="<?php echo $doctors_history_data['imaging_investigation4'];?>"><?php echo $doctors_history_data['imaging_investigation4'];?></a></span></li>
					  <?php }?>
					  <?php if($doctors_history_data['imaging_investigation5']!=''){?>
                      <li><img src="img/ans.jpg"><span class="que_inner"><a href="<?=SITE_PATH?>/modules/appointment/ajax.php?file=<?php echo $doctors_history_data['imaging_investigation5'];?>&folder=history" target="_new()" class="" data-href="<?php echo $doctors_history_data['imaging_investigation5'];?>"><?php echo $doctors_history_data['imaging_investigation5'];?></a></span></li>
					  <?php }?>
                    </ul>
                  </div>
                  
       
				<!-- end content -->
      </div>

    </div>
  </div>
</div>
<!--close modal popup-->