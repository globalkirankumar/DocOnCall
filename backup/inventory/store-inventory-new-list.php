<?php 

	include("include/config.php");
	include("include/functions.php");
	
  if($_SESSION['sess_username']==''){
		header("Location:".SITE_URL);
		}

if(isset($_SESSION['sess_username']))
{
	$table_middle_name = stripslashes(getField('name','inv_stores',$_SESSION['store_id']));
	$table_name = 'inv_'.$table_middle_name.'_inventory';
}
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>DOCTOR ON CALL</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>
<!-- Header Ends --> 

<!---------sidebar---->
<div class="content">
  <div class="container">
    <?php include("sidebar.php");?>
    
    <!-- Middle Content Start -->
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-header">
                <ul class="breadcrumb">
                  <li><a href="welcome.php">Home</a> </li>
                  <li><a href="store-inventory-new-list.php">Inventory History</a> </li>
                </ul>
              </div>
              <?php if($_SESSION['sess_msg']){ ?>
              <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;">
               <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
              <?php }?>
              <div class="vd_panel-inner-part-content">
                <div class="search_formbox listing_formbox">
                <!--    <input type="button" name="add" value="Add Brand" class="btn btn-fright" onclick="location.href='brand-addf.php'"> -->
                <div class="clearfix"></div>
                <form name="frm" method="post" action="brand-del.php" enctype="multipart/form-data">
                  <div class="panel panel-default table-responsive">
   <?php 
   
$where='';

$where.=" and store_id = '".$_SESSION['store_id']."' group by ses_id";

$start=0;

if(isset($_GET['start'])) $start=$_GET['start'];


$pagesize=10;


if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];


$order_by='id';


if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];


$order_by2='desc';


if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];


$sql=$obj->Query("select * from inv_bucket_inventory where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize",$debug=-1);

$sql2=$obj->query("select * from inv_bucket_inventory where 1=1 $where order by $order_by $order_by2",$debug=-1);

if($sql!='')
{
$reccnt=$obj->numRows($sql2);
}

if($reccnt==0){?>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <?php 
	/*	$new_product_query=$obj->Query("select * from inv_bucket_inventory where 1=1 group by ses_id");
		while($line_result=$obj->fetchNextObject($new_product_query))
    {
    ?>
                          <td><h6><?php echo $line_result->created_on ?><h6>&nbsp;&nbsp;<h6><?php echo $line_result->ses_id?></h6></td>
                        
	<?php }*/ ?>
                      </tr>
                    </table>
                    <?php } else { ?>
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr class="item">
                          <th>No.</th>
                          <th>Date</th>
                          <th>Internal ID</th>
                          <th>View</th>
                        </tr>
                      </thead>
                      <?php 
						$i=0;
						while($line_result=$obj->fetchNextObject($sql)){
							$i++;?>
                      <tbody>
                        <tr>
                          <td><?php echo $i+$start;?></td>
                          <td><?php echo date('d-m-Y (H:i:s)',strtotime($line_result->created_on));?></td>
                          <td><?php echo $line_result->ses_id;?></td>
                          <td><a href="store-inventory-new-list1.php?view=<?php echo $line_result->ses_id;?>">View</a></td>
                        </tr>
                      </tbody>
                      <?php } ?>
                    </table>
                  </div>
                  <ul class="pagination">
                    <?php include("include/paging.inc.php"); ?>
                  </ul>
                  </div>
                  <?php }?>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Footer Start -->
<?php include("footer.php");?>
<!-- Footer END -->
<body>
</html>
