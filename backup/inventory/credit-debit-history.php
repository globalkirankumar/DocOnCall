<?php 

	include("include/config.php");
	include("include/functions.php");
	
	if($_SESSION['sess_username']==''){
		header("Location:".SITE_URL);	
	}
?>

<!DOCTYPE html>

<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->

<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->

<!--[if gt IE 9]><!-->

<html>

<!--<![endif]-->

<head>
<meta charset="utf-8" />
<title>Inventory</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script>

	function checkall(objForm)



    {



	len = objForm.elements.length;



	var i=0;



	for( i=0 ; i<len ; i++){



		if (objForm.elements[i].type=='checkbox') 



		objForm.elements[i].checked=objForm.check_all.checked;



	}



   }



	function del_prompt(frmobj,comb)



		{

			if(comb=='Delete'){





				if(confirm ("Are you sure you want to delete record(s)"))



				{



					frmobj.action = "genericname-del.php";



					frmobj.what.value="Delete";



					frmobj.submit();



				}



				else{ 



				return false;



				}



			}



		else if(comb=='Deactivate'){

			

			frmobj.action = "genericname-del.php";

			

			frmobj.what.value="Deactivate";



			frmobj.submit();



		}



		else if(comb=='Activate'){



			frmobj.action = "genericname-del.php";



			frmobj.what.value="Activate";



			frmobj.submit();



		}

	}

</script>
<script type="text/javascript" language="javascript">

function validate(obj)

{

if(obj.generic.value==''){

alert("Please enter generic name");

obj.generic.focus();

return false;

}

}

</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>

<!-- Header Ends --> 

<!---------sidebar---->

<div class="content">
  <div class="container">
    <?php include("sidebar.php");?>
    
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-header">
                <ul class="breadcrumb">
                  <li><a href="welcome.php">Home</a> </li>
                  <li><a href="credit-debit-history.php">Credit- Debit for Store "<?php echo ucwords($_SESSION['store_name']); ?>"</a> </li>
                </ul>
              </div>
              <div class="vd_panel-inner-part-content">
                <div class="clearfix"></div>
              </div>
              <?php if($_SESSION['sess_msg']){ ?>
              <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
              <?php }?>
              <div class="vd_panel-inner-part-content">
                <div class="search_formbox listing_formbox">
                
                <!-- <input type="button" name="add" value="Add Service" class="btn btn-fright" onclick="location.href='service-addf.php'">-->
                
                <div class="clearfix"></div>
                <form name="frm" method="post" action="genericname-del.php" enctype="multipart/form-data">
                  <div class="panel panel-default table-responsive">
            <?php $sql3=$obj->query("select * from inv_bucket_inventory WHERE store_id='$_SESSION[store_id]' AND store_status='1' ");
					 while($line3=$obj->fetchNextObject($sql3))
					  	{
						  $qun=$line3->qty;
						  $price=$line3->price;
						  $total=$qun*$price;
						  $grandtotal += $total;
					  	}
					 /// echo "select SUM(amount) from inv_payments WHERE store_id='$_SESSION[store_id]' AND status='1'";
						$sql4=mysql_query("select SUM(amount) from inv_payments WHERE store_id='$_SESSION[store_id]' AND status='1' AND payment_type='1'");
						$line4=mysql_fetch_array($sql4);
						//print_r($line4);
				?>
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr class="item">
                          <th>Type</th>
                          <th>Total Due</th>
                          <th>Total Received</th>
                          <th>Balance</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Medicine</td>
                          <td><?php echo $grandtotal; ?></td>
                          <td><?php echo $line4['SUM(amount)'];?></td>
                          <td><?php echo abs($grandtotal - $line4['SUM(amount)'])?></td>
                        </tr>
      <?php  
			$sql_subscription_due=mysql_query("SELECT SUM(amount_taken) FROM inv_sale_subscription WHERE store_id='".$_SESSION['store_id']."' AND status=1");
	  		$row1=mysql_fetch_array($sql_subscription_due);?>
       <?php  
	$sql_subscription_paid=mysql_query("SELECT SUM(amount) FROM  inv_payments WHERE store_id='".$_SESSION['store_id']."' AND status=1 AND payment_type='2'");
	   $row3=mysql_fetch_array($sql_subscription_paid);?>
                        <tr>
                          <td>Subscription Plan</td>
                          <td><?php echo $row1['SUM(amount_taken)'] ;?></td>
                          <td><?php echo $row3['SUM(amount)'];?></td>
                          <td><?php echo $row1['SUM(amount_taken)'] - $row3['SUM(amount)'];?></td>
                        </tr>
                        <?php  $sql_service_due=mysql_query("SELECT SUM(amount_taken) FROM inv_sale_service WHERE store_id='".$_SESSION['store_id']."' AND status=1");
	   $row2=mysql_fetch_array($sql_service_due);?>
                        <?php  $sql_service_paid=mysql_query("SELECT SUM(amount) FROM  inv_payments WHERE store_id='".$_SESSION['store_id']."' AND status=1 AND payment_type='3'");
	   $row4=mysql_fetch_array($sql_service_paid);?>
                        <tr>
                          <td>Services Plan</td>
                          <td><?php echo $row2['SUM(amount_taken)'] ; ?></td>
                          <td><?php echo $row4['SUM(amount)'];?></td>
                          <td><?php echo $row2['SUM(amount_taken)'] - $row4['SUM(amount)'];?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="custom-edit">
                    <input type="hidden" name="what" value="what" />
                    
                    <!--   <input type="submit" name="Submit" value="Activate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />

                      <input type="submit" name="Submit" value="Deactivate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />

                      <input type="submit" name="Submit" value="Delete" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />--> 
                    
                  </div>
                  <ul class="pagination">
                    <?php include("include/paging.inc.php"); ?>
                  </ul>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Footer Start -->

<?php include("footer.php");?>

<!-- Footer END -->

<body>
</html>
