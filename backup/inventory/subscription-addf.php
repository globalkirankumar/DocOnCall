<?php 
	
	include("include/config.php");
	include("include/functions.php");
	
	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	
	}
		
	
	if($_REQUEST['submitFrom']=='Yes'){
	
		$name = $_REQUEST['name'];
		$validity_duration = $_REQUEST['validity_duration'];
		$status = $_REQUEST['status'];
		$subscription_price = $_REQUEST['subscription_price'];
		$description = nl2br($_REQUEST['description']);
		$validity_type = $_REQUEST['validity_type'];
		
		if($_REQUEST['id']==''){
			
			$CheckArr = $obj->query("select * from inv_subscription_plan where name='".$name."'",$debug=-1);
			
			$row = $obj->numRows($uArr);
			
			if($row==0){
			
			$obj->query("insert into inv_subscription_plan SET name='".$name."',validity_duration='".$validity_duration."',subscription_price='".$subscription_price."',description='".$description."',validity_type='".$validity_type."' ,status='".$status."',created_date=now()");
		
			$_SESSION['sess_msg']="Subscription plan added successfully";
			
			}else{
			
			$_SESSION['sess_msg']="Subscription plan already exits";
			
			 header("Location:subscription-addf.php");
			 
			 exit();
				
			}
			
		  }else{
		 
		   $sql=" update inv_subscription_plan set name='".$name."',validity_duration='".$validity_duration."',description='".$description."',subscription_price='".$subscription_price."',validity_type='".$validity_type."',status='".$status."',modified_date=now()";
		   
		 

	       $sql.=" where id='".$_REQUEST['id']."'";
		   
		   

	       $obj->query($sql);

	       $_SESSION['sess_msg']='Subscription plan  updated successfully';   

        }

      header("Location:subscription-list.php");

     exit();
}
	
if($_REQUEST['id']!=''){

$sql=$obj->query("select * from  inv_subscription_plan where id=".$_REQUEST['id']);

$result=$obj->fetchNextObject($sql);

}
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>DOCTOR ON CALL</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script type="text/javascript" language="javascript">
function validate(obj)
{
if(obj.name.value==''){
alert("Please enter subscription name");
obj.name.focus();
return false;
}
if(obj.status.value==''){
alert("Please Select Status");
obj.status.focus();
return false;
}

}
</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms "  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>
<!-- Header Ends --> 

<!---------sidebar---->
<div class="content">
  <div class="container">
    <?php include("sidebar.php");?>
    <!-- Middle Content Start -->
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-header">
                <ul class="breadcrumb">
                  <li><a href="welcome.php">Home</a> </li>
                  <li><a href="genericname-addf.php">
                    <?php if($_REQUEST['id']==''){?>
                    Add Subscription Plan
                    <?php } else { echo "Update Subscription Plan"; }?>
                    </a> </li>
                </ul>
              </div>
              <?php if($_SESSION['sess_msg']){ ?>
              <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
              <?php }?>
              <div class="vd_panel-inner-part-content"> <span  style="float:right; padding-right:10px;">
                <input type="button" name="add" value="View Subscription"  class="btn btn-fright" onclick="location.href='subscription-list.php'" />
                </span>
                <form name="frm" class="search_formbox more_search_formbox" action="" method="POST" onsubmit="return validate(this)">
                  <input type="hidden" name="submitFrom" value="Yes">
                  <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                  <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
                    <div class="form-group form-group2">
                      <div class="controls">
                        <input type="text" name="name" Placeholder="Subscription Name" id="name" value="<?php echo stripslashes($result->name);?>" required>
                      </div>
                    </div>
                    <div class="form-group form-group2">
                      <div class="controls">
                        <select name="validity_duration" required>
                          <option value="">Select Validity</option>
                          <?php for($i=1; $i<=10; $i++)
							{?>
                          <option value="<?php echo $i; ?>" <?php if($i==$result->validity_duration){ ?>selected<?php } ?>><?php echo $i; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group form-group2">
                      <div class="controls">
                        <select name="validity_type" required>
                          <option value="">Select Duration</option>
                          <option value="month" <?php if($result->validity_type == 'month'){ ?>selected<?php } ?>>Month</option>
                          <option value="year" <?php if($result->validity_type == 'year'){ ?>selected<?php } ?>>Year</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group form-group2">
                      <div class="controls">
                        <textarea name="description" required  placeholder="Description"><?php echo $result->description;?></textarea>
                      </div>
                    </div>
                    <div class="form-group form-group2">
                      <div class="controls">
                        <input type="number" required min="1" name="subscription_price" Placeholder="Price" id="subscription_price" value="<?php echo stripslashes($result->subscription_price);?>" required>
                      </div>
                    </div>
                    <div>
                      <div class="col-md-6 col-sm-6 col-xs-12 padding-none">
                        <div class="form-group form-group2">
                          <div class="controls">
                            <input type="text" name="title[]" placeholder="Title" value="" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 padding-none">
                        <div class="form-group form-group2">
                          <div class="controls">
                            <input type="text" name="att[]" placeholder="Attribute" value="" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <style>
                       .btn-add-remove{
						width: 30px;
						height: 30px;
						display: inline-block;
						background: #8cb185;
						color: #fff;
						line-height: 30px;
						font-weight: 300 !important;   
						}
						.btn-add-remove:hover, .btn-add-remove:focus, .btn-add-remove:active{
						color:#fff;
						background:#62885b;
						
						}
						.padding-none{
						padding:0px;
						}
                       </style>
                    <div id="resultField"></div>
                    <a class="btn-add-remove" href="javascript:void(0)" id="add"><i class="fa fa-plus"></i></a>&nbsp; <a class="btn-add-remove" href="javascript:void(0)" id="remove"><i class="fa fa-minus"></i></a>
                    <div class="col-lg-6 col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group form-group2">
                        <div class="controls">
                          <select name="status" id="status" required>
                            <option value="">Status</option>
                            <option value="1" <?php if($result->status=='1'){?>selected<?php } ?>>Active</option>
                            <option value="0" <?php if($result->status=='0'){?>selected<?php } ?>>Deactive</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="controls">
                      <input type="submit" class="btn greenbutton submit-btn-first" value="Submit">
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Footer Start -->
<?php include("footer.php");?>
<!-- Footer END --> 
<script type="text/javascript">
	$(document).ready(function(){
		var counter = 1;
		$("#add").click(function(){
		 var tblid="addedfield_"+counter;
 
			if(counter<10)
           {

		 $("#resultField").append('<div id="'+tblid+'"> <div class="col-md-6 col-sm-6 col-xs-12 padding-none"><div class="form-group form-group2"><div class="controls"><input type="text" name="title[]" placeholder="Title" value="" /></div></div></div><div class="col-md-6 col-sm-6 col-xs-12 padding-none"><div class="form-group form-group2"><div class="controls"><input type="text" name="att[]" placeholder="Attribute" value="" /></div></div></div></div>');
		 
		 counter++;
		 
		}
		 
		 })

		 $("#remove").click(function(){
		  counter=counter-1;
		  var tblid="#addedfield_"+counter;
		  $(tblid).remove();
		 })
	})
</script> 
<body>
</html>
