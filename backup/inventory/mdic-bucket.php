<?php 

	include("include/config.php");

	include("include/functions.php");

	if($_SESSION['sess_username']==''){

		header("Location:".SITE_URL);	
	}
?>

<!DOCTYPE html>

<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->

<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->

<!--[if gt IE 9]><!-->

<html>

<!--<![endif]-->

<head>
<meta charset="utf-8" />
<title>DOCTOR ON CALL</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms" data-smooth-scrolling="1">
<div class="vd_body">
  <header class="header-1" id="header">
    <?php include("header.php");?>
  </header>
  
  <!-- Header Ends --> 
  
  <!---------sidebar---->
  
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      
      <!-- Middle Content Start -->
      
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
                    <li><a href="mdic-bucket.php">Medicine Bucket</a> </li>
                  </ul>
                </div>
                
                <?php if($_SESSION['sess_msg']){ ?>
                <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> 
				<?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
                <?php }?>
               
                <div class="vd_panel-inner-part-content"> <span  style="float:right; padding-right:10px;">
                  <input type="button" name="add" value="Back"  class="btn btn-fright" onclick="location.href='medicine-listing.php'" />
                  </span>
                  <div class="search_formbox listing_formbox">
                    <div class="clearfix"></div>
                    <div class="panel panel-default table-responsive">
                      <?php 
					  
					$ses_id=session_id();
				
                $where='';

				$where.=" and is_sale_done='0' and store_id='$_SESSION[store_id]' and patient_id='$_SESSION[patient_id]' and ses_id='$ses_id'";
				
                $start=0;

                if(isset($_GET['start'])) $start=$_GET['start'];

                $pagesize=5;

                if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];

                
                $order_by='id';

                if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];


                $order_by2='desc';

                
                if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];


                $sql = $obj->query("select * from inv_sales where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize",$debug=-1);
                

                $sql2 = $obj->query("select * from  inv_sales where 1=1 $where order by $order_by $order_by2",$debug=-1);

                
                $reccnt=$obj->numRows($sql2);

                if($reccnt==0){?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="center" valign="middle"><font face="Arial, Helvetica, sans-serif" color="#FF0000" size="+1">No Record</font></td>
                        </tr>
                      </table>
                      <?php } else { ?>
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr class="item">
                            <th>No.</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Generic</th>
                            <th>Form Medicine</th>
                            <th>Strength</th>
                            <th>Usage</th>
                            <th>Expire Date</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <?php 
			 			$i=1;
						while($line=$obj->fetchNextObject($sql)){?>
                        <tbody>
                          <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo getField('name','inv_category',$line->category_id);?></td>
                            <td><?php echo getField('name','inv_brand',$line->brand_id);?></td>
                            <td><?php echo getField('generic_name','inv_generic_name',$line->generic_id);?></td>
                            <td><?php echo getField('form_of_medicine_name','inv_form_of_medicine',$line->formof_id);?></td>
                            <td><?php echo getField('strength_of_medicine_name','inv_strength_of_medicine',$line->strength_id);?></td>
                            <td><?php echo getField('usage_category_name','inv_usage_category',$line->usage_id);?></td>
                            <td><?php echo date('m/d/Y', strtotime($line->expire_date));?></td>
                             <td><?php echo $line->qty;?></td>
                            <td><?php echo $line->sale_price;?></td>
                            <?php if($line->is_sale_done=='1'){?>
                            <td>Sale</td>
                            <?php }else{ ?>
                            <td>Not Sale</td>
							 <?php } ?>
							<td><a href="medicine-del.php?id=<?php echo $line->id;?>" style="text-decoration:none;">Del</a></td>
                          </tr>
                          <?php
						  $ses_id=session_id();
						  $qtyArr = $obj->query("select sale_price,qty from inv_sales where store_id='$_SESSION[store_id]' and ses_id='$ses_id' and patient_id='$_SESSION[patient_id]' and is_sale_done='0'",$debug=-1);?>
                          <tr>
                          	<td colspan="8"></td>
                           
                          	<td><b>Total</b></td>
                             <?php while($totArr = $obj->fetchNextObject($qtyArr)){?>
                            <td><b><?php echo $totArr->sale_price*$totArr->qty;?></b></td>
                            <?php } ?>
                            
                            <td colspan="2"></td>
                        </tr>
                        </tbody>
                        
                        <?php } ?>
                      </table>
                    </div>
                    
                    <div class="custom-edit">                     
                      <input type="button" value="Sale"  class="btn btn-fright" onclick="location.href='medicine-blank.php'" />                   
                    </div>
                    <ul class="pagination">
                      <?php include("include/paging.inc.php"); ?>
                    </ul>
                  </div>
                  <?php }?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Footer Start -->
  
  <?php include("footer.php");?>

<!-- Footer END -->

<body>
</html>
