<?php 
	include("include/config.php");
	include("include/functions.php");
	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	
	}
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>Inventory</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
  <header class="header-1" id="header">
    <?php include("header.php");?>
  </header>
  <!-- Header Ends --> 
  
  <!---------sidebar---->
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      
      <!-- Middle Content Start -->
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
                    <li><a href="store-sale-inventory-history.php">Store sale medicine inventory history</a> </li>
                  </ul>
                </div>
                <?php if($_SESSION['sess_msg']){ ?>
                <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
                <?php }?>
                <div class="vd_panel-inner-part-content">
                  <div class="search_formbox listing_formbox">
                  <div class="clearfix"></div>
                 
                    <div class="panel panel-default table-responsive">
                      <?php 

$where=" and is_sale_done=1 and store_id='".$_SESSION['store_id']."'";

$start=0;


if(isset($_GET['start'])) $start=$_GET['start'];


$pagesize=200;


if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];


$order_by='id';


if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];


$order_by2='desc';


if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];


$sql=$obj->query("select * from inv_sales where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize");


$sql2=$obj->query("select * from inv_sales where 1=1 $where order by $order_by $order_by2",$debug=-1);

$reccnt=$obj->numRows($sql2);

if($reccnt==0){?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="center" valign="middle"><font face="Arial, Helvetica, sans-serif"   color="#FF0000" size="+1">No Record</font></td>
                        </tr>
                      </table>
                      <?php } else { ?>
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr class="item">
                            <th>No.</th>
                            <th>Category ID</th>
                            <th>Brand</th>
                            <th>Generic Name</th>
                            <th>Medicine form</th>
                            <th>Expire Date</th>
                            <th>Strength</th>
                            <th>Usage</th>
                             <th>Quantity</th>
                            <th>Price</th>
                          </tr>
                        </thead>
                        <?php 
						$i=0;
						$total = 0;
						while($line_result=$obj->fetchNextObject($sql)){ $i++;?>
                        <tbody>
                          <tr>
                            <td><?php echo $i+$start;?></td>
                            <td><?php echo stripslashes(getField('name','inv_category',$line_result->category_id)); ?></td>
                            <td><?php echo stripslashes(getField('name','inv_brand',$line_result->brand_id));?></td>
                            <td><?php echo stripslashes(getField('generic_name','inv_generic_name',$line_result->generic_id)); ?></td>
                            <td><?php echo stripslashes(getField('form_of_medicine_name','inv_form_of_medicine',$line_result->formof_id)); ?></td>
                            <td><?php echo date('d/m/Y',strtotime($line_result->expire_date));?></td>
                            <td><?php echo stripslashes(getField('strength_of_medicine_name','inv_strength_of_medicine',$line_result->strength_id));?></td>
                            <td><?php echo stripslashes(getField('usage_category_name','inv_usage_category',$line_result->usage_id));?></td>
                            <td><?php echo $line_result->qty; ?></td>
                            <td><?php echo $line_result->sale_price;?></td>
                          </tr>
                          <?php $total += $line_result->qty*$line_result->sale_price;?>
                        </tbody>
                        <?php } ?>
                        <tr>
                          <td colspan="13" ><span style="padding-left:570px;"><b>Grand Total : <?php echo $total; ?> </b></span></td>
                        </tr>
                      </table>
                    </div>
                    <ul class="pagination">
                      <?php include("include/paging.inc.php"); ?>
                    </ul>
                    </div>
                    <?php }?>
               
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Footer Start -->
  <?php include("footer.php");?>
<!-- Footer END -->
<body>
</html>
