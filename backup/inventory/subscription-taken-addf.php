<?php 
	
	include("include/config.php");
	include("include/functions.php");
	
	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	
	}
	
	if($_REQUEST['submitFrom']=='Yes'){
		
		
		$amount_taken = $_REQUEST['amout_taken'];
		$status = $_REQUEST['status'];
		
			$obj->query("insert into inv_sale_subscription SET amount_taken='$amount_taken',patcient_id='$_SESSION[patient_id]',subscription_id='$_REQUEST[id]',store_id='$_SESSION[store_id]',status='".$status."',created_by='$_SESSION[user_type]',created_on=now()");
		
			$_SESSION['sess_msg']="Subscription Sold successfully";
			
      header("Location:subscription-taken.php");

     exit();
}
	
if($_REQUEST['id']!=''){

$sql=$obj->query("select * from inv_subscription_plan where id=".$_REQUEST['id']);

$result=$obj->fetchNextObject($sql);

}
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>DOCTOR ON CALL</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script type="text/javascript" language="javascript">
function validate(obj)
{
if(obj.amt_take.value==''){
alert("Please enter amount taken");
obj.amt_take.focus();
return false;
}
}
</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms "  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>
<!-- Header Ends --> 

<!---------sidebar---->
<div class="content">
  <div class="container">
    <?php include("sidebar.php");?>
    <!-- Middle Content Start -->
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-header">
                <ul class="breadcrumb">
                  <li><a href="welcome.php">Home</a> </li>
                  <li><a href="">Subscription Taken</a> </li>
                </ul>
              </div>
              <?php if($_SESSION['sess_msg']){ ?>
              <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
              <?php }?>
              <div class="vd_panel-inner-part-content"> <span  style="float:right; padding-right:10px;"> </span>
                <form name="frm" class="search_formbox more_search_formbox" action="" method="POST" onsubmit="return validate(this)">
                  <input type="hidden" name="submitFrom" value="Yes">
                  <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                  <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
                    <div class="form-group form-group2">
                      <div class="controls">
                        <input type="text" name="name" Placeholder="Subscription Name" id="name" value="<?php echo stripslashes($result->name);?>" required readonly>
                      </div>
                    </div>
                    <div class="form-group form-group2">
                      <div class="controls">
                        <select name="validity_duration" required disabled="true">
                          <option value="">-------------------------Validity----------------------------------</option>
                          <?php for($i=1; $i<=10; $i++)
							{?>
                          <option value="<?php echo $i; ?>" <?php if($i==$result->validity_duration){ ?>selected<?php } ?>><?php echo $i; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group form-group2">
                      <div class="controls">
                        <select name="validity_type" required disabled="true">
                          <option value="">--------------------------Duration--------------------------------</option>
                          <option value="month" <?php if($result->validity_type == 'month'){ ?>selected<?php } ?>>Month</option>
                          <option value="year" <?php if($result->validity_type == 'year'){ ?>selected<?php } ?>>Year</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group form-group2">
                      <div class="controls"> 
                        <!-- <input type="text" name="name" Placeholder="Subscription Name" id="name" value="<?php echo stripslashes($result->name);?>" required>-->
                        <textarea name="description" required  placeholder="Description" readonly><?php echo $result->description;?></textarea>
                      </div>
                    </div>
                    <div class="form-group form-group2">
                      <div class="controls">
                        <input type="number" required min="1" name="subscription_price" Placeholder="Price" id="subscription_price" value="<?php echo stripslashes($result->subscription_price);?>" required readonly>
                      </div>
                    </div>
                    <div class="form-group form-group2">
                      <div class="controls">
                        <input type="number" required min="1" name="amout_taken" Placeholder="Amount Taken" id="amt_take" required>
                      </div>
                    </div>
                    <div class="controls">
                      <input type="submit" class="btn greenbutton submit-btn-first" value="Submit">
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Footer Start -->
<?php include("footer.php");?>
<!-- Footer END --> 

<script>
	$("#a").change(function()
		{
		var val=$(this).val();
		if(val=='1')
		{		
		$('#c').show(); 
		}
		});
		
	$("#b").change(function()
		{
		var val=$(this).val();
		if(val=='0')
		{
		$('#c').hide();
		}
	});
</script>
<body>
</html>
