<?php 
	
	include("include/config.php");
	include("include/functions.php");
	
	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	
	}
		
	if($_REQUEST['submitFrom']=='Yes'){
		
			$name=$_REQUEST['name'];
			$username=$_REQUEST['username'];	
			$password=md5($_REQUEST['password']);
			$store_id=$_REQUEST['store_id'];
			$usertype=$_REQUEST['usertype'];
			$status = $_REQUEST['status'];
		
		if($_REQUEST['id']==''){
			
			$CheckArr = $obj->query("select * from inv_users where username='".$username."'",$debug=-1);
			
			$row = $obj->numRows($uArr);
			
			if($row==0){
			
			$obj->query("insert into inv_users SET name='$name',username='$username',password='$password',store_id='$store_id',usertype='$usertype',status='".$status."',created_date=now()");
		
			$_SESSION['sess_msg']="Staff added successfully";
			
			}else{
			
			$_SESSION['sess_msg']="Staff email already exits";
			
			header("Location: staff-addf.php");
			
			exit();
				
			}
			
		  }else{
		 
			$sql=" update inv_users set name='$name',username='$username',password='$password',store_id='$store_id',usertype='$usertype',status='".$status."',modified_date=now()";

	       $sql.=" where id='".$_REQUEST['id']."'";

	       $obj->query($sql);

	       $_SESSION['sess_msg']='Staff updated successfully';   

        }

       header("Location: staff-list.php");

     exit();
}
	
if($_REQUEST['id']!=''){

$sql=$obj->query("select * from inv_users where id=".$_REQUEST['id']);

$result=$obj->fetchNextObject($sql);

}
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>DOCTOR ON CALL</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script type="text/javascript" language="javascript">
function validate(obj)
{
	
if(obj.store.value==''){
alert("Please select store name");
obj.store.focus();
return false;
}
	
if(obj.name.value==''){
alert("Please enter full name");
obj.name.focus();
return false;
}

if(obj.email.value==''){
alert("Please enter email address");
obj.email.focus();
return false;
}
else if(!obj.email.value.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/))
{
alert("Please enter valid email.");
obj.email.focus();
return false;
}

if(obj.pass.value==''){
alert("Please enter password");
obj.pass.focus();
return false;
}

if(obj.type.value==''){
alert("Please select user type");
obj.type.focus();
return false;
}

if(obj.status.value==''){
alert("Please select status");
obj.status.focus();
return false;
}

}
</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms "  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>
<!-- Header Ends --> 

<!---------sidebar---->
<div class="content">
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      <!-- Middle Content Start -->
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
                    <li><a href="staff-addf.php"><?php if($_REQUEST['id']==''){?>Add Staff<?php } else { echo "Update Staff"; }?></a> </li>
                  </ul>
                </div>
				<?php if($_SESSION['sess_msg']){ ?>
                <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> 
				<?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
                <?php }?>
                <div class="vd_panel-inner-part-content"> <span  style="float:right; padding-right:10px;">
                  <input type="button" name="add" value="View Staff"  class="btn btn-fright" onclick="location.href='staff-list.php'" />
                  </span>
                  <form name="frm" class="search_formbox more_search_formbox" action="" method="POST" onsubmit="return validate(this)" />
                  <input type="hidden" name="submitFrom" value="Yes">
                  <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                  <div class="vd_panel-inner-part-content">
				  
				  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group form-group2">
                        <div class="controls">
                          <select name="store_id" id="store" required>
                            <option value="">Select Store</option>
                            <?php $storArr = $obj->query("select * from inv_stores where status=1");
				  				while($storResult = $obj->fetchNextobject($storArr)){?>
                            <option value="<?php echo $storResult->id;?>" <?php if($result->store_id==$storResult->id){?>selected<?php } ?>> <?php echo stripslashes($storResult->name);?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
				
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group form-group2">
                        <div class="controls">
                          <input type="text" name="name" placeholder="Full Name" id="name" value="<?php echo stripslashes($result->name);?>" required>
                        </div>
                      </div>
                    </div>
					
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group form-group2">
                        <div class="controls">
                          <input type="email" name="username" placeholder="Email" id="email" value="<?php echo stripslashes($result->username);?>" required>
                        </div>
                      </div>
                    </div>
					
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group form-group2">
                        <div class="controls">
                     <input type="password" name="password" id="pass" value="<?php echo stripslashes(md5($result->password));?>" required>
                        </div>
                      </div>
                    </div>
                    
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group form-group2">
                        <div class="controls">
                          <select name="usertype" id="type" required>
                            <option value="">Select User Type</option>
							 <option value="staff" <?php if($result->usertype=='staff'){?>selected<?php } ?>>Staff</option>
                          </select>
                        </div>
                      </div>
                    </div>
				
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group form-group2">
                        <div class="controls">
                          <select name="status" id="status" required>
                            <option value="">Status</option>
                            <option value="1" <?php if($result->status=='1'){?>selected<?php } ?>>Active</option>
                            <option value="0" <?php if($result->status=='0'){?>selected<?php } ?>>Deactive</option>
                          </select>
                        </div>
                      </div>
                    </div>
					
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="controls">
                        <input type="submit" class="btn greenbutton submit-btn-first fright" value="Submit">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer Start -->
  <?php include("footer.php");?>
<!-- Footer END --> 
<body>
</html>
