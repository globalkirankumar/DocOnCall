<footer class="footer-1"  id="footer">

    <div class="vd_bottom ">

        <div class="container">

            <div class="row">

              <div class="col-xs-12">

                <div class="copyright">

                  	Copyright &copy;2016 Doctor on call. All Rights Reserved

                </div>

              </div>

            </div>

        </div>

    </div>

  </footer>

  </div>

  

<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<script type="text/javascript" src="js/jquery.js"></script>

<script type="text/javascript" src="js/bootstrap.min.js"></script>

<script type="text/javascript" src="plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="plugins/breakpoints/breakpoints.js"></script>

<script type="text/javascript" src="plugins/tagsInput/jquery.tagsinput.min.js"></script>

<script type="text/javascript" src="js/theme.js"></script>

<script type="text/javascript" src="custom/custom.js"></script>

<script src="js/jquery-ui.js"></script>  

  <script>

  $(function() {

    $( "#datepicker" ).datepicker();

  });

  </script>
  
  <script type="text/javascript">

var MIN_LENGTH = 2;

$(document).ready(function() {

	$("#keyword").keyup(function() {

		var keyword = $("#keyword").val();	
		
		

		if (keyword.length >= MIN_LENGTH) {
			
			//console.log(keyword)

			$.get( "getSearch.php", { keyword: keyword } )

			.done(function( data ) {
				//alert(data);
				console.log(data);

				$('#result_auto').html('').show();

				var results = jQuery.parseJSON(data);

				$(results).each(function(key, value) {
					
					$('#result_auto').append('<div class="item">' + value.generic + '</div>');

				})

			    $('.item').click(function() {

			    	var text = $(this).html();
					
			    	$('#keyword').val(text);

					$('#searchfrm').submit();

			    })

			});

		} else {

			$('#result_auto').html('');

		}

	});

    $("#keyword").blur(function(){

    		$("#result_auto").fadeOut(500);

    	})

        .focus(function() {		

		    if($('#keyword').val()!=''){

    	    $("#result_auto").show();

			}

    	});

});

</script> 

<script>
function gettown(id) {
	$.ajax({
	type:"POST",
	url:"getTown.php",
	data: { id : id },
	success: function(data){
		//alert(data);
		$("#ton").html(data);
	}
	});
}
</script>

  

