
<?php
  $adjacents = 3;
  if(empty($targetpage)) $targetpage = "?";
  if(empty($limit)) $limit = 30;
  $page = $_GET['page'];
  if ($page)
      $start = ($page - 1) * $limit;
  else
      $start = 0;
  
  if ($page == 0)
      $page = 1;
  $prev = $page - 1;
  $next = $page + 1;
  $lastpage = ceil($total_pages / $limit);
  $lpm1 = $lastpage - 1;
  
  $pagination = "";
  if ($lastpage > 1) {
      $pagination .= "<ul class=\"pagination\">";
      if ($page > 1)
          $pagination .= "<li><a href=\"$targetpage&page=$prev\">&laquo;</a><li>";
      else
          $pagination .= "<li  class=\"disabled\"><a href=\"javascript:void(0)\">&laquo;</a></li>";
      
      
      if ($lastpage < 7 + ($adjacents * 2)) {
          for ($counter = 1; $counter <= $lastpage; $counter++) {
              if ($counter == $page)
                  $pagination .= "<li  class=\"active\"><a href=\"javascript:void(0)\">$counter</a></li>";
              else
                  $pagination .= "<li> <a href=\"$targetpage&page=$counter\">$counter</a></li>";
          }
      } elseif ($lastpage > 5 + ($adjacents * 2)) {
          if ($page < 1 + ($adjacents * 2)) {
              for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                  if ($counter == $page)
                      $pagination .= "<li class=\"active\"><a href=\"javascript:void(0)\">$counter</a></li>";
                  else
                      $pagination .= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>";
              }
              $pagination .= "";
              $pagination .= "<li><a href=\"$targetpage&page=$lpm1\">$lpm1</a></li>";
              $pagination .= "<li><a href=\"$targetpage&page=$lastpage\">$lastpage</a></li>";
          } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
              $pagination .= "<li><a href=\"$targetpage&page=1\">1</a></li>";
              $pagination .= "<li><a href=\"$targetpage&page=2\">2</a></li>";
              $pagination .= "";
              for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                  if ($counter == $page)
                      $pagination .= "<li class=\"active\"><a href=\"javascript:void(0)\">$counter</a></li>";
                  else
                      $pagination .= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>";
              }
              $pagination .= "";
              $pagination .= "<li><a href=\"$targetpage&page=$lpm1\">$lpm1</a></li>";
              $pagination .= "<li><a href=\"$targetpage&page=$lastpage\">$lastpage</a></li>";
          } else {
              $pagination .= "<li><a href=\"$targetpage&page=1\">1</a></li>";
              $pagination .= "<li><a href=\"$targetpage&page=2\">2</a></li>";
              $pagination .= "";
              for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                  if ($counter == $page)
                      $pagination .= "<li class=\"active\"><a href=\"javascript:void(0)\">$counter</a></li>";
                  else
                      $pagination .= "<li><a href=\"$targetpage&page=$counter\">$counter</a></li>";
              }
          }
      }
      if ($page < $counter - 1)
          $pagination .= "<li><a href=\"$targetpage&page=$next\">&raquo;</a><li>";
      else
          $pagination .= "<li class=\"disabled\"> <a href=\"javascript:void(0)\">&raquo;</a></li>";
      $pagination .= "</ul>\n";
  }
?>