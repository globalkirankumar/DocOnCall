<?php 
	
	include("include/config.php");
	include("include/functions.php");
	
	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	
	}
	
	if($_REQUEST['submitFrom']=='Yes'){
		
		$category = $_REQUEST['category'];
		$fom = $_REQUEST['fom'];
		$status = $_REQUEST['status'];
		$a = $_REQUEST['a'];
		if($a=='1'){
			
			$category = $_REQUEST['category'];
			
			}else{
				
			$category = $a;
				
			}
		
		if($_REQUEST['id']==''){
			
			$CheckArr = $obj->query("select * from inv_form_of_medicine where form_of_medicine_name='".$fom."'",$debug=-1);
			
			$row = $obj->numRows($uArr);
			
			if($row==0){
			
			$obj->query("insert into inv_form_of_medicine SET form_of_medicine_name='".$fom."',category_id='".$category."',status='".$status."',created_date=now()");
		
			$_SESSION['sess_msg']="Form of medicine added successfully";
			
			}else{
			
			$_SESSION['sess_msg']="Form of medicine already exits";
			
			 header("Location:formofmadic-addf.php");
			 
			 exit();
				
			}
			
		  }else{
		 
		   $sql=" update inv_form_of_medicine set form_of_medicine_name='".$fom."',category_id='".$category."',status='".$status."',modified_date=now()";

	       $sql.=" where id='".$_REQUEST['id']."'";

	       $obj->query($sql);

	       $_SESSION['sess_msg']='Form of medicine updated successfully';   

        }

      header("Location:formofmadic-list.php");

     exit();
}
	
if($_REQUEST['id']!=''){

$sql=$obj->query("select * from inv_form_of_medicine where id=".$_REQUEST['id']);

$result=$obj->fetchNextObject($sql);

}
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>DOCTOR ON CALL</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script type="text/javascript" language="javascript">
function validate(obj)
{
if(obj.fom.value==''){
alert("Please Enter Form Of Medicine");
obj.fom.focus();
return false;
}
if(obj.status.value==''){
alert("Please Select Status");
obj.status.focus();
return false;
}

}
</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms "  data-smooth-scrolling="1">
<div class="vd_body">
  <header class="header-1" id="header">
    <?php include("header.php");?>
   
  </header>
  <!-- Header Ends --> 
  
  <!---------sidebar---->
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      <!-- Middle Content Start -->
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
					<li><a href="formofmadic-addf.php"><?php if($_REQUEST['id']==''){?>Add Form Of Medicine<?php } else { echo "Update Form Of Medicine"; }?></a> </li>
                  </ul>
                </div>
                
				<?php if($_SESSION['sess_msg']){ ?>
                <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> 
				<?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
                <?php }?>
				
                <div class="vd_panel-inner-part-content"> <span  style="float:right; padding-right:10px;">
                  <input type="button" name="add" value="View Form Of Medicine"  class="btn btn-fright" onclick="location.href='formofmadic-list.php'" />
                  </span>
                  <form name="frm" class="search_formbox more_search_formbox" action="" method="POST" onsubmit="return validate(this)">
                    <input type="hidden" name="submitFrom" value="Yes">
                    <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
					
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
					 <div class="form-group form-group2">
						<div class="controls">
                          <span class="custom">
						  <input type="radio" name="a" value="1" id="a" class="custom-radio" checked="checked"> Category specific
						  </span> 
                          <span class="custom">
						  <input type="radio" name="a" value="0" id="b" class="custom-radio"> Open for all 
						  </span> 
                        </div>
					  </div>
					
					  <div class="form-group form-group2" id="c">
						<div class="controls">
						<select name="category" id='required' required>
							<option value="">Category</option>
							<?php $catArr = $obj->query("select * from inv_category where status=1");?>
							<?php while($catResult = $obj->fetchNextObject($catArr)){?>
							<option value="<?php echo $catResult->id;?>" <?php if($catResult->id==$result->category_id){ ?>selected<?php } ?>><?php echo stripslashes($catResult->name);?></option>
							<?php } ?>
                        </select>
						</div>
						</div>
						
					 <div class="form-group form-group2">
                        <div class="controls">
                          <input type="text" name="fom" Placeholder="Form Of Medicine" id="fom" value="<?php echo stripslashes($result->form_of_medicine_name);?>" required>
                        </div>
                      </div>
					  
                      <div class="col-lg-6 col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
					  
                      <div class="form-group form-group2">
                        <div class="controls">
                          <select name="status" id="status" required>
                            <option value="">Status</option>                           
                            <option value="1" <?php if($result->status=='1'){?>selected<?php } ?>>Active</option>
                            <option value="0" <?php if($result->status=='0'){?>selected<?php } ?>>Deactive</option>
                          </select>
                        </div>
                      </div>
                    </div>
                      <div class="controls">
                        <input type="submit" class="btn greenbutton submit-btn-first" value="Submit">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer Start -->
  <?php include("footer.php");?>
<!-- Footer END --> 

<script>
	$("#a").change(function()
		{
		var val=$(this).val();
		if(val=='1')
		{
		$('#required').prop( "disabled", false );		
		$('#c').show(); 
		}
		});
		
	$("#b").change(function()
		{
		var val=$(this).val();
		if(val=='0')
		{
		$('#required').prop( "disabled", true );
		$('#c').hide();
		}
	});
</script>
<body>
</html>
